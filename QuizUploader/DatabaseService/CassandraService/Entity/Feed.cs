﻿using Cassandra;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;
using Amazon.S3;
using Amazon;
using Amazon.S3.Model;
using CassandraService.Utilities;
using CassandraService.Validation;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.CassandraUtilities;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [Serializable]
    public class Feed
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember(EmitDefaultValue = false)]
        public string FeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FeedType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedText FeedText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedImage FeedImage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedVideo FeedVideo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedSharedUrl FeedSharedUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReportPostId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReportState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReportPostState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ReportedPost> ReportedFeedPosts { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Comment> Comments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int DashboardFeedbackType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int DashboardReportedType { get; set; }

        [DataMember]
        public int NegativePoints { get; set; }

        [DataMember]
        public int PositivePoints { get; set; }

        [DataMember]
        public int NumberOfComments { get; set; }

        [DataMember]
        public bool HasVoted { get; set; }

        [DataMember]
        public bool IsUpVoted { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User User { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        [DataMember]
        public bool IsForEveryone { get; set; }

        [DataMember]
        public bool IsForDepartment { get; set; }

        [DataMember]
        public bool IsForPersonnel { get; set; }

        public enum VoteStatus
        {
            Reset = -999,
            Upvote = 1,
            Downvote = -1
        }

        public enum FeedValidStatus
        {
            Valid = 1,
            Hidden = -1,
            Deleted = -2
        }

        public FeedAuthenticationResponse AuthenticateUserForPostingFeed(string companyId,
                                                                         string userId)
        {
            FeedAuthenticationResponse response = new FeedAuthenticationResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(userId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    string feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                    response.FeedId = feedId;

                    response.IsAuthenticatedToPost = true;
                    response.Success = true;
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;

        }
        public FeedCreateResponse CreateFeedTextPost(string feedId,
                                                     string creatorUserId,
                                                     string companyId,
                                                     string content,
                                                     List<string> targetedDepartmentIds,
                                                     List<string> targetedUserIds,
                                                     bool isForAdmin)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.TargetedUsers = new List<Dictionary<string, object>>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(content))
                    {
                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        List<string> notificationList = CreateFeedPrivacy(creatorUserId,
                                                                          feedId,
                                                                          companyId,
                                                                          Int32.Parse(FeedTypeCode.TextPost),
                                                                          content,
                                                                          targetedDepartmentIds,
                                                                          targetedUserIds,
                                                                          isForAdmin,
                                                                          session);

                        long createdTimestamp = DateHelper.ConvertDateToLong(DateTime.UtcNow);

                        PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_text",
                                new List<string> { "id", "content", "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(feedId, content, true, 1, creatorUserId, createdTimestamp));

                        session.Execute(batchStatement);

                        // Create notification for targeted users
                        Notification notification = new Notification();

                        foreach (string userId in notificationList)
                        {
                            if (userId.Equals(creatorUserId))
                            {
                                continue;
                            }

                            string notificationText = notification.CreateFeedNotification(creatorUserId, companyId, userId, (int)Notification.NotificationFeedSubType.NewPost, Int32.Parse(FeedTypeCode.TextPost), feedId, string.Empty, string.Empty, createdTimestamp, session);

                            Dictionary<string, object> notificationDict = new Dictionary<string, object>();
                            notificationDict.Add("userId", userId);
                            notificationDict.Add("notificationText", notificationText);
                            notificationDict.Add("notificationNumber", notification.SelectNotificationNumberByUser(userId, companyId, session).NumberOfNotification);
                            response.TargetedUsers.Add(notificationDict);
                        }

                        // For feedback to admin
                        if (isForAdmin)
                        {
                            Dashboard dashboard = new Dashboard();
                            dashboard.CreateFeedbackToAdmin(creatorUserId, content, companyId, feedId, createdTimestamp, session);
                        }

                        response.FeedId = feedId;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed text post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextContent;
                        response.Success = false;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedCreateResponse CreateFeedImagePost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      List<string> imageUrls,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds,
                                                      bool isForAdmin)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.TargetedUsers = new List<Dictionary<string, object>>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (imageUrls.Count > 0)
                    {
                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        List<string> notificationList = CreateFeedPrivacy(creatorUserId,
                                                                          feedId,
                                                                          companyId,
                                                                          Int32.Parse(FeedTypeCode.ImagePost),
                                                                          caption,
                                                                          targetedDepartmentIds,
                                                                          targetedUserIds,
                                                                          isForAdmin,
                                                                          session);

                        long createdTimestamp = DateHelper.ConvertDateToLong(DateTime.UtcNow);

                        PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_image",
                            new List<string> { "id", "caption", "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(feedId, caption, true, 1, creatorUserId, createdTimestamp));

                        int order = 1;
                        foreach (string imageUrl in imageUrls)
                        {
                            string imageId = UUIDGenerator.GenerateUniqueIDForFeedMedia();

                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_image_url",
                                new List<string> { "id", "feed_id", "url", "in_order", "is_valid" }));
                            batchStatement.Add(preparedStatement.Bind(imageId, feedId, imageUrl, order, true));

                            order += 1;
                        }

                        session.Execute(batchStatement);

                        // Create notification for targeted users
                        Notification notification = new Notification();

                        foreach (string userId in notificationList)
                        {
                            if(userId.Equals(creatorUserId))
                            {
                                continue;
                            }

                            string notificationText = notification.CreateFeedNotification(creatorUserId, companyId, userId, (int)Notification.NotificationFeedSubType.NewPost, Int32.Parse(FeedTypeCode.ImagePost), feedId, string.Empty, string.Empty, createdTimestamp, session);

                            Dictionary<string, object> notificationDict = new Dictionary<string, object>();
                            notificationDict.Add("userId", userId);
                            notificationDict.Add("notificationText", notificationText);
                            notificationDict.Add("notificationNumber", notification.SelectNotificationNumberByUser(userId, companyId, session).NumberOfNotification);
                            response.TargetedUsers.Add(notificationDict);
                        }

                        // For feedback to admin
                        if (isForAdmin)
                        {
                            Dashboard dashboard = new Dashboard();
                            dashboard.CreateFeedbackToAdmin(creatorUserId, caption, companyId, feedId, createdTimestamp, session);
                        }

                        response.FeedId = feedId;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed image post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidImageContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidImageContent;
                        response.Success = false;
                    }

                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedCreateResponse CreateFeedVideoPost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      string videoUrl,
                                                      string videoThumbnailUrl,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds,
                                                      bool isForAdmin)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.TargetedUsers = new List<Dictionary<string, object>>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(videoUrl))
                    {
                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        List<string> notificationList = CreateFeedPrivacy(creatorUserId,
                                                                          feedId,
                                                                          companyId,
                                                                          Int32.Parse(FeedTypeCode.VideoPost),
                                                                          caption,
                                                                          targetedDepartmentIds,
                                                                          targetedUserIds,
                                                                          isForAdmin,
                                                                          session);

                        long createdTimestamp = DateHelper.ConvertDateToLong(DateTime.UtcNow);

                        PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_video",
                                new List<string> { "id", "caption", "video_url", "video_thumbnail_url", "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(feedId, caption, videoUrl, videoThumbnailUrl, true, 1, creatorUserId, createdTimestamp));

                        session.Execute(batchStatement);

                        // Create notification for targeted users
                        Notification notification = new Notification();

                        foreach (string userId in notificationList)
                        {
                            if (userId.Equals(creatorUserId))
                            {
                                continue;
                            }

                            string notificationText = notification.CreateFeedNotification(creatorUserId, companyId, userId, (int)Notification.NotificationFeedSubType.NewPost, Int32.Parse(FeedTypeCode.VideoPost), feedId, string.Empty, string.Empty, createdTimestamp, session);

                            Dictionary<string, object> notificationDict = new Dictionary<string, object>();
                            notificationDict.Add("userId", userId);
                            notificationDict.Add("notificationText", notificationText);
                            notificationDict.Add("notificationNumber", notification.SelectNotificationNumberByUser(userId, companyId, session).NumberOfNotification);
                            response.TargetedUsers.Add(notificationDict);
                        }

                        // For feedback to admin
                        if (isForAdmin)
                        {
                            Dashboard dashboard = new Dashboard();
                            dashboard.CreateFeedbackToAdmin(creatorUserId, caption, companyId, feedId, createdTimestamp, session);
                        }

                        response.FeedId = feedId;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed video post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidVideoContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidVideoContent;
                        response.Success = false;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedCreateResponse CreateFeedSharedUrlPost(string feedId,
                                                          string creatorUserId,
                                                          string companyId,
                                                          string caption,
                                                          string url,
                                                          string urlTitle,
                                                          string urlDescription,
                                                          string urlSiteName,
                                                          string urlImageUrl,
                                                          List<string> targetedDepartmentIds,
                                                          List<string> targetedUserIds,
                                                          bool isForAdmin)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.TargetedUsers = new List<Dictionary<string, object>>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(url))
                    {
                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        List<string> notificationList = CreateFeedPrivacy(creatorUserId,
                                                                          feedId,
                                                                          companyId,
                                                                          Int32.Parse(FeedTypeCode.SharedUrlPost),
                                                                          caption,
                                                                          targetedDepartmentIds,
                                                                          targetedUserIds,
                                                                          isForAdmin,
                                                                          session);

                        long createdTimestamp = DateHelper.ConvertDateToLong(DateTime.UtcNow);

                        PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_shared_url",
                                new List<string> { "id", "caption", "url", "url_title", "url_description", "url_site_name", "url_image_url", "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(feedId, caption, url, urlTitle, urlDescription, urlSiteName, urlImageUrl, true, 1, creatorUserId, createdTimestamp));

                        session.Execute(batchStatement);

                        // Create notification for targeted users
                        Notification notification = new Notification();

                        foreach (string userId in notificationList)
                        {
                            if (userId.Equals(creatorUserId))
                            {
                                continue;
                            }

                            string notificationText = notification.CreateFeedNotification(creatorUserId, companyId, userId, (int)Notification.NotificationFeedSubType.NewPost, Int32.Parse(FeedTypeCode.SharedUrlPost), feedId, string.Empty, string.Empty, createdTimestamp, session);

                            Dictionary<string, object> notificationDict = new Dictionary<string, object>();
                            notificationDict.Add("userId", userId);
                            notificationDict.Add("notificationText", notificationText);
                            notificationDict.Add("notificationNumber", notification.SelectNotificationNumberByUser(userId, companyId, session).NumberOfNotification);
                            response.TargetedUsers.Add(notificationDict);
                        }

                        // For feedback to admin
                        if (isForAdmin)
                        {
                            Dashboard dashboard = new Dashboard();
                            dashboard.CreateFeedbackToAdmin(creatorUserId, caption, companyId, feedId, createdTimestamp, session);
                        }

                        response.FeedId = feedId;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed shared url post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextContent;
                        response.Success = false;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public CommentCreateResponse CreateCommentText(string creatorUserId,
                                                       string companyId,
                                                       string feedId,
                                                       string content,
                                                       bool isAdmin = false,
                                                       ISession session = null)
        {
            CommentCreateResponse response = new CommentCreateResponse();
            response.TargetedUserId = string.Empty;
            response.NotificationText = string.Empty;
            response.NumberOfNotificationForTargetedUser = 0;
            response.Success = false;

            try
            {
                BatchStatement batchStatement = new BatchStatement();
                ErrorStatus hasErrorAsUser = null;
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    if (!isAdmin)
                    {
                        hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                    }
                    else
                    {
                        hasErrorAsUser = vh.isValidatedAsAdmin(creatorUserId, companyId, session);
                    }

                }

                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(content))
                    {
                        Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                        if (feedRow != null)
                        {
                            if (isAdmin)
                            {
                                creatorUserId = companyId;
                            }

                            string commentId = UUIDGenerator.GenerateUniqueIDForFeedComment();

                            DateTime currentDateTime = DateTime.UtcNow;
                            long currentDateTimeLong = DateHelper.ConvertDateToLong(currentDateTime);

                            PreparedStatement psCommentByFeed = session.Prepare(CQLGenerator.InsertStatement("comment_by_feed",
                                new List<string> { "comment_id", "feed_id", "comment_type", "commentor_user_id", "is_comment_valid", "comment_valid_status", "created_on_timestamp" }));
                            batchStatement.Add(psCommentByFeed.Bind(commentId, feedId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                            PreparedStatement psFeedCommentText = session.Prepare(CQLGenerator.InsertStatement("feed_comment_text",
                                new List<string> { "id", "feed_id", "content", "is_valid", "valid_status", "user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                            batchStatement.Add(psFeedCommentText.Bind(commentId, feedId, content, true, 1, creatorUserId, currentDateTimeLong, creatorUserId, currentDateTimeLong));

                            PreparedStatement psCommentByTimestamp = session.Prepare(CQLGenerator.InsertStatement("comment_by_feed_timestamp_desc",
                                new List<string> { "comment_id", "feed_id", "comment_type", "commentor_user_id", "is_comment_valid", "comment_valid_status", "created_on_timestamp" }));
                            batchStatement.Add(psCommentByTimestamp.Bind(commentId, feedId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                            psCommentByTimestamp = session.Prepare(CQLGenerator.InsertStatement("comment_by_feed_timestamp_asc",
                                new List<string> { "comment_id", "feed_id", "comment_type", "commentor_user_id", "is_comment_valid", "comment_valid_status", "created_on_timestamp" }));
                            batchStatement.Add(psCommentByTimestamp.Bind(commentId, feedId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                            session.Execute(batchStatement);

                            UpdateFeedCommentCounter(feedId, 1, session);

                            // Create notification for targeted user
                            string ownerUserId = feedRow.GetValue<string>("owner_user_id");
                            bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");

                            int feedType = feedRow.GetValue<int>("feed_type");
                            Notification notification = new Notification();

                            if (!isPostedByAdmin)
                            {
                                if (!ownerUserId.Equals(creatorUserId))
                                {
                                    string notificationText = notification.CreateFeedNotification(creatorUserId, companyId, ownerUserId, (int)Notification.NotificationFeedSubType.NewComment, feedType, feedId, string.Empty, string.Empty, currentDateTimeLong, session);
                                    response.NotificationText = notificationText;

                                    response.TargetedUserId = ownerUserId;

                                    // Get new notification number
                                    NotificationSelectNumberResponse notificationResponse = notification.SelectNotificationNumberByUser(ownerUserId, companyId, session);
                                    if (notificationResponse.Success)
                                    {
                                        response.NumberOfNotificationForTargetedUser = notificationResponse.NumberOfNotification;
                                        response.FeedId = feedId;
                                        response.CommentId = commentId;
                                        response.NotificationType = (int)Notification.NotificationType.Feed;
                                        response.NotificationSubType = (int)Notification.NotificationFeedSubType.NewComment;
                                    }
                                }
                            }
                            else
                            {
#warning Need loop through department users
                            }

                            response.Success = true;
                        }
                        else
                        {
                            Log.Error("Invalid feed post");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                            response.ErrorMessage = ErrorMessage.FeedInvalid;
                        }
                    }
                    else
                    {
                        Log.Error("Empty feed text comment");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextComment);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextComment;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ReplyCreateResponse CreateReplyText(string creatorUserId,
                                                   string companyId,
                                                   string feedId,
                                                   string commentId,
                                                   string content,
                                                   bool isAdmin = false)
        {
            ReplyCreateResponse response = new ReplyCreateResponse();
            response.NotificationText = string.Empty;
            response.TargetedUserId = string.Empty;
            response.NumberOfNotificationForTargetedUser = 0;
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();
                ErrorStatus hasErrorAsUser = null;

                if (!isAdmin)
                {
                    hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                }
                else
                {
                    hasErrorAsUser = vh.isValidatedAsAdmin(creatorUserId, companyId, session);
                }

                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(content))
                    {
                        Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                        if (feedRow != null)
                        {
                            Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);
                            if (commentRow != null)
                            {
                                if (isAdmin)
                                {
                                    creatorUserId = companyId;
                                }

                                string replyId = UUIDGenerator.GenerateUniqueIDForFeedCommentReply();

                                DateTime currentDateTime = DateTime.UtcNow;
                                long currentDateTimeLong = DateHelper.ConvertDateToLong(currentDateTime);

                                PreparedStatement psCommentByFeed = session.Prepare(CQLGenerator.InsertStatement("reply_by_comment",
                                    new List<string> { "reply_id", "comment_id", "reply_type", "commentor_user_id", "is_reply_valid", "reply_valid_status", "created_on_timestamp" }));
                                batchStatement.Add(psCommentByFeed.Bind(replyId, commentId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                                PreparedStatement psFeedCommentText = session.Prepare(CQLGenerator.InsertStatement("feed_reply_text",
                                    new List<string> { "id", "comment_id", "content", "is_valid", "valid_status", "user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                                batchStatement.Add(psFeedCommentText.Bind(replyId, commentId, content, true, 1, creatorUserId, currentDateTimeLong, creatorUserId, currentDateTimeLong));

                                PreparedStatement psCommentByTimestampDesc = session.Prepare(CQLGenerator.InsertStatement("reply_by_comment_timestamp_desc",
                                    new List<string> { "reply_id", "comment_id", "reply_type", "commentor_user_id", "is_reply_valid", "reply_valid_status", "created_on_timestamp" }));
                                batchStatement.Add(psCommentByTimestampDesc.Bind(replyId, commentId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                                PreparedStatement psCommentByTimestampAsc = session.Prepare(CQLGenerator.InsertStatement("reply_by_comment_timestamp_asc",
                                    new List<string> { "reply_id", "comment_id", "reply_type", "commentor_user_id", "is_reply_valid", "reply_valid_status", "created_on_timestamp" }));
                                batchStatement.Add(psCommentByTimestampAsc.Bind(replyId, commentId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                                session.Execute(batchStatement);

                                UpdateCommentReplyCounter(feedId, commentId, 1, session);

                                // Create notification for targeted user
                                string ownerUserId = commentRow.GetValue<string>("commentor_user_id");
                                bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");

                                int feedType = feedRow.GetValue<int>("feed_type");
                                Notification notification = new Notification();

                                if (!isPostedByAdmin)
                                {
                                    if (!ownerUserId.Equals(creatorUserId))
                                    {
                                        string notificationText = notification.CreateFeedNotification(creatorUserId, companyId, ownerUserId, (int)Notification.NotificationFeedSubType.NewReply, feedType, feedId, commentId, string.Empty, currentDateTimeLong, session);
                                        response.NotificationText = notificationText;

                                        response.TargetedUserId = ownerUserId;

                                        // Get new notification number
                                        NotificationSelectNumberResponse notificationResponse = notification.SelectNotificationNumberByUser(ownerUserId, companyId, session);
                                        if (notificationResponse.Success)
                                        {
                                            response.NumberOfNotificationForTargetedUser = notificationResponse.NumberOfNotification;
                                            response.FeedId = feedId;
                                            response.CommentId = commentId;
                                            response.ReplyId = replyId;
                                            response.NotificationType = (int)Notification.NotificationType.Feed;
                                            response.NotificationSubType = (int)Notification.NotificationFeedSubType.NewReply;
                                        }
                                    }
                                }
                                else
                                {
#warning Need loop through department users
                                }

                                response.Success = true;
                            }
                            else
                            {
                                Log.Error("Invalid feed comment");
                                response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                                response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                            }

                        }
                        else
                        {
                            Log.Error("Invalid feed post");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                            response.ErrorMessage = ErrorMessage.FeedInvalid;
                        }
                    }
                    else
                    {
                        Log.Error("Empty feed text comment");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextComment);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextComment;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public CommentSelectResponse SelectFeedComment(string requesterUserId,
                                                       string feedId,
                                                       string companyId)
        {
            CommentSelectResponse response = new CommentSelectResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow != null)
                {
                    Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                    if (feedRow != null)
                    {
                        int feedType = feedRow.GetValue<int>("feed_type");
                        string ownerUserId = feedRow.GetValue<string>("owner_user_id");
                        DateTime feedCreatedOnTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");

                        Feed feedPost = GetFeed(feedId, feedType, feedCreatedOnTimestamp, requesterUserId, new User().SelectUserBasic(ownerUserId, companyId, false, session, null, true).User, session);

                        PreparedStatement psCommentByTimestamp = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp_asc",
                            new List<string> { "comment_id", "comment_type", "commentor_user_id" }, new List<string> { "feed_id", "is_comment_valid" }));
                        BoundStatement bsCommentByTimestamp = psCommentByTimestamp.Bind(feedId, true);

                        RowSet commentByTimestampRowset = session.Execute(bsCommentByTimestamp);

                        List<Comment> comments = new List<Comment>();

                        foreach (Row commentByTimestampRow in commentByTimestampRowset)
                        {
                            string commentId = commentByTimestampRow.GetValue<string>("comment_id");
                            int commentType = commentByTimestampRow.GetValue<int>("comment_type");
                            string commentorUserId = commentByTimestampRow.GetValue<string>("commentor_user_id");

                            User commentorUser = new User().SelectUserBasic(commentorUserId, companyId, false, session, null, true).User;
                            comments.Add(GetCommentWithLatestReply(companyId, feedId, commentId, commentType, requesterUserId, commentorUser, session));
                        }

                        response.Success = true;
                        response.FeedPost = feedPost;
                        response.Comments = comments;
                    }
                    else
                    {
                        Log.Error("Invalid feed");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }

                }
                else
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ReplySelectResponse SelectCommentReply(string requesterUserId,
                                                      string feedId,
                                                      string commentId,
                                                      string companyId)
        {
            ReplySelectResponse response = new ReplySelectResponse();
            response.Success = false;

            ConnectionManager cm = new ConnectionManager();
            ISession session = cm.getMainSession();
            ValidationHandler vh = new ValidationHandler();

            try
            {
                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow != null)
                {
                    Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                    if (feedRow != null)
                    {
                        Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);
                        if (commentRow != null)
                        {
                            int commentType = commentRow.GetValue<int>("comment_type");
                            string commentorUserId = commentRow.GetValue<string>("commentor_user_id");

                            Comment comment = GetCommentWithLatestReply(companyId, feedId, commentId, commentType, requesterUserId, new User().SelectUserBasic(commentorUserId, companyId, false, session, null, true).User, session);

                            PreparedStatement psReplyByTimestamp = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment_timestamp_asc",
                                new List<string> { "reply_id", "reply_type", "commentor_user_id" }, new List<string> { "comment_id", "is_reply_valid" }));
                            BoundStatement bsReplyByTimestamp = psReplyByTimestamp.Bind(commentId, true);

                            RowSet replyByTimestampRowset = session.Execute(bsReplyByTimestamp);

                            List<Reply> replies = new List<Reply>();

                            foreach (Row replyByTimestampRow in replyByTimestampRowset)
                            {
                                string replyId = replyByTimestampRow.GetValue<string>("reply_id");
                                int replyType = replyByTimestampRow.GetValue<int>("reply_type");
                                string repliedUserId = replyByTimestampRow.GetValue<string>("commentor_user_id");

                                User commentorUser = new User().SelectUserBasic(repliedUserId, companyId, false, session).User;
                                replies.Add(GetReply(companyId, feedId, commentId, replyId, replyType, requesterUserId, commentorUser, session));
                            }

                            response.Success = true;
                            response.Comment = comment;
                            response.Replies = replies;
                        }
                        else
                        {
                            Log.Error("Invalid comment");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                            response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid feed");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }

                }
                else
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId, string adminUserId, ISession session = null)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psFeedPrivacy = null;
                BoundStatement bsFeedPrivacy = null;

                string modifiedUserId = string.Empty;

                if (!string.IsNullOrEmpty(ownerUserId))
                {
                    Row userRow = vh.ValidateUser(ownerUserId, companyId, session);
                    if (userRow == null)
                    {
                        Log.Error("Invalid user");
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                        new List<string>(), new List<string> { "company_id", "feed_id", "owner_user_id" }));
                    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, feedId, ownerUserId);

                    modifiedUserId = ownerUserId;
                }
                else
                {
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                        new List<string>(), new List<string> { "company_id", "feed_id" }));
                    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, feedId);

                    modifiedUserId = adminUserId;
                }

                Row feedPrivacyRow = session.Execute(bsFeedPrivacy).FirstOrDefault();

                if (feedPrivacyRow == null)
                {
                    Log.Error(string.Format("Feed post: {0} does not belong to userId: {1}", feedId, ownerUserId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }
                else
                {
                    bool isFeedValid = feedPrivacyRow.GetValue<bool>("is_feed_valid");
                    bool isForAdmin = feedPrivacyRow["is_for_admin"] != null ? feedPrivacyRow.GetValue<bool>("is_for_admin") : false;

                    if (!isFeedValid)
                    {
                        Log.Error("Feed is already invalid: " + feedId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                        return response;
                    }

                    BatchStatement batchStatement = new BatchStatement();

                    int feedType = feedPrivacyRow.GetValue<int>("feed_type");
                    DateTimeOffset createdTimestamp = feedPrivacyRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                    string tableName = string.Empty;
                    bool isNeedToDeleteFromS3 = true;
                    if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        tableName = "feed_text";
                        isNeedToDeleteFromS3 = false;
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                    {
                        tableName = "feed_image";
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                    {
                        tableName = "feed_video";
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                    {
                        tableName = "feed_shared_url";
                    }

                    PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                        new List<string> { "id" }, new List<string> { "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    BoundStatement bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, modifiedUserId, DateTime.UtcNow, feedId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    // Delete from S3
                    if (isNeedToDeleteFromS3)
                    {
                        String bucketName = "cocadre-" + companyId.ToLower();

                        using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                        {
                            ListObjectsRequest listRequest = new ListObjectsRequest();
                            listRequest.BucketName = bucketName;
                            listRequest.Prefix = "feeds/" + feedId;

                            ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                            foreach (S3Object imageObject in listResponse.S3Objects)
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = imageObject.Key;
                                s3Client.DeleteObject(deleteRequest);
                            }

                            DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                            deleteFolderRequest.BucketName = bucketName;
                            deleteFolderRequest.Key = "feeds/" + feedId;
                            s3Client.DeleteObject(deleteFolderRequest);
                        }
                    }

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy",
                        new List<string> { "company_id", "feed_id" }, new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, companyId, feedId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc",
                      new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, companyId, createdTimestamp, feedId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc",
                        new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, companyId, createdTimestamp, feedId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    session.Execute(batchStatement);

                    Dashboard dashboard = new Dashboard();

                    // Delete feedback
                    if (isForAdmin)
                    {
                        dashboard.DeleteFeedbackPost(companyId, feedId, session);
                    }

                    // Delete report
                    dashboard.DeleteReport(session, (int)Dashboard.ReportType.Feed, feedId, null, null, companyId, createdTimestamp);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId, string commentorUserId, string adminUserId, ISession session = null)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psCommentPrivacy = null;
                BoundStatement bsCommentPrivacy = null;

                string modifiedUserId = string.Empty;

                Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                if (feedRow == null)
                {
                    Log.Error("Invalid FeedId: " + feedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!string.IsNullOrEmpty(commentorUserId))
                {
                    Row userRow = vh.ValidateUser(commentorUserId, companyId, session);
                    if (userRow == null)
                    {
                        Log.Error("Invalid commentorUserId: " + commentorUserId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    psCommentPrivacy = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed",
                        new List<string>(), new List<string> { "feed_id", "comment_id", "commentor_user_id" }));
                    bsCommentPrivacy = psCommentPrivacy.Bind(feedId, commentId, commentorUserId);

                    modifiedUserId = commentorUserId;
                }
                else
                {
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    psCommentPrivacy = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed",
                        new List<string>(), new List<string> { "feed_id", "comment_id" }));
                    bsCommentPrivacy = psCommentPrivacy.Bind(feedId, commentId);

                    modifiedUserId = adminUserId;
                }

                Row commentRow = session.Execute(bsCommentPrivacy).FirstOrDefault();

                if (commentRow == null)
                {
                    Log.Error(string.Format("Comment post: {0} does not belong to userId: {1}", commentId, commentorUserId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }
                else
                {
                    bool isCommentValid = commentRow.GetValue<bool>("is_comment_valid");

                    if (!isCommentValid)
                    {
                        Log.Error("Comment is already invalid: " + commentId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                        return response;
                    }

                    BatchStatement batchStatement = new BatchStatement();

                    int commentType = commentRow.GetValue<int>("comment_type");
                    DateTimeOffset createdTimestamp = commentRow.GetValue<DateTimeOffset>("created_on_timestamp");

                    string tableName = string.Empty;
                    bool isNeedToDeleteFromS3 = true;
                    if (commentType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        tableName = "feed_comment_text";
                        isNeedToDeleteFromS3 = false;
                    }

                    PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                        new List<string> { "feed_id", "id" }, new List<string> { "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    BoundStatement bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, modifiedUserId, DateTime.UtcNow, feedId, commentId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    // Delete from S3
                    if (isNeedToDeleteFromS3)
                    {

                    }

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed",
                        new List<string> { "feed_id", "comment_id" }, new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, feedId, commentId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_asc",
                        new List<string> { "feed_id", "created_on_timestamp", "comment_id" }, new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, feedId, createdTimestamp, commentId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_desc",
                        new List<string> { "feed_id", "created_on_timestamp", "comment_id" }, new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, feedId, createdTimestamp, commentId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    session.Execute(batchStatement);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                        new List<string> { "feed_id" }, new List<string> { "comment" }, new List<int> { -1 }));
                    bsInvalidate = psInvalidate.Bind(feedId);
                    session.Execute(bsInvalidate);

                    // Delete report
                    Dashboard dashboard = new Dashboard();
                    dashboard.DeleteReport(session, (int)Dashboard.ReportType.Comment, feedId, commentId, null, companyId, null);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId, string commentorUserId, string adminUserId, ISession session = null)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psReplyPrivacy = null;
                BoundStatement bsReplyPrivacy = null;

                string modifiedUserId = string.Empty;

                Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                if (feedRow == null)
                {
                    Log.Error("Invalid FeedId: " + feedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);
                if (commentRow == null)
                {
                    Log.Error("Invalid CommentId: " + commentId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                    response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                    return response;
                }

                if (!string.IsNullOrEmpty(commentorUserId))
                {
                    Row userRow = vh.ValidateUser(commentorUserId, companyId, session);
                    if (userRow == null)
                    {
                        Log.Error("Invalid commentorUserId: " + commentorUserId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    psReplyPrivacy = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment",
                        new List<string>(), new List<string> { "comment_id", "reply_id", "commentor_user_id" }));
                    bsReplyPrivacy = psReplyPrivacy.Bind(commentId, replyId, commentorUserId);

                    modifiedUserId = commentorUserId;
                }
                else
                {
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    psReplyPrivacy = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment",
                        new List<string>(), new List<string> { "comment_id", "reply_id" }));
                    bsReplyPrivacy = psReplyPrivacy.Bind(commentId, replyId);

                    modifiedUserId = adminUserId;
                }

                Row replyRow = session.Execute(bsReplyPrivacy).FirstOrDefault();

                if (replyRow == null)
                {
                    Log.Error(string.Format("Reply post: {0} does not belong to userId: {1}", replyId, commentorUserId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }
                else
                {
                    bool isReplyValid = replyRow.GetValue<bool>("is_reply_valid");

                    if (!isReplyValid)
                    {
                        Log.Error("Reply is already invalid: " + replyId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                        return response;
                    }

                    BatchStatement batchStatement = new BatchStatement();

                    int replyType = replyRow.GetValue<int>("reply_type");
                    DateTimeOffset createdTimestamp = replyRow.GetValue<DateTimeOffset>("created_on_timestamp");

                    string tableName = string.Empty;
                    bool isNeedToDeleteFromS3 = true;
                    if (replyType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        tableName = "feed_reply_text";
                        isNeedToDeleteFromS3 = false;
                    }

                    PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                        new List<string> { "comment_id", "id" }, new List<string> { "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    BoundStatement bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, modifiedUserId, DateTime.UtcNow, commentId, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    // Delete from S3
                    if (isNeedToDeleteFromS3)
                    {

                    }

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment",
                        new List<string> { "comment_id", "reply_id" }, new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, commentId, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_desc",
                      new List<string> { "comment_id", "created_on_timestamp", "reply_id" }, new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, commentId, createdTimestamp, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_asc",
                        new List<string> { "comment_id", "created_on_timestamp", "reply_id" }, new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, commentId, createdTimestamp, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    session.Execute(batchStatement);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                        new List<string> { "feed_id", "comment_id" }, new List<string> { "reply" }, new List<int> { -1 }));
                    bsInvalidate = psInvalidate.Bind(feedId, commentId);
                    session.Execute(bsInvalidate);

                    // Delete report
                    Dashboard dashboard = new Dashboard();
                    dashboard.DeleteReport(session, (int)Dashboard.ReportType.Comment, feedId, commentId, replyId, companyId, null);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedVisibilityResponse ToggleFeedVisibilty(string feedId, string companyId, string adminUserId, Row feedPrivacyRow, bool isHidden, ISession session)
        {
            FeedVisibilityResponse response = new FeedVisibilityResponse();
            response.Success = false;

            try
            {
                string modifiedUserId = adminUserId;
                BatchStatement batchStatement = new BatchStatement();

                int feedType = feedPrivacyRow.GetValue<int>("feed_type");
                DateTimeOffset createdTimestamp = feedPrivacyRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                string tableName = string.Empty;
                if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                {
                    tableName = "feed_text";
                }
                else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                {
                    tableName = "feed_image";
                }
                else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                {
                    tableName = "feed_video";
                }
                else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                {
                    tableName = "feed_shared_url";
                }

                bool isValid = !isHidden;

                int validStatus = (int)Feed.FeedValidStatus.Valid;

                if (isHidden)
                {
                    validStatus = (int)Feed.FeedValidStatus.Hidden;
                }

                PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                    new List<string> { "id" }, new List<string> { "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                BoundStatement bsInvalidate = psInvalidate.Bind(isValid, validStatus, modifiedUserId, DateTime.UtcNow, feedId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy",
                    new List<string> { "company_id", "feed_id" }, new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, companyId, feedId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc",
                  new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, companyId, createdTimestamp, feedId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc",
                    new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, companyId, createdTimestamp, feedId);
                batchStatement = batchStatement.Add(bsInvalidate);

                session.Execute(batchStatement);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedVisibilityResponse ToggleCommentVisibilty(string feedId, string commentId, string adminUserId, Row commentRow, bool isHidden, ISession session)
        {
            FeedVisibilityResponse response = new FeedVisibilityResponse();
            response.Success = false;

            try
            {
                string modifiedUserId = adminUserId;
                BatchStatement batchStatement = new BatchStatement();

                int commentType = commentRow.GetValue<int>("comment_type");
                DateTimeOffset createdTimestamp = commentRow.GetValue<DateTimeOffset>("created_on_timestamp");

                string tableName = string.Empty;
                if (commentType == Int16.Parse(FeedTypeCode.TextPost))
                {
                    tableName = "feed_comment_text";
                }

                bool isValid = !isHidden;

                int validStatus = (int)Feed.FeedValidStatus.Valid;

                if (isHidden)
                {
                    validStatus = (int)Feed.FeedValidStatus.Hidden;
                }

                PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                    new List<string> { "feed_id", "id" }, new List<string> { "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                BoundStatement bsInvalidate = psInvalidate.Bind(isValid, validStatus, modifiedUserId, DateTime.UtcNow, feedId, commentId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed",
                 new List<string> { "feed_id", "comment_id" }, new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, feedId, commentId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_asc",
                  new List<string> { "feed_id", "created_on_timestamp", "comment_id" }, new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, feedId, createdTimestamp, commentId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_desc",
                    new List<string> { "feed_id", "created_on_timestamp", "comment_id" }, new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, feedId, createdTimestamp, commentId);
                batchStatement = batchStatement.Add(bsInvalidate);

                session.Execute(batchStatement);

                psInvalidate = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                    new List<string> { "feed_id" }, new List<string> { "comment" }, new List<int> { isHidden ? -1 : 1 }));
                bsInvalidate = psInvalidate.Bind(feedId);
                session.Execute(bsInvalidate);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedVisibilityResponse ToggleReplyVisibilty(string feedId, string commentId, string replyId, string adminUserId, Row replyRow, bool isHidden, ISession session)
        {
            FeedVisibilityResponse response = new FeedVisibilityResponse();
            response.Success = false;

            try
            {
                string modifiedUserId = adminUserId;
                BatchStatement batchStatement = new BatchStatement();

                int replyType = replyRow.GetValue<int>("reply_type");
                DateTimeOffset createdTimestamp = replyRow.GetValue<DateTimeOffset>("created_on_timestamp");

                string tableName = string.Empty;
                if (replyType == Int16.Parse(FeedTypeCode.TextPost))
                {
                    tableName = "feed_reply_text";
                }

                bool isValid = !isHidden;

                int validStatus = (int)Feed.FeedValidStatus.Valid;

                if (isHidden)
                {
                    validStatus = (int)Feed.FeedValidStatus.Hidden;
                }

                PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                    new List<string> { "comment_id", "id" }, new List<string> { "is_valid", "validStatus", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                BoundStatement bsInvalidate = psInvalidate.Bind(isValid, validStatus, modifiedUserId, DateTime.UtcNow, commentId, replyId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment",
                    new List<string> { "comment_id", "reply_id" }, new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, commentId, replyId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_desc",
                  new List<string> { "comment_id", "created_on_timestamp", "reply_id" }, new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, commentId, createdTimestamp, replyId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_asc",
                    new List<string> { "comment_id", "created_on_timestamp", "reply_id" }, new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, commentId, createdTimestamp, replyId);
                batchStatement = batchStatement.Add(bsInvalidate);

                session.Execute(batchStatement);

                psInvalidate = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                    new List<string> { "feed_id", "comment_id" }, new List<string> { "reply" }, new List<int> { isHidden ? -1 : 1 }));
                bsInvalidate = psInvalidate.Bind(feedId, commentId);
                session.Execute(bsInvalidate);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }


        public PointUpdateResponse UpdateFeedPoint(string voterUserId,
                                                   string feedId,
                                                   string companyId,
                                                   bool isUpVote)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.TargetedUserId = string.Empty;
            response.NotificationText = string.Empty;
            response.NumberOfNotificationForTargetedUser = 0;
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(voterUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                    if (feedRow != null)
                    {
                        int updateVoteResult = UpdateFeedPointCounter(voterUserId, feedId, isUpVote, session);

                        if (updateVoteResult == (int)VoteStatus.Upvote && isUpVote)
                        {
                            string ownerUserId = feedRow.GetValue<string>("owner_user_id");

                            if (!voterUserId.Equals(ownerUserId))
                            {
                                Notification notification = new Notification();

                                int feedType = feedRow.GetValue<int>("feed_type");
                                string notificationText = notification.CreateFeedNotification(voterUserId, companyId, ownerUserId, (int)Notification.NotificationFeedSubType.UpvotePost, feedType, feedId, string.Empty, string.Empty, DateHelper.ConvertDateToLong(DateTime.UtcNow), session);
                                response.NotificationText = notificationText;
                                response.TargetedUserId = ownerUserId;

                                response.NumberOfNotificationForTargetedUser = notification.SelectNotificationNumberByUser(ownerUserId, companyId, session).NumberOfNotification;
                                response.FeedId = feedId;
                                response.NotificationType = (int)Notification.NotificationType.Feed;
                                response.NotificationSubType = (int)Notification.NotificationFeedSubType.UpvotePost;
                            }

                        }

                        // Get pointers
                        Dictionary<string, int> pointDict = GetPointsForFeed(feedId, session);
                        int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
                        int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

                        response.Success = true;
                        response.HasVoted = updateVoteResult != (int)VoteStatus.Reset ? true : false;
                        response.IsUpVoted = updateVoteResult == (int)VoteStatus.Upvote ? true : false;
                        response.NegativePoints = negativePoints;
                        response.PositivePoints = positivePoints;

                    }
                    else
                    {
                        Log.Error("Invalid feed post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PointUpdateResponse UpdateCommentPoint(string voterUserId,
                                                      string feedId,
                                                      string commentId,
                                                      string companyId,
                                                      bool isUpVote)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.TargetedUserId = string.Empty;
            response.NotificationText = string.Empty;
            response.NumberOfNotificationForTargetedUser = 0;
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(voterUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                    if (feedRow != null)
                    {
                        Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);
                        if (commentRow != null)
                        {
                            int updateVoteResult = UpdateCommentPointCounter(voterUserId, feedId, commentId, isUpVote, session);
                            if (updateVoteResult == (int)VoteStatus.Upvote && isUpVote)
                            {
                                string ownerUserId = commentRow.GetValue<string>("commentor_user_id");
                                if (!voterUserId.Equals(ownerUserId))
                                {
                                    Notification notification = new Notification();

                                    int feedType = feedRow.GetValue<int>("feed_type");
                                    string notificationText = notification.CreateFeedNotification(voterUserId, companyId, ownerUserId, (int)Notification.NotificationFeedSubType.UpvoteComment, feedType, feedId, commentId, string.Empty, DateHelper.ConvertDateToLong(DateTime.UtcNow), session);
                                    response.NotificationText = notificationText;
                                    response.TargetedUserId = ownerUserId;

                                    response.NumberOfNotificationForTargetedUser = notification.SelectNotificationNumberByUser(ownerUserId, companyId, session).NumberOfNotification;
                                    response.FeedId = feedId;
                                    response.CommentId = commentId;
                                    response.NotificationType = (int)Notification.NotificationType.Feed;
                                    response.NotificationSubType = (int)Notification.NotificationFeedSubType.UpvoteComment;
                                }

                            }
                            // Get pointers
                            Dictionary<string, int> pointDict = GetPointsForComment(feedId, commentId, session);
                            int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
                            int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

                            response.Success = true;
                            response.HasVoted = updateVoteResult != (int)VoteStatus.Reset ? true : false;
                            response.IsUpVoted = updateVoteResult == (int)VoteStatus.Upvote ? true : false;
                            response.NegativePoints = negativePoints;
                            response.PositivePoints = positivePoints;
                        }
                        else
                        {
                            Log.Error("Invalid comment");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                            response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid feed post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PointUpdateResponse UpdateReplyPoint(string voterUserId,
                                                    string feedId,
                                                    string commentId,
                                                    string replyId,
                                                    string companyId,
                                                    bool isUpVote)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.TargetedUserId = string.Empty;
            response.NotificationText = string.Empty;
            response.NumberOfNotificationForTargetedUser = 0;
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(voterUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                    if (feedRow != null)
                    {
                        if (vh.ValidateFeedComment(feedId, commentId, session) != null)
                        {
                            Row replyRow = vh.ValidateCommentReply(commentId, replyId, session);
                            if (replyRow != null)
                            {
                                int updateVoteResult = UpdateReplyPointCounter(voterUserId, commentId, replyId, isUpVote, session);
                                if (updateVoteResult == (int)VoteStatus.Upvote && isUpVote)
                                {
                                    string ownerUserId = replyRow.GetValue<string>("commentor_user_id");
                                    if (!voterUserId.Equals(ownerUserId))
                                    {
                                        Notification notification = new Notification();

                                        int feedType = feedRow.GetValue<int>("feed_type");
                                        string notificationText = notification.CreateFeedNotification(voterUserId, companyId, ownerUserId, (int)Notification.NotificationFeedSubType.UpvoteReply, feedType, feedId, commentId, replyId, DateHelper.ConvertDateToLong(DateTime.UtcNow), session);
                                        response.NotificationText = notificationText;
                                        response.TargetedUserId = ownerUserId;

                                        response.NumberOfNotificationForTargetedUser = notification.SelectNotificationNumberByUser(ownerUserId, companyId, session).NumberOfNotification;
                                        response.FeedId = feedId;
                                        response.CommentId = commentId;
                                        response.ReplyId = replyId;
                                        response.NotificationType = (int)Notification.NotificationType.Feed;
                                        response.NotificationSubType = (int)Notification.NotificationFeedSubType.UpvoteReply;
                                    }
                                }

                                // Get pointers
                                Dictionary<string, int> pointDict = GetPointsForReply(commentId, replyId, session);
                                int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
                                int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

                                response.Success = true;
                                response.HasVoted = updateVoteResult != (int)VoteStatus.Reset ? true : false;
                                response.IsUpVoted = updateVoteResult == (int)VoteStatus.Upvote ? true : false;
                                response.NegativePoints = negativePoints;
                                response.PositivePoints = positivePoints;
                            }
                            else
                            {
                                Log.Error("Invalid reply");
                                response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidReply);
                                response.ErrorMessage = ErrorMessage.FeedInvalidReply;
                            }

                        }
                        else
                        {
                            Log.Error("Invalid comment");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                            response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid feed post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectPrivacyResponse SelectFeedPrivacy(string requesterUserId, 
                                                           string companyId,
                                                           string feedId)
        {
            FeedSelectPrivacyResponse response = new FeedSelectPrivacyResponse();
            response.Users = new List<User>();
            response.Departments = new List<Department>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if(userRow == null)
                {
                    Log.Error("Invalid userId: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);

                if (feedRow == null)
                {
                    Log.Error("Invalid feedId: " + feedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                string ownerUserId = feedRow.GetValue<string>("owner_user_id");
                bool isForEveryone = feedRow.GetValue<bool>("is_for_everyone");
                bool isForDepartment = feedRow.GetValue<bool>("is_for_department");
                bool isForUser = feedRow.GetValue<bool>("is_for_user");
                bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");

                Department departmentManager = new Department();
                User userManager = new User();

                List<Department> departments = departmentManager.GetAllDepartmentByUserId(requesterUserId, companyId, session).Departments;
                List<string> departmentIds = new List<string>();

                foreach (Department depart in departments)
                {
                    departmentIds.Add(depart.Id);
                }

                if (!ownerUserId.Equals(requesterUserId))
                {
                    if (!CheckPostForCurrentUser(requesterUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session))
                    {
                        Log.Error(string.Format("Feed post: {0} not available for user: {1}", feedId, requesterUserId));
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTargetedAudience);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTargetedAudience;
                        return response;
                    }
                }

                PreparedStatement ps = null;
                if(isForDepartment)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_department", 
                        new List<string>(), new List<string>{"feed_id"}));
                    RowSet departmentRowset = session.Execute(ps.Bind(feedId));

                    foreach(Row departmentRow in departmentRowset)
                    {
                        string departmentId = departmentRow.GetValue<string>("department_id");
                        Department selectedDepartment = departmentManager.GetDepartmentDetail(requesterUserId, companyId, departmentId, Department.QUERY_TYPE_BASIC, session).Department;

                        if(selectedDepartment != null)
                        {
                            response.Departments.Add(selectedDepartment);
                        }
                    }
                }

                if (isForUser)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_user", 
                        new List<string>(), new List<string>{"feed_id"}));
                    RowSet userRowset = session.Execute(ps.Bind(feedId));

                    foreach(Row targetedUserRow in userRowset)
                    {
                        string userId = targetedUserRow.GetValue<string>("user_id");
                        User selectedUser = userManager.SelectUserBasic(userId, companyId, true, session).User;

                        if(selectedUser != null)
                        {
                            response.Users.Add(selectedUser);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectResponse SelectCompanyFeedPost(string requesterUserId,
                                                        string companyId,
                                                        string searchContent,
                                                        int numberOfPostsLoaded,
                                                        DateTime? newestTimestamp,
                                                        DateTime? oldestTimestamp)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Feeds = new List<Feed>();
            response.Success = false;

            try
            {
                int limit = Int16.Parse(WebConfigurationManager.AppSettings["feed_limit"]);

                if (numberOfPostsLoaded > 0)
                {
                    limit += numberOfPostsLoaded;
                }

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow != null)
                {
                    PreparedStatement psDepartmentByUser = session.Prepare(CQLGenerator.SelectStatement("department_by_user",
                            new List<string> { "department_id" }, new List<string> { "user_id" }));
                    BoundStatement bsDepartmentByUser = psDepartmentByUser.Bind(requesterUserId);
                    RowSet departmentByUserRowset = session.Execute(bsDepartmentByUser);

                    List<string> departmentIds = new List<string>();

                    foreach (Row departmentByUserRow in departmentByUserRowset)
                    {
                        departmentIds.Add(departmentByUserRow.GetValue<string>("department_id"));
                    }

                    List<Feed> feedPosts = new List<Feed>();

                    PreparedStatement psFeedPrivacy = null;
                    BoundStatement bsFeedPrivacy = null;

                    bool isSelectCompleted = false;
                    while (isSelectCompleted == false)
                    {
                        // Since will be fetching all to avoid checking for update, then no need for newestTimestamp

                        //if (newestTimestamp == null && oldestTimestamp == null)
                        //{
                        //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp",
                        //        new List<string>(), new List<string> { "company_id", "owner_user_id" }, limit));
                        //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId);
                        //}
                        //else if (oldestTimestamp != null)
                        //{
                        //    if (currentDateTime == DateTime.MinValue)
                        //    {
                        //        currentDateTime = oldestTimestamp.Value;
                        //    }
                        //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp",
                        //        null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId, DateHelper.ConvertDateToLong(currentDateTime));
                        //}
                        //else if (newestTimestamp != null)
                        //{
                        //    if (currentDateTime == DateTime.MinValue)
                        //    {
                        //        currentDateTime = newestTimestamp.Value;
                        //    }
                        //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp",
                        //        null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.GreaterThan, limit));
                        //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId, DateHelper.ConvertDateToLong(currentDateTime));
                        //}

                        if (oldestTimestamp != null)
                        {
                            DateTime currentDateTime = oldestTimestamp.Value;
                            psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp_desc",
                                null, new List<string> { "company_id", "is_feed_valid" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                            bsFeedPrivacy = psFeedPrivacy.Bind(companyId, true, DateHelper.ConvertDateToLong(currentDateTime));
                        }
                        else
                        {
                            psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp_desc",
                                new List<string>(), new List<string> { "company_id", "is_feed_valid" }, limit));
                            bsFeedPrivacy = psFeedPrivacy.Bind(companyId, true);
                        }

                        RowSet allFeedRowSet = session.Execute(bsFeedPrivacy);
                        List<Row> allFeedRowList = new List<Row>();
                        allFeedRowList = allFeedRowSet.GetRows().ToList();

                        foreach (Row allFeedRow in allFeedRowList)
                        {
                            bool isPostForCurrentUser = false;

                            int feedType = allFeedRow.GetValue<int>("feed_type");
                            string feedId = allFeedRow.GetValue<string>("feed_id");
                            string ownerUserId = allFeedRow.GetValue<string>("owner_user_id");
                            bool isForEveryone = allFeedRow.GetValue<bool>("is_for_everyone");
                            bool isForDepartment = allFeedRow.GetValue<bool>("is_for_department");
                            bool isForUser = allFeedRow.GetValue<bool>("is_for_user");
                            DateTime feedCreatedOnTimestamp = allFeedRow.GetValue<DateTime>("feed_created_on_timestamp");

                            oldestTimestamp = feedCreatedOnTimestamp;

                            if (!string.IsNullOrEmpty(searchContent))
                            {
                                string feedTextContent = allFeedRow.GetValue<string>("feed_text_content");
                                bool isMatched = feedTextContent.Contains(searchContent);

                                if (ownerUserId.Equals(requesterUserId))
                                {
                                    if (isMatched)
                                    {
                                        isPostForCurrentUser = true;
                                    }
                                }
                                else
                                {
                                    if (isMatched)
                                    {
                                        isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session);
                                    }
                                }
                            }
                            else
                            {
                                if (ownerUserId.Equals(requesterUserId))
                                {
                                    isPostForCurrentUser = true;
                                }
                                else
                                {
                                    isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session);
                                }
                            }

                            if (isPostForCurrentUser)
                            {
                                User user = new User();
                                Feed feed = GetFeed(feedId, feedType, feedCreatedOnTimestamp, requesterUserId, user.SelectUserBasic(ownerUserId, companyId, false, session, null, true).User, session);

                                if (feed != null)
                                {
                                    feed.IsForEveryone = isForEveryone;
                                    feed.IsForDepartment = isForDepartment;
                                    feed.IsForPersonnel = isForUser;

                                    feedPosts.Add(feed);

                                    if (feedPosts.Count() == limit)
                                    {
                                        isSelectCompleted = true;
                                        break;
                                    }
                                }

                            }
                        }// for

                        if (allFeedRowList.Count() == 0)
                        {
                            isSelectCompleted = true;
                        }
                    }// while

                    response.Feeds = feedPosts;

                    NotificationSelectNumberResponse notificationResponse = new Notification().SelectNotificationNumberByUser(requesterUserId, companyId, session);

                    if (notificationResponse.Success)
                    {
                        response.NumberOfNewNotification = notificationResponse.NumberOfNotification;
                    }
                    else
                    {
                        response.NumberOfNewNotification = 0;
                    }

                    response.Success = true;
                }
                else
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectResponse SelectPersonnelFeedPost(string requesterUserId,
                                                          string ownerUserId,
                                                          string companyId,
                                                          string searchContent,
                                                          DateTime? newestTimestamp,
                                                          DateTime? oldestTimestamp)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Feeds = new List<Feed>();
            response.Success = false;

            try
            {
                int limit = Int16.Parse(WebConfigurationManager.AppSettings["feed_limit"]);

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                Row requesterUserRow = vh.ValidateUser(requesterUserId, companyId, session);
                if (requesterUserRow == null)
                {
                    Log.Error("Invalid requester user id: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                PreparedStatement psDepartmentByUser = session.Prepare(CQLGenerator.SelectStatement("department_by_user",
                                       new List<string> { "department_id" }, new List<string> { "user_id" }));
                BoundStatement bsDepartmentByUser = psDepartmentByUser.Bind(requesterUserId);
                RowSet departmentByUserRowset = session.Execute(bsDepartmentByUser);

                List<string> departmentIds = new List<string>();

                foreach (Row departmentByUserRow in departmentByUserRowset)
                {
                    departmentIds.Add(departmentByUserRow.GetValue<string>("department_id"));
                }

                List<Feed> feedPosts = new List<Feed>();

                PreparedStatement psFeedPrivacy = null;
                BoundStatement bsFeedPrivacy = null;

                bool isSelectCompleted = false;
                while (isSelectCompleted == false)
                {
                    // Since will be fetching all to avoid checking for update, then no need for newestTimestamp

                    //if (newestTimestamp == null && oldestTimestamp == null)
                    //{
                    //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp",
                    //        new List<string>(), new List<string> { "company_id", "owner_user_id" }, limit));
                    //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId);
                    //}
                    //else if (oldestTimestamp != null)
                    //{
                    //    if (currentDateTime == DateTime.MinValue)
                    //    {
                    //        currentDateTime = oldestTimestamp.Value;
                    //    }
                    //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp",
                    //        null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                    //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId, DateHelper.ConvertDateToLong(currentDateTime));
                    //}
                    //else if (newestTimestamp != null)
                    //{
                    //    if (currentDateTime == DateTime.MinValue)
                    //    {
                    //        currentDateTime = newestTimestamp.Value;
                    //    }
                    //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp",
                    //        null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.GreaterThan, limit));
                    //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId, DateHelper.ConvertDateToLong(currentDateTime));
                    //}

                    if (oldestTimestamp != null)
                    {
                        DateTime currentDateTime = oldestTimestamp.Value;
                        psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp_desc",
                            null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId, DateHelper.ConvertDateToLong(currentDateTime));
                    }
                    else
                    {
                        psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp_desc",
                            new List<string>(), new List<string> { "company_id", "owner_user_id" }, limit));
                        bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId);
                    }

                    RowSet allFeedRowSet = session.Execute(bsFeedPrivacy);
                    List<Row> allFeedRowList = new List<Row>();
                    allFeedRowList = allFeedRowSet.GetRows().ToList();

                    foreach (Row allFeedRow in allFeedRowList)
                    {
                        bool isFeedValid = allFeedRow.GetValue<bool>("is_feed_valid");
                        DateTime feedCreatedOnTimestamp = allFeedRow.GetValue<DateTime>("feed_created_on_timestamp");

                        oldestTimestamp = feedCreatedOnTimestamp;

                        if (isFeedValid)
                        {
                            bool isPostForCurrentUser = false;

                            int feedType = allFeedRow.GetValue<int>("feed_type");
                            string feedId = allFeedRow.GetValue<string>("feed_id");
                            bool isForEveryone = allFeedRow.GetValue<bool>("is_for_everyone");
                            bool isForDepartment = allFeedRow.GetValue<bool>("is_for_department");
                            bool isForUser = allFeedRow.GetValue<bool>("is_for_user");

                            if (!string.IsNullOrEmpty(searchContent))
                            {
                                string feedTextContent = allFeedRow.GetValue<string>("feed_text_content");
                                bool isMatched = feedTextContent.Contains(searchContent);

                                if (isMatched)
                                {
                                    isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session);
                                }
                            }
                            else
                            {
                                isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session);
                            }

                            if (isPostForCurrentUser)
                            {
                                User user = new User();

                                Feed feed = GetFeed(feedId, feedType, feedCreatedOnTimestamp, requesterUserId, user.SelectUserBasic(ownerUserId, companyId, false, session, null, true).User, session);
                                if (feed != null)
                                {
                                    feed.IsForEveryone = isForEveryone;
                                    feed.IsForDepartment = isForDepartment;
                                    feed.IsForPersonnel = isForUser;

                                    feedPosts.Add(feed);

                                    if (feedPosts.Count == limit)
                                    {
                                        isSelectCompleted = true;
                                        break;
                                    }
                                }

                            }
                        }

                    }// for

                    if (allFeedRowList.Count == 0)
                    {
                        isSelectCompleted = true;
                    }

                }// while

                response.Feeds = feedPosts;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public bool CheckPostForCurrentUser(string userId, string feedId, bool isForEveryone, bool isForDepartment, bool isForUser, List<string> departmentIds, ISession session)
        {

            bool isPostForCurrentUser = false;

            if (isForEveryone)
            {
                isPostForCurrentUser = true;
            }
            else
            {
                if (isForDepartment)
                {
                    foreach (string departmentId in departmentIds)
                    {
                        PreparedStatement psFeedTargetedDepartment = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_department",
                            new List<string>(), new List<string> { "feed_id", "department_id" }));
                        BoundStatement bsFeedTargetedDepartment = psFeedTargetedDepartment.Bind(feedId, departmentId);

                        Row departmentPrivacyRow = session.Execute(bsFeedTargetedDepartment).FirstOrDefault();

                        if (departmentPrivacyRow != null)
                        {
                            isPostForCurrentUser = true;
                            break;
                        }
                    }
                }

                if (!isPostForCurrentUser && isForUser)
                {
                    PreparedStatement psFeedTargetedUser = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_user",
                            new List<string>(), new List<string> { "feed_id", "user_id" }));
                    BoundStatement bsFeedTargetedUser = psFeedTargetedUser.Bind(feedId, userId);

                    Row userPrivacyRow = session.Execute(bsFeedTargetedUser).FirstOrDefault();

                    if (userPrivacyRow != null)
                    {
                        isPostForCurrentUser = true;
                    }
                }
            }

            return isPostForCurrentUser;
        }

        private Dictionary<string, bool> CheckVoteForFeed(string userId, string feedId, ISession session)
        {
            Dictionary<string, bool> resultDict = new Dictionary<string, bool>();

            PreparedStatement psFeedPoint = session.Prepare(CQLGenerator.SelectStatement("feed_point",
                            new List<string>(), new List<string> { "feed_id", "user_id" }));
            BoundStatement bsFeedPoint = psFeedPoint.Bind(feedId, userId);

            Row feedPointRow = session.Execute(bsFeedPoint).FirstOrDefault();

            resultDict.Add("hasVoted", false);
            resultDict.Add("isUpVoted", false);

            if (feedPointRow != null)
            {
                resultDict["hasVoted"] = true;

                int value = feedPointRow.GetValue<int>("value");

                resultDict["isUpVoted"] = value == 1 ? true : false;
            }

            return resultDict;
        }

        private Dictionary<string, bool> CheckVoteForComment(string userId, string commentId, ISession session)
        {
            Dictionary<string, bool> resultDict = new Dictionary<string, bool>();

            PreparedStatement psCommentPoint = session.Prepare(CQLGenerator.SelectStatement("comment_point",
                            new List<string>(), new List<string> { "comment_id", "user_id" }));
            BoundStatement bsCommentPoint = psCommentPoint.Bind(commentId, userId);

            Row commentPointRow = session.Execute(bsCommentPoint).FirstOrDefault();

            resultDict.Add("hasVoted", false);
            resultDict.Add("isUpVoted", false);

            if (commentPointRow != null)
            {
                resultDict["hasVoted"] = true;

                int value = commentPointRow.GetValue<int>("value");

                resultDict["isUpVoted"] = value == 1 ? true : false;
            }

            return resultDict;
        }

        private Dictionary<string, bool> CheckVoteForReply(string userId, string replyId, ISession session)
        {
            Dictionary<string, bool> resultDict = new Dictionary<string, bool>();

            PreparedStatement psReplyPoint = session.Prepare(CQLGenerator.SelectStatement("reply_point",
                            new List<string>(), new List<string> { "reply_id", "user_id" }));
            BoundStatement bsReplyPoint = psReplyPoint.Bind(replyId, userId);

            Row replyPointRow = session.Execute(bsReplyPoint).FirstOrDefault();

            resultDict.Add("hasVoted", false);
            resultDict.Add("isUpVoted", false);

            if (replyPointRow != null)
            {
                resultDict["hasVoted"] = true;

                int value = replyPointRow.GetValue<int>("value");

                resultDict["isUpVoted"] = value == 1 ? true : false;
            }

            return resultDict;
        }

        private Comment GetCommentWithLatestReply(string companyId, string feedId, string commentId, int commentType, string requesterUserId, User commentorUser, ISession session)
        {
            Comment comment = null;
            Reply latestReply = new Reply();

            // Get latest reply
            PreparedStatement psLatestReply = session.Prepare(CQLGenerator.SelectStatementWithLimit("reply_by_comment_timestamp_desc",
                   new List<string> { "reply_id", "reply_type", "commentor_user_id" }, new List<string> { "comment_id", "is_reply_valid" }, 1));
            BoundStatement bsLatestReply = psLatestReply.Bind(commentId, true);
            Row replyRow = session.Execute(bsLatestReply).FirstOrDefault();

            if (replyRow != null)
            {
                string replyId = replyRow.GetValue<string>("reply_id");
                int replyType = replyRow.GetValue<int>("reply_type");
                string replyCommentorUserId = replyRow.GetValue<string>("commentor_user_id");
                User replyCommentorUser = new User().SelectUserBasic(replyCommentorUserId, companyId, false, session).User;

                latestReply = GetReply(companyId, feedId, commentId, replyId, replyType, requesterUserId, replyCommentorUser, session);
            }

            // Get counter
            int numberOfReplies = GetNumberOfRepliesForComment(feedId, commentId, session);

            // Get pointers
            Dictionary<string, int> pointDict = GetPointsForComment(feedId, commentId, session);
            int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
            int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

            // Check for vote
            Dictionary<string, bool> voteDict = CheckVoteForComment(requesterUserId, commentId, session);
            bool hasVoted = voteDict["hasVoted"];
            bool isUpVoted = voteDict["isUpVoted"];

            if (commentType == Int16.Parse(FeedTypeCode.TextPost))
            {
                PreparedStatement psCommentText = session.Prepare(CQLGenerator.SelectStatement("feed_comment_text",
                    new List<string> { "user_id", "content", "created_on_timestamp" }, new List<string> { "id", "feed_id", "is_valid" }));
                BoundStatement bsCommentText = psCommentText.Bind(commentId, feedId, true);

                Row commentTextRow = session.Execute(bsCommentText).FirstOrDefault();

                if (commentTextRow != null)
                {
                    string content = commentTextRow.GetValue<string>("content");
                    string commentorUserId = commentTextRow.GetValue<string>("user_id");

                    DateTime createdOnTimestamp = commentTextRow.GetValue<DateTime>("created_on_timestamp");

                    comment = new Comment
                    {
                        FeedId = feedId,
                        CommentId = commentId,
                        CommentType = commentType,
                        CommentText = new CommentText
                        {
                            Content = content
                        },
                        Reply = latestReply,
                        NegativePoints = negativePoints,
                        PositivePoints = positivePoints,
                        NumberOfReplies = numberOfReplies,
                        HasVoted = hasVoted,
                        IsUpVoted = isUpVoted,
                        User = commentorUser,
                        CreatedOnTimestamp = createdOnTimestamp
                    };
                }

            }

            return comment;
        }

        private Reply GetReply(string companyId, string feedId, string commentId, string replyId, int replyType, string requesterUserId, User commentorUser, ISession session)
        {

            Reply reply = null;

            // Get pointers
            Dictionary<string, int> pointDict = GetPointsForReply(commentId, replyId, session);
            int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
            int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

            // Check for vote
            Dictionary<string, bool> voteDict = CheckVoteForReply(requesterUserId, replyId, session);
            bool hasVoted = voteDict["hasVoted"];
            bool isUpVoted = voteDict["isUpVoted"];

            if (replyType == Int16.Parse(FeedTypeCode.TextPost))
            {
                PreparedStatement psReplyText = session.Prepare(CQLGenerator.SelectStatement("feed_reply_text",
                    new List<string> { "user_id", "content", "created_on_timestamp" }, new List<string> { "id", "comment_id", "is_valid" }));
                BoundStatement bsReplyText = psReplyText.Bind(replyId, commentId, true);

                Row replyTextRow = session.Execute(bsReplyText).FirstOrDefault();

                if (replyTextRow != null)
                {
                    string content = replyTextRow.GetValue<string>("content");
                    DateTime createdOnTimestamp = replyTextRow.GetValue<DateTime>("created_on_timestamp");

                    reply = new Reply
                    {
                        FeedId = feedId,
                        CommentId = commentId,
                        ReplyId = replyId,
                        ReplyType = replyType,
                        ReplyText = new ReplyText
                        {
                            Content = content
                        },
                        NegativePoints = negativePoints,
                        PositivePoints = positivePoints,
                        HasVoted = hasVoted,
                        IsUpVoted = isUpVoted,
                        User = commentorUser,
                        CreatedOnTimestamp = createdOnTimestamp
                    };
                }

            }

            return reply;
        }


        public Feed GetFeed(string feedId, int feedType, DateTime feedCreatedOnTimestamp, string requesterUserId, User ownerUser, ISession session, bool checkForValid = true, string companyId = null, bool isConvertToTimezone = false)
        {
            Feed feed = null;

            // Get number of comments
            int numberOfComment = GetNumberOfCommentsForFeed(feedId, session);

            // Get pointers
            int negativePoints = 0;
            int positivePoints = 0;

            // Check for vote
            bool hasVoted = false;
            bool isUpVoted = false;

            FeedText feedText = null;
            FeedImage feedImage = null;
            FeedVideo feedVideo = null;
            FeedSharedUrl feedSharedUrl = null;

            bool isFeedAvailable = false;

            if (feedType == Int16.Parse(FeedTypeCode.TextPost))
            {
                PreparedStatement psFeedText = null;
                BoundStatement bsFeedText = null;

                if (checkForValid)
                {
                    psFeedText = session.Prepare(CQLGenerator.SelectStatement("feed_text",
                        new List<string> { "content" }, new List<string> { "id", "is_valid" }));
                    bsFeedText = psFeedText.Bind(feedId, true);
                }
                else
                {
                    psFeedText = session.Prepare(CQLGenerator.SelectStatement("feed_text",
                        new List<string> { "content" }, new List<string> { "id" }));
                    bsFeedText = psFeedText.Bind(feedId);
                }

                Row feedTextRow = session.Execute(bsFeedText).FirstOrDefault();

                if (feedTextRow != null)
                {
                    string content = feedTextRow.GetValue<string>("content");

                    feedText = new FeedText
                    {
                        Content = content
                    };

                    isFeedAvailable = true;
                }

            }
            else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
            {
                PreparedStatement psFeedImage = null;
                BoundStatement bsFeedImage = null;

                if (checkForValid)
                {
                    psFeedImage = session.Prepare(CQLGenerator.SelectStatement("feed_image",
                        new List<string> { "caption" }, new List<string> { "id", "is_valid" }));
                    bsFeedImage = psFeedImage.Bind(feedId, true);
                }
                else
                {
                    psFeedImage = session.Prepare(CQLGenerator.SelectStatement("feed_image",
                        new List<string> { "caption" }, new List<string> { "id" }));
                    bsFeedImage = psFeedImage.Bind(feedId);
                }


                Row feedImageRow = session.Execute(bsFeedImage).FirstOrDefault();

                if (feedImageRow != null)
                {
                    string caption = feedImageRow.GetValue<string>("caption");

                    PreparedStatement psFeedImageUrl = session.Prepare(CQLGenerator.SelectStatement("feed_image_url",
                        new List<string>(), new List<string> { "feed_id", "is_valid" }));
                    BoundStatement bsFeedImageUrl = psFeedImageUrl.Bind(feedId, true);

                    RowSet feedImageUrlRowSet = session.Execute(bsFeedImageUrl);

                    List<Image> images = new List<Image>();
                    foreach (Row feedImageUrlRow in feedImageUrlRowSet.GetRows())
                    {
                        Image image = new Image
                        {
                            ImageId = feedImageUrlRow.GetValue<string>("id"),
                            Url = feedImageUrlRow.GetValue<string>("url"),
                            Order = feedImageUrlRow.GetValue<int>("in_order")
                        };

                        images.Add(image);
                    }

                    feedImage = new FeedImage
                    {
                        Caption = caption,
                        Images = images
                    };

                    isFeedAvailable = true;
                }

            }
            else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
            {
                PreparedStatement psFeedVideo = null;
                BoundStatement bsFeedVideo = null;

                if (checkForValid)
                {
                    psFeedVideo = session.Prepare(CQLGenerator.SelectStatement("feed_video",
                        new List<string> { "video_url", "video_thumbnail_url", "caption" }, new List<string> { "id", "is_valid" }));
                    bsFeedVideo = psFeedVideo.Bind(feedId, true);
                }
                else
                {
                    psFeedVideo = session.Prepare(CQLGenerator.SelectStatement("feed_video",
                        new List<string> { "video_url", "video_thumbnail_url", "caption" }, new List<string> { "id" }));
                    bsFeedVideo = psFeedVideo.Bind(feedId);
                }


                Row feedVideoRow = session.Execute(bsFeedVideo).FirstOrDefault();

                if (feedVideoRow != null)
                {
                    string caption = feedVideoRow.GetValue<string>("caption");
                    string videoUrl = feedVideoRow.GetValue<string>("video_url");
                    string videoThumbnailUrl = feedVideoRow.GetValue<string>("video_thumbnail_url");

                    feedVideo = new FeedVideo
                    {
                        Caption = caption,
                        VideoUrl = videoUrl,
                        VideoThumbnailUrl = videoThumbnailUrl
                    };

                    isFeedAvailable = true;
                }

            }
            else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
            {
                PreparedStatement psFeedSharedUrl = null;
                BoundStatement bsFeedSharedUrl = null;

                if (checkForValid)
                {
                    psFeedSharedUrl = session.Prepare(CQLGenerator.SelectStatement("feed_shared_url",
                        new List<string> { "caption", "url", "url_title", "url_description", "url_site_name", "url_image_url" }, new List<string> { "id", "is_valid" }));
                    bsFeedSharedUrl = psFeedSharedUrl.Bind(feedId, true);
                }
                else
                {
                    psFeedSharedUrl = session.Prepare(CQLGenerator.SelectStatement("feed_shared_url",
                        new List<string> { "caption", "url", "url_title", "url_description", "url_site_name", "url_image_url" }, new List<string> { "id" }));
                    bsFeedSharedUrl = psFeedSharedUrl.Bind(feedId);
                }


                Row feedSharedUrlRow = session.Execute(bsFeedSharedUrl).FirstOrDefault();

                if (feedSharedUrlRow != null)
                {
                    string caption = feedSharedUrlRow.GetValue<string>("caption");
                    string url = feedSharedUrlRow.GetValue<string>("url");
                    string title = feedSharedUrlRow.GetValue<string>("url_title");
                    string description = feedSharedUrlRow.GetValue<string>("url_description");
                    string siteName = feedSharedUrlRow.GetValue<string>("url_site_name");
                    string imageUrl = feedSharedUrlRow.GetValue<string>("url_image_url");

                    feedSharedUrl = new FeedSharedUrl
                    {
                        Caption = caption,
                        Url = url,
                        Title = title,
                        Description = description,
                        SiteName = siteName,
                        ImageUrl = imageUrl
                    };

                    isFeedAvailable = true;
                }

            }

            if (isFeedAvailable)
            {
                // Get number of comments
                numberOfComment = GetNumberOfCommentsForFeed(feedId, session);

                // Get pointers
                Dictionary<string, int> pointDict = GetPointsForFeed(feedId, session);
                negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
                positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

                // Check for vote
                Dictionary<string, bool> voteDict = CheckVoteForFeed(requesterUserId, feedId, session);
                hasVoted = voteDict["hasVoted"];
                isUpVoted = voteDict["isUpVoted"];

                feed = new Feed
                {
                    FeedId = feedId,
                    FeedType = feedType,
                    FeedText = feedText,
                    FeedImage = feedImage,
                    FeedVideo = feedVideo,
                    FeedSharedUrl = feedSharedUrl,
                    NegativePoints = negativePoints,
                    PositivePoints = positivePoints,
                    NumberOfComments = numberOfComment,
                    HasVoted = hasVoted,
                    IsUpVoted = isUpVoted,
                    User = ownerUser,
                    CreatedOnTimestamp = !isConvertToTimezone ? feedCreatedOnTimestamp : DateHelper.ConvertDateToTimezoneSpecific(feedCreatedOnTimestamp, companyId, session)
                };
            }


            return feed;
        }

        public Dictionary<string, int> ResetFeedPoint(string feedId, int numberOfComments, ISession session)
        {
            Dictionary<string, int> updatedPoints = new Dictionary<string, int>();

            try
            {
                PreparedStatement psFeedCounter = null;

                PreparedStatement psFeedPoint = session.Prepare(CQLGenerator.SelectStatement("feed_point",
                    new List<string>(), new List<string> { "feed_id" }));
                BoundStatement bsFeedPoint = psFeedPoint.Bind(feedId);
                RowSet feedPointRowSet = session.Execute(bsFeedPoint);

                int negativePoint = 0;
                int positivePoint = 0;

                foreach (Row feedPointRow in feedPointRowSet)
                {
                    int value = feedPointRow.GetValue<int>("value");

                    if (value > 0)
                    {
                        positivePoint++;
                    }
                    else
                    {
                        negativePoint++;
                    }
                }

                psFeedCounter = session.Prepare(CQLGenerator.DeleteStatement("feed_counter",
                    new List<string> { "feed_id" }));
                session.Execute(psFeedCounter.Bind(feedId));

                psFeedCounter = session.Prepare(CQLGenerator.InsertStatement("feed_counter",
                    new List<string> { "feed_id", "negative_point", "positive_point", "comment" }));
                session.Execute(psFeedCounter.Bind(feedId, negativePoint, positivePoint, numberOfComments));

                updatedPoints.Add("negativePoint", negativePoint);
                updatedPoints.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return updatedPoints;
        }

        public Dictionary<string, int> ResetCommentPoint(string feedId, string commentId, int numberOfReplies, ISession session)
        {
            Dictionary<string, int> updatedPoints = new Dictionary<string, int>();

            try
            {
                PreparedStatement psCommentCounter = null;

                PreparedStatement psCommentPoint = session.Prepare(CQLGenerator.SelectStatement("comment_point",
                    new List<string>(), new List<string> { "comment_id" }));
                BoundStatement bsCommentPoint = psCommentPoint.Bind(commentId);
                RowSet commentPointRowSet = session.Execute(bsCommentPoint);

                int negativePoint = 0;
                int positivePoint = 0;

                foreach (Row commentPointRow in commentPointRowSet)
                {
                    int value = commentPointRow.GetValue<int>("value");

                    if (value > 0)
                    {
                        positivePoint++;
                    }
                    else
                    {
                        negativePoint++;
                    }
                }

                psCommentCounter = session.Prepare(CQLGenerator.DeleteStatement("comment_counter",
                    new List<string> { "feed_id", "comment_id" }));
                session.Execute(psCommentCounter.Bind(feedId, commentId));

                psCommentCounter = session.Prepare(CQLGenerator.InsertStatement("comment_counter",
                    new List<string> { "feed_id", "comment_id", "negative_point", "positive_point", "reply" }));
                session.Execute(psCommentCounter.Bind(feedId, commentId, negativePoint, positivePoint, numberOfReplies));

                updatedPoints.Add("negativePoint", negativePoint);
                updatedPoints.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return updatedPoints;
        }

        public Dictionary<string, int> ResetReplyPoint(string commentId, string replyId, ISession session)
        {
            Dictionary<string, int> updatedPoints = new Dictionary<string, int>();

            try
            {
                PreparedStatement psReplyCounter = null;

                PreparedStatement psReplyPoint = session.Prepare(CQLGenerator.SelectStatement("reply_point",
                    new List<string>(), new List<string> { "reply_id" }));
                BoundStatement bsReplyPoint = psReplyPoint.Bind(replyId);
                RowSet replyPointRowSet = session.Execute(bsReplyPoint);

                int negativePoint = 0;
                int positivePoint = 0;

                foreach (Row replyPointRow in replyPointRowSet)
                {
                    int value = replyPointRow.GetValue<int>("value");

                    if (value > 0)
                    {
                        positivePoint++;
                    }
                    else
                    {
                        negativePoint++;
                    }
                }

                psReplyCounter = session.Prepare(CQLGenerator.DeleteStatement("reply_counter",
                    new List<string> { "comment_id", "reply_id" }));
                session.Execute(psReplyCounter.Bind(commentId, replyId));

                psReplyCounter = session.Prepare(CQLGenerator.InsertStatement("reply_counter",
                    new List<string> { "comment_id", "reply_id", "negative_point", "positive_point" }));
                session.Execute(psReplyCounter.Bind(commentId, replyId, negativePoint, positivePoint));

                updatedPoints.Add("negativePoint", negativePoint);
                updatedPoints.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return updatedPoints;
        }

        public Dictionary<string, int> GetPointsForFeed(string feedId, ISession session)
        {
            Dictionary<string, int> pointDict = new Dictionary<string, int>();

            try
            {
                int negativePoint = 0;
                int positivePoint = 0;

                PreparedStatement psFeedCounter = session.Prepare(CQLGenerator.SelectStatement("feed_counter",
                    new List<string>(), new List<string> { "feed_id" }));
                BoundStatement bsFeedCounter = psFeedCounter.Bind(feedId);

                Row feedCounterRow = session.Execute(bsFeedCounter).FirstOrDefault();
                if (feedCounterRow != null)
                {
                    int numberOfComments = feedCounterRow.GetValue<Nullable<long>>("comment") == null ? 0 : (int)feedCounterRow.GetValue<long>("comment");
                    negativePoint = feedCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)feedCounterRow.GetValue<long>("negative_point");
                    positivePoint = feedCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)feedCounterRow.GetValue<long>("positive_point");

                    // Clean up script
                    if (negativePoint < 0 || positivePoint < 0)
                    {
                        Dictionary<string, int> updatedPoints = ResetFeedPoint(feedId, numberOfComments, session);
                        negativePoint = updatedPoints["negativePoint"];
                        positivePoint = updatedPoints["positivePoint"];
                    }
                }

                pointDict.Add("negativePoint", negativePoint);
                pointDict.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return pointDict;
        }

        public Dictionary<string, int> GetPointsForComment(string feedId, string commentId, ISession session)
        {
            Dictionary<string, int> pointDict = new Dictionary<string, int>();

            try
            {
                int negativePoint = 0;
                int positivePoint = 0;

                PreparedStatement psCommentCounter = session.Prepare(CQLGenerator.SelectStatement("comment_counter",
                    new List<string>(), new List<string> { "feed_id", "comment_id" }));
                BoundStatement bsCommentCounter = psCommentCounter.Bind(feedId, commentId);

                Row commentCounterRow = session.Execute(bsCommentCounter).FirstOrDefault();
                if (commentCounterRow != null)
                {
                    int numberOfReplies = commentCounterRow.GetValue<Nullable<long>>("reply") == null ? 0 : (int)commentCounterRow.GetValue<long>("reply");
                    negativePoint = commentCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)commentCounterRow.GetValue<long>("negative_point");
                    positivePoint = commentCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)commentCounterRow.GetValue<long>("positive_point");

                    // Clean up script
                    if (negativePoint < 0 || positivePoint < 0)
                    {
                        Dictionary<string, int> updatedPoints = ResetCommentPoint(feedId, commentId, numberOfReplies, session);
                        negativePoint = updatedPoints["negativePoint"];
                        positivePoint = updatedPoints["positivePoint"];
                    }
                }

                pointDict.Add("negativePoint", negativePoint);
                pointDict.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return pointDict;
        }

        public Dictionary<string, int> GetPointsForReply(string commentId, string replyId, ISession session)
        {
            Dictionary<string, int> pointDict = new Dictionary<string, int>();

            try
            {
                int negativePoint = 0;
                int positivePoint = 0;

                PreparedStatement psReplyCounter = session.Prepare(CQLGenerator.SelectStatement("reply_counter",
                    new List<string> { "negative_point", "positive_point" }, new List<string> { "comment_id", "reply_id" }));
                BoundStatement bsReplyCounter = psReplyCounter.Bind(commentId, replyId);

                Row replyCounterRow = session.Execute(bsReplyCounter).FirstOrDefault();
                if (replyCounterRow != null)
                {
                    negativePoint = replyCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)replyCounterRow.GetValue<long>("negative_point");
                    positivePoint = replyCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)replyCounterRow.GetValue<long>("positive_point");

                    // Clean up script
                    if (negativePoint < 0 || positivePoint < 0)
                    {
                        Dictionary<string, int> updatedPoints = ResetReplyPoint(commentId, replyId, session);
                        negativePoint = updatedPoints["negativePoint"];
                        positivePoint = updatedPoints["positivePoint"];
                    }
                }

                pointDict.Add("negativePoint", negativePoint);
                pointDict.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return pointDict;
        }

        public int GetNumberOfCommentsForFeed(string feedId, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psFeedCounter = session.Prepare(CQLGenerator.SelectStatement("feed_counter",
                    new List<string> { "comment" }, new List<string> { "feed_id" }));
                BoundStatement bsFeedCounter = psFeedCounter.Bind(feedId);

                Row feedCounterRow = session.Execute(bsFeedCounter).FirstOrDefault();
                if (feedCounterRow != null)
                {
                    number = feedCounterRow.GetValue<Nullable<long>>("comment") == null ? 0 : (int)feedCounterRow.GetValue<long>("comment");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        public int GetNumberOfRepliesForComment(string feedId, string commentId, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psCommentCounter = session.Prepare(CQLGenerator.SelectStatement("comment_counter",
                    new List<string> { "reply" }, new List<string> { "feed_id", "comment_id" }));
                BoundStatement bsCommentCounter = psCommentCounter.Bind(feedId, commentId);

                Row commentCounterRow = session.Execute(bsCommentCounter).FirstOrDefault();
                if (commentCounterRow != null)
                {
                    number = commentCounterRow.GetValue<Nullable<long>>("reply") == null ? 0 : (int)commentCounterRow.GetValue<long>("reply");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        private List<string> CreateFeedPrivacy(string ownerUserId,
                                               string feedId,
                                               string companyId,
                                               int feedType,
                                               string feedTextContent,
                                               List<string> targetedDepartmentIds,
                                               List<string> targetedUserIds,
                                               bool isForAdmin,
                                               ISession session)
        {
            List<string> targetedUserIdsForNotification = new List<string>();
            BatchStatement batch = new BatchStatement();
            PreparedStatement preparedStatement = null;

            try
            {
                bool isForDepartment = targetedDepartmentIds != null && targetedDepartmentIds.Count != 0 ? true : false;
                bool isForUser = targetedUserIds != null && targetedUserIds.Count != 0 ? true : false;
                bool isForEveryone = !isForAdmin && !isForDepartment && !isForUser ? true : false;

                if (!isForEveryone)
                {
                    if (isForDepartment)
                    {
                        User user = new User();
                        foreach (string departmentId in targetedDepartmentIds)
                        {
                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_targeted_department",
                                new List<string> { "feed_id", "department_id" }));
                            batch.Add(preparedStatement.Bind(feedId, departmentId));
                        }

                        targetedUserIdsForNotification.AddRange(user.SelectAllUserIdsByDepartmentIds(targetedDepartmentIds, ownerUserId, companyId, session));
                    }

                    if (isForUser)
                    {
                        foreach (string userId in targetedUserIds)
                        {
                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_targeted_user",
                                new List<string> { "feed_id", "user_id" }));
                            batch.Add(preparedStatement.Bind(feedId, userId));

                            if (!targetedUserIdsForNotification.Contains(userId))
                            {
                                targetedUserIdsForNotification.Add(userId);
                            }
                        }
                    }
                }

                // To ensure there is no duplication due to client spamming
                preparedStatement = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                    new List<string>(), new List<string> { "company_id", "feed_id" }));
                Row feedRow = session.Execute(preparedStatement.Bind(companyId, feedId)).FirstOrDefault();

                if(feedRow == null)
                {
                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_privacy",
                                new List<string> { "feed_id", "company_id", "owner_user_id", "feed_type", "is_feed_valid", "feed_valid_status", "is_for_everyone", "is_for_department", "is_for_user", "is_for_admin", "feed_created_on_timestamp", "is_posted_by_admin" }));
                    batch.Add(preparedStatement.Bind(feedId, companyId, ownerUserId, feedType, true, 1, isForEveryone, isForDepartment, isForUser, isForAdmin, DateHelper.ConvertDateToLong(DateTime.UtcNow), false));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_privacy_by_company_timestamp_desc",
                                new List<string> { "feed_id", "company_id", "owner_user_id", "feed_type", "feed_text_content", "is_feed_valid", "feed_valid_status", "is_for_everyone", "is_for_department", "is_for_user", "is_for_admin", "feed_created_on_timestamp", "is_posted_by_admin" }));
                    batch.Add(preparedStatement.Bind(feedId, companyId, ownerUserId, feedType, feedTextContent, true, 1, isForEveryone, isForDepartment, isForUser, isForAdmin, DateHelper.ConvertDateToLong(DateTime.UtcNow), false));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_privacy_by_company_timestamp_asc",
                        new List<string> { "feed_id", "company_id", "owner_user_id", "feed_type", "feed_text_content", "is_feed_valid", "feed_valid_status", "is_for_everyone", "is_for_department", "is_for_user", "is_for_admin", "feed_created_on_timestamp", "is_posted_by_admin" }));
                    batch.Add(preparedStatement.Bind(feedId, companyId, ownerUserId, feedType, feedTextContent, true, 1, isForEveryone, isForDepartment, isForUser, isForAdmin, DateHelper.ConvertDateToLong(DateTime.UtcNow), false));

                    session.Execute(batch);
                }
                else
                {
                    Log.Error("Feed got spammed by client: " + ownerUserId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return targetedUserIdsForNotification;
        }

        private Dictionary<string, List<string>> UpdateFeedPrivacy(string ownerUserId,
                                                                   string feedId,
                                                                   string companyId,
                                                                   int feedType,
                                                                   string feedTextContent,
                                                                   List<string> newTargetedDepartmentIds,
                                                                   List<string> newTargetedUserIds,
                                                                   DateTimeOffset feedCreatedOnTimestamp,
                                                                   ISession session)
        {
            List<string> targetedUsersForNotification = new List<string>();
            List<string> addedUsersForNotification = new List<string>();
            List<string> removedUsersForNotification = new List<string>();
            Dictionary<string, List<string>> updatedPrivacyDict = new Dictionary<string, List<string>>();
            BatchStatement deleteBatch = new BatchStatement();
            BatchStatement updateBatch = new BatchStatement();
            PreparedStatement ps = null;

            try
            {
                // Existing notified users
                Notification notificationManager = new Notification();
                List<string> alreadyNotifiedUsers = notificationManager.SelectTargetedUserIdForNotification(feedId, "NA", "NA", "NA", (int)Notification.NotificationType.Feed, (int)Notification.NotificationFeedSubType.NewPost, session);

                // Delete existing feed privacy
                ps = session.Prepare(CQLGenerator.DeleteStatement("feed_targeted_department",
                    new List<string> { "feed_id" }));
                deleteBatch.Add(ps.Bind(feedId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("feed_targeted_user",
                    new List<string> { "feed_id" }));
                deleteBatch.Add(ps.Bind(feedId));

                #warning Notification update
                // Do not remove notification for now
                // What if the user added back the deleted user?
                bool isForDepartment = newTargetedDepartmentIds != null && newTargetedDepartmentIds.Count != 0 ? true : false;
                bool isForUser = newTargetedUserIds != null && newTargetedUserIds.Count != 0 ? true : false;
                bool isForEveryone = !isForDepartment && !isForUser ? true : false;

                if (!isForEveryone)
                {
                    if (isForDepartment)
                    {
                        User user = new User();
                        foreach (string departmentId in newTargetedDepartmentIds)
                        {
                            ps = session.Prepare(CQLGenerator.InsertStatement("feed_targeted_department",
                                new List<string> { "feed_id", "department_id" }));
                            updateBatch.Add(ps.Bind(feedId, departmentId));
                        }

                        targetedUsersForNotification.AddRange(user.SelectAllUserIdsByDepartmentIds(newTargetedDepartmentIds, ownerUserId, companyId, session));
                    }

                    if (isForUser)
                    {
                        foreach (string userId in newTargetedUserIds)
                        {
                            ps = session.Prepare(CQLGenerator.InsertStatement("feed_targeted_user",
                                new List<string> { "feed_id", "user_id" }));
                            updateBatch.Add(ps.Bind(feedId, userId));

                            if (!targetedUsersForNotification.Contains(userId))
                            {
                                targetedUsersForNotification.Add(userId);
                            }
                        }
                    }
                }

                ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                    new List<string>(), new List<string> { "company_id", "feed_id" }));
                Row feedRow = session.Execute(ps.Bind(companyId, feedId)).FirstOrDefault();

                if (feedRow != null)
                {
                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy",
                                new List<string> { "feed_id", "company_id" }, new List<string> { "is_for_everyone", "is_for_department", "is_for_user", "feed_type" }, new List<string>()));
                    updateBatch.Add(ps.Bind(isForEveryone, isForDepartment, isForUser, feedType, feedId, companyId));

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc",
                        new List<string> { "feed_id", "feed_created_on_timestamp", "company_id" }, new List<string> { "feed_text_content", "is_for_everyone", "is_for_department", "is_for_user", "feed_type" }, new List<string>()));
                    updateBatch.Add(ps.Bind(feedTextContent, isForEveryone, isForDepartment, isForUser, feedType, feedId, feedCreatedOnTimestamp, companyId));

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc",
                        new List<string> { "feed_id", "feed_created_on_timestamp", "company_id" }, new List<string> { "feed_text_content", "is_for_everyone", "is_for_department", "is_for_user", "feed_type" }, new List<string>()));
                    updateBatch.Add(ps.Bind(feedTextContent, isForEveryone, isForDepartment, isForUser, feedType, feedId, feedCreatedOnTimestamp, companyId));

                    session.Execute(deleteBatch);
                    session.Execute(updateBatch);

                    addedUsersForNotification = targetedUsersForNotification.Except(alreadyNotifiedUsers).ToList();
                    removedUsersForNotification = alreadyNotifiedUsers.Except(targetedUsersForNotification).ToList();

                    updatedPrivacyDict.Add("AddedUsers", addedUsersForNotification);
                    updatedPrivacyDict.Add("RemovedUsers", removedUsersForNotification);
                }
                else
                {
                    Log.Error("Feed privacy not found");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return updatedPrivacyDict;
        }

        public void UpdateFeedCommentCounter(string feedId, int operatorValue, ISession session)
        {
            try
            {
                PreparedStatement preparedStatement = null;
                BoundStatement boundStatement = null;

                preparedStatement = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                    new List<string> { "feed_id" }, new List<string> { "comment" }, new List<int> { operatorValue }));
                boundStatement = preparedStatement.Bind(feedId);
                session.Execute(boundStatement);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UpdateCommentReplyCounter(string feedId, string commentId, int operatorValue, ISession session)
        {
            try
            {
                PreparedStatement preparedStatement = null;
                BoundStatement boundStatement = null;

                preparedStatement = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                    new List<string> { "feed_id", "comment_id" }, new List<string> { "reply" }, new List<int> { operatorValue }));
                boundStatement = preparedStatement.Bind(feedId, commentId);
                session.Execute(boundStatement);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public int UpdateFeedPointCounter(string userId, string feedId, bool isUpVote, ISession session)
        {
            int result = (int)VoteStatus.Reset;

            try
            {
                string pointColumnName = string.Empty;

                if (isUpVote)
                {
                    pointColumnName = CassandraColumn.FeedPointPositive;
                }
                else
                {
                    pointColumnName = CassandraColumn.FeedPointNegative;
                }

                PreparedStatement psFeedCounter = session.Prepare(CQLGenerator.SelectStatement("feed_counter",
                    new List<string>(), new List<string> { "feed_id" }));
                BoundStatement bsFeedCounter = psFeedCounter.Bind(feedId);
                Row feedCounterRow = session.Execute(bsFeedCounter).FirstOrDefault();

                int numberOfComments = 0;
                int currentPositivePoints = 0;
                int currentNegativePoints = 0;

                if (feedCounterRow != null)
                {
                    numberOfComments = feedCounterRow.GetValue<Nullable<long>>("comment") == null ? 0 : (int)feedCounterRow.GetValue<long>("comment");
                    currentPositivePoints = feedCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)feedCounterRow.GetValue<long>("positive_point");
                    currentNegativePoints = feedCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)feedCounterRow.GetValue<long>("negative_point");
                }

                PreparedStatement psFeedPoint = session.Prepare(CQLGenerator.SelectStatement("feed_point",
                    new List<string> { "value" }, new List<string> { "feed_id", "user_id" }));
                BoundStatement bsFeedPoint = psFeedPoint.Bind(feedId, userId);

                Row feedPointRow = session.Execute(bsFeedPoint).FirstOrDefault();

                if (feedPointRow != null)
                {
                    int value = feedPointRow.GetValue<int>("value");
                    int currentVote = 1;

                    if (!isUpVote)
                    {
                        currentVote = -1;
                    }


                    if (value != currentVote)
                    {
                        if (isUpVote && currentNegativePoints - 1 >= 0 || !isUpVote && currentPositivePoints - 1 >= 0)
                        {
                            // Add 1
                            psFeedCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                                new List<string> { "feed_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                            bsFeedCounter = psFeedCounter.Bind(feedId);
                            session.Execute(bsFeedCounter);

                            // Remove 1
                            psFeedCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                                new List<string> { "feed_id" }, new List<string> { isUpVote ? CassandraColumn.FeedPointNegative : CassandraColumn.FeedPointPositive }, new List<int> { -1 }));
                            bsFeedCounter = psFeedCounter.Bind(feedId);
                            session.Execute(bsFeedCounter);


                            psFeedPoint = session.Prepare(CQLGenerator.UpdateStatement("feed_point",
                                new List<string> { "feed_id", "user_id" }, new List<string> { "value", "voted_timestamp" }, new List<string>()));
                            bsFeedPoint = psFeedPoint.Bind(isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow), feedId, userId);
                            session.Execute(bsFeedPoint);

                            result = isUpVote ? (int)VoteStatus.Upvote : (int)VoteStatus.Downvote;
                        }
                        else
                        {
                            Log.Error(string.Format("FeedId: {0} has negative points", feedId));
                        }

                    }
                    // Reset
                    else
                    {
                        // Remove 1
                        psFeedCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                            new List<string> { "feed_id" }, new List<string> { pointColumnName }, new List<int> { -1 }));
                        bsFeedCounter = psFeedCounter.Bind(feedId);
                        session.Execute(bsFeedCounter);

                        psFeedPoint = session.Prepare(CQLGenerator.DeleteStatement("feed_point",
                            new List<string> { "feed_id", "user_id" }));
                        bsFeedPoint = psFeedPoint.Bind(feedId, userId);
                        session.Execute(bsFeedPoint);

                        result = (int)VoteStatus.Reset;

                    }
                }
                else
                {
                    psFeedCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                        new List<string> { "feed_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                    bsFeedCounter = psFeedCounter.Bind(feedId);
                    session.Execute(bsFeedCounter);

                    psFeedPoint = session.Prepare(CQLGenerator.InsertStatement("feed_point",
                        new List<string> { "feed_id", "user_id", "value", "voted_timestamp" }));
                    bsFeedPoint = psFeedPoint.Bind(feedId, userId, isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow));
                    session.Execute(bsFeedPoint);

                    result = isUpVote ? (int)VoteStatus.Upvote : (int)VoteStatus.Downvote;
                }


            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return result;
        }


        public int UpdateCommentPointCounter(string userId, string feedId, string commentId, bool isUpVote, ISession session)
        {
            int result = (int)VoteStatus.Reset;

            try
            {
                string pointColumnName = string.Empty;

                if (isUpVote)
                {
                    pointColumnName = CassandraColumn.FeedPointPositive;
                }
                else
                {
                    pointColumnName = CassandraColumn.FeedPointNegative;
                }

                PreparedStatement psCommentCounter = session.Prepare(CQLGenerator.SelectStatement("comment_counter",
                   new List<string>(), new List<string> { "feed_id", "comment_id" }));
                BoundStatement bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                Row commentCounterRow = session.Execute(bsCommentCounter).FirstOrDefault();

                int numberOfReplies = 0;
                int currentPositivePoints = 0;
                int currentNegativePoints = 0;

                if (commentCounterRow != null)
                {
                    numberOfReplies = commentCounterRow.GetValue<Nullable<long>>("reply") == null ? 0 : (int)commentCounterRow.GetValue<long>("reply");
                    currentPositivePoints = commentCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)commentCounterRow.GetValue<long>("positive_point");
                    currentNegativePoints = commentCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)commentCounterRow.GetValue<long>("negative_point");
                }

                PreparedStatement psCommentPoint = session.Prepare(CQLGenerator.SelectStatement("comment_point",
                    new List<string> { "value" }, new List<string> { "comment_id", "user_id" }));
                BoundStatement bsCommentPoint = psCommentPoint.Bind(commentId, userId);

                Row commentPointRow = session.Execute(bsCommentPoint).FirstOrDefault();

                if (commentPointRow != null)
                {
                    int value = commentPointRow.GetValue<int>("value");
                    int currentVote = 1;

                    if (!isUpVote)
                    {
                        currentVote = -1;
                    }

                    if (value != currentVote)
                    {
                        if (isUpVote && currentNegativePoints - 1 >= 0 || !isUpVote && currentPositivePoints - 1 >= 0)
                        {
                            // Add 1
                            psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                                new List<string> { "feed_id", "comment_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                            bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                            session.Execute(bsCommentCounter);

                            psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                                new List<string> { "feed_id", "comment_id" }, new List<string> { isUpVote ? CassandraColumn.FeedPointNegative : CassandraColumn.FeedPointPositive }, new List<int> { -1 }));
                            bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                            session.Execute(bsCommentCounter);

                            psCommentPoint = session.Prepare(CQLGenerator.UpdateStatement("comment_point",
                                new List<string> { "comment_id", "user_id" }, new List<string> { "value", "voted_timestamp" }, new List<string>()));
                            bsCommentPoint = psCommentPoint.Bind(isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow), commentId, userId);
                            session.Execute(bsCommentPoint);

                            result = isUpVote ? (int)VoteStatus.Upvote : (int)VoteStatus.Downvote;
                        }
                        else
                        {
                            Log.Error(string.Format("FeedId: {0} and CommentId: {1} has negative points", feedId, commentId));
                        }
                    }
                    // Reset
                    else
                    {
                        // Remove 1
                        psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                            new List<string> { "feed_id", "comment_id" }, new List<string> { pointColumnName }, new List<int> { -1 }));
                        bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                        session.Execute(bsCommentCounter);

                        psCommentPoint = session.Prepare(CQLGenerator.DeleteStatement("comment_point",
                            new List<string> { "comment_id", "user_id" }));
                        bsCommentPoint = psCommentPoint.Bind(commentId, userId);
                        session.Execute(bsCommentPoint);

                        result = (int)VoteStatus.Reset;

                    }
                }
                else
                {
                    psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                        new List<string> { "feed_id", "comment_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                    bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                    session.Execute(bsCommentCounter);

                    psCommentPoint = session.Prepare(CQLGenerator.InsertStatement("comment_point",
                        new List<string> { "comment_id", "user_id", "value", "voted_timestamp" }));
                    bsCommentPoint = psCommentPoint.Bind(commentId, userId, isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow));
                    session.Execute(bsCommentPoint);

                    result = isUpVote ? (int)VoteStatus.Upvote : (int)VoteStatus.Downvote;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return result;
        }

        public int UpdateReplyPointCounter(string userId, string commentId, string replyId, bool isUpVote, ISession session)
        {
            int result = (int)VoteStatus.Reset;

            try
            {
                string pointColumnName = string.Empty;

                if (isUpVote)
                {
                    pointColumnName = CassandraColumn.FeedPointPositive;
                }
                else
                {
                    pointColumnName = CassandraColumn.FeedPointNegative;
                }

                PreparedStatement psReplyCounter = session.Prepare(CQLGenerator.SelectStatement("reply_counter",
                   new List<string>(), new List<string> { "comment_id", "reply_id" }));
                BoundStatement bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                Row replyCounterRow = session.Execute(bsReplyCounter).FirstOrDefault();

                int currentPositivePoints = 0;
                int currentNegativePoints = 0;

                if (replyCounterRow != null)
                {
                    currentPositivePoints = replyCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)replyCounterRow.GetValue<long>("positive_point");
                    currentNegativePoints = replyCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)replyCounterRow.GetValue<long>("negative_point");
                }

                PreparedStatement psReplyPoint = session.Prepare(CQLGenerator.SelectStatement("reply_point",
                    new List<string> { "value" }, new List<string> { "reply_id", "user_id" }));
                BoundStatement bsReplyPoint = psReplyPoint.Bind(replyId, userId);

                Row replyPointRow = session.Execute(bsReplyPoint).FirstOrDefault();

                if (replyPointRow != null)
                {
                    int value = replyPointRow.GetValue<int>("value");
                    int currentVote = 1;

                    if (!isUpVote)
                    {
                        currentVote = -1;
                    }

                    if (value != currentVote)
                    {
                        if (isUpVote && currentNegativePoints - 1 >= 0 || !isUpVote && currentPositivePoints - 1 >= 0)
                        {
                            psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter",
                                new List<string> { "comment_id", "reply_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                            bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                            session.Execute(bsReplyCounter);

                            psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter",
                                new List<string> { "comment_id", "reply_id" }, new List<string> { isUpVote ? CassandraColumn.FeedPointNegative : CassandraColumn.FeedPointPositive }, new List<int> { -1 }));
                            bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                            session.Execute(bsReplyCounter);

                            psReplyPoint = session.Prepare(CQLGenerator.UpdateStatement("reply_point",
                                new List<string> { "reply_id", "user_id" }, new List<string> { "value", "voted_timestamp" }, new List<string>()));
                            bsReplyPoint = psReplyPoint.Bind(isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow), replyId, userId);
                            session.Execute(bsReplyPoint);

                            result = isUpVote ? (int)VoteStatus.Upvote : (int)VoteStatus.Downvote;
                        }
                        else
                        {
                            Log.Error(string.Format("CommentId: {0} and ReplyId: {1} has negative points", commentId, replyId));
                        }
                    }
                    // Reset
                    else
                    {
                        // Remove 1
                        psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter",
                            new List<string> { "comment_id", "reply_id" }, new List<string> { pointColumnName }, new List<int> { -1 }));
                        bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                        session.Execute(bsReplyCounter);

                        psReplyPoint = session.Prepare(CQLGenerator.DeleteStatement("reply_point",
                            new List<string> { "reply_id", "user_id" }));
                        bsReplyPoint = psReplyPoint.Bind(replyId, userId);
                        session.Execute(bsReplyPoint);

                        result = (int)VoteStatus.Reset;

                    }
                }
                else
                {
                    psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter",
                        new List<string> { "comment_id", "reply_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                    bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                    session.Execute(bsReplyCounter);

                    psReplyPoint = session.Prepare(CQLGenerator.InsertStatement("reply_point",
                        new List<string> { "reply_id", "user_id", "value", "voted_timestamp" }));
                    bsReplyPoint = psReplyPoint.Bind(replyId, userId, isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow));
                    session.Execute(bsReplyPoint);

                    result = isUpVote ? (int)VoteStatus.Upvote : (int)VoteStatus.Downvote;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return result;
        }

        public FeedUpdateResponse UpdateToFeedTextPost(string ownerUserId,
                                                       string companyId,
                                                       string currentFeedId,
                                                       string content,
                                                       List<string> targetedDepartmentIds,
                                                       List<string> targetedUserIds)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.TargetedUsers = new List<Dictionary<string, object>>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(ownerUserId, companyId, session);

                if (hasErrorAsUser != null)
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(content))
                {
                    Log.Error("Empty feed text post");
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextContent);
                    response.ErrorMessage = ErrorMessage.FeedInvalidTextContent;
                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(currentFeedId, companyId, session);

                if (feedRow == null)
                {
                    Log.Error("Invalid feed post: " + currentFeedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!feedRow.GetValue<string>("owner_user_id").Equals(ownerUserId))
                {
                    Log.Error(string.Format("Invalid owner: {0} for feed post: {1}", ownerUserId, currentFeedId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }

                int feedType = feedRow.GetValue<int>("feed_type");
                DateTime createdTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");
                DateTimeOffset createdTimeOffset = feedRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                PreparedStatement ps = null;

                if (feedType != Convert.ToInt16(FeedTypeCode.TextPost))
                {
                    // Delete from relevant table
                    if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("feed_image",
                            new List<string> { "id" }));
                        deleteBatch.Add(ps.Bind(currentFeedId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("feed_image_url",
                            new List<string> { "feed_id" }));
                        deleteBatch.Add(ps.Bind(currentFeedId));
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("feed_video",
                           new List<string> { "id" }));
                        deleteBatch.Add(ps.Bind(currentFeedId));
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("feed_shared_url",
                           new List<string> { "id" }));
                        deleteBatch.Add(ps.Bind(currentFeedId));
                    }

                    // Insert into text table
                    ps = session.Prepare(CQLGenerator.InsertStatement("feed_text",
                            new List<string> { "id", "content", "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }));
                    updateBatch.Add(ps.Bind(currentFeedId, content, true, 1, ownerUserId, createdTimeOffset));

                    // Delete from S3
                    String bucketName = "cocadre-" + companyId.ToLower();

                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        ListObjectsRequest listRequest = new ListObjectsRequest();
                        listRequest.BucketName = bucketName;
                        listRequest.Prefix = "feeds/" + currentFeedId;

                        ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                        foreach (S3Object imageObject in listResponse.S3Objects)
                        {
                            DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                            deleteRequest.BucketName = bucketName;
                            deleteRequest.Key = imageObject.Key;
                            s3Client.DeleteObject(deleteRequest);
                        }

                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        deleteFolderRequest.Key = "feeds/" + currentFeedId;
                        s3Client.DeleteObject(deleteFolderRequest);
                    }
                }
                else
                {
                    // Update text table
                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_text",
                        new List<string> { "id" }, new List<string> { "content", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(content, DateTime.UtcNow, currentFeedId));
                }

                // Update privacy
                Dictionary<string, List<string>> targetedUserNotificationDict = UpdateFeedPrivacy(ownerUserId,
                                                                                                  currentFeedId,
                                                                                                  companyId,
                                                                                                  Int32.Parse(FeedTypeCode.TextPost),
                                                                                                  content,
                                                                                                  targetedDepartmentIds,
                                                                                                  targetedUserIds,
                                                                                                  createdTimeOffset,
                                                                                                  session);

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                // Create notification for new targeted users
                if (targetedUserNotificationDict.ContainsKey("AddedUsers"))
                {
                    Notification notification = new Notification();

                    foreach (string userId in (List<string>)targetedUserNotificationDict["AddedUsers"])
                    {
                        string notificationText = notification.CreateFeedNotification(ownerUserId, companyId, userId, (int)Notification.NotificationFeedSubType.NewPost, Int32.Parse(FeedTypeCode.TextPost), currentFeedId, string.Empty, string.Empty, DateHelper.ConvertDateToLong(DateTime.UtcNow), session);

                        Dictionary<string, object> notificationDict = new Dictionary<string, object>();
                        notificationDict.Add("userId", userId);
                        notificationDict.Add("notificationText", notificationText);
                        notificationDict.Add("notificationNumber", notification.SelectNotificationNumberByUser(userId, companyId, session).NumberOfNotification);
                        response.TargetedUsers.Add(notificationDict);
                    }
                }
                
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToFeedImagePost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> updatedImageUrls,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.TargetedUsers = new List<Dictionary<string, object>>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(ownerUserId, companyId, session);

                if (hasErrorAsUser != null)
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(currentFeedId, companyId, session);

                if (feedRow == null)
                {
                    Log.Error("Invalid feed post: " + currentFeedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!feedRow.GetValue<string>("owner_user_id").Equals(ownerUserId))
                {
                    Log.Error(string.Format("Invalid owner: {0} for feed post: {1}", ownerUserId, currentFeedId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }

                int feedType = feedRow.GetValue<int>("feed_type");
                DateTime createdTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");
                DateTimeOffset createdTimeOffset = feedRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                PreparedStatement ps = null;

                // Edit only caption of image
                if (feedType == Convert.ToInt16(FeedTypeCode.ImagePost))
                {
                    // Update image table
                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_image",
                        new List<string> { "id" }, new List<string> { "caption", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(caption, DateTime.UtcNow, currentFeedId));

                    // Delete from S3
                    String bucketName = "cocadre-" + companyId.ToLower();

                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("feed_image_url",
                            new List<string>(), new List<string> { "feed_id" }));
                        RowSet imageRowset = session.Execute(ps.Bind(currentFeedId));

                        foreach(Row imageRow in imageRowset)
                        {
                            string url = imageRow.GetValue<string>("url");
                            string[] arrayUrl = url.Split('/');
                            string fileName = arrayUrl[arrayUrl.Count() - 1];
                            string fileExtension = fileName.Split('.')[1];
                            string actualFileName = fileName.Split('_')[0];

                            try
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = "feeds/" + currentFeedId + "/" + actualFileName + "_original." + fileExtension;
                                s3Client.DeleteObject(deleteRequest);

                                deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = "feeds/" + currentFeedId + "/" + actualFileName + "_small." + fileExtension;
                                s3Client.DeleteObject(deleteRequest);

                                deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = "feeds/" + currentFeedId + "/" + actualFileName + "_medium." + fileExtension;
                                s3Client.DeleteObject(deleteRequest);

                                deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = "feeds/" + currentFeedId + "/" + actualFileName + "_large." + fileExtension;
                                s3Client.DeleteObject(deleteRequest);
                            }
                            catch (AmazonS3Exception s3Exception)
                            {
                                Log.Error(s3Exception.Message, s3Exception.InnerException);
                            }
                        }
                    }
                   
                    // Update image url table
                    ps = session.Prepare(CQLGenerator.DeleteStatement("feed_image_url",
                        new List<string> { "feed_id" }));
                    deleteBatch.Add(ps.Bind(currentFeedId));

                    int order = 1;
                    foreach (string imageUrl in updatedImageUrls)
                    {
                        string imageId = UUIDGenerator.GenerateUniqueIDForFeedMedia();

                        ps = session.Prepare(CQLGenerator.InsertStatement("feed_image_url",
                            new List<string> { "id", "feed_id", "url", "in_order", "is_valid" }));
                        updateBatch.Add(ps.Bind(imageId, currentFeedId, imageUrl, order, true));

                        order += 1;
                    }

                    // Update privacy
                    Dictionary<string, List<string>> targetedUserNotificationDict = UpdateFeedPrivacy(ownerUserId,
                                                                                                      currentFeedId,
                                                                                                      companyId,
                                                                                                      Int32.Parse(FeedTypeCode.ImagePost),
                                                                                                      caption,
                                                                                                      targetedDepartmentIds,
                                                                                                      targetedUserIds,
                                                                                                      createdTimeOffset,
                                                                                                      session);


                    session.Execute(deleteBatch);
                    session.Execute(updateBatch);


                    // Create notification for new targeted users
                    if (targetedUserNotificationDict.ContainsKey("AddedUsers"))
                    {
                        Notification notification = new Notification();

                        foreach (string userId in (List<string>)targetedUserNotificationDict["AddedUsers"])
                        {
                            string notificationText = notification.CreateFeedNotification(ownerUserId, companyId, userId, (int)Notification.NotificationFeedSubType.NewPost, Int32.Parse(FeedTypeCode.ImagePost), currentFeedId, string.Empty, string.Empty, DateHelper.ConvertDateToLong(DateTime.UtcNow), session);

                            Dictionary<string, object> notificationDict = new Dictionary<string, object>();
                            notificationDict.Add("userId", userId);
                            notificationDict.Add("notificationText", notificationText);
                            notificationDict.Add("notificationNumber", notification.SelectNotificationNumberByUser(userId, companyId, session).NumberOfNotification);
                            response.TargetedUsers.Add(notificationDict);
                        }
                    }

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToFeedVideoPost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.TargetedUsers = new List<Dictionary<string, object>>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(ownerUserId, companyId, session);

                if (hasErrorAsUser != null)
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(currentFeedId, companyId, session);

                if (feedRow == null)
                {
                    Log.Error("Invalid feed post: " + currentFeedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!feedRow.GetValue<string>("owner_user_id").Equals(ownerUserId))
                {
                    Log.Error(string.Format("Invalid owner: {0} for feed post: {1}", ownerUserId, currentFeedId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }

                int feedType = feedRow.GetValue<int>("feed_type");
                DateTime createdTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");
                DateTimeOffset createdTimeOffset = feedRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                PreparedStatement ps = null;

                // Edit only caption of video
                if(feedType == Convert.ToInt16(FeedTypeCode.VideoPost))
                {
                    // Update video table
                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_video",
                        new List<string> { "id" }, new List<string> { "caption", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(caption, DateTime.UtcNow, currentFeedId));

                    // Update privacy
                    Dictionary<string, List<string>> targetedUserNotificationDict = UpdateFeedPrivacy(ownerUserId,
                                                                                                      currentFeedId,
                                                                                                      companyId,
                                                                                                      Int32.Parse(FeedTypeCode.VideoPost),
                                                                                                      caption,
                                                                                                      targetedDepartmentIds,
                                                                                                      targetedUserIds,
                                                                                                      createdTimeOffset,
                                                                                                      session);

                    //session.Execute(deleteBatch);
                    session.Execute(updateBatch);

                    // Create notification for new targeted users
                    if (targetedUserNotificationDict.ContainsKey("AddedUsers"))
                    {
                        Notification notification = new Notification();

                        foreach (string userId in (List<string>)targetedUserNotificationDict["AddedUsers"])
                        {
                            string notificationText = notification.CreateFeedNotification(ownerUserId, companyId, userId, (int)Notification.NotificationFeedSubType.NewPost, Int32.Parse(FeedTypeCode.VideoPost), currentFeedId, string.Empty, string.Empty, DateHelper.ConvertDateToLong(DateTime.UtcNow), session);

                            Dictionary<string, object> notificationDict = new Dictionary<string, object>();
                            notificationDict.Add("userId", userId);
                            notificationDict.Add("notificationText", notificationText);
                            notificationDict.Add("notificationNumber", notification.SelectNotificationNumberByUser(userId, companyId, session).NumberOfNotification);
                            response.TargetedUsers.Add(notificationDict);
                        }
                    }

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToSharedUrlPost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.TargetedUsers = new List<Dictionary<string, object>>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(ownerUserId, companyId, session);

                if (hasErrorAsUser != null)
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(currentFeedId, companyId, session);

                if (feedRow == null)
                {
                    Log.Error("Invalid feed post: " + currentFeedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!feedRow.GetValue<string>("owner_user_id").Equals(ownerUserId))
                {
                    Log.Error(string.Format("Invalid owner: {0} for feed post: {1}", ownerUserId, currentFeedId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }

                int feedType = feedRow.GetValue<int>("feed_type");
                DateTime createdTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");
                DateTimeOffset createdTimeOffset = feedRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                PreparedStatement ps = null;

                // Edit only caption of url
                if (feedType == Convert.ToInt16(FeedTypeCode.SharedUrlPost))
                {
                    // Update url table
                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_shared_url",
                        new List<string> { "id" }, new List<string> { "caption", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(caption, DateTime.UtcNow, currentFeedId));

                    // Update privacy
                    Dictionary<string, List<string>> targetedUserNotificationDict = UpdateFeedPrivacy(ownerUserId,
                                                                                                      currentFeedId,
                                                                                                      companyId,
                                                                                                      Int32.Parse(FeedTypeCode.SharedUrlPost),
                                                                                                      caption,
                                                                                                      targetedDepartmentIds,
                                                                                                      targetedUserIds,
                                                                                                      createdTimeOffset,
                                                                                                      session);

                    //session.Execute(deleteBatch);
                    session.Execute(updateBatch);

                    // Create notification for new targeted users
                    if (targetedUserNotificationDict.ContainsKey("AddedUsers"))
                    {
                        Notification notification = new Notification();

                        foreach (string userId in (List<string>)targetedUserNotificationDict["AddedUsers"])
                        {
                            string notificationText = notification.CreateFeedNotification(ownerUserId, companyId, userId, (int)Notification.NotificationFeedSubType.NewPost, Int32.Parse(FeedTypeCode.SharedUrlPost), currentFeedId, string.Empty, string.Empty, DateHelper.ConvertDateToLong(DateTime.UtcNow), session);

                            Dictionary<string, object> notificationDict = new Dictionary<string, object>();
                            notificationDict.Add("userId", userId);
                            notificationDict.Add("notificationText", notificationText);
                            notificationDict.Add("notificationNumber", notification.SelectNotificationNumberByUser(userId, companyId, session).NumberOfNotification);
                            response.TargetedUsers.Add(notificationDict);
                        }
                    }

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }


    #region Feed type post
    public class FeedText
    {
        [DataMember(EmitDefaultValue = false)]
        public string Content { get; set; }
    }
    public class FeedImage
    {
        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Image> Images { get; set; }
    }
    public class Image
    {
        [DataMember(EmitDefaultValue = false)]
        public string ImageId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Url { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Order { get; set; }
    }
    public class FeedVideo
    {
        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VideoUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VideoThumbnailUrl { get; set; }

    }
    public class FeedSharedUrl
    {
        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Url { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SiteName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ImageUrl { get; set; }
    }
    #endregion

    #region Comment
    public class Comment
    {
        [DataMember(EmitDefaultValue = false)]
        public string FeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CommentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CommentType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public CommentText CommentText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Reply Reply { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Reply> Replies { get; set; }

        [DataMember]
        public int NegativePoints { get; set; }

        [DataMember]
        public int PositivePoints { get; set; }

        [DataMember]
        public int NumberOfReplies { get; set; }

        [DataMember]
        public bool HasVoted { get; set; }

        [DataMember]
        public bool IsUpVoted { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User User { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReportPostId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReportState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ReportedPost> ReportedCommentPosts { get; set; }
    }

    public class CommentText
    {
        [DataMember(EmitDefaultValue = false)]
        public string Content { get; set; }
    }
    #endregion

    #region Reply
    public class Reply
    {
        [DataMember(EmitDefaultValue = false)]
        public string FeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CommentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReplyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReplyType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ReplyText ReplyText { get; set; }

        [DataMember]
        public int NegativePoints { get; set; }

        [DataMember]
        public int PositivePoints { get; set; }

        [DataMember]
        public bool HasVoted { get; set; }

        [DataMember]
        public bool IsUpVoted { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User User { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReportPostId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReportState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ReportedPost> ReportedReplyPosts { get; set; }
    }

    public class ReplyText
    {
        [DataMember(EmitDefaultValue = false)]
        public string Content { get; set; }
    }
    #endregion

    #region ReportedPost
    public class ReportedPost
    {
        [DataMember(EmitDefaultValue = false)]
        public string ReportId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReportState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Reason { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset ReportedTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User ReportedUser { get; set; }
    }
    #endregion

}