﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;

namespace CassandraService.Entity
{
    public class UpdateScript
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public UpdateScriptResponse InsertValidStatusColumnToFeed(string adminUserId, string companyId)
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement ps = null;
                BoundStatement bs = null;

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Feed Privacy
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedCompanyId = row.GetValue<string>("company_id");
                    string feedId = row.GetValue<string>("feed_id");
                    bool isValid = row.GetValue<bool>("is_feed_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy", new List<string> { "company_id", "feed_id" }, new List<string> { "feed_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedCompanyId, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Privacy Desc
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy_by_company_timestamp_desc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedCompanyId = row.GetValue<string>("company_id");
                    string feedId = row.GetValue<string>("feed_id");
                    bool isValid = row.GetValue<bool>("is_feed_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc", new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "feed_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedCompanyId, createdTimestamp, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Privacy Asc
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy_by_company_timestamp_asc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedCompanyId = row.GetValue<string>("company_id");
                    string feedId = row.GetValue<string>("feed_id");
                    bool isValid = row.GetValue<bool>("is_feed_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc", new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "feed_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedCompanyId, createdTimestamp, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Text
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_text", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_text", new List<string> { "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Image
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_image", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_image", new List<string> { "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Video
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_video", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_video", new List<string> { "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Shared Url
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_shared_url", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_shared_url", new List<string> { "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Comment
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_comment_text", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_comment_text", new List<string> { "feed_id", "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId, commentId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Comment by feed
                ps = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("comment_id");
                    bool isValid = row.GetValue<bool>("is_comment_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed", new List<string> { "feed_id", "comment_id" }, new List<string> { "comment_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId, commentId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Comment by feed asc
                ps = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp_asc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("comment_id");
                    bool isValid = row.GetValue<bool>("is_comment_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_asc", new List<string> { "feed_id", "created_on_timestamp", "comment_id" }, new List<string> { "comment_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId, createdTimestamp, commentId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Comment by feed desc
                ps = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp_desc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("comment_id");
                    bool isValid = row.GetValue<bool>("is_comment_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_desc", new List<string> { "feed_id", "created_on_timestamp", "comment_id" }, new List<string> { "comment_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId, createdTimestamp, commentId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Reply
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_reply_text", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_reply_text", new List<string> { "comment_id", "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, commentId, replyId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Reply by comment
                ps = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("reply_id");
                    bool isValid = row.GetValue<bool>("is_reply_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment", new List<string> { "comment_id", "reply_id" }, new List<string> { "reply_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, commentId, replyId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Reply by comment desc
                ps = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment_timestamp_desc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("reply_id");
                    bool isValid = row.GetValue<bool>("is_reply_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_desc", new List<string> { "comment_id", "created_on_timestamp", "reply_id" }, new List<string> { "reply_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, commentId, createdTimestamp, replyId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Reply by comment asc
                ps = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment_timestamp_asc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("reply_id");
                    bool isValid = row.GetValue<bool>("is_reply_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_asc", new List<string> { "comment_id", "created_on_timestamp", "reply_id" }, new List<string> { "reply_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, commentId, createdTimestamp, replyId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse InsertNewColumnsAndTablesToChallenge()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement ps = null;
                BoundStatement bs = null;

                // Challenge Questions
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_question", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string topicId = row.GetValue<string>("topic_id");
                    string challengeId = row.GetValue<string>("id");

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("challenge_question", new List<string> { "topic_id", "id" }, new List<string> { "base_score", "scoring_calculation_type" }, new List<string>()));
                    bs = ps.Bind((Int32.Parse(DefaultResource.ChallengeBaseScore)), (int)ChallengeQuestion.ChallengeScoringCalculationType.HalfBaseWithTimeBonus, topicId, challengeId);
                    batchStatement.Add(bs);
                }

                mainSession.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Full challenge history
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("full_challenge_history", new List<string>(), new List<string>()));
                rowSet = mainSession.Execute(ps.Bind());

                ChallengeQuestion question = new ChallengeQuestion();
                foreach (Row row in rowSet)
                {
                    string companyId = row.GetValue<string>("company_id");
                    string challengeId = row.GetValue<string>("challenge_id");
                    int order = row.GetValue<int>("question_order");

                    string questionId = row.GetValue<string>("question_id");
                    string topicId = row.GetValue<string>("topic_id");
                    string topicCategoryId = row.GetValue<string>("topic_category_id");

                    ChallengeQuestion updatedQuestion = question.SelectQuestion(questionId, topicId, companyId, null, mainSession).Question;
                    if (updatedQuestion != null)
                    {
                        List<string> playerIds = row.GetValue<List<string>>("players_ids");

                        bool bonusQuestion = updatedQuestion.DifficultyLevel == (int)ChallengeQuestion.QuestionDifficultyLevel.Hard ? true : false;

                        string answerId = row.GetValue<string>("answer");
                        string initiatorUserAnswerId = row.GetValue<string>("answer_of_initiated_user");
                        string challengedUserAnswerId = row.GetValue<string>("answer_of_challenged_user");

                        int scoreOfInitiatorUser = 0;
                        int scoreOfChallengedUser = 0;
                        int scoreMultiplier = row.GetValue<int>("score_multiplier");

                        float timeAssigned = row.GetValue<float>("time_answering");
                        float timeTakenByInitiatedUser = row.GetValue<float?>("time_taken_by_initiated_user") == null ? 0 : row.GetValue<float>("time_taken_by_initiated_user");
                        float timeTakenByChallengedUser = row.GetValue<float?>("time_taken_by_challenged_user") == null ? 0 : row.GetValue<float>("time_taken_by_challenged_user");

                        BatchStatement bsExp = new BatchStatement();
                        PreparedStatement psExp = null;

                        if (row.GetValue<int?>("base_score") == null)
                        {
                            if (!string.IsNullOrEmpty(initiatorUserAnswerId) && answerId.Equals(initiatorUserAnswerId))
                            {
                                scoreOfInitiatorUser = question.CalculateScore(timeAssigned, timeTakenByInitiatedUser, bonusQuestion, scoreMultiplier);

                                string userId = playerIds[0];
                                string allocationId = UUIDGenerator.GenerateUniqueIDForExpAllocation();

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));
                            }

                            if (!string.IsNullOrEmpty(challengedUserAnswerId) && answerId.Equals(challengedUserAnswerId))
                            {
                                scoreOfChallengedUser = question.CalculateScore(timeAssigned, timeTakenByChallengedUser, bonusQuestion, scoreMultiplier);

                                string userId = playerIds[1];
                                string allocationId = UUIDGenerator.GenerateUniqueIDForExpAllocation();

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));
                            }

                            analyticSession.Execute(bsExp);
                            bsExp = new BatchStatement();
                        }

                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("full_challenge_history", new List<string> { "company_id", "challenge_id", "question_order" }, new List<string> { "base_score", "scoring_calculation_type", "score_of_initiated_user", "score_of_challenged_user", "answer_on_timestamp_by_initiated_user", "answer_on_timestamp_by_challenged_user" }, new List<string>()));
                        bs = ps.Bind((Int32.Parse(DefaultResource.ChallengeBaseScore)), (int)ChallengeQuestion.ChallengeScoringCalculationType.HalfBaseWithTimeBonus, scoreOfInitiatorUser, scoreOfChallengedUser, DateTime.UtcNow, DateTime.UtcNow, companyId, challengeId, order);
                        batchStatement.Add(bs);
                    }
                    else
                    {
                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("full_challenge_history", new List<string> { "company_id", "challenge_id", "question_order" }, new List<string> { "base_score", "scoring_calculation_type" }, new List<string>()));
                        bs = ps.Bind((Int32.Parse(DefaultResource.ChallengeBaseScore)), (int)ChallengeQuestion.ChallengeScoringCalculationType.HalfBaseWithTimeBonus, companyId, challengeId, order);
                        batchStatement.Add(bs);
                    }

                }

                mainSession.Execute(batchStatement);
                batchStatement = new BatchStatement();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse InsertTableChallengeByTopic()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();

                PreparedStatement ps = null;
                BoundStatement bs = null;

                // Challenge Questions
                ps = session.Prepare(CQLGenerator.SelectStatement("challenge_question_stats_by_user", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string userId = row.GetValue<string>("user_id");
                    string challengeId = row.GetValue<string>("challenge_id");
                    string topicId = row.GetValue<string>("topic_id");
                    string questionId = row.GetValue<string>("question_id");
                    bool isCorrect = row.GetValue<bool>("is_correct");
                    DateTimeOffset answeredTimestamp = row.GetValue<DateTimeOffset>("answered_timestamp");

                    ps = session.Prepare(CQLGenerator.InsertStatement("challenge_question_stats_by_topic",
                        new List<string> { "user_id", "challenge_id", "topic_id", "question_id", "is_correct", "answered_timestamp" }));
                    bs = ps.Bind(userId, challengeId, topicId, questionId, isCorrect, answeredTimestamp);
                    session.Execute(bs);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse CreateNewTablesForUserToken()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = null;

                ps = session.Prepare(CQLGenerator.SelectStatement("user_token", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string email = row.GetValue<string>("email");
                    string userToken = row.GetValue<string>("user_token");

                    ps = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "email" }));
                    rowSet = session.Execute(ps.Bind(email));

                    foreach (Row userRow in rowSet)
                    {
                        BatchStatement bs = new BatchStatement();

                        string userId = userRow.GetValue<string>("user_id");
                        string companyId = userRow.GetValue<string>("company_id");

                        ps = session.Prepare(CQLGenerator.InsertStatement("user_authentication_token",
                            new List<string> { "email", "user_token", "user_id", "company_id", "is_token_valid", "last_modified_timestamp" }));
                        bs.Add(ps.Bind(email, userToken, userId, companyId, false, DateTime.UtcNow));

                        ps = session.Prepare(CQLGenerator.InsertStatement("user_authentication_token_by_user",
                            new List<string> { "email", "user_token", "user_id", "company_id", "is_token_valid", "last_modified_timestamp" }));
                        bs.Add(ps.Bind(email, userToken, userId, companyId, false, DateTime.UtcNow));

                        session.Execute(bs);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse CreateNewTablesForNotification()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            string targetedUserId = string.Empty;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                ps = session.Prepare(CQLGenerator.SelectStatement("notification_privacy", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    batchStatement = new BatchStatement();

                    string notificationId = row.GetValue<string>("notification_id");
                    targetedUserId = row.GetValue<string>("targeted_user_id");
                    string challengeId = row.GetValue<string>("challenge_id");
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("reply_id");
                    int? feedType = row["feed_type"] == null ? null : row.GetValue<int?>("feed_type");
                    bool? isSeen = row.GetValue<bool?>("is_seen");
                    bool? isFetched = row.GetValue<bool?>("is_fetched");
                    int notificationSubType = row.GetValue<int>("notification_subtype");
                    int notificationType = row.GetValue<int>("notification_type");


                    if (row["notification_created_on_timestamp"] == null)
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("notification_privacy",
                            new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }));
                        batchStatement.Add(ps.Bind(challengeId, feedId, commentId, replyId, targetedUserId, notificationType, notificationSubType, notificationId));
                        session.Execute(batchStatement);
                        break;
                    }

                    DateTimeOffset createdOnTimestampOffset = row.GetValue<DateTimeOffset>("notification_created_on_timestamp");
                    DateTime createdOnTimestamp = row.GetValue<DateTime>("notification_created_on_timestamp");

                    if (isFetched == null)
                    {
                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy",
                                new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                        batchStatement.Add(ps.Bind(true, challengeId, feedId, commentId, replyId, targetedUserId, notificationType, notificationSubType, notificationId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_desc",
                           new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                        batchStatement.Add(ps.Bind(true, targetedUserId, createdOnTimestampOffset, notificationId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_asc",
                           new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                        batchStatement.Add(ps.Bind(true, targetedUserId, createdOnTimestampOffset, notificationId));

                        isFetched = true;
                    }

                    if (isSeen == null)
                    {
                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy",
                                new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batchStatement.Add(ps.Bind(false, challengeId, feedId, commentId, replyId, targetedUserId, notificationType, notificationSubType, notificationId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_desc",
                           new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batchStatement.Add(ps.Bind(false, targetedUserId, createdOnTimestampOffset, notificationId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_asc",
                           new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batchStatement.Add(ps.Bind(false, targetedUserId, createdOnTimestampOffset, notificationId));

                        isSeen = false;
                    }

                    ps = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_challenge",
                        new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestampOffset));

                    ps = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_feed",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestampOffset));

                    ps = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_comment",
                        new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestampOffset));

                    ps = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_reply",
                        new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestampOffset));

                    session.Execute(batchStatement);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                Log.Error(targetedUserId);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse CreateNewColumnsForUser()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string userId = row.GetValue<string>("id");
                    DateTimeOffset createdOnTimestamp = row.GetValue<DateTimeOffset>("created_on_timestamp");
                    ps = session.Prepare(CQLGenerator.UpdateStatement("user_basic",
                        new List<string> { "id" }, new List<string> { "invited_on_timestamp", "is_email_sent" }, new List<string>()));
                    batchStatement.Add(ps.Bind(createdOnTimestamp, true, userId));
                }

                session.Execute(batchStatement);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse CreateNewColumnsForCompany()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                ps = session.Prepare(CQLGenerator.SelectStatement("company", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string companyId = row.GetValue<string>("id");
                    string companyName = row.GetValue<string>("title");
                    string primaryAdminUserId = row.GetValue<string>("created_by_user_id");

                    string defaultPersonalInvitationDescription = string.Format(DefaultResource.CompanyEmailPersonnelInvitationDescription, companyName);
                    string defaultAdminInvitationDescription = string.Format(DefaultResource.CompanyEmailAdminInvitationDescription, companyName);

                    string clientBannerUrl = DefaultResource.CompanyClientBannerUrl;
                    if(row["client_banner_url"] != null)
                    {
                        clientBannerUrl = row.GetValue<string>("client_banner_url");
                    }

                    string profilePopupBanner = DefaultResource.CompanyProfilePopupUrl;
                    if (row["profile_popup_banner_url"] != null)
                    {
                        profilePopupBanner = row.GetValue<string>("profile_popup_banner_url");
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("company", new List<string> { "id" },
                        new List<string> { "primary_admin_user_id", "email_square_logo_url", "email_personnel_invitation_title", "email_personnel_invitation_description", "email_personnel_invitation_support_info", 
                        "email_admin_invitation_title", "email_admin_invitation_description", "email_admin_invitation_support_info", 
                        "email_forgot_password_title", "email_forgot_password_description", "email_forgot_password_support_info", 
                        "email_reset_password_title", "email_reset_password_description", "email_reset_password_support_info",
                        "admin_website_header_logo_url",
                        "client_banner_url", "client_pull_refresh_banner_url", "client_matchup_banner_url", "client_profile_popup_banner_url"}, new List<string>()));

                    batchStatement.Add(ps.Bind(primaryAdminUserId, DefaultResource.CompanyEmailSquareImageUrl, DefaultResource.CompanyEmailPersonnelInvitationTitle, defaultPersonalInvitationDescription, DefaultResource.CompanyEmailPersonnelInvitationSupportInfo,
                        DefaultResource.CompanyEmailAdminInvitationTitle, defaultAdminInvitationDescription, DefaultResource.CompanyEmailAdminInvitationSupportInfo,
                        DefaultResource.CompanyEmailForgotPasswordTitle, DefaultResource.CompanyEmailForgotPasswordDescription, DefaultResource.CompanyEmailForgotPasswordSupportInfo,
                        DefaultResource.CompanyEmailResetPasswordTitle, DefaultResource.CompanyEmailResetPasswordDescription, DefaultResource.CompanyEmailResetPasswordSupportInfo,
                        DefaultResource.CompanyHeaderImageUrl,
                        clientBannerUrl, DefaultResource.CompanyClientPullRefreshImageUrl, DefaultResource.CompanyMatchupBannerUrl, profilePopupBanner, companyId));
                }

                session.Execute(batchStatement);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;

        }
    }

}