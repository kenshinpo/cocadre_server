﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.Error.Classes
{
    [DataContract]
    public class ErrorStatus
    {
        [DataMember(EmitDefaultValue = false)]
        public Boolean IsValid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ErrorCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ErrorMessage { get; set; }
    }
}