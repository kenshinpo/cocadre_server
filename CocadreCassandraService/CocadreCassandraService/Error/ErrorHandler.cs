﻿using CocadreCassandraService.App_GlobalResources;
using CocadreCassandraService.Error.Classes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.Error
{
    [DataContract]
    public class ErrorHandler
    {
        [DataMember(EmitDefaultValue = false)]
        public static ErrorStatus NoError
        {
            get { return new ErrorStatus { IsValid = true, ErrorCode = 0, ErrorMessage = String.Empty }; }
        }

        [DataMember(EmitDefaultValue = false)]
        public static ErrorStatus SystemError
        {
            get { return new ErrorStatus { IsValid = false, ErrorCode = Int16.Parse(ErrorCode.SystemError), ErrorMessage = ErrorCode.SystemError }; }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus DuplicatedEmail
        {
            get { return new ErrorStatus { IsValid = false, ErrorCode = Int16.Parse(ErrorCode.UserDuplicatedEmail), ErrorMessage = ErrorCode.UserDuplicatedEmail }; }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus MismatchEmailOrPassword
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.UserMismatchEmailOrPassword);
                error_status.ErrorMessage = ErrorMessage.UserMismatchEmailOrPassword;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus EmptyEmailOrPassword
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.UserEmptyEmailOrPassword);
                error_status.ErrorMessage = ErrorMessage.UserEmptyEmailOrPassword;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public static ErrorStatus InvalidUserAccount
        {
            get { return new ErrorStatus { IsValid = false, ErrorCode = Int16.Parse(ErrorCode.UserInvalid), ErrorMessage = ErrorCode.UserInvalid }; }
        }

        [DataMember(EmitDefaultValue = false)]
        public static ErrorStatus InvalidCompany
        {
            get { return new ErrorStatus { IsValid = false, ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid), ErrorMessage = ErrorCode.CompanyInvalid }; }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus InvalidDepartment
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.DepartmentIsInvalid);
                error_status.ErrorMessage = ErrorMessage.DepartmentIsInvalid;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus InvalidProfileImageBasedString
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.UserInvalidProfileImageBasedString);
                error_status.ErrorMessage = ErrorMessage.UserInvalidProfileImageBasedString;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus InvalidChallenge
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.ChallengeInvalid);
                error_status.ErrorMessage = ErrorMessage.ChallengeInvalid;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus PlayerNotFoundForChallenge
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.ChallengeInvalidPlayer);
                error_status.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public static ErrorStatus AccountIsNotAdmin
        {
            get { return new ErrorStatus { IsValid = false, ErrorCode = Int16.Parse(ErrorCode.UserIsNotAdmin), ErrorMessage = ErrorMessage.UserIsNotAdmin }; }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus TopicTitleIsEmpty
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.TopicMissingTitle);
                error_status.ErrorMessage = ErrorMessage.TopicMissingTitle;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus TopicCategoryTitleIsEmpty
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.CategoryMissingTitle);
                error_status.ErrorMessage = ErrorCode.CategoryMissingTitle;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus  TopicLessSelectedQuestions
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.TopicLessActiveQuestions);
                error_status.ErrorMessage = ErrorMessage.TopicLessActiveQuestions;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus TopicInsufficientActiveQuestions
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.TopicLessActiveQuestions);
                error_status.ErrorMessage = ErrorMessage.TopicLessActiveQuestions;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus TopicCategoryNotTally
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                error_status.ErrorMessage = ErrorMessage.CategoryInvalid;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus InvalidTopicCategory
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                error_status.ErrorMessage = ErrorMessage.CategoryInvalid;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus InvalidTopic
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                error_status.ErrorMessage = ErrorMessage.TopicInvalid;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus TopicQuestionIsEmpty
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.TopicQuestionMissing);
                error_status.ErrorMessage = ErrorMessage.TopicQuestionMissing;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus TopicQuestionChoiceIsEmpty
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.TopicChoiceMissing);
                error_status.ErrorMessage = ErrorMessage.TopicChoiceMissing;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus InvalidTopicRound
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.ChallengeInvalidRound);
                error_status.ErrorMessage = ErrorMessage.ChallengeInvalidRound;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus TopicDeleteFailedAsQuestionStillExists
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.TopicQuestionStillExists);
                error_status.ErrorMessage = ErrorMessage.TopicQuestionStillExists;
                return error_status;
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public ErrorStatus FeedPermissionNotGranted
        {
            get
            {
                ErrorStatus error_status = new ErrorStatus();
                error_status.ErrorCode = Int16.Parse(ErrorCode.FeedPermissionNotGranted);
                error_status.ErrorMessage = ErrorMessage.FeedPermissionNotGranted;
                return error_status;
            }
        }
    }
}