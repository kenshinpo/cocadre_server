﻿using CocadreCassandraService.App_GlobalResources;
using System;
using System.Configuration;

namespace CocadreCassandraService.IDGenerator
{
    /// <summary> 
    /// Generate unique IDs for DB
    /// </summary> 
    public class UUIDGenerator
    {
        public static String GenerateNumberPasswordForUser()
        {
            Random random = new Random();
            string id = random.Next(10000, 99999).ToString();
            return id;
        }

        public static String GeneratePasswordForUser()
        {
            string id = Guid.NewGuid().ToString();
            return id.Replace("-", "").Substring(0, 6);
        }

        public static String GenerateUniqueIDForUser()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.User + id.Replace("-", "");
        }

        public static String GenerateUniqueIDForCompany()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Company + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForDepartment()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Department + id.Replace("-", "");
        }

        #region Feed
        public static string GenerateUniqueIDForFeedPrivacy()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedPrivacy + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeed()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Feed + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedComment()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedComment + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedCommentReply()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedCommentReply + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedMedia()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedMedia + id.Replace("-", "");
        }
        #endregion

        public static string GenerateUniqueIDForTopicCategory()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeTopicCategory + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForTopic()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeTopic + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForQuestion()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeQuestion + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForQuizChallenge()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Challenge + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForQuizChallengeChoice()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeQuestionChoice + id.Replace("-", "");
        }

        #region Log
        public static string GenerateUniqueIDForLog()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Log + id.Replace("-", "");
        }
        #endregion

        #region User Token
        public static string GenerateUniqueTokenForUser()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AuthenticationToken + id.Replace("-", "");
        }
        #endregion
    }
}
