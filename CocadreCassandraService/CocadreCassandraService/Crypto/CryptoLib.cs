﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace CocadreCassandraService.Crypto
{
    public class CryptoLib
    {
        static readonly char[] CharacterMatrixForRandomIVStringGeneration = {
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
			'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
			'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
		};

        public static string GenerateRandomSalt(int length)
        {
            char[] hash = new char[length];
            byte[] randomBytes = new byte[length];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(randomBytes);
            }

            for (int i = 0; i < hash.Length; i++)
            {
                int ptr = randomBytes[i] % CharacterMatrixForRandomIVStringGeneration.Length;
                hash[i] = CharacterMatrixForRandomIVStringGeneration[ptr];
            }

            return new string(hash);
        }

        public static string EncryptTextWithSalt (string plain_text, string random_salt)
        {
            // Convert plain text into a byte array.
            byte[] plain_text_byte = Encoding.UTF8.GetBytes(plain_text);

            // Convert salt into a byte array.
            byte[] random_salt_byte = Encoding.UTF8.GetBytes(random_salt);

            // Allocate array, which will hold plain text and salt.
            byte[] plain_text_with_salt_bytes = new byte[plain_text_byte.Length + random_salt_byte.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plain_text_byte.Length; i++)
            {
                plain_text_with_salt_bytes[i] = plain_text_byte[i];
            }

            // Append salt bytes to the resulting array.
            for (int i = 0; i < random_salt_byte.Length; i++)
            {
                plain_text_with_salt_bytes[plain_text_byte.Length + i] = random_salt_byte[i];
            }

            HashAlgorithm hash = new SHA256Managed();

            // Compute hash value of our plain text with appended salt.
            byte[] hash_bytes = hash.ComputeHash(plain_text_with_salt_bytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hash_with_salt_bytes = new byte[hash_bytes.Length + random_salt_byte.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hash_bytes.Length; i++)
            {
                hash_with_salt_bytes[i] = hash_bytes[i];
            }

            // Append salt bytes to the result.
            for (int i = 0; i < random_salt_byte.Length; i++)
            {
                hash_with_salt_bytes[hash_bytes.Length + i] = random_salt_byte[i];
            }
  
            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hash_with_salt_bytes);

            // Return the result.
            return hashValue;
        }
    }
}