﻿using Cassandra;
using log4net;
using System;
using System.Configuration;
using System.IO;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Web.Hosting;

namespace CocadreCassandraService.Connection
{
    public class ConnectionManager
    {
        private Cluster mainCluster = null;
        private Cluster authCluster = null;

        private ISession mainSession = null;
        private ISession analyticSession = null;

        private static readonly string serverName = ConfigurationManager.AppSettings["cassandra_server_name"];
        private static readonly string mainKeyspace = ConfigurationManager.AppSettings["cassandra_main_keyspace"];
        private static readonly string analyticKeyspace = ConfigurationManager.AppSettings["cassandra_analytic_keyspace"];
        private static readonly string instaServerName = ConfigurationManager.AppSettings["cassandra_server_insta_name"];
        private static readonly string oneNetServerName = ConfigurationManager.AppSettings["cassandra_server_1net_name"];
        private static readonly string devStagingServerName = ConfigurationManager.AppSettings["cassandra_server_dev_staging_name"];

        private static readonly string serverIp = ConfigurationManager.AppSettings["cassandra_server_ip"];
        private static readonly string username = ConfigurationManager.AppSettings["cassandra_username"];
        private static readonly string password = ConfigurationManager.AppSettings["cassandra_password"];

        private static readonly string certFileName = ConfigurationManager.AppSettings["cassandra_cert_filename"];
        private static readonly string certKey = ConfigurationManager.AppSettings["cassandra_cert_key"];

        private static readonly ILog Log = LogManager.GetLogger(typeof(ConnectionManager));


        public ISession getMainSession()
        {
            if (mainSession == null)
            {
                ConnectToKeyspace(serverName, mainKeyspace);
            }

            return mainSession;
        }

        public ISession getAnalyticSession()
        {
            if (analyticSession == null)
            {
                ConnectToKeyspace(serverName, analyticKeyspace);
            }

            return analyticSession;
        }

        private void ConnectToKeyspace(string serverName, string keyspace)
        {
            try
            {
                Cluster cluster;
                if(keyspace.Equals(mainKeyspace))
                {
                    cluster = mainCluster;
                }
                else
                {
                    cluster = authCluster;
                }

                string[] addresses = serverIp.Split(',');

                if (serverName.Equals(instaServerName))
                {
                    //Log.Debug("Connecting");

                    cluster = keyspace.Equals(mainKeyspace) ? mainCluster : authCluster;
                    string dataCenter = ConfigurationManager.AppSettings["cassandra_data_center"];

                    SSLOptions ssloptions = new SSLOptions(SslProtocols.Tls, true, new RemoteCertificateValidationCallback(ValidateServerCertificate));

                    cluster = Cluster.Builder()
                        .AddContactPoints(addresses)
                        .WithLoadBalancingPolicy(new DCAwareRoundRobinPolicy(dataCenter))
                        .WithPort(9042)
                        .WithSSL(ssloptions)
                        .WithAuthProvider(new PlainTextAuthProvider(username, password))
                        .Build();
                }
                else
                {
                    cluster = Cluster.Builder().WithCredentials(username, password).AddContactPoints(addresses).Build();
                }

                if (keyspace.Equals(mainKeyspace))
                {
                    mainSession = cluster.Connect(keyspace);
                }
                else
                {
                    analyticSession = cluster.Connect(keyspace);
                }
                
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }
        }

        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            try
            {
                string cert_path = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", certFileName);

                X509Certificate2 cert2 = new X509Certificate2(cert_path, certKey);

                X509Certificate2Collection xc = new X509Certificate2Collection(cert2);

                if (certificate.Equals(cert2))
                {
                    //logger.Debug("Cert is valid");
                    return true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return false;
        }

        public void Close()
        {
            if (mainSession != null)
            {
                mainSession.Dispose();
                mainCluster.Shutdown();
            }

            if (analyticSession != null)
            {
                analyticSession.Dispose();
                authCluster.Shutdown();
            }
        }
    }
}