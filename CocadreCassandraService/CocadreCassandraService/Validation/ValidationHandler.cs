﻿using Cassandra;
using CocadreCassandraService.App_GlobalResources;
using CocadreCassandraService.Connection;
using CocadreCassandraService.CQL;
using CocadreCassandraService.Entity;
using CocadreCassandraService.Error;
using CocadreCassandraService.Error.Classes;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Reflection;

namespace CocadreCassandraService.Validation
{
    public class ValidationHandler
    {
        public static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public ErrorStatus isValidatedAsAdmin(string adminUserId,
                                              string companyId,
                                              ISession session)
        {
            Row company_row = ValidateCompany(companyId, session);

            if (company_row != null)
            {
                Row user_row = ValidateUser(adminUserId, companyId, session);

                if (user_row != null)
                {
                    if (ValidateAdminAccountType(adminUserId, companyId, session))
                    {
                        return null;
                    }
                    else
                    {
                        return ErrorHandler.AccountIsNotAdmin;
                    }
                }
                else
                {
                    return ErrorHandler.InvalidUserAccount;
                }
            }

            return ErrorHandler.InvalidCompany;
        }

        public ErrorStatus isValidatedAsUser(string userId,
                                             string companyId,
                                            ISession session)
        {
            Row companyRow = ValidateCompany(companyId, session);

            if (companyRow != null)
            {
                Row userRow = ValidateUser(userId, companyId, session);

                if (userRow != null)
                {
                    return null;
                }
                else
                {
                    return ErrorHandler.InvalidUserAccount;
                }
            }

            return ErrorHandler.InvalidCompany;
        }

        public ErrorStatus isValidatedAsUserToPostFeed(string userId,
                                                       string companyId,
                                                       ISession session)
        {
            Row companyRow = ValidateCompany(companyId, session);

            if (companyRow != null)
            {
                Row userRow = ValidateUser(userId, companyId, session);

                if (userRow != null)
                {
                    PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("setting",
                                      new List<string> { "feed_post_user_type" }, new List<string> { "setting_key" }));
                    BoundStatement bs = ps.Bind(SettingKey.PostFeed);
                    Row settingForFeedPost = session.Execute(bs).FirstOrDefault();

                    if (settingForFeedPost != null)
                    {
                        int feedPostUserType = Int16.Parse(settingForFeedPost["feed_post_user_type"].ToString());
                        if (feedPostUserType == Int16.Parse(PermissionCode.FeedPermissionCodeAdminOnly) || feedPostUserType == Int16.Parse(PermissionCode.FeedPermissionCodeAdminAndModerator))
                        {
                            return null;
                        }
                        else
                        {
                            if (feedPostUserType == Int16.Parse(PermissionCode.FeedPermissionCodeAllPersonnel))
                            {
                                ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_user_suspension",
                                     new List<string>(), new List<string> { "user_id", "company_id" }));
                                bs = ps.Bind(userId, companyId);
                                Row suspensionRow = session.Execute(bs).FirstOrDefault();

                                if (suspensionRow != null)
                                {
                                    return new ErrorHandler().FeedPermissionNotGranted;
                                }

                                return null;
                            }
                            else
                            {
#warning Need check for all departments. Currently only check for the first department in the list.
                                string departmentId = ((List<string>)userRow["belongs_to_department_ids"])[0];

                                ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_department_allowed",
                                     new List<string>(), new List<string> { "department_id", "company_id" }));
                                bs = ps.Bind(departmentId, companyId);
                                Row suspensionRow = session.Execute(bs).FirstOrDefault();

                                if (suspensionRow == null)
                                {
                                    return new ErrorHandler().FeedPermissionNotGranted;
                                }

                                return null;
                            }
                        }
                    }

                    return ErrorHandler.SystemError;
                }
                else
                {
                    return ErrorHandler.InvalidUserAccount;
                }
            }

            return ErrorHandler.InvalidCompany;
        }

        public ErrorStatus isValidatedAsNewTopic(string topicTitle, int selectedNumberOfQuestions)
        {
            if (string.IsNullOrEmpty(topicTitle))
            {
                return new ErrorHandler().TopicTitleIsEmpty;
            }
            else if (selectedNumberOfQuestions < Int16.Parse(ConfigurationManager.AppSettings["minimum_selected_questions_setting"].ToString()))
            {
                return new ErrorHandler().TopicLessSelectedQuestions;
            }

            return null;
        }

        public ErrorStatus isValidatedAsNewTopicCategory(string topicCategoryTitle)
        {
            if (string.IsNullOrEmpty(topicCategoryTitle))
            {
                return new ErrorHandler().TopicCategoryTitleIsEmpty;
            }

            return null;
        }

        public ErrorStatus isValidatedAsNewQuestionContent(string questionContent)
        {
            if (string.IsNullOrEmpty(questionContent))
            {
                return new ErrorHandler().TopicQuestionIsEmpty;
            }

            return null;
        }

        public ErrorStatus isValidatedAsNewQuestionContent(string questionContentImageUrl, string questionContentMd5)
        {
            if (string.IsNullOrEmpty(questionContentImageUrl) || string.IsNullOrEmpty(questionContentMd5))
            {
                return new ErrorHandler().TopicQuestionIsEmpty;
            }

            return null;
        }

        public ErrorStatus isValidatedAsNewQuestionChoiceContent(string firstChoiceContent, string secondChoiceContent, string thirdChoiceContent, string fourthChoiceContent)
        {
            if (string.IsNullOrEmpty(firstChoiceContent) || string.IsNullOrEmpty(secondChoiceContent) || string.IsNullOrEmpty(thirdChoiceContent) || string.IsNullOrEmpty(fourthChoiceContent))
            {
                return new ErrorHandler().TopicQuestionChoiceIsEmpty;
            }

            return null;
        }

        public ErrorStatus isValidatedAsNewQuestionChoiceContent(string firstChoiceContentImageUrl, string firstChoiceContentMd5, string secondChoiceContentImageUrl, string secondChoiceContentMd5, string thirdChoiceContentImageUrl, string thirdChoiceContentMd5, string fourthChoiceContentImageUrl, string fourthChoiceContentMd5)
        {
            if ((string.IsNullOrEmpty(firstChoiceContentImageUrl) && string.IsNullOrEmpty(firstChoiceContentMd5)) || (string.IsNullOrEmpty(secondChoiceContentImageUrl) && string.IsNullOrEmpty(secondChoiceContentMd5)) || (string.IsNullOrEmpty(thirdChoiceContentImageUrl) && string.IsNullOrEmpty(thirdChoiceContentMd5)) || (string.IsNullOrEmpty(fourthChoiceContentImageUrl) && string.IsNullOrEmpty(fourthChoiceContentMd5)))
            {
                return new ErrorHandler().TopicQuestionChoiceIsEmpty;
            }

            return null;
        }

        public Row ValidateTopicCategory(string companyId,
                                         string topicCategoryId,
                                         ISession session)
        {
            Row categoryRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("topic_category",
                                      new List<string>(), new List<string> { "id", "company_id", "is_valid" }));
                BoundStatement bs = ps.Bind(topicCategoryId, companyId, true);
                categoryRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return categoryRow;
        }

        public Row ValidateTopic(string companyId,
                                 string topicCategoryId,
                                 string topicId,
                                 ISession session)
        {
            Row topicRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("topic",
                                     new List<string>(), new List<string> { "id", "category_id" }));
                BoundStatement bs = ps.Bind(topicId, topicCategoryId);
                topicRow = session.Execute(bs).FirstOrDefault();

                if (topicRow != null)
                {
                    int status = topicRow.GetValue<int>("status");
                    if (status == Topic.TopicStatus.CODE_DELETED)
                    {
                        topicRow = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return topicRow;
        }

        public Row ValidateTopicQuestion(string questionId,
                                         string topicId,
                                         ISession session)
        {
            Row questionRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("challenge_question",
                                     new List<string>(), new List<string> { "id", "topic_id" }));
                BoundStatement bs = ps.Bind(questionId, topicId);
                questionRow = session.Execute(bs).FirstOrDefault();

                if (questionRow != null)
                {
                    int status = questionRow.GetValue<int>("status");
                    if (status == ChallengeQuestion.QuestionStatus.CODE_DELETED)
                    {
                        questionRow = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return questionRow;
        }

        public Row ValidateChallenge(string challengeId,
                                     string companyId, 
                                     ISession session)
        {
            Row historyRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("challenge_history",
                                     new List<string>(), new List<string> { "company_id", "id", "is_valid" }));
                BoundStatement bs = ps.Bind(companyId, challengeId, true);
                historyRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return historyRow;
        }


        public Row ValidateCompany(string companyId, ISession session)
        {
            Row companyRow = null;

            try
            {
                if(!string.IsNullOrEmpty(companyId))
                {
                    PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("company",
                        new List<string>(), new List<string> { "id", "is_valid" }));
                    BoundStatement bs = ps.Bind(companyId, true);
                    companyRow = session.Execute(bs).FirstOrDefault();
                }
                else
                {
                    Log.Error("CompanyId is empty -> Cannot validate company");
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return companyRow;
        }

        public Row ValidateDepartment(string departmentId,
                                      string companyId,
                                      ISession session)
        {
            Row departmentRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("department",
                                      new List<string>(), new List<string> { "id", "company_id", "is_valid" }));
                BoundStatement bs = ps.Bind(departmentId, companyId, true);
                departmentRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return departmentRow;
        }

        public Row ValidateUser(string userId,
                                string companyId,
                                ISession session)
        {
            Row userRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string>(), new List<string> { "user_id", "account_status" }));
                BoundStatement bs = ps.Bind(userId, CocadreCassandraService.Entity.User.AccountStatus.CODE_ACTIVE);
                Row userAccountTypeRow = session.Execute(bs).FirstOrDefault();
                if (userAccountTypeRow != null)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string>(), new List<string> { "id" }));
                    bs = ps.Bind(userId);
                    userRow = session.Execute(bs).FirstOrDefault();
                    if (userRow != null)
                    {
                        return userRow;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return userRow;
        }

        public bool ValidateAdminAccountType(string userId,
                                             string companyId, ISession session)
        {
            try
            {

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type",
                       new List<string> { "account_type" }, new List<string> { "user_id" }));
                BoundStatement bs = ps.Bind(userId);
                Row admin_row = session.Execute(bs).FirstOrDefault();
                if (admin_row != null)
                {
                    if (admin_row.GetValue<int>("account_type") == CocadreCassandraService.Entity.User.AccountType.CODE_MODERATER)
                    {
#warning Check moderator access rights
                    }
                    return admin_row.GetValue<int>("account_type") >= CocadreCassandraService.Entity.User.AccountType.CODE_ADMIN ? true : false;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return false;
        }

        public bool ValidateFeedPost(string feedId,
                                     string companyId,
                                     ISession session)
        {
            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                       new List<string>(), new List<string> { "company_id", "feed_id" }));
                BoundStatement bs = ps.Bind(companyId, feedId);
                Row feedRow = session.Execute(bs).FirstOrDefault();

                if (feedRow != null)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return false;
        }

        public Row ValidateFeedComment(string feedId,
                                        string commentId, 
                                        ISession session)
        {
            Row commentByFeedRow = null;

            try
            {

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed",
                       new List<string>(), new List<string> { "comment_id", "feed_id", "is_comment_valid" }));
                BoundStatement bs = ps.Bind(commentId, feedId, true);
                commentByFeedRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return commentByFeedRow;
        }

        public Row ValidateCommentReply(string commentId,
                                         string replyId,
                                         ISession session)
        {
            Row replyByFeedRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment",
                       new List<string>(), new List<string> { "comment_id", "reply_id", "is_reply_valid" }));
                BoundStatement bs = ps.Bind(commentId, replyId, true);
                replyByFeedRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return replyByFeedRow;
        }
    }
}