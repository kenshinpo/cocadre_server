﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.Entity
{
    [DataContract]
    public class ChallengeChoice
    {
        private static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public string Id { get; set; }

        public string Content { get; set; }

        public string ContentImageUrl { get; set; }

        public string ContentImageMd5 { get; set; }
    }
}