﻿using Cassandra;
using CocadreCassandraService.App_GlobalResources;
using CocadreCassandraService.Connection;
using CocadreCassandraService.CQL;
using CocadreCassandraService.Date;
using CocadreCassandraService.Error.Classes;
using CocadreCassandraService.IDGenerator;
using CocadreCassandraService.ServiceResponses;
using CocadreCassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.Entity
{
    [DataContract]
    public class Topic
    {
        private static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [DataMember]
        public const int CODE_CHALLENGE = 1;
        [DataMember]
        public const int CODE_COURSEWARE = 2;

        [DataMember(EmitDefaultValue = false)]
        public string TopicId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TopicTitle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TopicLogoUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public TopicStatus Status { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TopicType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SelectedNumberOfQuestions { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalNumberOfQuestions { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TopicDescription { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public TopicCategory TopicCategory { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Department> TargetedDepartments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime LastModifiedTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ChallengeQuestion> Questions { get; set; }

        [DataContract]
        public class TopicStatus
        {
            public const int CODE_DELETED = -1;
            public const int CODE_UNLISTED = 1;
            public const int CODE_ACTIVE = 2;
            public const int CODE_HIDDEN = 3;

            [DataMember]
            public int Code { get; private set; }
            [DataMember]
            public String Title { get; private set; }

            public TopicStatus(int code)
            {
                switch (code)
                {
                    case CODE_DELETED:
                        Code = CODE_DELETED;
                        Title = "Deleted";
                        break;
                    case CODE_UNLISTED:
                        Code = CODE_UNLISTED;
                        Title = "Unlisted";
                        break;
                    case CODE_ACTIVE:
                        Code = CODE_ACTIVE;
                        Title = "Active";
                        break;
                    case CODE_HIDDEN:
                        Code = CODE_HIDDEN;
                        Title = "Hidden";
                        break;
                    default:
                        break;
                }
            }
        }

        [DataContract]
        public class DropdownNumberOfQuestions
        {
            [DataMember]
            public int Value { get; set; }
            [DataMember]
            public String DisplayText { get; set; }
        }

        public List<TopicStatus> SelectTopicStatusForDropdown()
        {
            List<TopicStatus> topicStatuses = new List<TopicStatus>();
            try
            {
                topicStatuses.Add(new TopicStatus(TopicStatus.CODE_ACTIVE));
                topicStatuses.Add(new TopicStatus(TopicStatus.CODE_UNLISTED));
                topicStatuses.Add(new TopicStatus(TopicStatus.CODE_HIDDEN));
                topicStatuses = topicStatuses.OrderBy(x => x.Code).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicStatuses;
        }

        public List<DropdownNumberOfQuestions> SelectNumberOfQuestionsForDropdown()
        {
            List<DropdownNumberOfQuestions> numbers = new List<DropdownNumberOfQuestions>();
            int minimum = Int16.Parse(ConfigurationManager.AppSettings["minimum_selected_questions_setting"].ToString());
            int maximum = Int16.Parse(ConfigurationManager.AppSettings["maximum_selected_questions_setting"].ToString());
            int recommended = Int16.Parse(ConfigurationManager.AppSettings["recommended_selected_questions_setting"].ToString());

            for (int index = minimum; index <= maximum; index++)
            {
                DropdownNumberOfQuestions number = new DropdownNumberOfQuestions();
                string displayText = index.ToString();
                if (index == recommended)
                {
                    displayText = string.Format("Recommended - {0} questions", index);
                }

                number.DisplayText = displayText;
                number.Value = index;

                numbers.Add(number);
                numbers = numbers.OrderBy(x => x.Value).ToList();
            }

            return numbers;
        }

        public TopicSelectIconResponse SelectAllTopicIcons(string adminUserId,
                                                           string companyId)
        {
            TopicSelectIconResponse response = new TopicSelectIconResponse();
            response.TopicIconUrls = new List<string>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement psTopicIcon = session.Prepare(CQLGenerator.SelectStatement("default_topic_icon", new List<string>(), new List<string>()));
                RowSet topicIconRowset = session.Execute(psTopicIcon.Bind());

                foreach (Row topicIconRow in topicIconRowset)
                {
                    response.TopicIconUrls.Add(topicIconRow.GetValue<string>("icon_url"));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public TopicCreateResponse CreateTopic(string adminUserId,
                                               string companyId,
                                               string topicTitle,
                                               string topicLogoUrl,
                                               string topicDescription,
                                               string categoryId,
                                               string categoryTitle,
                                               List<string> targetedDepartmentIds,
                                               int numberOfSelectedQuestions)
        {
            TopicCreateResponse response = new TopicCreateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                DateTime currentDate = DateTime.UtcNow;
                BatchStatement batchStatement = new BatchStatement();

                if (string.IsNullOrEmpty(categoryId))
                {
                    if (!string.IsNullOrEmpty(categoryTitle))
                    {
                        categoryId = UUIDGenerator.GenerateUniqueIDForTopicCategory();
                        PreparedStatement psCategory = session.Prepare(CQLGenerator.InsertStatement("topic_category",
                            new List<string> { "id", "title", "company_id", "is_valid", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                        batchStatement = batchStatement.Add(psCategory.Bind(categoryId, categoryTitle, companyId, true, adminUserId, DateHelper.ConvertDateToLong(currentDate), adminUserId, DateHelper.ConvertDateToLong(currentDate)));

                        PreparedStatement psCategoryByTitle = session.Prepare(CQLGenerator.InsertStatement("topic_category_by_company_title",
                            new List<string> { "topic_category_id", "topic_category_title", "company_id" }));
                        batchStatement = batchStatement.Add(psCategoryByTitle.Bind(categoryId, categoryTitle, companyId));
                    }
                    else
                    {
                        response.ErrorCode = Int16.Parse(ErrorCode.CategoryMissingTitle);
                        response.ErrorMessage = ErrorMessage.CategoryMissingTitle;
                        return response;
                    }
                }
                else
                {
                    Row categoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);
                    if (categoryRow == null)
                    {
                        response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                        response.ErrorMessage = ErrorMessage.CategoryInvalid;
                        return response;
                    }
                }

                string topicId = UUIDGenerator.GenerateUniqueIDForTopic();

                PreparedStatement psTopic = session.Prepare(CQLGenerator.InsertStatement("topic",
                        new List<string> { "id", "category_id", "title", "logo_url", "description", "type", "status", "selected_number_of_questions", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));

                PreparedStatement psTopicPrivacy = session.Prepare(CQLGenerator.InsertStatement("topic_privacy",
                    new List<string> { "topic_id", "company_id", "topic_type", "is_for_everyone", "is_for_department", "is_for_user", "is_for_custom_group" }));

                PreparedStatement psTopicByCategory = session.Prepare(CQLGenerator.InsertStatement("topic_by_category",
                    new List<string> { "topic_id", "topic_category_id" }));
                PreparedStatement psCategoryByTopic = session.Prepare(CQLGenerator.InsertStatement("category_by_topic",
                    new List<string> { "topic_id", "topic_category_id" }));

                batchStatement = batchStatement
                   .Add(psTopic.Bind(topicId, categoryId, topicTitle, topicLogoUrl, topicDescription, CODE_CHALLENGE, TopicStatus.CODE_UNLISTED, numberOfSelectedQuestions, adminUserId, DateHelper.ConvertDateToLong(currentDate), adminUserId, DateHelper.ConvertDateToLong(currentDate)))
                   .Add(psTopicPrivacy.Bind(topicId, companyId, CODE_CHALLENGE, false, true, false, false))
                   .Add(psTopicByCategory.Bind(topicId, categoryId))
                   .Add(psCategoryByTopic.Bind(topicId, categoryId));


                foreach (string departmentId in targetedDepartmentIds)
                {
                    Row departmentRow = vh.ValidateDepartment(departmentId, companyId, session);
                    if (departmentRow != null)
                    {
                        PreparedStatement psDepartment = session.Prepare(CQLGenerator.InsertStatement("topic_targeted_department",
                            new List<string> { "topic_id", "department_id" }));
                        batchStatement = batchStatement.Add(psDepartment.Bind(topicId, departmentId));

                        psDepartment = session.Prepare(CQLGenerator.InsertStatement("department_targeted_topic",
                            new List<string> { "topic_id", "department_id" }));
                        batchStatement = batchStatement.Add(psDepartment.Bind(topicId, departmentId));

                    }
                }

                session.Execute(batchStatement);
                response.TopicId = topicId;
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }



        public TopicSelectResponse SelectTopicBasic(string topicId,
                                                    string adminUserId,
                                                    string companyId,
                                                    string topicCategoryId,
                                                    TopicCategory topicCategory,
                                                    ISession session = null,
                                                    string startsWithName = null)
        {
            TopicSelectResponse response = new TopicSelectResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                if (topicCategory == null && !string.IsNullOrEmpty(topicCategoryId))
                {
                    topicCategory = new TopicCategory().SelectCategory(topicCategoryId, companyId, adminUserId, session).TopicCategory;
                }
                else if (topicCategory == null && string.IsNullOrEmpty(topicCategoryId))
                {
                    PreparedStatement psCategoryByTopic = session.Prepare(CQLGenerator.SelectStatement("category_by_topic",
                        new List<string> { "topic_category_id" }, new List<string> { "topic_id" }));
                    BoundStatement bsCategoryByTopic = psCategoryByTopic.Bind(topicId);
                    Row categoryByTopicRow = session.Execute(bsCategoryByTopic).FirstOrDefault();

                    if (categoryByTopicRow != null)
                    {
                        topicCategoryId = categoryByTopicRow.GetValue<string>("topic_category_id");
                        topicCategory = new TopicCategory().SelectCategory(topicCategoryId, companyId, adminUserId, session).TopicCategory;
                    }
                    else
                    {
                        Log.Error("Topic does not have a category: " + topicId);
                    }
                }

                PreparedStatement psTopic = null;
                BoundStatement bsTopic = null;

                if (topicCategory != null)
                {
                    psTopic = session.Prepare(CQLGenerator.SelectStatement("topic",
                    new List<string> { "title", "logo_url", "description", "type", "status", "selected_number_of_questions" }, new List<string> { "category_id", "id" }));
                    bsTopic = psTopic.Bind(topicCategoryId, topicId);
                    Row topicRow = session.Execute(bsTopic).FirstOrDefault();

                    if (topicRow != null)
                    {
                        int status = topicRow.GetValue<int>("status");
                        string topicTitle = topicRow.GetValue<string>("title");

                        bool isFound = true;

                        if (!string.IsNullOrEmpty(startsWithName))
                        {
                            isFound = topicTitle.ToLower().StartsWith(startsWithName.ToLower());
                        }

                        if (status > TopicStatus.CODE_DELETED && isFound)
                        {
                            string logoUrl = topicRow.GetValue<string>("logo_url");
                            string description = topicRow.GetValue<string>("description");
                            int selectedNumberOfQuestions = topicRow.GetValue<int>("selected_number_of_questions");
                            int totalNumberOfQuestions = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ALL_EXCEPT_DELETE, session);
                            response.Topic = new Topic
                            {
                                TopicId = topicId,
                                TopicLogoUrl = logoUrl,
                                TopicTitle = topicTitle,
                                TopicDescription = description,
                                Status = new TopicStatus(status),
                                TopicCategory = topicCategory,
                                TotalNumberOfQuestions = totalNumberOfQuestions,
                                SelectedNumberOfQuestions = selectedNumberOfQuestions
                            };
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public CategorySelectAllWithTopicResponse SelectAllTopicBasicByUserAndCategory(string requesterUserId,
                                                                                       string companyId)
        {
            CategorySelectAllWithTopicResponse response = new CategorySelectAllWithTopicResponse();
            response.TopicCategories = new List<TopicCategory>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                List<Department> departments = new Department().GetAllDepartmentByUserId(requesterUserId, companyId, session).Departments;

                // Fetch all valid topic category sorted by title
                PreparedStatement psCategoryByTitle = session.Prepare(CQLGenerator.SelectStatement("topic_category_by_company_title",
                            new List<string>(), new List<string> { "company_id" }));
                BoundStatement bsCategoryByTitle = psCategoryByTitle.Bind(companyId);
                RowSet categoryByTitleRowset = session.Execute(bsCategoryByTitle);

                foreach (Row categoryByTitleRow in categoryByTitleRowset)
                {
                    string categoryId = categoryByTitleRow.GetValue<string>("topic_category_id");
                    string categoryTitle = categoryByTitleRow.GetValue<string>("topic_category_title");

                    TopicCategory category = new TopicCategory
                    {
                        Id = categoryId,
                        Title = categoryTitle,
                        Topics = new List<Topic>()
                    };

                    // Fetch all valid topic in category sorted by timestamp
                    PreparedStatement psTopicByTimestamp = session.Prepare(CQLGenerator.SelectStatement("topic_by_company_category_timestamp",
                        new List<string>(), new List<string> { "company_id", "topic_category_id" }));
                    BoundStatement bsTopicByTimestamp = psTopicByTimestamp.Bind(companyId, categoryId);
                    RowSet topicByTimestampRowset = session.Execute(bsTopicByTimestamp);

                    foreach (Row topicByTimestampRow in topicByTimestampRowset)
                    {
                        string topicId = topicByTimestampRow.GetValue<string>("topic_id");

                        if (CheckTopicForCurrentUser(topicId, departments, session))
                        {
                            // Select topic
                            PreparedStatement psTopic = session.Prepare(CQLGenerator.SelectStatement("topic",
                                new List<string> { "title", "logo_url", "type", "status", "description" }, new List<string> { "category_id", "id" }));
                            BoundStatement bsTopic = psTopic.Bind(category.Id, topicId);
                            Row topicRow = session.Execute(bsTopic).FirstOrDefault();

                            if(topicRow != null)
                            {
                                int status = topicRow.GetValue<int>("status");

                                if (status == TopicStatus.CODE_ACTIVE)
                                {
                                    string topicDescription = topicRow.GetValue<string>("description");
                                    string topicTitle = topicRow.GetValue<string>("title");
                                    string logoUrl = topicRow.GetValue<string>("logo_url");
                                    category.Topics.Add(new Topic
                                    {
                                        TopicId = topicId,
                                        TopicTitle = topicTitle,
                                        TopicDescription = topicDescription,
                                        TopicLogoUrl = logoUrl
                                    });
                                }
                            }
                            else
                            {
                                Log.Error(string.Format("Topic table having error with -> topicId: {0}, categoryId: {1}", topicId, categoryId));
                            }
                           
                        }
                    }

                    if (category.Topics.Count > 0)
                    {
                        response.TopicCategories.Add(category);
                    }

                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public TopicSelectAllBasicResponse SelectAllTopicBasicByOpponent(string initiatorUserId,
                                                                         string challengedUserId,
                                                                         string companyId,
                                                                         string topicStartsWithName)
        {
            TopicSelectAllBasicResponse response = new TopicSelectAllBasicResponse();
            response.Topics = new List<Topic>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(initiatorUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                es = vh.isValidatedAsUser(challengedUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Department department = new Department();
                List<Department> initiatorDepartments = department.GetAllDepartmentByUserId(initiatorUserId, companyId, session).Departments;
                List<Department> challengedDepartments = department.GetAllDepartmentByUserId(challengedUserId, companyId, session).Departments;

                List<string> initiatorTopicIds = SelectTopicIdByDepartments(initiatorDepartments, session);
                List<string> challengedTopicIds = SelectTopicIdByDepartments(challengedDepartments, session);

                List<string> visibleTopicIds = initiatorTopicIds.Intersect(challengedTopicIds).ToList();

                foreach (string visibleTopicId in visibleTopicIds)
                {
                    Topic topicFound = SelectTopicBasic(visibleTopicId, null, companyId, null, null, session, topicStartsWithName).Topic;

                    if (topicFound != null && topicFound.Status.Code == (int)TopicStatus.CODE_ACTIVE)
                    {
                        response.Topics.Add(topicFound);
                    }

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        private List<string> SelectTopicIdByDepartments(List<Department> departments, ISession session)
        {
            List<string> topicIds = new List<string>();

            try
            {
                foreach (Department department in departments)
                {
                    string departmentId = department.Id;

                    // Fetch all valid topic by department
                    PreparedStatement psTopicByDepartment = session.Prepare(CQLGenerator.SelectStatement("department_targeted_topic",
                        new List<string>(), new List<string> { "department_id" }));
                    BoundStatement bsTopicByDepartment = psTopicByDepartment.Bind(departmentId);
                    RowSet topicByDepartmentRowset = session.Execute(bsTopicByDepartment);

                    foreach (Row topicByDepartmentRow in topicByDepartmentRowset)
                    {
                        string topicId = topicByDepartmentRow.GetValue<string>("topic_id");
                        topicIds.Add(topicId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicIds;
        }

        public TopicSelectResponse SelectTopicDetail(string topicId,
                                                     string adminUserId,
                                                     string companyId)
        {
            TopicSelectResponse response = new TopicSelectResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement psCategoryByTopic = session.Prepare(CQLGenerator.SelectStatement("category_by_topic",
                                       new List<string> { "topic_category_id" }, new List<string> { "topic_id" }));
                BoundStatement bsCategoryByTopic = psCategoryByTopic.Bind(topicId);
                Row categoryByTopicRow = session.Execute(bsCategoryByTopic).FirstOrDefault();

                string topicCategoryId = categoryByTopicRow.GetValue<string>("topic_category_id");
                TopicCategory topicCategory = new TopicCategory().SelectCategory(topicCategoryId, companyId, adminUserId, session).TopicCategory;

                PreparedStatement psTopic = session.Prepare(CQLGenerator.SelectStatement("topic",
                    new List<string> { "title", "logo_url", "description", "type", "selected_number_of_questions", "status" }, new List<string> { "category_id", "id" }));
                BoundStatement bsTopic = psTopic.Bind(topicCategoryId, topicId);
                Row topicRow = session.Execute(bsTopic).FirstOrDefault();

                if (topicRow != null)
                {
                    int status = topicRow.GetValue<int>("status");

                    if (status > TopicStatus.CODE_DELETED)
                    {
                        string topicTitle = topicRow.GetValue<string>("title");
                        string logoUrl = topicRow.GetValue<string>("logo_url");
                        string description = topicRow.GetValue<string>("description");
                        int selectedNumberOfQuestions = topicRow.GetValue<int>("selected_number_of_questions");
                        int totalNumberOfQuestions = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ALL_EXCEPT_DELETE, session);

                        PreparedStatement psDepartment = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_department",
                            new List<string> { "department_id" }, new List<string> { "topic_id" }));
                        BoundStatement bsDepartment = psDepartment.Bind(topicId);
                        RowSet departmentRowset = session.Execute(bsDepartment);

                        List<Department> departments = new Department().GetAllDepartment(adminUserId, companyId, Department.QUERY_TYPE_BASIC, session).Departments;

                        foreach (Row departmentRow in departmentRowset)
                        {
                            string departmentId = departmentRow.GetValue<string>("department_id");

                            Department targetedDepartment = departments.Where(department => department.Id.Equals(departmentId)).FirstOrDefault();

                            if (targetedDepartment != null)
                            {
                                targetedDepartment.IsTargetedForTopic = true;
                            }
                        }

                        response.Topic = new Topic
                        {
                            TopicId = topicId,
                            TopicLogoUrl = logoUrl,
                            TopicTitle = topicTitle,
                            TopicDescription = description,
                            Status = new TopicStatus(status),
                            TopicCategory = topicCategory,
                            TotalNumberOfQuestions = totalNumberOfQuestions,
                            SelectedNumberOfQuestions = selectedNumberOfQuestions,
                            TargetedDepartments = departments,
                            Questions = new ChallengeQuestion().SelectAllQuestionsForTopic(adminUserId, companyId, topicId, topicCategoryId, session).Questions
                        };

                        response.Success = true;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public TopicSelectAllBasicResponse SelectAllTopicBasicByCategoryAndDepartment(string adminUserId,
                                                                                      string companyId,
                                                                                      string selectedTopicCategoryId,
                                                                                      string selectedDepartmentId)
        {
            TopicSelectAllBasicResponse response = new TopicSelectAllBasicResponse();
            response.Topics = new List<Topic>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                List<string> filteredIdsForCategoryList = new List<string>();
                List<string> filteredIdsForDepartmentList = new List<string>();
                List<string> filteredIdsList = new List<string>();

                bool isFilterApplied = false;

                TopicCategory topicCategory = null;


                if (!string.IsNullOrEmpty(selectedDepartmentId))
                {
                    filteredIdsForDepartmentList = SelectTopicIdByDepartmentId(selectedDepartmentId, session);
                    isFilterApplied = true;
                }

                if (!string.IsNullOrEmpty(selectedTopicCategoryId))
                {
                    filteredIdsForCategoryList = SelectTopicIdByCategory(selectedTopicCategoryId, session);
                    isFilterApplied = true;
                    topicCategory = new TopicCategory().SelectCategory(selectedTopicCategoryId, companyId, adminUserId, session).TopicCategory;
                }

                if (filteredIdsForCategoryList.Count > 0 && filteredIdsForDepartmentList.Count == 0)
                {
                    filteredIdsList = filteredIdsForCategoryList;
                }
                else if (filteredIdsForDepartmentList.Count > 0 && filteredIdsForCategoryList.Count == 0)
                {
                    filteredIdsList = filteredIdsForDepartmentList;
                }
                else
                {
                    filteredIdsList = filteredIdsForCategoryList.Intersect(filteredIdsForDepartmentList).ToList();
                }

                // No filter is applied
                if (!isFilterApplied)
                {
                    foreach (TopicCategory category in new TopicCategory().SelectAllCategoriesForDropdown(adminUserId, companyId, session).TopicCategories)
                    {
                        PreparedStatement psTopic = session.Prepare(CQLGenerator.SelectStatement("topic",
                            new List<string> { "id", "title", "logo_url", "type", "status" }, new List<string> { "category_id" }));
                        BoundStatement bsTopic = psTopic.Bind(category.Id);
                        RowSet topicRowset = session.Execute(bsTopic);

                        foreach (Row topicRow in topicRowset)
                        {
                            int status = topicRow.GetValue<int>("status");
                            if (status > TopicStatus.CODE_DELETED)
                            {
                                string topicId = topicRow.GetValue<string>("id");
                                string topicTitle = topicRow.GetValue<string>("title");
                                string logoUrl = topicRow.GetValue<string>("logo_url");
                                int totalNumberOfQuestions = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ALL_EXCEPT_DELETE, session);

                                response.Topics.Add(new Topic
                                {
                                    TopicId = topicId,
                                    TopicLogoUrl = logoUrl,
                                    TopicTitle = topicTitle,
                                    Status = new TopicStatus(status),
                                    TopicCategory = category,
                                    TotalNumberOfQuestions = totalNumberOfQuestions
                                });
                            }

                        }
                    }
                }
                else
                {
                    foreach (string topicId in filteredIdsList)
                    {
                        response.Topics.Add(SelectTopicBasic(topicId, adminUserId, companyId, selectedTopicCategoryId, topicCategory, null).Topic);
                    }
                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public bool CheckTopicForCurrentUser(string topicId, List<Department> departments, ISession session)
        {
            try
            {
                foreach (Department department in departments)
                {
                    string departmentId = department.Id;

                    PreparedStatement psTopicByDepartment = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_department",
                        new List<string>(), new List<string> { "department_id", "topic_id" }));
                    BoundStatement bsTopicByDepartment = psTopicByDepartment.Bind(departmentId, topicId);
                    Row topicByDepartmentRow = session.Execute(bsTopicByDepartment).FirstOrDefault();

                    if (topicByDepartmentRow != null)
                    {
                        return true;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);

            }

            return false;
        }

        private List<Department> SelectDepartmentByTopicId(string topicId,
                                                           string companyId,
                                                           ISession session)
        {
            List<Department> departments = new List<Department>();

            try
            {
                PreparedStatement psDepartmentByTopic = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_department",
                                              new List<string> { "department_id" }, new List<string> { "topic_id" }));
                BoundStatement bsDepartmentByTopic = psDepartmentByTopic.Bind(topicId);
                RowSet topicByDepartmentRowset = session.Execute(bsDepartmentByTopic);

                foreach (Row topicByDepartmentRow in topicByDepartmentRowset)
                {
                    Department department = new Department();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return departments;
        }

        private List<string> SelectTopicIdByDepartmentId(string departmentId,
                                                        ISession session)
        {
            List<string> topicIds = new List<string>();

            try
            {
                PreparedStatement psTopicByDepartment = null;
                BoundStatement bsTopicByDepartment = null;

                psTopicByDepartment = session.Prepare(CQLGenerator.SelectStatement("department_targeted_topic",
                                               new List<string> { "topic_id" }, new List<string> { "department_id" }));
                bsTopicByDepartment = psTopicByDepartment.Bind(departmentId);
                RowSet topicByDepartmentRowset = session.Execute(bsTopicByDepartment);

                foreach (Row topicByDepartmentRow in topicByDepartmentRowset)
                {
                    topicIds.Add(topicByDepartmentRow.GetValue<string>("topic_id"));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicIds;
        }

        private List<string> SelectTopicIdByCategory(string categoryId,
                                                     ISession session)
        {
            List<string> topicIds = new List<string>();

            try
            {
                PreparedStatement psTopicByCategory = null;
                BoundStatement bsTopicByCategory = null;

                psTopicByCategory = session.Prepare(CQLGenerator.SelectStatement("topic_by_category",
                                              new List<string> { "topic_id" }, new List<string> { "topic_category_id" }));
                bsTopicByCategory = psTopicByCategory.Bind(categoryId);
                RowSet topicRowset = session.Execute(bsTopicByCategory);

                foreach (Row topicRow in topicRowset)
                {
                    topicIds.Add(topicRow.GetValue<string>("topic_id"));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicIds;
        }

        public TopicUpdateResponse UpdateTopicStatus(string adminUserId,
                                                     string companyId,
                                                     string topicId,
                                                     string categoryId,
                                                     int status,
                                                     ISession session = null,
                                                     Row topicRow = null)
        {
            TopicUpdateResponse response = new TopicUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (session == null)
                {
                    session = cm.getMainSession();
                }

                if (topicRow == null)
                {
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    // Check topic category
                    Row topicCategoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);

                    if (topicCategoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                        response.ErrorMessage = ErrorMessage.CategoryInvalid;
                        return response;
                    }

                    // Check topic row
                    topicRow = vh.ValidateTopic(companyId, categoryId, topicId, session);

                    if (topicRow == null)
                    {
                        Log.Error("Invalid topicId: " + topicId);
                        response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                        response.ErrorMessage = ErrorMessage.TopicInvalid;
                        return response;
                    }
                }


                PreparedStatement psTopicByTitle = null;
                PreparedStatement psTopicByTimestamp = null;
                PreparedStatement psTopic = null;
                //PreparedStatement psTopicPrivacy = null;

                BatchStatement batchStatement = new BatchStatement();

                string topicTitle = topicRow.GetValue<string>("title");
                DateTime createdTimestamp = topicRow.GetValue<DateTime>("created_on_timestamp");

                if (status == (int)TopicStatus.CODE_ACTIVE)
                {
                    int selectedNumberOfQuestions = topicRow.GetValue<int>("selected_number_of_questions");
                    int numberOfActiveQuestion = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ACTIVE, session);

                    if (numberOfActiveQuestion < selectedNumberOfQuestions)
                    {
                        response.ErrorCode = Convert.ToInt16(ErrorCode.TopicLessActiveQuestions);
                        response.ErrorMessage = ErrorMessage.TopicLessActiveQuestions;
                        return response;
                    }

                    psTopicByTitle = session.Prepare(CQLGenerator.InsertStatement("topic_by_company_title",
                        new List<string> { "topic_id", "topic_title", "company_id" }));

                    psTopicByTimestamp = session.Prepare(CQLGenerator.InsertStatement("topic_by_company_category_timestamp",
                        new List<string> { "topic_id", "topic_category_id", "company_id", "topic_created_on_timestamp" }));
                }
                // DELETED/ UNLISTED/ HIDDEN
                else
                {
                    psTopicByTitle = session.Prepare(CQLGenerator.DeleteStatement("topic_by_company_title",
                           new List<string> { "topic_id", "topic_title", "company_id" }));

                    psTopicByTimestamp = session.Prepare(CQLGenerator.DeleteStatement("topic_by_company_category_timestamp",
                        new List<string> { "topic_id", "topic_category_id", "company_id", "topic_created_on_timestamp" }));

#warning Deleted: remove privacy
                    ISession analyticSession = cm.getAnalyticSession();
                    Analytic analytic = new Analytic();
                    if (status == TopicStatus.CODE_DELETED)
                    {
                        PreparedStatement psTopicByCategory = session.Prepare(CQLGenerator.DeleteStatement("topic_by_category",
                          new List<string> { "topic_id", "topic_category_id" }));

                        PreparedStatement psCategoryByTopic = session.Prepare(CQLGenerator.DeleteStatement("category_by_topic",
                          new List<string> { "topic_id", "topic_category_id" }));

                        batchStatement = batchStatement
                            .Add(psTopicByCategory.Bind(topicId, categoryId))
                            .Add(psCategoryByTopic.Bind(topicId, categoryId));

                        analytic.RemoveFromLeaderboard(false, true, companyId, analyticSession, null, null, topicId);
                    }
                    else if (status == TopicStatus.CODE_HIDDEN)
                    {
                        analytic.HideFromLeaderboard(false, true, companyId, analyticSession, null, null, topicId);
                    }
                }

                psTopic = session.Prepare(CQLGenerator.UpdateStatement("topic",
                    new List<string> { "category_id", "id" }, new List<string> { "status" }, new List<string>()));

                batchStatement = batchStatement
                    .Add(psTopicByTitle.Bind(topicId, topicTitle, companyId))
                    .Add(psTopicByTimestamp.Bind(topicId, categoryId, companyId, createdTimestamp))
                    .Add(psTopic.Bind(status, categoryId, topicId));

                session.Execute(batchStatement);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        // Applies not to delete status
        public TopicUpdateResponse UpdateTopic(string adminUserId,
                                               string companyId,
                                               string topicId,
                                               string newTitle,
                                               string newLogoUrl,
                                               string newDescription,
                                               string newCategoryId,
                                               string newCategoryTitle,
                                               int newStatus,
                                               List<string> newTargetedDepartmentIds,
                                               int newNumberOfSelectedQuestions)
        {
            TopicUpdateResponse response = new TopicUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (!string.IsNullOrEmpty(newCategoryId))
                {
                    // Check topic category
                    Row topicCategoryRow = vh.ValidateTopicCategory(companyId, newCategoryId, session);

                    if (topicCategoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + newCategoryId);
                        response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                        response.ErrorMessage = ErrorMessage.CategoryInvalid;
                        return response;
                    }
                }

                PreparedStatement psCategory = null;
                BoundStatement bsCategory = null;

                // Check for old categoryId
                psCategory = session.Prepare(CQLGenerator.SelectStatement("category_by_topic", new List<string> { "topic_category_id" }, new List<string> { "topic_id" }));
                bsCategory = psCategory.Bind(topicId);
                string oldCategoryId = session.Execute(bsCategory).FirstOrDefault().GetValue<string>("topic_category_id");

                // Check topic row
                Row topicRow = vh.ValidateTopic(companyId, oldCategoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                if (newStatus != (int)TopicStatus.CODE_DELETED)
                {
                    // Check status first
                    bool isUpdateStatus = true;
                    int currentTopicStatus = topicRow.GetValue<int>("status");

                    if(currentTopicStatus == newStatus)
                    {
                        isUpdateStatus = false;
                    }
                    else
                    {
                        if (newStatus == (int)TopicStatus.CODE_ACTIVE)
                        {
                            //int selectedNumberOfQuestions = topicRow.GetValue<int>("selected_number_of_questions");
                            int numberOfActiveQuestion = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ACTIVE, session);

                            if (numberOfActiveQuestion < newNumberOfSelectedQuestions)
                            {
                                response.ErrorCode = Convert.ToInt16(ErrorCode.TopicLessActiveQuestions);
                                response.ErrorMessage = ErrorMessage.TopicLessActiveQuestions;
                                return response;
                            }
                        }
                    }
                   

                    bool isUpdateCategory = true;

                    if (!string.IsNullOrEmpty(newCategoryId))
                    {
                        // Change of category
                        if (oldCategoryId.Equals(newCategoryId))
                        {
                            isUpdateCategory = false;
                        }
                    }
                    else
                    {
                        // Need to create a new category
                        CategoryCreateResponse createCategoryResponse = new TopicCategory().Create(adminUserId, companyId, newCategoryTitle, session);
                        if (!createCategoryResponse.Success)
                        {
                            response.ErrorCode = Convert.ToInt16(createCategoryResponse.ErrorCode);
                            response.ErrorMessage = createCategoryResponse.ErrorMessage;
                            return response;
                        }

                        newCategoryId = createCategoryResponse.NewCategoryId;
                    }

                    bool isUpdateDepartment = true;

                    // Check for old department list
                    PreparedStatement psDepartment = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_department", new List<string> { "department_id" }, new List<string> { "topic_id" }));
                    BoundStatement bsDepartment = psDepartment.Bind(topicId);
                    RowSet departmentRowset = session.Execute(bsDepartment);

                    List<string> oldDepartmentIds = new List<string>();

                    foreach (Row departmentRow in departmentRowset)
                    {
                        oldDepartmentIds.Add(departmentRow.GetValue<string>("department_id"));
                    }

                    bool isDepartmentListEqual = newTargetedDepartmentIds.All(oldDepartmentIds.Contains) && newTargetedDepartmentIds.Count == oldDepartmentIds.Count;
                    isUpdateDepartment = isDepartmentListEqual ? false : true;


                    // Check for old title
                    string oldTopicTitle = topicRow.GetValue<string>("title");
                    string oldCreatedUserId = topicRow.GetValue<string>("created_by_user_id");
                    DateTimeOffset oldCreatedTimestamp = topicRow.GetValue<DateTimeOffset>("created_on_timestamp");

                    bool isUpdateTitle = !newTitle.Equals(oldTopicTitle);

                    BatchStatement deleteBatchStatement = new BatchStatement();
                    BatchStatement updateBatchStatement = new BatchStatement();

                    if (isUpdateCategory)
                    {
                        //Delete old category id relationship
                        psCategory = session.Prepare(CQLGenerator.DeleteStatement("topic_by_category", new List<string> { "topic_category_id", "topic_id" }));
                        bsCategory = psCategory.Bind(oldCategoryId, topicId);
                        deleteBatchStatement = deleteBatchStatement.Add(bsCategory);

                        psCategory = session.Prepare(CQLGenerator.DeleteStatement("category_by_topic", new List<string> { "topic_category_id", "topic_id" }));
                        bsCategory = psCategory.Bind(oldCategoryId, topicId);
                        deleteBatchStatement = deleteBatchStatement.Add(bsCategory);

                        psCategory = session.Prepare(CQLGenerator.DeleteStatement("topic_by_company_category_timestamp", new List<string> { "company_id", "topic_category_id", "topic_created_on_timestamp", "topic_id" }));
                        bsCategory = psCategory.Bind(companyId, oldCategoryId, oldCreatedTimestamp, topicId);
                        deleteBatchStatement = deleteBatchStatement.Add(bsCategory);

                        psCategory = session.Prepare(CQLGenerator.DeleteStatement("topic", new List<string> { "category_id", "id" }));
                        bsCategory = psCategory.Bind(oldCategoryId, topicId);
                        deleteBatchStatement = deleteBatchStatement.Add(bsCategory);

                        //Add new category id relationship
                        psCategory = session.Prepare(CQLGenerator.InsertStatement("topic_by_category", new List<string> { "topic_category_id", "topic_id" }));
                        bsCategory = psCategory.Bind(newCategoryId, topicId);
                        updateBatchStatement = updateBatchStatement.Add(bsCategory);

                        psCategory = session.Prepare(CQLGenerator.InsertStatement("category_by_topic", new List<string> { "topic_category_id", "topic_id" }));
                        bsCategory = psCategory.Bind(newCategoryId, topicId);
                        updateBatchStatement = updateBatchStatement.Add(bsCategory);

                        psCategory = session.Prepare(CQLGenerator.InsertStatement("topic_by_company_category_timestamp", new List<string> { "company_id", "topic_category_id", "topic_created_on_timestamp", "topic_id" }));
                        bsCategory = psCategory.Bind(companyId, newCategoryId, oldCreatedTimestamp, topicId);
                        updateBatchStatement = updateBatchStatement.Add(bsCategory);
                    }

                    if (isUpdateDepartment)
                    {
                        //Delete old department id relationship
                        foreach (string oldDeparmentId in oldDepartmentIds)
                        {
                            psDepartment = session.Prepare(CQLGenerator.DeleteStatement("department_targeted_topic", new List<string> { "department_id", "topic_id" }));
                            bsDepartment = psDepartment.Bind(oldDeparmentId, topicId);
                            deleteBatchStatement = deleteBatchStatement.Add(bsDepartment);

                            psDepartment = session.Prepare(CQLGenerator.DeleteStatement("topic_targeted_department", new List<string> { "department_id", "topic_id" }));
                            bsDepartment = psDepartment.Bind(oldDeparmentId, topicId);
                            deleteBatchStatement = deleteBatchStatement.Add(bsDepartment);
                        }

                        // Add new department id relationship
                        foreach (string newDepartmentId in newTargetedDepartmentIds)
                        {
                            psDepartment = session.Prepare(CQLGenerator.InsertStatement("department_targeted_topic", new List<string> { "department_id", "topic_id" }));
                            bsDepartment = psDepartment.Bind(newDepartmentId, topicId);
                            updateBatchStatement = updateBatchStatement.Add(bsDepartment);

                            psDepartment = session.Prepare(CQLGenerator.InsertStatement("topic_targeted_department", new List<string> { "department_id", "topic_id" }));
                            bsDepartment = psDepartment.Bind(newDepartmentId, topicId);
                            updateBatchStatement = updateBatchStatement.Add(bsDepartment);
                        }
                    }

                    if (isUpdateTitle)
                    {
                        //Delete topic by company title relationship
                        PreparedStatement psTopicByTitle = session.Prepare(CQLGenerator.DeleteStatement("topic_by_company_category_timestamp", new List<string> { "company_id", "topic_category_id", "topic_created_on_timestamp", "topic_id" }));
                        BoundStatement bsTopicByTitle = psTopicByTitle.Bind(companyId, oldCategoryId, oldCreatedTimestamp, topicId);
                        deleteBatchStatement = deleteBatchStatement.Add(bsTopicByTitle);

                        // Add topic by company title relationship
                        psTopicByTitle = session.Prepare(CQLGenerator.InsertStatement("topic_by_company_category_timestamp", new List<string> { "company_id", "topic_category_id", "topic_created_on_timestamp", "topic_id" }));
                        bsTopicByTitle = psTopicByTitle.Bind(companyId, newCategoryId, oldCreatedTimestamp, topicId);
                        updateBatchStatement = updateBatchStatement.Add(bsTopicByTitle);
                    }

                    PreparedStatement psTopic = session.Prepare(CQLGenerator.UpdateStatement("topic",
                        new List<string> { "category_id", "id" }, new List<string> { "title", "logo_url", "description", "type", "status", "selected_number_of_questions", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    BoundStatement bsTopic = psTopic.Bind(newTitle, newLogoUrl, newDescription, CODE_CHALLENGE, newStatus, newNumberOfSelectedQuestions, oldCreatedUserId, oldCreatedTimestamp, adminUserId, DateTime.UtcNow, newCategoryId, topicId);
                    updateBatchStatement = updateBatchStatement.Add(bsTopic);

                    session.Execute(deleteBatchStatement);
                    session.Execute(updateBatchStatement);

                    if (isUpdateStatus)
                    {
                        UpdateTopicStatus(adminUserId, companyId, topicId, newCategoryId, newStatus, session, topicRow);
                    }

                    response.Success = true;
                    response.NewTopicCategoryId = newCategoryId;
                }
                else
                {
                    UpdateTopicStatus(adminUserId, companyId, topicId, newCategoryId, TopicStatus.CODE_DELETED, session, topicRow);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        #region Play game
        public ChallengeCreateResponse CreateChallengeWithTopicId(string companyId,
                                                                  string initiatorUserId,
                                                                  string challengedUserId,
                                                                  string topicId,
                                                                  string categoryId)
        {
            ChallengeCreateResponse response = new ChallengeCreateResponse();
            response.Players = new List<User>();
            response.Topic = new Topic();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                // Check players
                User user = new User();
                User initiatorUser = user.SelectUserBasic(initiatorUserId, companyId, true, session).User;
                User challengedUser = user.SelectUserBasic(challengedUserId, companyId, true, session).User;

                if (initiatorUser == null)
                {
                    Log.Error("Invalid userId: " + initiatorUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                if (challengedUser == null)
                {
                    Log.Error("Invalid userId: " + challengedUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                Topic challengedTopic = SelectTopicBasic(topicId, null, companyId, categoryId, null, session).Topic;

                if (challengedTopic == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                response.Players.Add(initiatorUser);
                response.Players.Add(challengedUser);

                response.Topic = challengedTopic;

                List<ChallengeQuestion> questions = new ChallengeQuestion().CreateChallengeQuestionsWithTopic(challengedTopic, session);

                string challengeId = UUIDGenerator.GenerateUniqueIDForQuizChallenge();
                PreparedStatement psHistory = session.Prepare(CQLGenerator.InsertStatement("challenge_history",
                    new List<string> { "id", "topic_id", "topic_category_id", "company_id", "players_ids", "selected_number_of_questions", "is_live", "is_valid" }));

                List<string> player_ids = new List<string>();
                BatchStatement batchStatement = new BatchStatement()
                                    .Add(psHistory.Bind(challengeId, topicId, categoryId, companyId, new List<string> { initiatorUserId, challengedUserId }, challengedTopic.SelectedNumberOfQuestions, true, true));

                PreparedStatement psFullHistory = null;
                int order = 1;
                foreach (ChallengeQuestion question in questions)
                {
                    psFullHistory = session.Prepare(CQLGenerator.InsertStatement("full_challenge_history",
                                        new List<string> { "challenge_id", 
                                                           "company_id", 
                                                           "topic_id", 
                                                           "topic_category_id", 
                                                           "question_id", 
                                                           "players_ids", 
                                                           "type", 
                                                           "question_order", 
                                                           "content", 
                                                           "content_image_url", 
                                                           "content_image_md5",
                                                           "content_image_background_color_code",
                                                           "choice_type", 

                                                           "first_choice_id",
                                                           "first_choice",
                                                           "first_choice_image_url", 
                                                           "first_choice_image_md5",
 
                                                           "second_choice_id", 
                                                           "second_choice", 
                                                           "second_choice_image_url", 
                                                           "second_choice_image_md5", 

                                                           "third_choice_id", 
                                                           "third_choice", 
                                                           "third_choice_image_url",
                                                           "third_choice_image_md5",

                                                           "fourth_choice_id", 
                                                           "fourth_choice", 
                                                           "fourth_choice_image_url",
                                                           "fourth_choice_image_md5", 

                                                           "answer", 
                                                           "time_answering", 
                                                           "time_reading", 
                                                           "difficulty_level", 
                                                           "score_multiplier" }));
                    batchStatement = batchStatement.Add(psFullHistory.Bind(
                        challengeId,
                        companyId,
                        topicId,
                        categoryId,
                        question.Id,
                        new List<string> { initiatorUserId, challengedUserId },
                        question.QuestionType,
                        order++,
                        question.Content, question.ContentImageUrl, question.ContentImageMd5, question.ContentImageBackgroundColorCode,
                        question.ChoiceType,
                        question.FirstChoiceId, question.FirstChoice, question.FirstChoiceContentImageUrl, question.FirstChoiceContentImageMd5,
                        question.SecondChoiceId, question.SecondChoice, question.SecondChoiceContentImageUrl, question.SecondChoiceContentImageMd5,
                        question.ThirdChoiceId, question.ThirdChoice, question.ThirdChoiceContentImageUrl, question.ThirdChoiceContentImageMd5,
                        question.FourthChoiceId, question.FourthChoice, question.FourthChoiceContentImageUrl, question.FourthChoiceContentImageMd5,
                        question.CorrectAnswer,
                        question.TimeAssignedForAnswering, question.TimeAssignedForReading,
                        question.DifficultyLevel,
                        question.ScoreMultiplier));
                }

                session.Execute(batchStatement);

                //Notification notification = new Notification();
                //notification.CreateGameNotification(challengedUserId, Notification.NotificationType.ChallengedToGame, challengeId, initiatorUserId, topicId, session);

                response.ChallengeId = challengeId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ChallengeCreateResponse SelectChallengeWithChallengeId(string companyId,
                                                                      string challengeId,
                                                                      string requesterUserId)
        {
            ChallengeCreateResponse response = new ChallengeCreateResponse();
            response.Players = new List<User>();
            response.Topic = new Topic();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row historyRow = vh.ValidateChallenge(challengeId, companyId, session);

                if (historyRow != null && historyRow.GetValue<DateTime?>("completed_on_timestamp") == null)
                {
                    List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");

                    if (playerIds.Contains(requesterUserId))
                    {
                        string initiatorUserId = playerIds[0];
                        string challengedUserId = playerIds[1];

                        // Check players
                        User user = new User();
                        User initiatorUser = user.SelectUserBasic(initiatorUserId, companyId, true, session).User;
                        User challengedUser = user.SelectUserBasic(challengedUserId, companyId, true, session).User;

                        if (initiatorUser == null)
                        {
                            Log.Error("Invalid userId: " + initiatorUserId);
                            response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                            response.ErrorMessage = ErrorMessage.UserInvalid;
                            return response;
                        }

                        if (challengedUser == null)
                        {
                            Log.Error("Invalid userId: " + challengedUserId);
                            response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                            response.ErrorMessage = ErrorMessage.UserInvalid;
                            return response;
                        }

                        string topicId = historyRow.GetValue<string>("topic_id");
                        string topicCategoryId = historyRow.GetValue<string>("topic_category_id");

                        Topic challengedTopic = SelectTopicBasic(topicId, null, companyId, topicCategoryId, null, session).Topic;

                        if (challengedTopic == null)
                        {
                            Log.Error("Invalid topicId: " + topicId);
                            response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                            response.ErrorMessage = ErrorMessage.TopicInvalid;
                            return response;
                        }

                        response.Players.Add(initiatorUser);
                        response.Players.Add(challengedUser);

                        response.Topic = challengedTopic;
                        response.ChallengeId = challengeId;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Invalid playerId: " + requesterUserId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalidPlayer);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    }
                }
                else
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ChallengeStartWithoutOpponentResponse StartChallengeWithoutOpponent(string challengeId, string requesterUserId, string companyId)
        {
            ChallengeStartWithoutOpponentResponse response = new ChallengeStartWithoutOpponentResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row historyRow = vh.ValidateChallenge(challengeId, companyId, session);

                if (historyRow != null && historyRow.GetValue<DateTime?>("completed_on_timestamp") == null)
                {
                    List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");

                    if (playerIds.Contains(requesterUserId))
                    {
                        PreparedStatement psChallenge = session.Prepare(CQLGenerator.UpdateStatement("challenge_history",
                                        new List<string> { "id", "company_id" }, new List<string> { "is_live" }, new List<string>()));
                        BoundStatement bsChallenge = psChallenge.Bind(false, challengeId, companyId);
                        session.Execute(bsChallenge);

                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Invalid playerId: " + requesterUserId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalidPlayer);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    }
                }
                else
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;

            }

            return response;
        }

        public ChallengeOfflineSelectOpponentAnswerResponse SelectOpponentAnswerForOfflineGame(string challengeId, string requesterUserId, string companyId, int round)
        {
            ChallengeOfflineSelectOpponentAnswerResponse response = new ChallengeOfflineSelectOpponentAnswerResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row historyRow = vh.ValidateChallenge(challengeId, companyId, session);

                if (historyRow != null && historyRow.GetValue<DateTime?>("completed_on_timestamp") == null)
                {
                    List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");

                    if (playerIds.Contains(requesterUserId))
                    {
                        bool isCurrentUserInitiator = true ? playerIds.First().Equals(requesterUserId) : false;

                        PreparedStatement psFullHistory = session.Prepare(CQLGenerator.SelectStatement("full_challenge_history",
                            new List<string> { "answer", "answer_of_challenged_user", "time_taken_by_challenged_user", "answer_of_initiated_user", "time_taken_by_initiated_user" }, new List<string> { "company_id", "challenge_id", "question_order" }));
                        BoundStatement bsFullHistory = psFullHistory.Bind(companyId, challengeId, round);

                        Row fullHistoryRow = session.Execute(bsFullHistory).FirstOrDefault();

                        if (fullHistoryRow != null)
                        {
                            bool isCorrectAnswer = false;
                            float timeTaken = 0.0f;
                            string opponentUserId = null;
                            bool hasOpponentAnswered = false;

                            string questionAnswer = fullHistoryRow.GetValue<string>("answer");

                            if (isCurrentUserInitiator)
                            {
                                // Take challenged user
                                string challengedUserAnswer = fullHistoryRow.GetValue<string>("answer_of_challenged_user");
                                if (!string.IsNullOrEmpty(challengedUserAnswer))
                                {
                                    hasOpponentAnswered = true;
                                    isCorrectAnswer = questionAnswer.Equals(challengedUserAnswer);
                                    timeTaken = fullHistoryRow.GetValue<float>("time_taken_by_challenged_user");
                                    opponentUserId = playerIds[1];
                                }

                            }
                            else
                            {
                                // Take initiated user
                                string initiatedUserAnswer = fullHistoryRow.GetValue<string>("answer_of_initiated_user");

                                if (!string.IsNullOrEmpty(initiatedUserAnswer))
                                {
                                    hasOpponentAnswered = true;
                                    isCorrectAnswer = questionAnswer.Equals(initiatedUserAnswer);
                                    timeTaken = fullHistoryRow.GetValue<float>("time_taken_by_initiated_user");
                                    opponentUserId = playerIds[0];
                                }
                            }

                            response.Success = true;
                            response.IsOpponentAnswerCorrect = isCorrectAnswer;
                            response.TimeTakenByOpponent = timeTaken;
                            response.OpponentUserId = opponentUserId;
                            response.HasOpponentAnswered = hasOpponentAnswered;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid playerId: " + requesterUserId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalidPlayer);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    }
                }
                else
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;

            }

            return response;
        }

        public ChallengeIsReadyResponse SetPlayerReadyForChallenge(string requesterUserId,
                                                                   string companyId,
                                                                   string challengeId)
        {
            ChallengeIsReadyResponse response = new ChallengeIsReadyResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row historyRow = vh.ValidateChallenge(challengeId, companyId, session);

                if (historyRow != null && historyRow.GetValue<DateTime?>("completed_on_timestamp") == null)
                {
                    List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");

                    if (playerIds.Contains(requesterUserId))
                    {
                        bool isInitiator = true ? playerIds.FindIndex(user => user.Equals(requesterUserId)) == 0 : false;
                        bool isLive = historyRow.GetValue<bool>("is_live");
                        DateTimeOffset initiatedUserTimestamp = historyRow["initiated_user_started_on_timestamp"] != null ? historyRow.GetValue<DateTimeOffset>("initiated_user_started_on_timestamp") : DateTime.MinValue;
                        DateTimeOffset challengedUserTimestamp = historyRow["challenged_user_started_on_timestamp"] != null ? historyRow.GetValue<DateTimeOffset>("challenged_user_started_on_timestamp") : DateTime.MinValue;

                        bool haveBothPlayersAccepted = false;
                        DateTimeOffset currentTime = DateTime.UtcNow;

                        PreparedStatement psHistory = null;
                        BatchStatement batchStatement = new BatchStatement();

                        if (isInitiator)
                        {
                            psHistory = session.Prepare(CQLGenerator.UpdateStatement("challenge_history",
                                new List<string> { "company_id", "id" }, new List<string> { "initiated_user_started_on_timestamp" }, new List<string>()));
                            batchStatement = batchStatement.Add(psHistory.Bind(currentTime, companyId, challengeId));

                            if (challengedUserTimestamp != DateTime.MinValue)
                            {
                                haveBothPlayersAccepted = true;
                            }
                            else
                            {
                                // Create a request for challenge if challenger have not accept
                                string topicId = historyRow.GetValue<string>("topic_id");
                                string categoryId = historyRow.GetValue<string>("topic_category_id");

                                string initiatorUserId = playerIds[0];
                                string challengedUserId = playerIds[1];
                                PreparedStatement psHistoryByTimestamp = session.Prepare(CQLGenerator.InsertStatement("challenge_history_by_initiated_timestamp",
                                    new List<string> { "challenge_id", "challenged_user_id", "initiated_user_id", "topic_id", "topic_category_id", "initiated_on_timestamp" }));
                                batchStatement = batchStatement.Add(psHistoryByTimestamp.Bind(challengeId, challengedUserId, initiatorUserId, topicId, categoryId, currentTime));
                            }
                        }
                        else
                        {
                            psHistory = session.Prepare(CQLGenerator.UpdateStatement("challenge_history",
                                new List<string> { "company_id", "id" }, new List<string> { "challenged_user_started_on_timestamp" }, new List<string>()));
                            batchStatement = batchStatement.Add(psHistory.Bind(currentTime, companyId, challengeId));

                            if (initiatedUserTimestamp != DateTime.MinValue)
                            {
                                haveBothPlayersAccepted = true;

                                // Delete request once accepted
                                PreparedStatement psHistoryByTimestamp = session.Prepare(CQLGenerator.DeleteStatement("challenge_history_by_initiated_timestamp",
                                    new List<string> { "challenge_id", "challenged_user_id", "initiated_on_timestamp" }));
                                batchStatement = batchStatement.Add(psHistoryByTimestamp.Bind(challengeId, requesterUserId, initiatedUserTimestamp));
                            }
                        }

                        session.Execute(batchStatement);

                        response.Success = true;
                        response.HaveBothPlayersAccepted = haveBothPlayersAccepted;
                        response.IsLive = isLive;
                    }
                    else
                    {
                        Log.Error("Invalid playerId: " + requesterUserId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalidPlayer);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    }
                }
                else
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public ChallengeInvalidateResponse InvalidateChallenge(string userId, string companyId, string challengeId, int reason, Row historyRow = null, ISession session = null)
        {
            ChallengeInvalidateResponse response = new ChallengeInvalidateResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    historyRow = vh.ValidateChallenge(challengeId, companyId, session);
                }

                DateTimeOffset currentTime = DateTime.UtcNow;
                BatchStatement batchStatement = new BatchStatement();

                if (historyRow != null)
                {
                    List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");
                    DateTimeOffset initiatedUserTimestamp = historyRow.GetValue<DateTimeOffset>("initiated_user_started_on_timestamp");
                    string initiatorUserId = playerIds[0];
                    string challengedUserId = playerIds[1];

                    PreparedStatement psChallenge = session.Prepare(CQLGenerator.UpdateStatement("challenge_history",
                        new List<string> { "id", "company_id" }, new List<string> { "is_valid", "reason", "invalidated_by_user_id", "invalidated_on_timestamp" }, new List<string>()));
                    batchStatement = batchStatement.Add(psChallenge.Bind(false, reason, userId, currentTime, challengeId, companyId));

                    // Delete request once invalidated
                    PreparedStatement psHistoryByTimestamp = session.Prepare(CQLGenerator.DeleteStatement("challenge_history_by_initiated_timestamp",
                        new List<string> { "challenge_id", "challenged_user_id", "initiated_on_timestamp" }));
                    batchStatement = batchStatement.Add(psHistoryByTimestamp.Bind(challengeId, challengedUserId, initiatedUserTimestamp));

                    session.Execute(batchStatement);

                    response.Success = true;
                    response.InitiatedUserId = initiatorUserId;
                    response.ChallengedUserId = challengedUserId;
                }
                else
                {
                    Log.Error("Challenge already invalided: " + challengeId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        #endregion

    }
}