﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Cassandra;
using CocadreCassandraService.App_GlobalResources;
using CocadreCassandraService.Connection;
using CocadreCassandraService.CQL;
using CocadreCassandraService.Date;
using CocadreCassandraService.Error.Classes;
using CocadreCassandraService.IDGenerator;
using CocadreCassandraService.Validation;
using log4net;
using CocadreCassandraService.ServiceResponses;
using CocadreCassandraService.Properties;

namespace CocadreCassandraService.Entity
{
    [DataContract]
    public class Department
    {
        public static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public const int QUERY_TYPE_BASIC = 1;
        public const int QUERY_TYPE_DETAIL = 2;

        [DataMember(EmitDefaultValue = false)]
        public string Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Position { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsTargetedForTopic { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public long CountOfUsers { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<User> Users { get; set; }

        public DepartmentDetailResponse GetDepartmentDetail(String adminUserId, String companyId, String departmentId, int queryType)
        {
            DepartmentDetailResponse response = new DepartmentDetailResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" }));
                BoundStatement bs = ps.Bind(companyId, departmentId, true);
                Row row = session.Execute(bs).FirstOrDefault();
                if (row != null)
                {
                    if (queryType == QUERY_TYPE_DETAIL)
                    {
                        PreparedStatement ps_counter = session.Prepare(CQLGenerator.CountStatement("user_by_department", new List<String> { "department_id" }));
                        BoundStatement bs_counter = ps_counter.Bind(row.GetValue<String>("id"));
                        Row row_counter = session.Execute(bs_counter).FirstOrDefault();
                        response.Department = new Department { Id = departmentId, Title = row.GetValue<String>("title"), CountOfUsers = row_counter.GetValue<long>("count") };
                    }
                    else
                    {
                        response.Department = new Department { Id = departmentId, Title = row.GetValue<String>("title") };
                    }

                    response.Success = true;
                }
                else
                {
                    response.Department = null;
                    response.Success = true;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public DepartmentListResponse GetAllDepartment(String adminUserId, String companyId, int queryType, ISession session = null)
        {
            DepartmentListResponse response = new DepartmentListResponse();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    #region Step 1. Check data.
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    #region Step 1.1 Check Admin account's validation.
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                    #endregion
                    #endregion
                }


                #region Step 2. Read database.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "is_valid" }));
                BoundStatement bs = ps.Bind(companyId, true);
                RowSet rowSet = session.Execute(bs);
                if (rowSet != null)
                {
                    List<Department> departments = new List<Department>();
                    foreach (Row row in rowSet.GetRows())
                    {
                        switch (queryType)
                        {
                            case QUERY_TYPE_DETAIL:
                                PreparedStatement ps_counter = session.Prepare(CQLGenerator.CountStatement("user_by_department", new List<String> { "department_id" }));
                                BoundStatement bs_counter = ps_counter.Bind(row.GetValue<String>("id"));
                                Row row_counter = session.Execute(bs_counter).FirstOrDefault();
                                departments.Add(new Department { Id = row.GetValue<String>("id"), Title = row.GetValue<String>("title"), CountOfUsers = row_counter.GetValue<long>("count") });
                                break;

                            case QUERY_TYPE_BASIC:
                                departments.Add(new Department { Id = row.GetValue<String>("id"), Title = row.GetValue<String>("title") });
                                break;

                            default:
                                break;
                        }
                    }
                    response.Departments = departments;
                    response.Success = true;
                }
                else
                {
                    response.Departments = null;
                    response.Success = true;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public DepartmentListResponse GetAllDepartmentByUserId(string userId, string companyId, ISession session)
        {
            DepartmentListResponse response = new DepartmentListResponse();
            response.Departments = new List<Department>();
            response.Success = false;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs = ps.Bind(userId);
                RowSet rowSet = session.Execute(bs);

                foreach (Row row in rowSet)
                {
                    string departmentId = row.GetValue<string>("department_id");

                    PreparedStatement psDepartment = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" }));
                    BoundStatement bsDepartment = psDepartment.Bind(companyId, departmentId, true);
                    Row departmentRow = session.Execute(bsDepartment).FirstOrDefault();

                    if (departmentRow != null)
                    {
                        response.Departments.Add(new Department { Id = departmentRow.GetValue<string>("id"), Title = departmentRow.GetValue<string>("title") });
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public DepartmentListResponse GetAllDepartmentByTopicId(string topicId, string companyId, ISession session)
        {
            DepartmentListResponse response = new DepartmentListResponse();
            response.Departments = new List<Department>();
            response.Success = false;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_department", new List<string> { }, new List<string> { "topic_id" }));
                BoundStatement bs = ps.Bind(topicId);
                RowSet rowSet = session.Execute(bs);

                foreach (Row row in rowSet)
                {
                    string departmentId = row.GetValue<string>("department_id");

                    PreparedStatement psDepartment = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" }));
                    BoundStatement bsDepartment = psDepartment.Bind(companyId, departmentId, true);
                    Row departmentRow = session.Execute(bsDepartment).FirstOrDefault();

                    if (departmentRow != null)
                    {
                        response.Departments.Add(new Department { Id = departmentRow.GetValue<string>("id"), Title = departmentRow.GetValue<string>("title") });
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public DepartmentCreateResponse Create(string adminUserId, string companyId, string departmentTitle)
        {
            DepartmentCreateResponse response = new DepartmentCreateResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                PreparedStatement preparedStatement = null;
                BoundStatement boundStatement = null;
                BatchStatement batchStatement = new BatchStatement();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check Title of Department.
                if (string.IsNullOrEmpty(departmentTitle.Trim()))
                {
                    Log.Error(ErrorMessage.DepartmentNameCantBeEmpty);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentNameCantBeEmpty);
                    response.ErrorMessage = ErrorMessage.DepartmentNameCantBeEmpty;
                    return response;
                }

                preparedStatement = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { "id", "title" }, new List<string> { "company_id", "is_valid" }));
                boundStatement = preparedStatement.Bind(companyId, true);
                RowSet rowSet = session.Execute(boundStatement);
                foreach (Row row in rowSet)
                {
                    if (row.GetValue<string>("title").ToLower().Trim().Equals(departmentTitle.ToLower().Trim()))
                    {
                        Log.Error(ErrorMessage.DepartmentNameHasBeenUsed);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentNameHasBeenUsed);
                        response.ErrorMessage = ErrorMessage.DepartmentNameHasBeenUsed;
                        return response;
                    }
                }

                #endregion
                #endregion

                #region Step 2. Insert new department to databse.
                String departmentId = UUIDGenerator.GenerateUniqueIDForDepartment();

                PreparedStatement ps_department = session.Prepare(CQLGenerator.InsertStatement("department",
                      new List<string> { "company_id", "id", "created_by_user_id", "created_on_timestamp", "is_valid", "last_modified_by_user_id", "last_modified_timestamp", "title" }));

                PreparedStatement ps_department_by_company_title = session.Prepare(CQLGenerator.InsertStatement("department_by_company", new List<string> { "company_id", "department_id" }));

                batchStatement = batchStatement
                    .Add(ps_department.Bind(companyId, departmentId, adminUserId, DateTime.UtcNow, true, adminUserId, DateTime.UtcNow, departmentTitle))
                    .Add(ps_department_by_company_title.Bind(companyId, departmentId));

                session.Execute(batchStatement);
                response.Success = true;

                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public DepartmentUpdateResponse Update(string adminUserId, string companyId, string departmentId, string departmentTitle)
        {
            DepartmentUpdateResponse response = new DepartmentUpdateResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                PreparedStatement preparedStatement = null;
                BatchStatement batchStatement = new BatchStatement();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check Title of Department.
                if (string.IsNullOrEmpty(departmentTitle.Trim()))
                {
                    Log.Error(ErrorMessage.DepartmentNameCantBeEmpty);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentNameCantBeEmpty);
                    response.ErrorMessage = ErrorMessage.DepartmentNameCantBeEmpty;
                    return response;
                }

                preparedStatement = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { "id", "title" }, new List<string> { "company_id", "is_valid" }));
                RowSet rowSet = session.Execute(preparedStatement.Bind(companyId, true));
                foreach (Row r in rowSet)
                {
                    if (r.GetValue<string>("title").ToLower().Trim().Equals(departmentTitle.ToLower().Trim()))
                    {
                        Log.Error(ErrorMessage.DepartmentNameHasBeenUsed);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentNameHasBeenUsed);
                        response.ErrorMessage = ErrorMessage.DepartmentNameHasBeenUsed;
                        return response;
                    }
                }
                #endregion
                #endregion

                #region Step 2. Update department.
                #region Step 2.1 Update department table.
                preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("department", new List<string> { "company_id", "id" }, new List<string> { "title", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                batchStatement.Add(preparedStatement.Bind(departmentTitle, adminUserId, DateTime.UtcNow, companyId, departmentId));
                #endregion

                session.Execute(batchStatement);
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public DepartmentDeleteResponse Delete(string adminUserId, string companyId, string departmentId)
        {
            DepartmentDeleteResponse response = new DepartmentDeleteResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check DepartmentId.
                if (string.IsNullOrEmpty(departmentId.Trim()))
                {
                    Log.Error(ErrorMessage.DepartmentIdIsInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentIdIsInvalid);
                    response.ErrorMessage = ErrorMessage.DepartmentIdIsInvalid;
                    return response;
                }
                #endregion

                #region Step 1.3 Check users count of department. If count > 0, cannot delete department.
                PreparedStatement ps_counter = session.Prepare(CQLGenerator.CountStatement("user_by_department", new List<String> { "department_id" }));
                BoundStatement bs_counter = ps_counter.Bind(departmentId);
                Row row_counter = session.Execute(bs_counter).FirstOrDefault();
                if (row_counter != null && row_counter.GetValue<long>("count") > 0)
                {
                    Log.Error(ErrorMessage.DepartmentIsNotEmpty);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentIsNotEmpty);
                    response.ErrorMessage = ErrorMessage.DepartmentIsNotEmpty;
                    return response;
                }
                #endregion

                #endregion

                #region Step 2. Delete department.
                BatchStatement batchStatement = new BatchStatement();

                #region Step 2.1 Delete department table. (Update is_valid to false)
                PreparedStatement ps_department = session.Prepare(CQLGenerator.UpdateStatement("department", new List<string> { "company_id", "id" }, new List<string> { "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                BoundStatement bs_department = ps_department.Bind(false, adminUserId, DateTime.UtcNow, companyId, departmentId);
                batchStatement.Add(bs_department);
                #endregion

                #region Step 2.2 Delete department_by_company table.
                PreparedStatement ps_department_by_company = session.Prepare(CQLGenerator.DeleteStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                BoundStatement bs_department_by_company = ps_department_by_company.Bind(companyId, departmentId);
                batchStatement.Add(bs_department_by_company);
                #endregion

                #region Step 2.3 Delete topic_targeted_department table.
                PreparedStatement ps_department_targeted_topic = session.Prepare(CQLGenerator.SelectStatement("department_targeted_topic", new List<string> { }, new List<string> { "department_id" }));
                BoundStatement bs_department_targeted_topic = ps_department_targeted_topic.Bind(departmentId);
                RowSet rowSet = session.Execute(bs_department_targeted_topic);
                if (rowSet != null)
                {
                    foreach (Row row in rowSet)
                    {
                        PreparedStatement ps_department_by_topic = session.Prepare(CQLGenerator.DeleteStatement("topic_targeted_department", new List<string> { "topic_id", "department_id" }));
                        BoundStatement bs_department_by_topic = ps_department_by_topic.Bind(row.GetValue<string>("topic_id"), departmentId);
                        batchStatement.Add(bs_department_by_topic);
                    }
                }
                #endregion

                #region Step 2.4 Delete department_targeted_topic table.
                ps_department_targeted_topic = session.Prepare(CQLGenerator.DeleteStatement("department_targeted_topic", new List<string> { "department_id" }));
                bs_department_targeted_topic = ps_department_targeted_topic.Bind(departmentId);
                batchStatement.Add(bs_department_targeted_topic);
                #endregion

                session.Execute(batchStatement);
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }
    }
}