﻿using Cassandra;
using CocadreCassandraService.Connection;
using CocadreCassandraService.CQL;
using CocadreCassandraService.Date;
using CocadreCassandraService.IDGenerator;
using CocadreCassandraService.ServiceResponses;
using CocadreCassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Hosting;
using Microsoft.VisualBasic.FileIO;
using System.Runtime.Serialization;
using CocadreCassandraService.App_GlobalResources;

namespace CocadreCassandraService.Entity
{
    public class Analytic
    {
        public static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public enum ChallengeStatType
        {
            Draw = 1,
            Lost = 2,
            NetworkError = 3,
            Surrender = 4,
            Win = 5,
        }

        public void WriteExpAToTable()
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();

                string expAFileName = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "exp-a.csv");
                TextFieldParser parser = new TextFieldParser(expAFileName);
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadLine();

                BatchStatement batchStatement = new BatchStatement();

                List<Dictionary<string, int>> expList = new List<Dictionary<string, int>>();
                while (!parser.EndOfData)
                {
                    Dictionary<string, int> expDict = new Dictionary<string, int>();
                    string[] fields = parser.ReadFields();

                    int level = Int32.Parse(fields[0].Trim());
                    int exp = Int32.Parse(fields[1].Trim());

                    expDict.Add("level", level);
                    expDict.Add("exp", exp);
                    expList.Add(expDict);
                }

                for (int index = 0; index < expList.Count; index++)
                {
                    PreparedStatement psExp = session.Prepare(CQLGenerator.InsertStatement("experience_scheme_a",
                        new List<string> { "level", "from_exp", "to_exp" }));

                    if (index + 1 < expList.Count)
                    {
                        batchStatement = batchStatement.Add(psExp.Bind(expList[index]["level"], expList[index]["exp"], expList[index + 1]["exp"] - 1));
                    }
                    else
                    {
                        batchStatement = batchStatement.Add(psExp.Bind(expList[index]["level"], expList[index]["exp"], expList[index]["exp"]));
                    }
                }

                session.Execute(batchStatement);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }
        }

        public void UpdateExpOfUser(string userId, int score, ISession session)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                PreparedStatement psExp = session.Prepare(CQLGenerator.UpdateCounterStatement("experience_by_user",
                    new List<string> { "user_id" }, new List<string> { "exp" }, new List<int> { score }));
                BoundStatement bsExp = psExp.Bind(userId);
                session.Execute(bsExp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

        }

        public ChallengeSelectStatsResponse SelectStatsForChallenge(string topicId, string requesterUserId, string opponentUserId, string companyId)
        {
            ChallengeSelectStatsResponse response = new ChallengeSelectStatsResponse();
            response.Exp = new Exp();
            response.TopPlayers = new List<User>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                Exp currentUserExp = SelectLevelOfUser(requesterUserId, session);

                Exp opponentExp = SelectLevelOfUser(opponentUserId, session);
                response.OpponentLevel = opponentExp.CurrentLevel;

                PreparedStatement psLeaderboard = session.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted", new List<string> { "user_id" }, new List<string> { "topic_id", "is_visible" }));
                BoundStatement bsLeaderboard = psLeaderboard.Bind(topicId, true);
                RowSet leaderboardRowset = session.Execute(bsLeaderboard);

                foreach (Row leaderboardRow in leaderboardRowset)
                {
                    string userId = leaderboardRow.GetValue<string>("user_id");
                    Exp leaderboardUserExp = SelectLevelOfUser(userId, session);

                    int level = leaderboardUserExp.CurrentLevel;

                    User player = new User().SelectUserBasic(userId, companyId, false, mainSession).User;
                    player.Level = level;
                    response.TopPlayers.Add(player);
                }

                response.Exp = currentUserExp;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public Exp SelectLevelOfUser(string userId, ISession session)
        {
            Exp levelExp = new Exp();

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                PreparedStatement psExpByUser = session.Prepare(CQLGenerator.SelectStatement("experience_by_user", new List<string> { "exp" }, new List<string> { "user_id" }));
                BoundStatement bsExpByUser = psExpByUser.Bind(userId);
                Row expByUserRow = session.Execute(bsExpByUser).FirstOrDefault();

                int expPoint = 0;

                if(expByUserRow != null)
                {
                    expPoint = (int)expByUserRow.GetValue<long>("exp");
                }

                PreparedStatement psExp = session.Prepare("SELECT * FROM experience_scheme_a WHERE from_exp <= ? ALLOW FILTERING");
                BoundStatement bsExp = psExp.Bind(expPoint);
                RowSet expRowset = session.Execute(bsExp);

                List<Dictionary<string, int>> expList = new List<Dictionary<string, int>>();

                foreach (Row expRow in expRowset)
                {
                    Dictionary<string, int> expDict = new Dictionary<string, int>();
                    expDict.Add("level", expRow.GetValue<int>("level"));
                    expDict.Add("fromExp", expRow.GetValue<int>("from_exp"));
                    expDict.Add("toExp", expRow.GetValue<int>("to_exp"));

                    expList.Add(expDict);
                }

                int currentLevel = 0;
                int currentExp = 0;
                int nextLevel = 0;
                int nextExpToReach = 0;

                expList = expList.OrderByDescending(expDict => expDict["fromExp"]).ToList();
                currentLevel = expList[0]["level"];
                currentExp = expPoint;
                nextLevel = currentLevel + 1;
                nextExpToReach = expList[0]["toExp"] + 1;

                levelExp.CurrentLevel = currentLevel;
                levelExp.CurrentExp = currentExp;
                levelExp.NextLevel = nextLevel;
                levelExp.NextExpToReach = nextExpToReach;

            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return levelExp;
        }

        public void UpdateUserActivity(string userId,
                                       string companyId,
                                       bool isActive)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                string logId = UUIDGenerator.GenerateUniqueIDForLog();
                DateTime currentDatetime = DateTime.UtcNow;

                PreparedStatement psUserActivityLog = session.Prepare(CQLGenerator.InsertStatement("user_activity_log",
                    new List<string> { "id", "company_id", "user_id", "is_active", "activity_datestamp", "activity_date_timestamp" }));
                BoundStatement bsUserActivityLog = psUserActivityLog.Bind(logId, companyId, userId, isActive, DateHelper.ConvertDateToLong(currentDatetime.Date), DateHelper.ConvertDateToLong(currentDatetime));

                session.Execute(bsUserActivityLog);

                Log.Debug("Updated activity successfully");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public UserUpdateLoginResponse UpdateUserLogin(string userId,
                                                       string companyId,
                                                       bool isLogin)
        {
            UserUpdateLoginResponse response = new UserUpdateLoginResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                string logId = UUIDGenerator.GenerateUniqueIDForLog();
                DateTime currentDatetime = DateTime.UtcNow;

                PreparedStatement psUserLoginLog = session.Prepare(CQLGenerator.InsertStatement("user_login_log",
                    new List<string> { "id", "company_id", "user_id", "is_login", "login_datestamp", "login_date_timestamp" }));
                BoundStatement bsUserLoginLog = psUserLoginLog.Bind(logId, companyId, userId, isLogin, DateHelper.ConvertDateToLong(currentDatetime.Date), DateHelper.ConvertDateToLong(currentDatetime));

                session.Execute(bsUserLoginLog);

                if (!isLogin)
                {
                    UserSelectDeviceTokenResponse userResponse = new User().SelectUserDeviceToken(userId);
                    response.DeviceToken = userResponse.DeviceToken;
                    response.DeviceType = userResponse.DeviceType;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public void UpdateLeaderboardStatsByQuestion(string userId, string topicId, string questionId, string challengeId, string mainDepartmentId, string companyId, bool isCorrect, ISession session)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;

                PreparedStatement psChallengeStats = session.Prepare(CQLGenerator.SelectStatementWithLimit("challenge_question_stats_by_user",
                    new List<string>(), new List<string> { "user_id", "question_id" }, 1));
                BoundStatement bsChallengeStats = psChallengeStats.Bind(userId, questionId);
                Row challengeStatsRow = session.Execute(bsChallengeStats).FirstOrDefault();

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                // Question is answered before by this current user
                if (challengeStatsRow != null)
                {
                    bool isAnsweredCorrectlyPreviously = challengeStatsRow.GetValue<bool>("is_correct");
                    // Now correct
                    if (isCorrect)
                    {
                        // Previously answer wrongly, but now correct
                        if (!isAnsweredCorrectlyPreviously)
                        {
                            //------------------------- Topic leaderboard by user ------------------------
                            PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user",
                                   new List<string>(), new List<string> { "user_id", "topic_id" }));
                            BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, topicId);
                            Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                            int numberAnswerCorrectlyByUser = 0;
                            DateTimeOffset lastUpdatedTimestampByUser = currentTime;

                            if (leaderBoardRow != null)
                            {
                                numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                                lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                            }

                            //------------------------- Company leaderboard by user ------------------------
                            psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                                   new List<string>(), new List<string> { "user_id" }));
                            bsLeaderBoard = psLeaderBoard.Bind(userId);
                            leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                            int numberAnswerCorrectlyByCompany = 0;
                            DateTimeOffset lastUpdatedTimestampByCompany = currentTime;

                            if (leaderBoardRow != null)
                            {
                                numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                                lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                            }

                            //------------------------- Company leaderboard by department ------------------------
                            psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                                new List<string>(), new List<string> { "department_id" }));
                            bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                            leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                            int numberAnswerCorrectlyByDepartment = 0;
                            DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                            if (leaderBoardRow != null)
                            {
                                numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                                lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                            }

                            //------------------------- Department leaderboard by user ------------------------
                            psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                                new List<string>(), new List<string> { "user_id" }));
                            bsLeaderBoard = psLeaderBoard.Bind(userId);
                            leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                            int numberAnswerCorrectlyByDepartmentUser = 0;
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

                            if (leaderBoardRow != null)
                            {
                                numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                                lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                            }

                            // Delete
                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));


                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));


                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));


                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));


                            // Update
                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user",
                                new List<string> { "user_id", "topic_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByUser + 1, currentTime, userId, topicId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("topic_leaderboard_by_user_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + 1, currentTime, topicId, true));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_topic_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + 1, currentTime, topicId, true));


                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + 1, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + 1, currentTime, companyId));


                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + 1, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + 1, currentTime, mainDepartmentId));


                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + 1, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + 1, currentTime, mainDepartmentId));
                        }
                    }
                    // Now wrong
                    else
                    {
                        // Previously answer correctly, but now wrong
                        if (isAnsweredCorrectlyPreviously)
                        {
                            //------------------------- Topic leaderboard by user ------------------------
                            PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user",
                                   new List<string>(), new List<string> { "user_id", "topic_id" }));
                            BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, topicId);
                            Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                            int numberAnswerCorrectlyByUser = 0;
                            DateTimeOffset lastUpdatedTimestampByUser = currentTime;

                            if (leaderBoardRow != null)
                            {
                                numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                                lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                            }

                            //------------------------- Company leaderboard by user ------------------------
                            psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                                   new List<string>(), new List<string> { "user_id" }));
                            bsLeaderBoard = psLeaderBoard.Bind(userId);
                            leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                            int numberAnswerCorrectlyByCompany = 0;
                            DateTimeOffset lastUpdatedTimestampByCompany = currentTime;

                            if (leaderBoardRow != null)
                            {
                                numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                                lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                            }

                            //------------------------- Company leaderboard by department ------------------------
                            psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                                new List<string>(), new List<string> { "department_id" }));
                            bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                            leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                            int numberAnswerCorrectlyByDepartment = 0;
                            DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                            if (leaderBoardRow != null)
                            {
                                numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                                lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                            }

                            //------------------------- Department leaderboard by user ------------------------
                            psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                                new List<string>(), new List<string> { "user_id" }));
                            bsLeaderBoard = psLeaderBoard.Bind(userId);
                            leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                            int numberAnswerCorrectlyByDepartmentUser = 0;
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

                            if (leaderBoardRow != null)
                            {
                                numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                                lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                            }

                            // Delete
                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id"}));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id"}));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));


                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));


                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));


                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));


                            // Update
                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user",
                                new List<string> { "user_id", "topic_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByUser - 1, currentTime, userId, topicId));

                            if (numberAnswerCorrectlyByUser - 1 > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("topic_leaderboard_by_user_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser - 1, currentTime, topicId, true));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_topic_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser - 1, currentTime, topicId, true));
                            }

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - 1, currentTime, userId));

                            if (numberAnswerCorrectlyByCompany - 1 > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - 1, currentTime, companyId));
                            }


                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - 1, currentTime, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - 1 > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - 1, currentTime, mainDepartmentId));
                            }


                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - 1, currentTime, userId));

                            if (numberAnswerCorrectlyByDepartmentUser - 1 > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - 1, currentTime, mainDepartmentId));
                            }


                        }

                    }
                }
                // Question is not answered before by this current user
                else
                {
                    if (isCorrect)
                    {
                        //------------------------- Topic leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id", "topic_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, topicId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByUser = 0;
                        DateTimeOffset lastUpdatedTimestampByUser = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByCompany = 0;
                        DateTimeOffset lastUpdatedTimestampByCompany = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByDepartment = 0;
                        DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByDepartmentUser = 0;
                        DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));


                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));


                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));


                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));


                        // Update
                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user",
                            new List<string> { "user_id", "topic_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByUser + 1, currentTime, userId, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + 1, currentTime, topicId, true));

                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + 1, currentTime, topicId, true));


                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + 1, currentTime, userId));

                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + 1, currentTime, companyId));


                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                            new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + 1, currentTime, mainDepartmentId));

                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + 1, currentTime, mainDepartmentId));


                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + 1, currentTime, userId));

                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + 1, currentTime, mainDepartmentId));

                    }
                }

                if (!string.IsNullOrEmpty(challengeId))
                {
                    psChallengeStats = session.Prepare(CQLGenerator.InsertStatement("challenge_question_stats_by_user",
                        new List<string> { "user_id", "topic_id", "challenge_id", "question_id", "is_correct", "answered_timestamp" }));
                    bsChallengeStats = psChallengeStats.Bind(userId, topicId, challengeId, questionId, isCorrect, currentTime);
                    updateStatements = updateStatements.Add(bsChallengeStats);
                }


                session.Execute(deleteStatements);
                session.Execute(updateStatements);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RemoveFromLeaderboard(bool forUser, bool forTopic, string companyId, ISession session, string userId = null, string mainDepartmentId = null, string topicId = null)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                // Topic is removed, so leaderboard need to be updated
                if (forTopic && !string.IsNullOrEmpty(topicId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByTopic = session.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted",
                           new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsLeaderBoardByTopic = psLeaderBoardByTopic.Bind(topicId);
                    RowSet leaderBoardByTopicRowset = session.Execute(bsLeaderBoardByTopic);


                    foreach (Row leaderBoardByTopicRow in leaderBoardByTopicRowset)
                    {
                        userId = leaderBoardByTopicRow.GetValue<string>("user_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByTopicRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByTopicRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                            new List<string> { "user_id", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
                else if (forUser && !string.IsNullOrEmpty(userId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByUser = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                           new List<string>(), new List<string> { "user_id" }));
                    BoundStatement bsLeaderBoardByUser = psLeaderBoardByUser.Bind(userId);
                    RowSet leaderBoardByUserRowset = session.Execute(bsLeaderBoardByUser);


                    foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                    {
                        topicId = leaderBoardByUserRow.GetValue<string>("topic_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                            new List<string> { "user_id", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public void HideFromLeaderboard(bool forUser, bool forTopic, string companyId, ISession session, string userId = null, string mainDepartmentId = null, string topicId = null)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                // Topic is hidden, so leaderboard need to be updated
                if (forTopic && !string.IsNullOrEmpty(topicId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByTopic = session.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted",
                           new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsLeaderBoardByTopic = psLeaderBoardByTopic.Bind(topicId);
                    RowSet leaderBoardByTopicRowset = session.Execute(bsLeaderBoardByTopic);


                    foreach (Row leaderBoardByTopicRow in leaderBoardByTopicRowset)
                    {
                        userId = leaderBoardByTopicRow.GetValue<string>("user_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByTopicRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByTopicRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));


                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
                else if (forUser && !string.IsNullOrEmpty(userId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByUser = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                           new List<string>(), new List<string> { "user_id" }));
                    BoundStatement bsLeaderBoardByUser = psLeaderBoardByUser.Bind(userId);
                    RowSet leaderBoardByUserRowset = session.Execute(bsLeaderBoardByUser);


                    foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                    {
                        topicId = leaderBoardByUserRow.GetValue<string>("topic_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public void UnhideFromLeaderboard(bool forUser, bool forTopic, string companyId, ISession session, string userId = null, string mainDepartmentId = null, string topicId = null)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                // Topic is hidden, so leaderboard need to be updated
                if (forTopic && !string.IsNullOrEmpty(topicId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByTopic = session.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted",
                           new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsLeaderBoardByTopic = psLeaderBoardByTopic.Bind(topicId);
                    RowSet leaderBoardByTopicRowset = session.Execute(bsLeaderBoardByTopic);


                    foreach (Row leaderBoardByTopicRow in leaderBoardByTopicRowset)
                    {
                        userId = leaderBoardByTopicRow.GetValue<string>("user_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByTopicRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByTopicRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));


                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, companyId));

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
                else if (forUser && !string.IsNullOrEmpty(userId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByUser = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                           new List<string>(), new List<string> { "user_id" }));
                    BoundStatement bsLeaderBoardByUser = psLeaderBoardByUser.Bind(userId);
                    RowSet leaderBoardByUserRowset = session.Execute(bsLeaderBoardByUser);


                    foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                    {
                        topicId = leaderBoardByUserRow.GetValue<string>("topic_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, companyId));

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SwitchLeaderboardForDepartment(string userId, string companyId, string currentDepartmentId, string newDepartmentId)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();

                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                //------------------------- Department leaderboard by user ------------------------
                PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id", "department_id" }));
                BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, currentDepartmentId);
                Row leaderBoardByUserRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                // Only if the user has contributed some points to that department
                if (leaderBoardByUserRow != null)
                {
                    int numberAnswerCorrectlyByDepartmentUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                    DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    if (numberAnswerCorrectlyByDepartmentUser > 0)
                    {
                        // Current department points
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                               new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(currentDepartmentId);

                        Row currentDepartmentRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyForCurrentDepartment = currentDepartmentRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByCurrentDepartment = currentDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        // New department points
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                               new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(newDepartmentId);

                        Row newDepartmentRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyForNewDepartment = 0;
                        DateTimeOffset lastUpdatedTimestampByNewDepartment = currentTime;

                        if (newDepartmentRow != null)
                        {
                            numberAnswerCorrectlyForNewDepartment = newDepartmentRow.GetValue<int>("number_answered_correctly");
                            lastUpdatedTimestampByNewDepartment = newDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        //------------------------- Company leaderboard by department (Current Department) ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                                   new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForCurrentDepartment - numberAnswerCorrectlyByDepartmentUser, currentTime, currentDepartmentId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForCurrentDepartment, lastUpdatedTimestampByCurrentDepartment, currentDepartmentId));

                        if (numberAnswerCorrectlyForCurrentDepartment - numberAnswerCorrectlyByDepartmentUser > 0)
                        {
                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForCurrentDepartment - numberAnswerCorrectlyByDepartmentUser, currentTime, currentDepartmentId));
                        }

                        //------------------------- Company leaderboard by department (New Department) ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                                   new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForNewDepartment + numberAnswerCorrectlyByDepartmentUser, currentTime, newDepartmentId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForNewDepartment, lastUpdatedTimestampByNewDepartment, newDepartmentId));

                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                                   new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForNewDepartment + numberAnswerCorrectlyByDepartmentUser, currentTime, newDepartmentId));

                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(newDepartmentId, numberAnswerCorrectlyByDepartmentUser, currentTime, userId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                   new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, currentDepartmentId));

                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, currentTime, newDepartmentId));


                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UpdateChallengeStats(string companyId, string userId, ChallengeStatType stat, ISession session)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                string columnStatName = string.Empty;

                switch (stat)
                {
                    case ChallengeStatType.Draw:
                        columnStatName = "total_draws";
                        break;
                    case ChallengeStatType.Lost:
                        columnStatName = "total_losses";
                        break;
                    case ChallengeStatType.NetworkError:
                        columnStatName = "total_network_errors";
                        break;
                    case ChallengeStatType.Surrender:
                        columnStatName = "total_surrenders";
                        break;
                    case ChallengeStatType.Win:
                        columnStatName = "total_win";
                        break;

                }

                PreparedStatement psChallengeStats = session.Prepare(CQLGenerator.UpdateCounterStatement("challenge_total_stats_counter_by_user",
                    new List<string> { "company_id", "user_id" }, new List<string> { columnStatName, "total_games_played" }, new List<int> { 1, 1 }));
                BoundStatement bChallengeStats = psChallengeStats.Bind(companyId, userId);

                session.Execute(bChallengeStats);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        [DataContract]
        public class Exp
        {
            [DataMember]
            public int CurrentLevel { get; set; }
            [DataMember]
            public int CurrentExp { get; set; }
            [DataMember]
            public int NextLevel { get; set; }
            [DataMember]
            public int NextExpToReach { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string Rank { get; set; }
        }
    }
}
