﻿using Cassandra;
using CocadreCassandraService.CQL;
using CocadreCassandraService.Date;
using CocadreCassandraService.IDGenerator;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.Entity
{
    public class Notification
    {
        public static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [DataMember(EmitDefaultValue = false)]
        public string Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TargetedUserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedChallengeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Text { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public NotificationType Type { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime Timestamp { get; set; }

        [DataContract]
        public enum NotificationType
        {
            [EnumMember]
            ChallengedToGame = 1,

            [EnumMember]
            ViewResultByInitiator = 2,
            
            [EnumMember]
            ViewResultByChallenged = 3
        }
       
        public bool CreateGameNotification(string targetedUserId, NotificationType notificationType, string taggedChallengeId, string textUserId, string textTopicId, ISession session)
        {
            try
            {
                string logId = UUIDGenerator.GenerateUniqueIDForLog();
                DateTime currentDatetime = DateTime.UtcNow;

                string text = string.Empty;
                if(notificationType == NotificationType.ChallengedToGame)
                {
                    text = string.Format("{0}| has challenged you in  |{1}.", textUserId, textTopicId);
                }
                else if (notificationType == NotificationType.ViewResultByInitiator)
                {
                    text = string.Format("{2}| completed your challenge in  |{1}.", textUserId, textTopicId);
                }
                else if (notificationType == NotificationType.ViewResultByChallenged)
                {
                    text = string.Format("You completed |{0}|'s  challenge in  |{1}.", textUserId, textTopicId);
                }

                PreparedStatement psNotification = session.Prepare(CQLGenerator.InsertStatement("game_notification",
                    new List<string> { "id", "targeted_user_id", "type", "tagged_challenge_id", "text", "timestamp" }));
                BoundStatement bsNotification = psNotification.Bind(logId, targetedUserId, (int)notificationType, taggedChallengeId, text, DateHelper.ConvertDateToLong(currentDatetime));

                session.Execute(bsNotification);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return false;
        }
    }
}