﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.Entity
{
    public class EmailTemplate
    {
        public class Type
        {
            public const int CREATED_USER = 1;
            public const int CREATED_ADMIN = 2;
        }

        public String Tilte { get; set; }
        public Boolean IsHtmlFormat { get; set; }
        public String Body { get; set; }

        public EmailTemplate(int type, String password = null)
        {
            switch (type)
            {
                case Type.CREATED_USER:
                    Tilte = "Welcome to CoCadre";
                    IsHtmlFormat = true;
                    Body = @"Hi user,<br />
You have a new account at CoCadre.<br />
Your email address will be username and your password will be " + password + @"<br />
For CoCadre mobile app: <br />
Android download here <br />
iOS download here <br /><br />
Regards, <br />
Admin";
                    break;

                case Type.CREATED_ADMIN:
                    Tilte = "Welcome to CoCadre";
                    IsHtmlFormat = true;
                    Body = @"Hi, <br />
You are invited to CoCadre, our Human Capital Solution. <br />
Your login ID is your e-mail address <br />
Your password will be " + password + @"<br />
You need to download the APP on testflight.<br />
Regards, <br />
Admin";

//Hi admin,<br />
//You have a new account at CoCadre.<br />
//Your email address will be username and your password will be " + password + @"<br />
//For CoCadre mobile app: <br />
//Android download here <br />
//iOS download here <br /><br />
//Regards, <br />
//Admin";
                    break;


                default:
                    break;
            }
        }

    }
}