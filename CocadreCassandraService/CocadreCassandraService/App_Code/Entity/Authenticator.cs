﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Cassandra;
using CocadreCassandraService.App_GlobalResources;
using CocadreCassandraService.Connection;
using CocadreCassandraService.CQL;
using CocadreCassandraService.Crypto;
using CocadreCassandraService.Error;
using CocadreCassandraService.Error.Classes;
using CocadreCassandraService.IDGenerator;
using CocadreCassandraService.ServiceResponses;
using CocadreCassandraService.Validation;
using log4net;

namespace CocadreCassandraService.Entity
{
    public class Authenticator
    {
        public static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public LoginByAdminResponse LoginByAdmin(String email, String password)
        {
            LoginByAdminResponse response = new LoginByAdminResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check input data.
                if (String.IsNullOrEmpty(email))
                {
                    Log.Error(ErrorMessage.UserMismatchEmailOrPassword);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserMismatchEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserMismatchEmailOrPassword;
                    return response;
                }

                if (String.IsNullOrEmpty(password))
                {
                    Log.Error(ErrorMessage.UserMismatchEmailOrPassword);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserMismatchEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserMismatchEmailOrPassword;
                    return response;
                }

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string> { }, new List<string> { "email", }));
                BoundStatement bs = ps.Bind(email);
                RowSet rowSet = session.Execute(bs);

                if (rowSet != null)
                {
                    List<Company> companies = new List<Company>();
                    foreach (Row row in rowSet.GetRows())
                    {
                        String hashedPassword = CryptoLib.EncryptTextWithSalt(password, row.GetValue<String>("salt"));
                        if (hashedPassword.Equals(row.GetValue<String>("hashed_password")))
                        {
                            ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
                            bs = ps.Bind(row.GetValue<String>("user_id"));
                            Row rowUser = session.Execute(bs).FirstOrDefault();
                            if (rowUser != null && rowUser.GetValue<int>("account_status") == User.AccountStatus.CODE_ACTIVE && rowUser.GetValue<int>("account_type") > User.AccountType.CODE_NORMAL_USER)
                            {
                                ps = session.Prepare(CQLGenerator.SelectStatement("company", new List<string> { }, new List<string> { "id" }));
                                bs = ps.Bind(row.GetValue<String>("company_id"));
                                Row rowCompany = session.Execute(bs).FirstOrDefault();

                                ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                                bs = ps.Bind(row.GetValue<String>("user_id"));
                                Row rowAdmin = session.Execute(bs).FirstOrDefault();
                                User user = new User { UserId = rowAdmin.GetValue<String>("id"), FirstName = rowAdmin.GetValue<String>("first_name"), LastName = rowAdmin.GetValue<String>("last_name"), Email = rowAdmin.GetValue<String>("email"), ProfileImageUrl = rowAdmin.GetValue<String>("profile_image_url"), Type = new User.AccountType(rowUser.GetValue<int>("account_type")) };
                                companies.Add(new Company { CompanyId = row.GetValue<String>("company_id"), CompanyLogoUrl = rowCompany.GetValue<String>("logo_url"), CompanyTitle = rowCompany.GetValue<String>("title"), Admin = user });
                            }
                        }
                        if (companies.Count > 0)
                        {
                            response.Companies = companies;
                            response.Success = true;
                        }
                        else
                        {
                            Log.Error(ErrorMessage.UserInvalid);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                            response.ErrorMessage = ErrorMessage.UserInvalid;
                        }
                    }
                }
                else
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ErrorStatus IsAdmin(String adminUserId, String compnayId, ISession session)
        {
            ErrorStatus es = new ErrorStatus();
            try
            {
                es = IsValidCompany(compnayId, session);
                if (es.IsValid)
                {
                    return es;
                }
                else
                {
                    return es;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                es = ErrorHandler.SystemError;
                return es;
            }
        }

        public static ErrorStatus IsValidCompany(String companyId, ISession session)
        {
            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("company", new List<string>(), new List<string> { "id", "is_valid" }));
                BoundStatement bs = ps.Bind(companyId, true);
                Row row = session.Execute(bs).FirstOrDefault();
                if (row != null)
                {
                    return ErrorHandler.NoError;
                }
                else
                {

                    return ErrorHandler.InvalidCompany;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                return ErrorHandler.SystemError;
            }
        }


        public Row CheckAuthenticationExists(string email,
                                             ISession session)
        {
            Row authenticationRow = null;
            try
            {
                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                    new List<string>(), new List<string> { "email" }));
                BoundStatement bsAuthentication = psAuthentication.Bind(email);
                authenticationRow = session.Execute(bsAuthentication).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return authenticationRow;
        }

        public AuthenticationSelectUserResponse SelectAuthenticatedUser(string email,
                                                                        string encryptedPassword)
        {
            AuthenticationSelectUserResponse response = new AuthenticationSelectUserResponse();
            response.Companies = new List<Company>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                    new List<string> { "user_id", "salt", "company_id", "hashed_password", "is_default" }, new List<string> { "email" }));
                BoundStatement bsAuthentication = psAuthentication.Bind(email);
                RowSet authenticationRowSet = session.Execute(bsAuthentication);

                foreach (Row authenticationRow in authenticationRowSet)
                {
                    CryptoLib crypto = new CryptoLib();
                    string salt = authenticationRow.GetValue<string>("salt");
                    string currentHashedPassword = CryptoLib.EncryptTextWithSalt(encryptedPassword, salt);
                    string dbHashedPassword = authenticationRow.GetValue<string>("hashed_password");

                    if (currentHashedPassword.Equals(dbHashedPassword))
                    {
                        string userId = authenticationRow.GetValue<string>("user_id");
                        string companyId = authenticationRow.GetValue<string>("company_id");

                        Row companyRow = vh.ValidateCompany(companyId, session);
                        if (companyRow == null)
                        {
                            Log.Error("Company invalid for login: " + companyId);
                            continue;
                        }

                        PreparedStatement psUserAccountType = session.Prepare(CQLGenerator.SelectStatement("user_account_type",
                            new List<string> { }, new List<string> { "user_id", "account_status" }));
                        BoundStatement bsUserAccountType = psUserAccountType.Bind(userId, Int32.Parse(UserStatus.Active));
                        Row accountRow = session.Execute(bsUserAccountType).FirstOrDefault();
                        if (accountRow == null)
                        {
                            Log.Error("Account has been deleted: " + email);
                            continue;
                        }

                        string companyTitle = companyRow.GetValue<string>("title");
                        string companyLogoUrl = companyRow.GetValue<string>("logo_url");
                        bool isPasswordDefault = authenticationRow["is_default"] != null ? authenticationRow.GetValue<bool>("is_default") : true;
                        Company company = new Company
                        {
                            CompanyId = companyId,
                            CompanyTitle = companyTitle,
                            CompanyLogoUrl = companyLogoUrl,
                            UserId = userId,
                            IsPasswordDefault = isPasswordDefault
                        };
                        response.Companies.Add(company);
                    }                   
                }

                if(response.Companies.Count == 0)
                {
                    Log.Debug("Mismatch email or password");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserMismatchEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserMismatchEmailOrPassword;
                }
                else
                {
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public bool CreateAuthenticatedUser(string companyId,
                                            string newUserId,
                                            string adminUserId,
                                            string email,
                                            string plainPassword,
                                            ISession session)
        {
            bool response = false;

            try
            {
                CryptoLib crypto = new CryptoLib();
                string randomSalt = CryptoLib.GenerateRandomSalt(32);
                string hashedPassword = CryptoLib.EncryptTextWithSalt(plainPassword, randomSalt);

                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.InsertStatement("user_authentication",
                    new List<string> { "user_id", "email", "hashed_password", "salt", "company_id", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                BoundStatement bsAuthentication = psAuthentication.Bind(newUserId, email, hashedPassword, randomSalt, companyId, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow);
                session.Execute(bsAuthentication);

                response = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public bool UpdateCompanyForAuthenticatedUser(string email,
                                                      string userId,
                                                      string updatedCompanyId,
                                                      ISession session)
        {
            bool response = false;

            try
            {
                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.UpdateStatement("user_authentication",
                    new List<string> { "email", "user_id" }, new List<string> { "company_id" }, new List<string>()));
                BoundStatement bsAuthentication = psAuthentication.Bind(updatedCompanyId, email, userId);
                session.Execute(bsAuthentication);

                response = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public bool DeleteAuthenticatedUser(string email,
                                            string userId,
                                            ISession session)
        {
            bool response = false;

            try
            {
                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.DeleteStatement("user_authentication",
                    new List<string> { "email", "user_id" }));
                BoundStatement bsAuthentication = psAuthentication.Bind(email, userId);
                session.Execute(bsAuthentication);

                response = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public string SelectAuthenticationToken(string userId,
                                                ISession session)
        {
            string userToken = string.Empty;

            try
            {
                PreparedStatement psUserToken = session.Prepare(CQLGenerator.SelectStatement("user_token",
                    new List<string> { "token" }, new List<string> { "user_id" }));
                BoundStatement bsUserToken = psUserToken.Bind(userId);
                Row userTokenRow = session.Execute(bsUserToken).FirstOrDefault();

                if (userTokenRow != null)
                {
                    string token = userTokenRow.GetValue<string>("token");
                    userToken = token;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return userToken;
        }

        public string CreateAuthenticationToken(string companyId,
                                                string userId,
                                                ISession session)
        {
            string userToken = string.Empty;

            try
            {
                BatchStatement batchStatements = new BatchStatement();

                string token = UUIDGenerator.GenerateUniqueTokenForUser();

                PreparedStatement psUserToken = session.Prepare(CQLGenerator.SelectStatement("user_token",
                    new List<string> { "user_token" }, new List<string> { "user_id", "is_token_valid" }));
                BoundStatement bsUserToken = psUserToken.Bind(userId, true);
                Row userTokenRow = session.Execute(bsUserToken).FirstOrDefault();

                if (userTokenRow != null)
                {
                    string oldToken = userTokenRow.GetValue<string>("user_token");

                    psUserToken = session.Prepare(CQLGenerator.UpdateStatement("user_token",
                        new List<string> { "user_id", "user_token" }, new List<string> { "is_token_valid", "last_modified_timestamp" }, new List<string>()));
                    bsUserToken = psUserToken.Bind(false, DateTime.UtcNow, userId, oldToken);
                    batchStatements = batchStatements.Add(bsUserToken);
                }

                psUserToken = session.Prepare(CQLGenerator.InsertStatement("user_token",
                    new List<string> { "user_id", "user_token", "is_token_valid", "last_modified_timestamp" }));
                bsUserToken = psUserToken.Bind(userId, token, true, DateTime.UtcNow);

                batchStatements = batchStatements.Add(bsUserToken);

                session.Execute(batchStatements);

                userToken = token;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return userToken;
        }
    }
}