﻿using Cassandra;
using CocadreCassandraService.App_GlobalResources;
using CocadreCassandraService.Connection;
using CocadreCassandraService.CQL;
using CocadreCassandraService.Date;
using CocadreCassandraService.Error.Classes;
using CocadreCassandraService.IDGenerator;
using CocadreCassandraService.ServiceResponses;
using CocadreCassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.Entity
{
    [DataContract]
    public class ChallengeQuestion
    {
        [DataMember]
        public const int QUESTION_TYPE_NORMAL = 1;
        [DataMember]
        public const int QUESTION_TYPE_LONG = 2;
        [DataMember]
        public const int QUESTION_TYPE_PICTURE = 3;

        [DataMember]
        public const int CHOICE_TYPE_TEXT = 1;
        [DataMember]
        public const int CHOICE_TYPE_PICTURE = 2;

        private static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [DataMember(EmitDefaultValue = false)]
        public string Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int QuestionType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Content { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ContentImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ContentImageMd5 { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ContentImageBackgroundColorCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ChoiceType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FirstChoiceId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FirstChoice { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FirstChoiceContentImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FirstChoiceContentImageMd5 { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SecondChoiceId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SecondChoice { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SecondChoiceContentImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SecondChoiceContentImageMd5 { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ThirdChoiceId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ThirdChoice { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ThirdChoiceContentImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ThirdChoiceContentImageMd5 { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FourthChoiceId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FourthChoice { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FourthChoiceContentImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FourthChoiceContentImageMd5 { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CorrectAnswer { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public float TimeAssignedForReading { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public float TimeAssignedForAnswering { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int DifficultyLevel { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ScoreMultiplier { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public double Frequency { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Status { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CreatorUserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset CreatedOnTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset LastModifiedTimestamp { get; set; }

        [DataContract]
        public class QuestionStatus
        {
            public const int CODE_DELETED = -1;
            public const int CODE_ALL_EXCEPT_DELETE = 0;
            public const int CODE_ACTIVE = 1;
            public const int CODE_HIDDEN = 2;

            [DataMember]
            public int Code { get; private set; }

            [DataMember]
            public String Title { get; private set; }

            public QuestionStatus(int code)
            {
                switch (code)
                {
                    case CODE_DELETED:
                        Code = CODE_DELETED;
                        Title = "Deleted";
                        break;
                    case CODE_ACTIVE:
                        Code = CODE_ACTIVE;
                        Title = "Active";
                        break;
                    case CODE_HIDDEN:
                        Code = CODE_HIDDEN;
                        Title = "Hidden";
                        break;
                    default:
                        break;
                }
            }
        }

        public int QuestionTypeNormal()
        {
            return QUESTION_TYPE_NORMAL;
        }

        public int QuestionTypeLong()
        {
            return QUESTION_TYPE_LONG;
        }

        public int QuestionTypePicture()
        {
            return QUESTION_TYPE_PICTURE;
        }

        public int ChoiceTypeText()
        {
            return CHOICE_TYPE_TEXT;
        }

        public int ChoiceTypePicture()
        {
            return CHOICE_TYPE_PICTURE;
        }

        public string GenerateQuestionID()
        {
            return UUIDGenerator.GenerateUniqueIDForQuestion();
        }

        public List<QuestionStatus> SelectQuestionStatusForDropdown()
        {
            List<QuestionStatus> questionStatuses = new List<QuestionStatus>();
            try
            {
                questionStatuses.Add(new QuestionStatus(QuestionStatus.CODE_ACTIVE));
                questionStatuses.Add(new QuestionStatus(QuestionStatus.CODE_HIDDEN));
                questionStatuses = questionStatuses.OrderBy(x => x.Code).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return questionStatuses;
        }

        public QuestionSelectResponse SelectQuestion(string questionId, string topicId, string companyId, string adminUserId, ISession session = null, Row questionRow = null, bool isChoiceRandomize = false, bool isForChallenge = false)
        {
            QuestionSelectResponse response = new QuestionSelectResponse();
            response.Question = new ChallengeQuestion();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                if (adminUserId != null)
                {
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                if (questionRow == null)
                {
                    PreparedStatement psQuestion = session.Prepare(CQLGenerator.SelectStatement("challenge_question",
                    new List<string>(), new List<string> { "topic_id", "id" }));
                    BoundStatement bsQuestion = psQuestion.Bind(topicId, questionId);
                    questionRow = session.Execute(bsQuestion).FirstOrDefault();
                }

                int status = QuestionStatus.CODE_ACTIVE;

                if (!isForChallenge)
                {
                    status = questionRow.GetValue<int>("status");
                }

                if (status != QuestionStatus.CODE_DELETED)
                {
                    ChallengeQuestion question = new ChallengeQuestion();

                    int questionType = questionRow.GetValue<int>("type");
                    int choiceType = questionRow.GetValue<int>("choice_type");

                    string content = questionRow.GetValue<string>("content");
                    string contentImageUrl = questionRow.GetValue<string>("content_image_url");
                    string contentImageMd5 = questionRow.GetValue<string>("content_image_md5");
                    string contentImageBackgroundColorCode = questionRow["content_image_background_color_code"] != null ? questionRow.GetValue<string>("content_image_background_color_code") : "#000000";

                    string firstChoiceId = questionRow.GetValue<string>("first_choice_id");
                    string firstChoice = questionRow.GetValue<string>("first_choice");
                    string firstChoiceImageUrl = questionRow.GetValue<string>("first_choice_image_url");
                    string firstChoiceImageMd5 = questionRow.GetValue<string>("first_choice_image_md5");

                    string secondChoiceId = questionRow.GetValue<string>("second_choice_id");
                    string secondChoice = questionRow.GetValue<string>("second_choice");
                    string secondChoiceImageUrl = questionRow.GetValue<string>("second_choice_image_url");
                    string secondChoiceImageMd5 = questionRow.GetValue<string>("second_choice_image_md5");

                    string thirdChoiceId = questionRow.GetValue<string>("third_choice_id");
                    string thirdChoice = questionRow.GetValue<string>("third_choice");
                    string thirdChoiceImageUrl = questionRow.GetValue<string>("third_choice_image_url");
                    string thirdChoiceImageMd5 = questionRow.GetValue<string>("third_choice_image_md5");

                    string fourthChoiceId = questionRow.GetValue<string>("fourth_choice_id");
                    string fourthChoice = questionRow.GetValue<string>("fourth_choice");
                    string fourthChoiceImageUrl = questionRow.GetValue<string>("fourth_choice_image_url");
                    string fourthChoiceImageMd5 = questionRow.GetValue<string>("fourth_choice_image_md5");

                    float timeAnswering = questionRow.GetValue<float>("time_answering");
                    float timeReading = questionRow.GetValue<float>("time_reading");
                    int difficultyLevel = questionRow.GetValue<int>("difficulty_level");

                    int scoreMultiplier = questionRow.GetValue<int>("score_multiplier");

                    double frequency = 0.0;
                    DateTimeOffset createdTimestamp = new DateTimeOffset();
                    string answer = string.Empty;

                    if (!isForChallenge)
                    {
                        frequency = questionRow.GetValue<double>("frequency");
                        createdTimestamp = questionRow.GetValue<DateTimeOffset>("created_on_timestamp");
                        answer = questionRow.GetValue<string>("answer");
                    }


                    if (isChoiceRandomize)
                    {
                        List<ChallengeChoice> challengeChoices = new List<ChallengeChoice>();
                        ChallengeChoice choiceOne;
                        ChallengeChoice choiceTwo;
                        ChallengeChoice choiceThree;
                        ChallengeChoice choiceFour;

                        choiceOne = new ChallengeChoice { Id = firstChoiceId, Content = firstChoice, ContentImageUrl = firstChoiceImageUrl, ContentImageMd5 = firstChoiceImageMd5 };
                        choiceTwo = new ChallengeChoice { Id = secondChoiceId, Content = secondChoice, ContentImageUrl = secondChoiceImageUrl, ContentImageMd5 = secondChoiceImageMd5 };
                        choiceThree = new ChallengeChoice { Id = thirdChoiceId, Content = thirdChoice, ContentImageUrl = thirdChoiceImageUrl, ContentImageMd5 = thirdChoiceImageMd5 };
                        choiceFour = new ChallengeChoice { Id = fourthChoiceId, Content = fourthChoice, ContentImageUrl = fourthChoiceImageUrl, ContentImageMd5 = fourthChoiceImageMd5 };

                        challengeChoices.Add(choiceOne);
                        challengeChoices.Add(choiceTwo);
                        challengeChoices.Add(choiceThree);
                        challengeChoices.Add(choiceFour);

                        challengeChoices = challengeChoices.OrderBy(choice => Guid.NewGuid()).ToList();

                        firstChoiceId = challengeChoices[0].Id;
                        firstChoice = challengeChoices[0].Content;
                        firstChoiceImageUrl = challengeChoices[0].ContentImageUrl;
                        firstChoiceImageMd5 = challengeChoices[0].ContentImageMd5;

                        secondChoiceId = challengeChoices[1].Id;
                        secondChoice = challengeChoices[1].Content;
                        secondChoiceImageUrl = challengeChoices[1].ContentImageUrl;
                        secondChoiceImageMd5 = challengeChoices[1].ContentImageMd5;

                        thirdChoiceId = challengeChoices[2].Id;
                        thirdChoice = challengeChoices[2].Content;
                        thirdChoiceImageUrl = challengeChoices[2].ContentImageUrl;
                        thirdChoiceImageMd5 = challengeChoices[2].ContentImageMd5;

                        fourthChoiceId = challengeChoices[3].Id;
                        fourthChoice = challengeChoices[3].Content;
                        fourthChoiceImageUrl = challengeChoices[3].ContentImageUrl;
                        fourthChoiceImageMd5 = challengeChoices[3].ContentImageMd5;
                    }

                    #region Question
                    if (questionType == QUESTION_TYPE_LONG || questionType == QUESTION_TYPE_NORMAL)
                    {

                        if (choiceType == CHOICE_TYPE_TEXT)
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ChoiceType = choiceType,
                                Status = status,

                                FirstChoiceId = firstChoiceId,
                                FirstChoice = firstChoice,

                                SecondChoiceId = secondChoiceId,
                                SecondChoice = secondChoice,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoice = thirdChoice,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoice = fourthChoice,

                                CorrectAnswer = answer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                DifficultyLevel = difficultyLevel,
                                ScoreMultiplier = scoreMultiplier,
                                Frequency = frequency,

                                CreatedOnTimestamp = createdTimestamp
                            };
                        }
                        else
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ChoiceType = choiceType,
                                Status = status,

                                FirstChoiceId = firstChoiceId,
                                FirstChoiceContentImageUrl = FirstChoiceContentImageUrl,
                                FirstChoiceContentImageMd5 = FirstChoiceContentImageMd5,

                                SecondChoiceId = secondChoiceId,
                                SecondChoiceContentImageUrl = SecondChoiceContentImageUrl,
                                SecondChoiceContentImageMd5 = SecondChoiceContentImageMd5,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoiceContentImageUrl = ThirdChoiceContentImageUrl,
                                ThirdChoiceContentImageMd5 = ThirdChoiceContentImageMd5,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoiceContentImageUrl = FourthChoiceContentImageUrl,
                                FourthChoiceContentImageMd5 = FourthChoiceContentImageMd5,

                                CorrectAnswer = answer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                DifficultyLevel = difficultyLevel,
                                ScoreMultiplier = scoreMultiplier,
                                Frequency = frequency,

                                CreatedOnTimestamp = createdTimestamp
                            };
                        }
                    }
                    else
                    {
                        if (choiceType == CHOICE_TYPE_TEXT)
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ContentImageUrl = contentImageUrl,
                                ContentImageMd5 = contentImageMd5,
                                ContentImageBackgroundColorCode = contentImageBackgroundColorCode,
                                ChoiceType = choiceType,
                                Status = status,

                                FirstChoiceId = firstChoiceId,
                                FirstChoice = firstChoice,

                                SecondChoiceId = secondChoiceId,
                                SecondChoice = secondChoice,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoice = thirdChoice,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoice = fourthChoice,

                                CorrectAnswer = answer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                DifficultyLevel = difficultyLevel,
                                ScoreMultiplier = scoreMultiplier,
                                Frequency = frequency,

                                CreatedOnTimestamp = createdTimestamp
                            };
                        }
                        else
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ContentImageUrl = contentImageUrl,
                                ContentImageMd5 = contentImageMd5,
                                ContentImageBackgroundColorCode = contentImageBackgroundColorCode,
                                ChoiceType = choiceType,
                                Status = status,

                                FirstChoiceId = firstChoiceId,
                                FirstChoiceContentImageUrl = FirstChoiceContentImageUrl,
                                FirstChoiceContentImageMd5 = FirstChoiceContentImageMd5,

                                SecondChoiceId = secondChoiceId,
                                SecondChoiceContentImageUrl = SecondChoiceContentImageUrl,
                                SecondChoiceContentImageMd5 = SecondChoiceContentImageMd5,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoiceContentImageUrl = ThirdChoiceContentImageUrl,
                                ThirdChoiceContentImageMd5 = ThirdChoiceContentImageMd5,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoiceContentImageUrl = FourthChoiceContentImageUrl,
                                FourthChoiceContentImageMd5 = FourthChoiceContentImageMd5,

                                CorrectAnswer = answer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                DifficultyLevel = difficultyLevel,
                                ScoreMultiplier = scoreMultiplier,
                                Frequency = frequency,

                                CreatedOnTimestamp = createdTimestamp
                            };
                        }
                    }
                    #endregion Question

                    response.Question = question;
                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public QuestionSelectAllResponse SelectAllQuestionsForTopic(string adminUserId,
                                                                    string companyId,
                                                                    string topicId,
                                                                    string topicCategoryId,
                                                                    ISession session)
        {
            QuestionSelectAllResponse response = new QuestionSelectAllResponse();
            response.Questions = new List<ChallengeQuestion>();
            response.Success = false;

            try
            {
                if (session != null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement psQuestion = session.Prepare(CQLGenerator.SelectStatement("challenge_question",
                                    new List<string>(), new List<string> { "topic_id" }));
                BoundStatement bsQuestion = psQuestion.Bind(topicId);
                RowSet questionRowset = session.Execute(bsQuestion);

                foreach (Row questionRow in questionRowset)
                {
                    string questionId = questionRow.GetValue<string>("id");
                    ChallengeQuestion question = SelectQuestion(questionId, topicId, companyId, adminUserId, session, questionRow).Question;
                    if (question != null)
                    {
                        response.Questions.Add(question);
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public QuestionCreateResponse CreateNewQuestion(string adminUserId,
                                                        string companyId,
                                                        string questionId,
                                                        string topicId,
                                                        string categoryId,
                                                        int questionType,
                                                        string questionContent,
                                                        string questionContentImageUrl,
                                                        string questionContentImageMd5,
                                                        string questionContentImageBackgroundColorCode,
                                                        int choiceType,
                                                        string firstChoiceContent,
                                                        string firstChoiceImageUrl,
                                                        string firstChoiceImageMd5,
                                                        string secondChoiceContent,
                                                        string secondChoiceImageUrl,
                                                        string secondChoiceImageMd5,
                                                        string thirdChoiceContent,
                                                        string thirdChoiceImageUrl,
                                                        string thirdChoiceImageMd5,
                                                        string fourthChoiceContent,
                                                        string fourthChoiceImageUrl,
                                                        string fourthChoiceImageMd5,
                                                        float timeAssignedForReading,
                                                        float timeAssignedForAnswering,
                                                        int difficultyLevel,
                                                        int scoreMultiplier,
                                                        double frequency,
                                                        int questionStatus)
        {
            QuestionCreateResponse response = new QuestionCreateResponse();
            response.Success = false;

            try
            {
                if (string.IsNullOrEmpty(questionId))
                {
                    questionId = UUIDGenerator.GenerateUniqueIDForQuestion();
                }

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                // Check question 
                if (!CheckQuestion(questionType, questionContent, questionContentImageUrl, questionContentImageMd5))
                {
                    Log.Error("Invalid question content");
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicQuestionMissing);
                    response.ErrorMessage = ErrorMessage.TopicQuestionMissing;
                    return response;
                }

                // Check choice
                if (!CheckChoice(choiceType,
                    firstChoiceContent,
                    secondChoiceContent,
                    thirdChoiceContent,
                    fourthChoiceContent,
                    firstChoiceImageUrl,
                    firstChoiceImageMd5,
                    secondChoiceImageUrl,
                    secondChoiceImageMd5,
                    thirdChoiceImageUrl,
                    thirdChoiceImageMd5,
                    fourthChoiceImageUrl,
                    fourthChoiceImageMd5))
                {
                    Log.Error("Invalid choices");
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicChoiceMissing);
                    response.ErrorMessage = ErrorMessage.TopicChoiceMissing;
                    return response;
                }

                PreparedStatement psQuestion = null;
                BoundStatement bsQuestion = null;
                DateTime currentDate = DateTime.UtcNow;

                if (string.IsNullOrEmpty(questionContentImageBackgroundColorCode))
                {
                    questionContentImageBackgroundColorCode = "#000000";
                }

                string firstChoiceId = UUIDGenerator.GenerateUniqueIDForQuizChallengeChoice();
                string secondChoiceId = UUIDGenerator.GenerateUniqueIDForQuizChallengeChoice();
                string thirdChoiceId = UUIDGenerator.GenerateUniqueIDForQuizChallengeChoice();
                string fourthChoiceId = UUIDGenerator.GenerateUniqueIDForQuizChallengeChoice();
                string correctAnswerId = firstChoiceId;

                // Text question
                if (questionType == QUESTION_TYPE_LONG || questionType == QUESTION_TYPE_NORMAL)
                {
                    if (choiceType == CHOICE_TYPE_TEXT)
                    {
                        psQuestion = session.Prepare(CQLGenerator.InsertStatement("challenge_question",
                                        new List<string> { "id", "topic_id", "type", "content", "choice_type", "first_choice_id", "first_choice", "second_choice_id", "second_choice", "third_choice_id", "third_choice", "fourth_choice_id", "fourth_choice", "answer", "time_answering", "time_reading", "difficulty_level", "score_multiplier", "frequency", "status", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                        bsQuestion = psQuestion.Bind(questionId, topicId, questionType, questionContent, choiceType, firstChoiceId, firstChoiceContent, secondChoiceId, secondChoiceContent, thirdChoiceId, thirdChoiceContent, fourthChoiceId, fourthChoiceContent, correctAnswerId, timeAssignedForAnswering, timeAssignedForReading, difficultyLevel, scoreMultiplier, frequency, questionStatus, adminUserId, DateHelper.ConvertDateToLong(currentDate), adminUserId, DateHelper.ConvertDateToLong(currentDate));
                    }
                    else
                    {
                        psQuestion = session.Prepare(CQLGenerator.InsertStatement("challenge_question",
                                       new List<string> { "id", "topic_id", "type", "content", "choice_type", "first_choice_id", "first_choice_image_url", "first_choice_image_md5", "second_choice_id", "second_choice_image_url", "second_choice_image_md5", "third_choice_id", "third_choice_image_url", "third_choice_image_md5", "fourth_choice_id", "fourth_choice_image_url", "fourth_choice_image_md5", "answer", "time_answering", "time_reading", "difficulty_level", "score_multiplier", "frequency", "status", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                        bsQuestion = psQuestion.Bind(questionId, topicId, questionType, questionContent, choiceType, firstChoiceId, firstChoiceImageUrl, firstChoiceImageMd5, secondChoiceId, secondChoiceImageUrl, secondChoiceImageMd5, thirdChoiceId, thirdChoiceImageUrl, thirdChoiceImageMd5, fourthChoiceId, fourthChoiceImageUrl, fourthChoiceImageMd5, correctAnswerId, timeAssignedForAnswering, timeAssignedForReading, difficultyLevel, scoreMultiplier, frequency, questionStatus, adminUserId, DateHelper.ConvertDateToLong(currentDate), adminUserId, DateHelper.ConvertDateToLong(currentDate));
                    }

                    session.Execute(bsQuestion);
                    response.Success = true;
                }
                // Image question
                else
                {
                    if (choiceType == CHOICE_TYPE_TEXT)
                    {
                        psQuestion = session.Prepare(CQLGenerator.InsertStatement("challenge_question",
                                        new List<string> { "id", "topic_id", "type", "content", "content_image_url", "content_image_md5", "content_image_background_color_code", "choice_type", "first_choice_id", "first_choice", "second_choice_id", "second_choice", "third_choice_id", "third_choice", "fourth_choice_id", "fourth_choice", "answer", "time_answering", "time_reading", "difficulty_level", "score_multiplier", "frequency", "status", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                        bsQuestion = psQuestion.Bind(questionId, topicId, questionType, questionContent, questionContentImageUrl, questionContentImageMd5, questionContentImageBackgroundColorCode, choiceType, firstChoiceId, firstChoiceContent, secondChoiceId, secondChoiceContent, thirdChoiceId, thirdChoiceContent, fourthChoiceId, fourthChoiceContent, correctAnswerId, timeAssignedForAnswering, timeAssignedForReading, difficultyLevel, scoreMultiplier, frequency, questionStatus, adminUserId, DateHelper.ConvertDateToLong(currentDate), adminUserId, DateHelper.ConvertDateToLong(currentDate));
                    }
                    else
                    {
                        psQuestion = session.Prepare(CQLGenerator.InsertStatement("challenge_question",
                                       new List<string> { "id", "topic_id", "type", "content", "content_image_url", "content_image_md5", "content_image_background_color_code", "choice_type", "first_choice_id", "first_choice_image_url", "first_choice_image_md5", "second_choice_id", "second_choice_image_url", "second_choice_image_md5", "third_choice_id", "third_choice_image_url", "third_choice_image_md5", "fourth_choice_id", "fourth_choice_image_url", "fourth_choice_image_md5", "answer", "time_answering", "time_reading", "difficulty_level", "score_multiplier", "frequency", "status", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                        bsQuestion = psQuestion.Bind(questionId, topicId, questionType, questionContent, questionContentImageUrl, questionContentImageMd5, questionContentImageBackgroundColorCode, choiceType, firstChoiceId, firstChoiceImageUrl, firstChoiceImageMd5, secondChoiceId, secondChoiceImageUrl, secondChoiceImageMd5, thirdChoiceId, thirdChoiceImageUrl, thirdChoiceImageMd5, fourthChoiceId, fourthChoiceImageUrl, fourthChoiceImageMd5, correctAnswerId, timeAssignedForAnswering, timeAssignedForReading, difficultyLevel, scoreMultiplier, frequency, questionStatus, adminUserId, DateHelper.ConvertDateToLong(currentDate), adminUserId, DateHelper.ConvertDateToLong(currentDate));
                    }

                    session.Execute(bsQuestion);
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public List<ChallengeQuestion> CreateChallengeQuestionsWithTopic(Topic topic, ISession session)
        {
            List<ChallengeQuestion> questions = new List<ChallengeQuestion>();

            try
            {
                PreparedStatement psQuestion = session.Prepare(CQLGenerator.CountStatement("challenge_question",
                    new List<string> { "topic_id", "status" }));
                BoundStatement bsQuestion = psQuestion.Bind(topic.TopicId, QuestionStatus.CODE_ACTIVE);
                Row questionCountRow = session.Execute(bsQuestion).FirstOrDefault();

                int totalNumberofActiveQuestions = (int)questionCountRow.GetValue<long>("count");

                if (topic.SelectedNumberOfQuestions <= totalNumberofActiveQuestions)
                {
                    psQuestion = session.Prepare(CQLGenerator.SelectStatement("challenge_question",
                    new List<string>(), new List<string> { "topic_id", "status" }));
                    bsQuestion = psQuestion.Bind(topic.TopicId, QuestionStatus.CODE_ACTIVE);
                    RowSet questionRowset = session.Execute(bsQuestion);

                    List<Dictionary<string, string>> unsortedQuestions = new List<Dictionary<string, string>>();

                    foreach (Row questionRow in questionRowset)
                    {
                        string questionId = questionRow.GetValue<string>("id");
                        int difficultyLevel = questionRow.GetValue<int>("difficulty_level");

                        Dictionary<string, string> questionDict = new Dictionary<string, string>();

                        questionDict.Add("id", questionId);
                        questionDict.Add("difficultyLevel", difficultyLevel.ToString());

                        unsortedQuestions.Add(questionDict);
                    }

                    List<Dictionary<string, string>> sortedQuestionIds = SortRandomQuestionsByDifficulty(unsortedQuestions, topic.SelectedNumberOfQuestions);

                    foreach (Dictionary<string, string> sortedQuestion in sortedQuestionIds)
                    {
                        string questionId = sortedQuestion["id"];
                        ChallengeQuestion question = SelectQuestion(questionId, topic.TopicId, null, null, session, null, true).Question;
                        if (question == null)
                        {
                            return null;
                        }

                        questions.Add(question);
                    }
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return questions;
        }

        private List<Dictionary<string, string>> SortRandomQuestionsByDifficulty(List<Dictionary<string, string>> questions, int selectedNumberOfQuestions)
        {
            List<Dictionary<string, string>> sortedQuestions = new List<Dictionary<string, string>>();

            try
            {
                int numberOfEasy = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 1).Count();
                int numberOfNormal = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 2).Count();
                int numberOfHard = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 3).Count();

                List<Dictionary<string, string>> easyList = new List<Dictionary<string, string>>();
                List<Dictionary<string, string>> normalList = new List<Dictionary<string, string>>();
                List<Dictionary<string, string>> hardList = new List<Dictionary<string, string>>();

                if (numberOfEasy + numberOfNormal + numberOfHard == selectedNumberOfQuestions)
                {
                    easyList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 1).OrderBy(question => Guid.NewGuid()).Take(numberOfEasy).ToList();
                    normalList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 2).OrderBy(question => Guid.NewGuid()).Take(numberOfNormal).ToList();
                    hardList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 3).OrderBy(question => Guid.NewGuid()).Take(numberOfHard).ToList();
                }
                else
                {
                    int hardBonus = 1;

                    if (numberOfHard == 0)
                    {
                        int easyHalf = (int)Math.Ceiling(selectedNumberOfQuestions / 2.0);
                        int normalHalf = selectedNumberOfQuestions - easyHalf;

                        // If number of easy is less than required => number of normal must be more than required
                        if (numberOfEasy < easyHalf)
                        {
                            easyList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 1).OrderBy(question => Guid.NewGuid()).Take(numberOfEasy).ToList();
                            normalList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 2).OrderBy(question => Guid.NewGuid()).Take(selectedNumberOfQuestions - numberOfEasy).ToList();
                        }
                        else
                        {
                            // Number of easy is more than required

                            // Number of normal is more than required
                            if (numberOfNormal > normalHalf)
                            {
                                easyList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 1).OrderBy(question => Guid.NewGuid()).Take(easyHalf).ToList();
                                normalList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 2).OrderBy(question => Guid.NewGuid()).Take(normalHalf).ToList();
                            }
                            // Number of normal is less than required
                            else
                            {
                                normalList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 2).OrderBy(question => Guid.NewGuid()).Take(numberOfNormal).ToList();
                                easyList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 1).OrderBy(question => Guid.NewGuid()).Take(selectedNumberOfQuestions - numberOfNormal).ToList();
                            }
                        }
                    }
                    else
                    {
                        int easyHalf = (int)Math.Ceiling((selectedNumberOfQuestions - hardBonus) / 2.0);
                        int normalHalf = selectedNumberOfQuestions - easyHalf - hardBonus;

                        hardList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 3).OrderBy(question => Guid.NewGuid()).Take(hardBonus).ToList();

                        // If number of easy is less than required => number of normal must be more than required
                        if (numberOfEasy < easyHalf)
                        {
                            easyList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 1).OrderBy(question => Guid.NewGuid()).Take(numberOfEasy).ToList();
                            normalList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 2).OrderBy(question => Guid.NewGuid()).Take(selectedNumberOfQuestions - numberOfEasy - hardBonus).ToList();
                        }
                        else
                        {
                            // Number of easy is more than required

                            // Number of normal is more than required
                            if (numberOfNormal > normalHalf)
                            {
                                easyList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 1).OrderBy(question => Guid.NewGuid()).Take(easyHalf).ToList();
                                normalList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 2).OrderBy(question => Guid.NewGuid()).Take(normalHalf).ToList();
                            }
                            // Number of normal is less than required
                            else
                            {
                                normalList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 2).OrderBy(question => Guid.NewGuid()).Take(numberOfNormal).ToList();
                                easyList = questions.Where(question => Int16.Parse(question["difficultyLevel"]) == 1).OrderBy(question => Guid.NewGuid()).Take(selectedNumberOfQuestions - numberOfNormal - hardBonus).ToList();
                            }
                        }
                    }
                }

                sortedQuestions.AddRange(easyList);
                sortedQuestions.AddRange(normalList);
                sortedQuestions.AddRange(hardList);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return sortedQuestions;
        }


        private bool CheckChoice(int choiceType,
                                 string firstChoiceContent,
                                 string secondChoiceContent,
                                 string thirdChoiceContent,
                                 string fourthChoiceContent,
                                 string firstChoiceImageUrl,
                                 string firstChoiceImageMd5,
                                 string secondChoiceImageUrl,
                                 string secondChoiceImageMd5,
                                 string thirdChoiceImageUrl,
                                 string thirdChoiceImageMd5,
                                 string fourthChoiceImageUrl,
                                 string fourthChoiceImageMd5)
        {
            bool isValid = true;

            if (choiceType == CHOICE_TYPE_TEXT)
            {
                if (string.IsNullOrEmpty(firstChoiceContent) || string.IsNullOrEmpty(secondChoiceContent) || string.IsNullOrEmpty(thirdChoiceContent) || string.IsNullOrEmpty(fourthChoiceContent))
                {
                    return false;
                }
            }
            else
            {
                if ((string.IsNullOrEmpty(firstChoiceImageUrl) && string.IsNullOrEmpty(firstChoiceImageMd5)) || (string.IsNullOrEmpty(secondChoiceImageUrl) && string.IsNullOrEmpty(secondChoiceImageMd5)) || (string.IsNullOrEmpty(thirdChoiceImageUrl) && string.IsNullOrEmpty(thirdChoiceImageMd5)) || (string.IsNullOrEmpty(fourthChoiceImageUrl) && string.IsNullOrEmpty(fourthChoiceImageMd5)))
                {
                    return false;
                }

            }

            return isValid;
        }

        private bool CheckQuestion(int questionType, string questionContext, string imageQuestionUrl, string imageQuestionMd5)
        {
            bool isValid = true;

            if (questionType == QUESTION_TYPE_LONG || questionType == QUESTION_TYPE_NORMAL)
            {
                if (string.IsNullOrEmpty(questionContext))
                {
                    isValid = false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(questionContext) || string.IsNullOrEmpty(imageQuestionUrl) || string.IsNullOrEmpty(imageQuestionMd5))
                {
                    isValid = false;
                }
            }

            return isValid;
        }

        public int SelectNumberOfQuestions(string topicId, int questionStatus, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psQuestion = null;
                BoundStatement bsQuestion = null;

                Row questionRow = null;

                if (questionStatus == QuestionStatus.CODE_ACTIVE)
                {
                    psQuestion = session.Prepare(CQLGenerator.CountStatement("challenge_question",
                        new List<string> { "topic_id", "status" }));
                    bsQuestion = psQuestion.Bind(topicId, QuestionStatus.CODE_ACTIVE);
                    questionRow = session.Execute(bsQuestion).FirstOrDefault();
                }
                else if (questionStatus == QuestionStatus.CODE_ALL_EXCEPT_DELETE)
                {
                    psQuestion = session.Prepare(CQLGenerator.CountStatement("challenge_question",
                       new List<string> { "topic_id" }));
                    bsQuestion = psQuestion.Bind(topicId);
                    questionRow = session.Execute(bsQuestion).FirstOrDefault();
                }

                if (questionRow != null)
                {
                    number = (int)questionRow.GetValue<long>("count");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        public QuestionUpdateResponse UpdateQuestion(string adminUserId,
                                                    string companyId,
                                                    string questionId,
                                                    string topicId,
                                                    string categoryId,
                                                    int questionType,
                                                    string questionContent,
                                                    string questionContentImageUrl,
                                                    string questionContentImageMd5,
                                                    string questionContentImageBackgroundColorCode,
                                                    int choiceType,
                                                    string firstChoiceContent,
                                                    string firstChoiceImageUrl,
                                                    string firstChoiceImageMd5,
                                                    string secondChoiceContent,
                                                    string secondChoiceImageUrl,
                                                    string secondChoiceImageMd5,
                                                    string thirdChoiceContent,
                                                    string thirdChoiceImageUrl,
                                                    string thirdChoiceImageMd5,
                                                    string fourthChoiceContent,
                                                    string fourthChoiceImageUrl,
                                                    string fourthChoiceImageMd5,
                                                    float timeAssignedForReading,
                                                    float timeAssignedForAnswering,
                                                    int difficultyLevel,
                                                    int scoreMultiplier,
                                                    double frequency,
                                                    int newQuestionStatus)
        {
            QuestionUpdateResponse response = new QuestionUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                Row questionRow = vh.ValidateTopicQuestion(questionId, topicId, session);

                if (questionRow == null)
                {
                    Log.Error("Invalid questionId: " + questionId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicQuestionInvalid);
                    response.ErrorMessage = ErrorMessage.TopicQuestionInvalid;
                    return response;
                }

                // Check question 
                if (!CheckQuestion(questionType, questionContent, questionContentImageUrl, questionContentImageMd5))
                {
                    Log.Error("Invalid question content");
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicQuestionMissing);
                    response.ErrorMessage = ErrorMessage.TopicQuestionMissing;
                    return response;
                }

                // Check choice
                if (!CheckChoice(choiceType,
                    firstChoiceContent,
                    secondChoiceContent,
                    thirdChoiceContent,
                    fourthChoiceContent,
                    firstChoiceImageUrl,
                    firstChoiceImageMd5,
                    secondChoiceImageUrl,
                    secondChoiceImageMd5,
                    thirdChoiceImageUrl,
                    thirdChoiceImageMd5,
                    fourthChoiceImageUrl,
                    fourthChoiceImageMd5))
                {
                    Log.Error("Invalid choices");
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicChoiceMissing);
                    response.ErrorMessage = ErrorMessage.TopicChoiceMissing;
                    return response;
                }

                if (newQuestionStatus != QuestionStatus.CODE_DELETED)
                {
                    // Question status can only be active/ hidden
                    // Check status first
                    bool isUpdateStatus = true;
                    int currentTopicStatus = topicRow.GetValue<int>("status");
                    int currentTopicSelectedQuestion = topicRow.GetValue<int>("selected_number_of_questions");
                    int currentQuestionStatus = questionRow.GetValue<int>("status");

                    if(currentQuestionStatus == newQuestionStatus)
                    {
                        isUpdateStatus = false;
                    }
                    else
                    {
                        if (newQuestionStatus == (int)QuestionStatus.CODE_HIDDEN)
                        {
                            //int selectedNumberOfQuestions = topicRow.GetValue<int>("selected_number_of_questions");
                            int numberOfActiveQuestion = SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ACTIVE, session) - 1;

                            if (numberOfActiveQuestion < currentTopicSelectedQuestion)
                            {
                                // Still let the update go
                                response.ErrorCode = Convert.ToInt16(ErrorCode.TopicLessActiveQuestions);
                                response.ErrorMessage = ErrorMessage.TopicLessActiveQuestions;
                                isUpdateStatus = false;
                            }

                            isUpdateStatus = true;
                        }
                    }
                    


                    if (string.IsNullOrEmpty(questionContentImageBackgroundColorCode))
                    {
                        questionContentImageBackgroundColorCode = "#000000";
                    }

                    PreparedStatement psQuestion = session.Prepare(CQLGenerator.UpdateStatement("challenge_question",
                        new List<string> { "topic_id", "id" }, new List<string> { "type", "content", "content_image_url", "content_image_md5", "content_image_background_color_code", "choice_type", "first_choice", "first_choice_image_url", "first_choice_image_md5", "second_choice", "second_choice_image_url", "second_choice_image_md5", "third_choice", "third_choice_image_url", "third_choice_image_md5", "fourth_choice", "fourth_choice_image_url", "fourth_choice_image_md5", "time_answering", "time_reading", "difficulty_level", "score_multiplier", "frequency", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    BoundStatement bsQuestion = psQuestion.Bind(questionType, questionContent, questionContentImageUrl, questionContentImageMd5, questionContentImageBackgroundColorCode, choiceType, firstChoiceContent, firstChoiceImageUrl, firstChoiceImageMd5, secondChoiceContent, secondChoiceImageUrl, secondChoiceImageMd5, thirdChoiceContent, thirdChoiceImageUrl, thirdChoiceImageMd5, fourthChoiceContent, fourthChoiceImageUrl, fourthChoiceImageMd5, timeAssignedForAnswering, timeAssignedForReading, difficultyLevel, scoreMultiplier, frequency, adminUserId, DateHelper.ConvertDateToLong(DateTime.UtcNow), topicId, questionId);
                    session.Execute(bsQuestion);

                    if (isUpdateStatus)
                    {
                        response = UpdateQuestionStatus(adminUserId, companyId, topicId, categoryId, questionId, newQuestionStatus, topicRow, session);
                    }

                    response.Success = true;
                }
                else
                {
                    response = UpdateQuestionStatus(adminUserId, companyId, topicId, categoryId, questionId, QuestionStatus.CODE_DELETED, topicRow, session);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public QuestionUpdateResponse UpdateQuestionStatus(string adminUserId,
                                                           string companyId,
                                                           string topicId,
                                                           string categoryId,
                                                           string questionId,
                                                           int status,
                                                           Row topicRow = null,
                                                           ISession session = null)
        {
            QuestionUpdateResponse response = new QuestionUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (session == null)
                {
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    // Check topic category
                    Row topicCategoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);

                    if (topicCategoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                        response.ErrorMessage = ErrorMessage.CategoryInvalid;
                        return response;
                    }

                    // Check topic row
                    topicRow = vh.ValidateTopic(companyId, categoryId, topicId, session);

                    if (topicRow == null)
                    {
                        Log.Error("Invalid topicId: " + topicId);
                        response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                        response.ErrorMessage = ErrorMessage.TopicInvalid;
                        return response;
                    }
                }


                PreparedStatement psChallengeQuestion = null;
                BoundStatement bsChallengeQuestion = null;

                BatchStatement batchStatement = new BatchStatement();

                string topicTitle = topicRow.GetValue<string>("title");
                int topicStatus = topicRow.GetValue<int>("status");
                DateTimeOffset createdTimestamp = topicRow.GetValue<DateTimeOffset>("created_on_timestamp");

                // Active or hidden topic
                if (topicStatus != (int)Topic.TopicStatus.CODE_UNLISTED)
                {
                    // So long as new status is deleted or hidden
                    if (status == (int)QuestionStatus.CODE_DELETED || status == (int)QuestionStatus.CODE_HIDDEN)
                    {
                        int selectedNumberOfQuestions = topicRow.GetValue<int>("selected_number_of_questions");
                        int numberOfActiveQuestion = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ACTIVE, session) - 1;

                        if (numberOfActiveQuestion < selectedNumberOfQuestions)
                        {
                            response.ErrorCode = Convert.ToInt16(ErrorCode.TopicLessActiveQuestions);
                            response.ErrorMessage = ErrorMessage.TopicLessActiveQuestions;
                            return response;
                        }

                        // Deleted
                        if (status == (int)QuestionStatus.CODE_DELETED)
                        {
                            psChallengeQuestion = session.Prepare(CQLGenerator.DeleteStatement("challenge_question",
                                new List<string> { "topic_id", "id" }));
                            bsChallengeQuestion = psChallengeQuestion.Bind(topicId, questionId);
                        }
                        // Hidden
                        else
                        {
                            psChallengeQuestion = session.Prepare(CQLGenerator.UpdateStatement("challenge_question",
                                new List<string> { "topic_id", "id" }, new List<string> { "status" }, new List<string>()));
                            bsChallengeQuestion = psChallengeQuestion.Bind(status, topicId, questionId);
                        }


                        session.Execute(bsChallengeQuestion);

                        // Remove from leaderboard
                        List<User> users = new User().GetAllUserForAdmin(adminUserId, companyId, null, 0, 0, session).Users;

                        ISession analyticSession = cm.getAnalyticSession();
                        Analytic analytic = new Analytic();

                        foreach (User user in users)
                        {
                            analytic.UpdateLeaderboardStatsByQuestion(user.UserId, topicId, questionId, null, user.Departments[0].Id, companyId, false, session);
                        }
                    }


                }
                // Unlisted topic
                else
                {
                    if (status == (int)QuestionStatus.CODE_DELETED)
                    {
                        psChallengeQuestion = session.Prepare(CQLGenerator.DeleteStatement("challenge_question",
                            new List<string> { "topic_id", "id" }));
                        bsChallengeQuestion = psChallengeQuestion.Bind(topicId, questionId);
                        session.Execute(bsChallengeQuestion);
                    }
                    else
                    {
                        psChallengeQuestion = session.Prepare(CQLGenerator.UpdateStatement("challenge_question",
                            new List<string> { "topic_id", "id" }, new List<string> { "status" }, new List<string>()));
                        bsChallengeQuestion = psChallengeQuestion.Bind(status, topicId, questionId);
                        session.Execute(bsChallengeQuestion);
                    }

                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        #region Play game
        public QuestionSelectAllResponse SelectAllQuestionsForChallenge(string requesterUserId,
                                                                        string companyId,
                                                                        string challengeId)
        {
            QuestionSelectAllResponse response = new QuestionSelectAllResponse();
            response.Questions = new List<ChallengeQuestion>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row historyRow = vh.ValidateChallenge(challengeId, companyId, session);

                if (historyRow != null && historyRow.GetValue<DateTime?>("completed_on_timestamp") == null)
                {
                    List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");

                    if (playerIds.Contains(requesterUserId))
                    {
                        PreparedStatement psFullHistory = session.Prepare(CQLGenerator.SelectStatement("full_challenge_history",
                            new List<string>(), new List<string> { "challenge_id", "company_id" }));
                        BoundStatement bsFullHistory = psFullHistory.Bind(challengeId, companyId);
                        RowSet fullHistoryRowset = session.Execute(bsFullHistory);

                        foreach (Row fullHistoryRow in fullHistoryRowset)
                        {
                            string questionId = fullHistoryRow.GetValue<string>("question_id");
                            response.Questions.Add(SelectQuestion(questionId, null, null, null, session, fullHistoryRow, false, true).Question);
                        }

                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Invalid playerId: " + requesterUserId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalidPlayer);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    }
                }
                else
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }


        public QuestionAnswerResponse AnswerChallengeQuestion(string challengeId,
                                                              string companyId,
                                                              string userId,
                                                              string answer,
                                                              float timeTaken,
                                                              string questionId,
                                                              string topicId,
                                                              int round)
        {
            QuestionAnswerResponse response = new QuestionAnswerResponse();
            response.Success = false;

            try
            {

                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(userId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row historyRow = vh.ValidateChallenge(challengeId, companyId, mainSession);

                if (historyRow != null)
                {
                    if (historyRow != null && historyRow.GetValue<DateTime?>("completed_on_timestamp") == null)
                    {
                        List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");
                        int selectedNumberOfQuestion = Int16.Parse(historyRow["selected_number_of_questions"].ToString());
                        bool isLive = historyRow.GetValue<bool>("is_live");
                        if (playerIds.Contains(userId))
                        {
                            bool isInitiator = true ? playerIds.FindIndex(user => user.Equals(userId)) == 0 : false;

                            string correctAnswer = string.Empty;
                            string answerOfOpponent = string.Empty;
                            bool isCorrect = false;
                            float timeAssigned = 0.0f;
                            int scoreMultiplier = 0;
                            float timeTakenByOpponent = 0.0f;
                            string opponentId = string.Empty;
                            bool isLastRound = false;

                            Dictionary<string, object> resultDict = SelectOpponentHistory(isInitiator, challengeId, companyId, round, mainSession);
                            opponentId = playerIds[isInitiator ? 1 : 0];
                            timeAssigned = (float)resultDict["timeAssigned"];
                            timeTakenByOpponent = resultDict["timeTakenByOpponent"] == null ? 0.0f : (float)resultDict["timeTakenByOpponent"];
                            correctAnswer = (string)resultDict["correctAnswer"];
                            answerOfOpponent = resultDict["answerOfOpponent"] == null ? "0" : (string)resultDict["answerOfOpponent"];
                            scoreMultiplier = (int)resultDict["scoreMultiplier"];
                            isCorrect = correctAnswer.Equals(answer) ? true : false;
                            isLastRound = selectedNumberOfQuestion == round ? true : false;

                            Analytic analytic = new Analytic();

                            int score = 0;
                            if (isCorrect)
                            {
                                score = CalculateScore(timeAssigned, timeTaken, isLastRound, scoreMultiplier);
                                analytic.UpdateExpOfUser(userId, score, analyticSession);
                            }

                            PreparedStatement preparedStatement = null;
                            BoundStatement boundStatement = null;

                            if (isInitiator)
                            {
                                preparedStatement = mainSession.Prepare(CQLGenerator.UpdateStatement("full_challenge_history",
                                           new List<string> { "challenge_id", "company_id", "question_order" }, new List<string> { "answer_of_initiated_user", "time_taken_by_initiated_user", "score_of_initiated_user" }, new List<string>()));
                            }
                            else
                            {
                                preparedStatement = mainSession.Prepare(CQLGenerator.UpdateStatement("full_challenge_history",
                                    new List<string> { "challenge_id", "company_id", "question_order" }, new List<string> { "answer_of_challenged_user", "time_taken_by_challenged_user", "score_of_challenged_user" }, new List<string>()));
                            }

                            mainSession.Execute(preparedStatement.Bind(answer, timeTaken, score, challengeId, companyId, round));
                            List<Department> departments = new Department().GetAllDepartmentByUserId(userId, companyId, mainSession).Departments;
                            analytic.UpdateLeaderboardStatsByQuestion(userId, topicId, questionId, challengeId, departments[0].Id, companyId, isCorrect, analyticSession);

                            #region Challenge complete
                            if (round == selectedNumberOfQuestion)
                            {
                                int sumOfInitiator = 0;
                                int sumOfChallenged = 0;
                                bool isCompleted = true;
                                for (int index = 0; index < selectedNumberOfQuestion; index++)
                                {
                                    preparedStatement = mainSession.Prepare(CQLGenerator.SelectStatement("full_challenge_history",
                                        new List<string> { "score_of_initiated_user", "score_of_challenged_user" }, new List<string> { "challenge_id", "company_id", "question_order" }));
                                    boundStatement = preparedStatement.Bind(challengeId, companyId, index + 1);
                                    Row fullHistoryRow = mainSession.Execute(boundStatement).FirstOrDefault();

                                    if (fullHistoryRow["score_of_initiated_user"] != null && fullHistoryRow["score_of_challenged_user"] != null)
                                    {
                                        sumOfInitiator += fullHistoryRow.GetValue<int>("score_of_initiated_user");
                                        sumOfChallenged += fullHistoryRow.GetValue<int>("score_of_challenged_user");
                                    }
                                    else
                                    {
                                        isCompleted = false;
                                        break;
                                    }
                                }

                                if (isCompleted)
                                {
                                    BatchStatement batchStatement = new BatchStatement();
                                    string winnerId = string.Empty;
                                    string initiatorUserId = playerIds[0];
                                    string challengedUserId = playerIds[1];
                                    if (sumOfInitiator != sumOfChallenged)
                                    {
                                        winnerId = sumOfInitiator > sumOfChallenged ? playerIds[0] : playerIds[1];

                                        Log.Debug("Winner is: " + winnerId);

                                        preparedStatement = mainSession.Prepare(CQLGenerator.UpdateStatement("challenge_history",
                                            new List<string> { "company_id", "id" }, new List<string> { "winner_id" }, new List<string>()));

                                        boundStatement = preparedStatement.Bind(winnerId, companyId, challengeId);
                                        batchStatement.Add(boundStatement);
                                    }

                                    preparedStatement = mainSession.Prepare(CQLGenerator.UpdateStatement("challenge_history",
                                        new List<string> { "company_id", "id" }, new List<string> { "completed_on_timestamp" }, new List<string>()));

                                    boundStatement = preparedStatement.Bind(DateTime.UtcNow, companyId, challengeId);
                                    batchStatement.Add(boundStatement);

                                    if (!isInitiator)
                                    {
                                        // Update challenged table (Play safe)
                                        preparedStatement = mainSession.Prepare(CQLGenerator.DeleteStatement("challenge_history_by_initiated_timestamp",
                                            new List<string> { "challenge_id", "challenged_user_id", "initiated_on_timestamp" }));
                                        batchStatement = batchStatement.Add(preparedStatement.Bind(challengeId, challengedUserId, historyRow.GetValue<DateTimeOffset>("initiated_user_started_on_timestamp")));
                                    }


                                    // Update history
                                    preparedStatement = mainSession.Prepare(CQLGenerator.InsertStatement("challenge_history_by_user",
                                       new List<string> { "challenge_id", "user_id", "opponent_user_id", "completed_on_timestamp" }));
                                    boundStatement = preparedStatement.Bind(challengeId, initiatorUserId, challengedUserId, DateTime.UtcNow);

                                    batchStatement.Add(boundStatement);

                                    preparedStatement = mainSession.Prepare(CQLGenerator.InsertStatement("challenge_history_by_user",
                                        new List<string> { "challenge_id", "user_id", "opponent_user_id", "completed_on_timestamp" }));
                                    boundStatement = preparedStatement.Bind(challengeId, challengedUserId, initiatorUserId, DateTime.UtcNow);

                                    batchStatement.Add(boundStatement);

                                    preparedStatement = mainSession.Prepare(CQLGenerator.InsertStatement("challenge_history_by_topic",
                                        new List<string> { "challenge_id", "topic_id", "user_id", "completed_on_timestamp" }));
                                    boundStatement = preparedStatement.Bind(challengeId, topicId, initiatorUserId, DateTime.UtcNow);

                                    batchStatement.Add(boundStatement);

                                    preparedStatement = mainSession.Prepare(CQLGenerator.InsertStatement("challenge_history_by_topic",
                                        new List<string> { "challenge_id", "topic_id", "user_id", "completed_on_timestamp" }));
                                    boundStatement = preparedStatement.Bind(challengeId, topicId, challengedUserId, DateTime.UtcNow);

                                    batchStatement.Add(boundStatement);

                                    mainSession.Execute(batchStatement);

                                    // Update analytic challenge stats
                                    if (string.IsNullOrEmpty(winnerId))
                                    {
                                        analytic.UpdateChallengeStats(companyId, initiatorUserId, Analytic.ChallengeStatType.Draw, analyticSession);
                                        analytic.UpdateChallengeStats(companyId, challengedUserId, Analytic.ChallengeStatType.Draw, analyticSession);
                                    }
                                    else
                                    {
                                        if (winnerId.Equals(initiatorUserId))
                                        {
                                            analytic.UpdateChallengeStats(companyId, initiatorUserId, Analytic.ChallengeStatType.Win, analyticSession);
                                            analytic.UpdateChallengeStats(companyId, challengedUserId, Analytic.ChallengeStatType.Lost, analyticSession);
                                        }
                                        else
                                        {
                                            analytic.UpdateChallengeStats(companyId, initiatorUserId, Analytic.ChallengeStatType.Lost, analyticSession);
                                            analytic.UpdateChallengeStats(companyId, challengedUserId, Analytic.ChallengeStatType.Win, analyticSession);
                                        }
                                    }

                                }
                            }
                            #endregion

                            response.AnswerOfOpponent = answerOfOpponent;
                            response.CorrectAnswer = correctAnswer;
                            response.IsCorrect = isCorrect;
                            response.IsInitiator = isInitiator;
                            response.IsLastRound = isLastRound;
                            response.IsLive = isLive;
                            response.OpponentId = opponentId;
                            response.TimeTakenByOpponent = timeTakenByOpponent;
                            response.Success = true;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid playerId: " + userId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalidPlayer);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    }
                }
                else
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private Dictionary<string, object> SelectOpponentHistory(bool isInitiator, string challengeId, string companyId, int round, ISession session)
        {
            Dictionary<string, object> resultDict = new Dictionary<string, object>();
            try
            {
                string columnAnswerOfUser = isInitiator ? "answer_of_challenged_user" : "answer_of_initiated_user";
                string columnTimeTakenByUser = isInitiator ? "time_taken_by_challenged_user" : "time_taken_by_initiated_user";

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("full_challenge_history",
                                            new List<string> { "answer", columnAnswerOfUser, columnTimeTakenByUser, "time_answering", "score_multiplier" }, new List<string> { "challenge_id", "company_id", "question_order" }));
                BoundStatement bs = ps.Bind(challengeId, companyId, round);
                Row opponentHistoryRow = session.Execute(bs).FirstOrDefault();

                resultDict.Add("timeAssigned", opponentHistoryRow.GetValue<float>("time_answering"));
                resultDict.Add("correctAnswer", opponentHistoryRow.GetValue<string>("answer"));
                resultDict.Add("scoreMultiplier", opponentHistoryRow.GetValue<int>("score_multiplier"));
                resultDict.Add("timeTakenByOpponent", opponentHistoryRow[columnTimeTakenByUser] == null ? 0.0f : opponentHistoryRow.GetValue<float>(columnTimeTakenByUser));
                resultDict.Add("answerOfOpponent", opponentHistoryRow[columnAnswerOfUser] == null ? "0" : opponentHistoryRow.GetValue<string>(columnAnswerOfUser));

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return resultDict;
        }

        private int CalculateScore(float timeAssigned, float timeTaken, bool isBonusQuestion, int scoreMultiplier)
        {
            int maxScoreForNormal = Int16.Parse(ConfigurationManager.AppSettings["score_max_for_normal_question"].ToString());
            int maxScoreForBonus = Int16.Parse(ConfigurationManager.AppSettings["score_max_for_bonus_question"].ToString());

            double halfPoint = (isBonusQuestion ? maxScoreForBonus : maxScoreForNormal) / 2.0;

            double timeScore = ((timeAssigned - timeTaken) / timeAssigned) * halfPoint;

            int totalScore = (int)Math.Round(timeScore + halfPoint) * scoreMultiplier;

            return totalScore;
        }
        #endregion

    }
}