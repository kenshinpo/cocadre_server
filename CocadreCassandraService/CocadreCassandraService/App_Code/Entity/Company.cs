﻿using System;
using System.Runtime.Serialization;

namespace CocadreCassandraService.Entity
{
    [DataContract]
    public class Company
    {
        [DataMember(EmitDefaultValue = false)]
        public String CompanyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public String CompanyLogoUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public String CompanyTitle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public String UserId { get; set; }

        [DataMember]
        public bool IsPasswordDefault { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User Admin { get; set; }

        //[DataMember(EmitDefaultValue = false)]
        //public String AdminUserId { get; set; }

        //[DataMember(EmitDefaultValue = false)]
        //public String AdminUserEmail { get; set; }

        //[DataMember(EmitDefaultValue = false)]
        //public User.AccountType AccountType { get; set; }
    }
}