﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Runtime.Serialization;
using Cassandra;
using CocadreCassandraService.App_GlobalResources;
using CocadreCassandraService.Connection;
using CocadreCassandraService.Error.Classes;
using CocadreCassandraService.ServiceResponses;
using CocadreCassandraService.Validation;
using log4net;

namespace CocadreCassandraService.Entity
{
    [DataContract]
    public class AccountKind
    {
        public static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [DataMember(EmitDefaultValue = false)]
        public int Order { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int StatusCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TypeCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public String Title { get; set; }


        public AccountKindListResponse GetAllAccountKind(String adminUserId, String companyId)
        {
            AccountKindListResponse response = new AccountKindListResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Collect all account type and status.
                List<AccountKind> accountKinds = new List<AccountKind>();
                accountKinds.Add(new AccountKind { Order = 0, StatusCode = new User.AccountStatus(User.AccountStatus.CODE_ACTIVE).Code, TypeCode = 0, Title = new User.AccountStatus(User.AccountStatus.CODE_ACTIVE).Title });
                accountKinds.Add(new AccountKind { Order = 1, StatusCode = 0, TypeCode = new User.AccountType(User.AccountType.CODE_ADMIN).Code, Title = new User.AccountType(User.AccountType.CODE_ADMIN).Title });
                accountKinds.Add(new AccountKind { Order = 2, StatusCode = 0, TypeCode = new User.AccountType(User.AccountType.CODE_MODERATER).Code, Title = new User.AccountType(User.AccountType.CODE_MODERATER).Title });
                accountKinds.Add(new AccountKind { Order = 3, StatusCode = 0, TypeCode = new User.AccountType(User.AccountType.CODE_NORMAL_USER).Code, Title = new User.AccountType(User.AccountType.CODE_NORMAL_USER).Title });
                accountKinds.Add(new AccountKind { Order = 4, StatusCode = new User.AccountStatus(User.AccountStatus.CODE_SUSPENEDED).Code, TypeCode = 0, Title = new User.AccountStatus(User.AccountStatus.CODE_SUSPENEDED).Title });
                accountKinds.Add(new AccountKind { Order = 5, StatusCode = new User.AccountStatus(User.AccountStatus.CODE_DELETING).Code, TypeCode = 0, Title = ConfigurationManager.AppSettings["user_status_title_recently_deleted"] });
                response.AccountKinds = accountKinds;
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }
    }
}