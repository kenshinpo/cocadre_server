﻿using Cassandra;
using CocadreCassandraService.App_GlobalResources;
using CocadreCassandraService.Connection;
using CocadreCassandraService.CQL;
using CocadreCassandraService.Date;
using CocadreCassandraService.Error.Classes;
using CocadreCassandraService.IDGenerator;
using CocadreCassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;
using CocadreCassandraService.ServiceResponses;
using Amazon.S3;
using Amazon;
using Amazon.S3.Model;

namespace CocadreCassandraService.Entity
{
    [DataContract]
    public class Feed
    {
        [DataMember(EmitDefaultValue = false)]
        public string FeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FeedType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedText FeedText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedImage FeedImage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedVideo FeedVideo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedSharedUrl FeedSharedUrl { get; set; }

        [DataMember]
        public int NegativePoints { get; set; }

        [DataMember]
        public int PositivePoints { get; set; }

        [DataMember]
        public int NumberOfComments { get; set; }

        [DataMember]
        public bool HasVoted { get; set; }

        [DataMember]
        public bool IsUpVoted { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User User { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        public static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public FeedAuthenticationResponse AuthenticateUserForPostingFeed(string companyId,
                                                                         string userId)
        {
            FeedAuthenticationResponse response = new FeedAuthenticationResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(userId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    string feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                    response.FeedId = feedId;

                    response.IsAuthenticatedToPost = true;
                    response.Success = true;
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;

        }
        public FeedCreateResponse CreateFeedTextPost(string feedId,
                                                     string creatorUserId,
                                                     string companyId,
                                                     string content,
                                                     List<string> targetedDepartmentIds,
                                                     List<string> targetedUserIds)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(content))
                    {
                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        foreach (BoundStatement boundStatement in CreateFeedPrivacy(creatorUserId,
                                                                                    feedId,
                                                                                    companyId,
                                                                                    Int32.Parse(FeedTypeCode.TextPost),
                                                                                    content,
                                                                                    targetedDepartmentIds,
                                                                                    targetedUserIds, session))
                        {
                            batchStatement = batchStatement.Add(boundStatement);
                        }

                        PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_text",
                                new List<string> { "id", "content", "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(feedId, content, true, creatorUserId, DateHelper.ConvertDateToLong(DateTime.UtcNow)));

                        session.Execute(batchStatement);

                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed text post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextContent;
                        response.Success = false;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedCreateResponse CreateFeedImagePost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      List<string> imageUrls,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (imageUrls.Count > 0)
                    {
                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        foreach (BoundStatement boundStatement in CreateFeedPrivacy(creatorUserId,
                                                                                    feedId,
                                                                                    companyId,
                                                                                    Int32.Parse(FeedTypeCode.ImagePost),
                                                                                    caption,
                                                                                    targetedDepartmentIds,
                                                                                    targetedUserIds,
                                                                                    session))
                        {
                            batchStatement = batchStatement.Add(boundStatement);
                        }

                        PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_image",
                            new List<string> { "id", "caption", "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(feedId, caption, true, creatorUserId, DateHelper.ConvertDateToLong(DateTime.UtcNow)));

                        int order = 1;
                        foreach (string imageUrl in imageUrls)
                        {
                            string imageId = UUIDGenerator.GenerateUniqueIDForFeedMedia();

                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_image_url",
                                new List<string> { "id", "feed_id", "url", "in_order", "is_valid" }));
                            batchStatement.Add(preparedStatement.Bind(imageId, feedId, imageUrl, order, true));

                            order += 1;
                        }

                        session.Execute(batchStatement);

                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed image post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidImageContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidImageContent;
                        response.Success = false;
                    }

                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedCreateResponse CreateFeedVideoPost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      string videoUrl,
                                                      string videoThumbnailUrl,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(videoUrl))
                    {
                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        foreach (BoundStatement boundStatement in CreateFeedPrivacy(creatorUserId,
                                                                                    feedId,
                                                                                    companyId,
                                                                                    Int32.Parse(FeedTypeCode.VideoPost),
                                                                                    caption,
                                                                                    targetedDepartmentIds,
                                                                                    targetedUserIds, session))
                        {
                            batchStatement = batchStatement.Add(boundStatement);
                        }

                        PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_video",
                                new List<string> { "id", "caption", "video_url", "video_thumbnail_url", "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(feedId, caption, videoUrl, videoThumbnailUrl, true, creatorUserId, DateHelper.ConvertDateToLong(DateTime.UtcNow)));

                        session.Execute(batchStatement);

                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed video post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidVideoContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidVideoContent;
                        response.Success = false;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedCreateResponse CreateFeedSharedUrlPost(string feedId,
                                                          string creatorUserId,
                                                          string companyId,
                                                          string caption,
                                                          string url,
                                                          string urlTitle,
                                                          string urlDescription,
                                                          string urlSiteName,
                                                          string urlImageUrl,
                                                          List<string> targetedDepartmentIds,
                                                          List<string> targetedUserIds)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(url))
                    {
                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        foreach (BoundStatement boundStatement in CreateFeedPrivacy(creatorUserId,
                                                                                    feedId,
                                                                                    companyId,
                                                                                    Int32.Parse(FeedTypeCode.SharedUrlPost),
                                                                                    caption,
                                                                                    targetedDepartmentIds,
                                                                                    targetedUserIds, session))
                        {
                            batchStatement = batchStatement.Add(boundStatement);
                        }

                        PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_shared_url",
                                new List<string> { "id", "caption", "url", "url_title", "url_description", "url_site_name", "url_image_url", "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(feedId, caption, url, urlTitle, urlDescription, urlSiteName, urlImageUrl, true, creatorUserId, DateHelper.ConvertDateToLong(DateTime.UtcNow)));

                        session.Execute(batchStatement);

                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed shared url post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextContent;
                        response.Success = false;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public CommentCreateResponse CreateCommentText(string creatorUserId,
                                                       string companyId,
                                                       string feedId,
                                                       string content)
        {
            CommentCreateResponse response = new CommentCreateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(content))
                    {
                        if (vh.ValidateFeedPost(feedId, companyId, session))
                        {
                            string commentId = UUIDGenerator.GenerateUniqueIDForFeedComment();

                            DateTime currentDateTime = DateTime.UtcNow;
                            long currentDateTimeLong = DateHelper.ConvertDateToLong(currentDateTime);

                            PreparedStatement psCommentByFeed = session.Prepare(CQLGenerator.InsertStatement("comment_by_feed",
                                new List<string> { "comment_id", "feed_id", "comment_type", "commentor_user_id", "is_comment_valid", "created_on_timestamp" }));
                            batchStatement.Add(psCommentByFeed.Bind(commentId, feedId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, currentDateTimeLong));

                            PreparedStatement psFeedCommentText = session.Prepare(CQLGenerator.InsertStatement("feed_comment_text",
                                new List<string> { "id", "feed_id", "content", "is_valid", "user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                            batchStatement.Add(psFeedCommentText.Bind(commentId, feedId, content, true, creatorUserId, currentDateTimeLong, creatorUserId, currentDateTimeLong));

                            PreparedStatement psCommentByTimestamp = session.Prepare(CQLGenerator.InsertStatement("comment_by_feed_timestamp",
                                new List<string> { "comment_id", "feed_id", "comment_type", "commentor_user_id", "is_comment_valid", "created_on_timestamp" }));
                            batchStatement.Add(psCommentByTimestamp.Bind(commentId, feedId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, currentDateTimeLong));

                            session.Execute(batchStatement);

                            UpdateFeedCommentCounter(feedId, 1, session);

                            response.Success = true;
                        }
                        else
                        {
                            Log.Error("Invalid feed post");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                            response.ErrorMessage = ErrorMessage.FeedInvalid;
                        }
                    }
                    else
                    {
                        Log.Error("Empty feed text comment");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextComment);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextComment;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ReplyCreateResponse CreateReplyText(string creatorUserId,
                                                   string companyId,
                                                   string feedId,
                                                   string commentId,
                                                   string content)
        {
            ReplyCreateResponse response = new ReplyCreateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(content))
                    {
                        if (vh.ValidateFeedPost(feedId, companyId, session))
                        {
                            if (vh.ValidateFeedComment(feedId, commentId, session) != null)
                            {
                                string replyId = UUIDGenerator.GenerateUniqueIDForFeedCommentReply();

                                DateTime currentDateTime = DateTime.UtcNow;
                                long currentDateTimeLong = DateHelper.ConvertDateToLong(currentDateTime);

                                PreparedStatement psCommentByFeed = session.Prepare(CQLGenerator.InsertStatement("reply_by_comment",
                                    new List<string> { "reply_id", "comment_id", "reply_type", "commentor_user_id", "is_reply_valid", "created_on_timestamp" }));
                                batchStatement.Add(psCommentByFeed.Bind(replyId, commentId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, currentDateTimeLong));

                                PreparedStatement psFeedCommentText = session.Prepare(CQLGenerator.InsertStatement("feed_reply_text",
                                    new List<string> { "id", "comment_id", "content", "is_valid", "user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                                batchStatement.Add(psFeedCommentText.Bind(replyId, commentId, content, true, creatorUserId, currentDateTimeLong, creatorUserId, currentDateTimeLong));

                                PreparedStatement psCommentByTimestampDesc = session.Prepare(CQLGenerator.InsertStatement("reply_by_comment_timestamp_desc",
                                    new List<string> { "reply_id", "comment_id", "reply_type", "commentor_user_id", "is_reply_valid", "created_on_timestamp" }));
                                batchStatement.Add(psCommentByTimestampDesc.Bind(replyId, commentId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, currentDateTimeLong));

                                PreparedStatement psCommentByTimestampAsc = session.Prepare(CQLGenerator.InsertStatement("reply_by_comment_timestamp_asc",
                                    new List<string> { "reply_id", "comment_id", "reply_type", "commentor_user_id", "is_reply_valid", "created_on_timestamp" }));
                                batchStatement.Add(psCommentByTimestampAsc.Bind(replyId, commentId, Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, currentDateTimeLong));

                                session.Execute(batchStatement);

                                UpdateCommentReplyCounter(feedId, commentId, 1, session);

                                response.Success = true;
                            }
                            else
                            {
                                Log.Error("Invalid feed comment");
                                response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                                response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                            }

                        }
                        else
                        {
                            Log.Error("Invalid feed post");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                            response.ErrorMessage = ErrorMessage.FeedInvalid;
                        }
                    }
                    else
                    {
                        Log.Error("Empty feed text comment");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextComment);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextComment;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public CommentSelectResponse SelectFeedComment(string requesterUserId,
                                                       string feedId,
                                                       string companyId)
        {
            CommentSelectResponse response = new CommentSelectResponse();
            response.Success = false;

            ConnectionManager cm = new ConnectionManager();
            ISession session = cm.getMainSession();
            ValidationHandler vh = new ValidationHandler();

            BatchStatement batchStatement = new BatchStatement();
            try
            {
                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow != null)
                {
                    if (vh.ValidateFeedPost(feedId, companyId, session))
                    {
                        PreparedStatement psCommentByTimestamp = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp",
                            new List<string> { "comment_id", "comment_type", "commentor_user_id" }, new List<string> { "feed_id", "is_comment_valid" }));
                        BoundStatement bsCommentByTimestamp = psCommentByTimestamp.Bind(feedId, true);

                        RowSet commentByTimestampRowset = session.Execute(bsCommentByTimestamp);

                        List<Comment> comments = new List<Comment>();

                        foreach (Row commentByTimestampRow in commentByTimestampRowset)
                        {
                            string commentId = commentByTimestampRow.GetValue<string>("comment_id");
                            int commentType = commentByTimestampRow.GetValue<int>("comment_type");
                            string commentorUserId = commentByTimestampRow.GetValue<string>("commentor_user_id");

                            User commentorUser = new User().SelectUserBasic(commentorUserId, companyId, false, session).User;
                            comments.Add(GetCommentWithLatestReply(companyId, feedId, commentId, commentType, requesterUserId, commentorUser, session));
                        }

                        response.Success = true;
                        response.Comments = comments;
                    }
                    else
                    {
                        Log.Error("Invalid feed");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }

                }
                else
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ReplySelectResponse SelectCommentReply(string requesterUserId,
                                                      string feedId,
                                                      string commentId,
                                                      string companyId)
        {
            ReplySelectResponse response = new ReplySelectResponse();
            response.Success = false;

            ConnectionManager cm = new ConnectionManager();
            ISession session = cm.getMainSession();
            ValidationHandler vh = new ValidationHandler();

            BatchStatement batchStatement = new BatchStatement();
            try
            {
                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow != null)
                {
                    if (vh.ValidateFeedPost(feedId, companyId, session))
                    {
                        if (vh.ValidateFeedComment(feedId, commentId, session) != null)
                        {
                            PreparedStatement psReplyByTimestamp = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment_timestamp_asc",
                                new List<string> { "reply_id", "reply_type", "commentor_user_id" }, new List<string> { "comment_id", "is_reply_valid" }));
                            BoundStatement bsReplyByTimestamp = psReplyByTimestamp.Bind(commentId, true);

                            RowSet replyByTimestampRowset = session.Execute(bsReplyByTimestamp);

                            List<Reply> replies = new List<Reply>();

                            foreach (Row replyByTimestampRow in replyByTimestampRowset)
                            {
                                string replyId = replyByTimestampRow.GetValue<string>("reply_id");
                                int replyType = replyByTimestampRow.GetValue<int>("reply_type");
                                string commentorUserId = replyByTimestampRow.GetValue<string>("commentor_user_id");

                                User commentorUser = new User().SelectUserBasic(commentorUserId, companyId, false, session).User;
                                replies.Add(GetReply(companyId, commentId, replyId, replyType, requesterUserId, commentorUser, session));
                            }

                            response.Success = true;
                            response.Replies = replies;
                        }
                        else
                        {
                            Log.Error("Invalid comment");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                            response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid feed");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }

                }
                else
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId, string adminUserId)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psFeedPrivacy = null;
                BoundStatement bsFeedPrivacy = null;

                string modifiedUserId = string.Empty;

                if (!string.IsNullOrEmpty(ownerUserId))
                {
                    Row userRow = vh.ValidateUser(ownerUserId, companyId, session);
                    if (userRow == null)
                    {
                        Log.Error("Invalid user");
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                        new List<string>(), new List<string> { "company_id", "feed_id", "owner_user_id" }));
                    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, feedId, ownerUserId);

                    modifiedUserId = ownerUserId;
                }
                else
                {
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                        new List<string>(), new List<string> { "company_id", "feed_id" }));
                    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, feedId);

                    modifiedUserId = adminUserId;
                }

                Row feedPrivacyRow = session.Execute(bsFeedPrivacy).FirstOrDefault();

                if (feedPrivacyRow == null)
                {
                    Log.Error(string.Format("Feed post: {0} does not belong to userId: {1}", feedId, ownerUserId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }
                else
                {
                    bool isFeedValid = feedPrivacyRow.GetValue<bool>("is_feed_valid");

                    if (!isFeedValid)
                    {
                        Log.Error("Feed is already invalid: " + feedId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                        return response;
                    }

                    BatchStatement batchStatement = new BatchStatement();

                    int feedType = feedPrivacyRow.GetValue<int>("feed_type");
                    DateTimeOffset createdTimestamp = feedPrivacyRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                    string tableName = string.Empty;
                    bool isNeedToDeleteFromS3 = true;
                    if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        tableName = "feed_text";
                        isNeedToDeleteFromS3 = false;
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                    {
                        tableName = "feed_image";
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                    {
                        tableName = "feed_video";
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                    {
                        tableName = "feed_shared_url";
                    }

                    PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                        new List<string> { "id" }, new List<string> { "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    BoundStatement bsInvalidate = psInvalidate.Bind(false, modifiedUserId, DateTime.UtcNow, feedId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    // Delete from S3
                    if (isNeedToDeleteFromS3)
                    {
                        String bucketName = "cocadre-" + companyId.ToLower();

                        using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(ConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), ConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                        {
                            ListObjectsRequest listRequest = new ListObjectsRequest();
                            listRequest.BucketName = bucketName;
                            listRequest.Prefix = "feeds/" + feedId;

                            ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                            foreach (S3Object imageObject in listResponse.S3Objects)
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = imageObject.Key;
                                s3Client.DeleteObject(deleteRequest);
                            }

                            DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                            deleteFolderRequest.BucketName = bucketName;
                            deleteFolderRequest.Key = "feeds/" + feedId;
                            s3Client.DeleteObject(deleteFolderRequest);
                        }
                    }

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy",
                        new List<string> { "company_id", "feed_id" }, new List<string> { "is_feed_valid" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, companyId, feedId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc",
                      new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "is_feed_valid" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, companyId, createdTimestamp, feedId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc",
                        new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "is_feed_valid" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, companyId, createdTimestamp, feedId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    session.Execute(batchStatement);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId, string commentorUserId, string adminUserId)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psCommentPrivacy = null;
                BoundStatement bsCommentPrivacy = null;

                string modifiedUserId = string.Empty;

                if (!vh.ValidateFeedPost(feedId, companyId, session))
                {
                    Log.Error("Invalid FeedId: " + feedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!string.IsNullOrEmpty(commentorUserId))
                {
                    Row userRow = vh.ValidateUser(commentorUserId, companyId, session);
                    if (userRow == null)
                    {
                        Log.Error("Invalid commentorUserId: " + commentorUserId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    psCommentPrivacy = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed",
                        new List<string>(), new List<string> { "feed_id", "comment_id", "commentor_user_id" }));
                    bsCommentPrivacy = psCommentPrivacy.Bind(feedId, commentId, commentorUserId);

                    modifiedUserId = commentorUserId;
                }
                else
                {
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    psCommentPrivacy = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed",
                        new List<string>(), new List<string> { "feed_id", "comment_id" }));
                    bsCommentPrivacy = psCommentPrivacy.Bind(feedId, commentId);

                    modifiedUserId = adminUserId;
                }

                Row commentRow = session.Execute(bsCommentPrivacy).FirstOrDefault();

                if (commentRow == null)
                {
                    Log.Error(string.Format("Comment post: {0} does not belong to userId: {1}", commentId, commentorUserId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }
                else
                {
                    bool isCommentValid = commentRow.GetValue<bool>("is_comment_valid");

                    if (!isCommentValid)
                    {
                        Log.Error("Comment is already invalid: " + commentId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                        return response;
                    }

                    BatchStatement batchStatement = new BatchStatement();

                    int commentType = commentRow.GetValue<int>("comment_type");
                    DateTimeOffset createdTimestamp = commentRow.GetValue<DateTimeOffset>("created_on_timestamp");

                    string tableName = string.Empty;
                    bool isNeedToDeleteFromS3 = true;
                    if (commentType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        tableName = "feed_comment_text";
                        isNeedToDeleteFromS3 = false;
                    }

                    PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                        new List<string> { "feed_id", "id" }, new List<string> { "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    BoundStatement bsInvalidate = psInvalidate.Bind(false, modifiedUserId, DateTime.UtcNow, feedId, commentId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    // Delete from S3
                    if (isNeedToDeleteFromS3)
                    {

                    }

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp",
                      new List<string> { "feed_id", "created_on_timestamp", "comment_id" }, new List<string> { "is_comment_valid" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, feedId, createdTimestamp, commentId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    session.Execute(batchStatement);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                        new List<string> { "feed_id" }, new List<string> { "comment" }, new List<int> { -1 }));
                    bsInvalidate = psInvalidate.Bind(feedId);
                    session.Execute(bsInvalidate);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId, string commentorUserId, string adminUserId)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psReplyPrivacy = null;
                BoundStatement bsReplyPrivacy = null;

                string modifiedUserId = string.Empty;

                if (!vh.ValidateFeedPost(feedId, companyId, session))
                {
                    Log.Error("Invalid FeedId: " + feedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);
                if (commentRow == null)
                {
                    Log.Error("Invalid CommentId: " + commentId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                    response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                    return response;
                }

                if (!string.IsNullOrEmpty(commentorUserId))
                {
                    Row userRow = vh.ValidateUser(commentorUserId, companyId, session);
                    if (userRow == null)
                    {
                        Log.Error("Invalid commentorUserId: " + commentorUserId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    psReplyPrivacy = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment",
                        new List<string>(), new List<string> { "comment_id", "reply_id", "commentor_user_id" }));
                    bsReplyPrivacy = psReplyPrivacy.Bind(commentId, replyId, commentorUserId);

                    modifiedUserId = commentorUserId;
                }
                else
                {
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    psReplyPrivacy = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment",
                        new List<string>(), new List<string> { "comment_id", "reply_id" }));
                    bsReplyPrivacy = psReplyPrivacy.Bind(commentId, replyId);

                    modifiedUserId = adminUserId;
                }

                Row replyRow = session.Execute(bsReplyPrivacy).FirstOrDefault();

                if (replyRow == null)
                {
                    Log.Error(string.Format("Reply post: {0} does not belong to userId: {1}", replyId, commentorUserId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }
                else
                {
                    bool isReplyValid = replyRow.GetValue<bool>("is_reply_valid");

                    if (!isReplyValid)
                    {
                        Log.Error("Reply is already invalid: " + replyId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                        return response;
                    }

                    BatchStatement batchStatement = new BatchStatement();

                    int replyType = replyRow.GetValue<int>("reply_type");
                    DateTimeOffset createdTimestamp = replyRow.GetValue<DateTimeOffset>("created_on_timestamp");

                    string tableName = string.Empty;
                    bool isNeedToDeleteFromS3 = true;
                    if (replyType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        tableName = "feed_reply_text";
                        isNeedToDeleteFromS3 = false;
                    }

                    PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                        new List<string> { "comment_id", "id" }, new List<string> { "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    BoundStatement bsInvalidate = psInvalidate.Bind(false, modifiedUserId, DateTime.UtcNow, commentId, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    // Delete from S3
                    if (isNeedToDeleteFromS3)
                    {

                    }

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_desc",
                      new List<string> { "comment_id", "created_on_timestamp", "reply_id" }, new List<string> { "is_reply_valid" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, commentId, createdTimestamp, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_asc",
                        new List<string> { "comment_id", "created_on_timestamp", "reply_id" }, new List<string> { "is_reply_valid" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, commentId, createdTimestamp, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    session.Execute(batchStatement);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                        new List<string> { "feed_id", "comment_id" }, new List<string> { "reply" }, new List<int> { -1 }));
                    bsInvalidate = psInvalidate.Bind(feedId, commentId);
                    session.Execute(bsInvalidate);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PointUpdateResponse UpdateFeedPoint(string voterUserId,
                                                   string feedId,
                                                   string companyId,
                                                   bool isUpVote)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(voterUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (vh.ValidateFeedPost(feedId, companyId, session))
                    {
                        BoundStatement boundStatement = UpdateFeedPointCounter(voterUserId, feedId, isUpVote, session);

                        if (boundStatement != null)
                        {
                            session.Execute(boundStatement);
                        }

                        // Get pointers
                        Dictionary<string, int> pointDict = GetPointsForFeed(feedId, session);
                        int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
                        int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

                        response.Success = true;
                        response.IsUpVoted = isUpVote;
                        response.NegativePoints = negativePoints;
                        response.PositivePoints = positivePoints;

                    }
                    else
                    {
                        Log.Error("Invalid feed post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PointUpdateResponse UpdateCommentPoint(string voterUserId,
                                                      string feedId,
                                                      string commentId,
                                                      string companyId,
                                                      bool isUpVote)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(voterUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (vh.ValidateFeedPost(feedId, companyId, session))
                    {
                        if (vh.ValidateFeedComment(feedId, commentId, session) != null)
                        {
                            BoundStatement boundStatement = UpdateCommentPointCounter(voterUserId, feedId, commentId, isUpVote, session);

                            if (boundStatement != null)
                            {
                                session.Execute(boundStatement);
                            }

                            // Get pointers
                            Dictionary<string, int> pointDict = GetPointsForComment(feedId, commentId, session);
                            int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
                            int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

                            response.Success = true;
                            response.IsUpVoted = isUpVote;
                            response.NegativePoints = negativePoints;
                            response.PositivePoints = positivePoints;
                        }
                        else
                        {
                            Log.Error("Invalid comment");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                            response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid feed post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PointUpdateResponse UpdateReplyPoint(string voterUserId,
                                                    string feedId,
                                                    string commentId,
                                                    string replyId,
                                                    string companyId,
                                                    bool isUpVote)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(voterUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    if (vh.ValidateFeedPost(feedId, companyId, session))
                    {
                        if (vh.ValidateFeedComment(feedId, commentId, session) != null)
                        {
                            if (vh.ValidateCommentReply(commentId, replyId, session) != null)
                            {
                                BoundStatement boundStatement = UpdateReplyPointCounter(voterUserId, commentId, replyId, isUpVote, session);

                                if (boundStatement != null)
                                {
                                    session.Execute(boundStatement);
                                }

                                // Get pointers
                                Dictionary<string, int> pointDict = GetPointsForReply(commentId, replyId, session);
                                int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
                                int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

                                response.Success = true;
                                response.IsUpVoted = isUpVote;
                                response.NegativePoints = negativePoints;
                                response.PositivePoints = positivePoints;
                            }
                            else
                            {
                                Log.Error("Invalid reply");
                                response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidReply);
                                response.ErrorMessage = ErrorMessage.FeedInvalidReply;
                            }

                        }
                        else
                        {
                            Log.Error("Invalid comment");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                            response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid feed post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectResponse SelectCompanyFeedPost(string requesterUserId,
                                                        string companyId,
                                                        string searchContent,
                                                        int numberOfPostsLoaded,
                                                        DateTime? newestTimestamp,
                                                        DateTime? oldestTimestamp)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Success = false;

            try
            {
                int limit = Int16.Parse(ConfigurationManager.AppSettings["feed_limit"]);

                if (numberOfPostsLoaded > 0)
                {
                    limit += numberOfPostsLoaded;
                }

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow != null)
                {
                    PreparedStatement psDepartmentByUser = session.Prepare(CQLGenerator.SelectStatement("department_by_user",
                            new List<string> { "department_id" }, new List<string> { "user_id" }));
                    BoundStatement bsDepartmentByUser = psDepartmentByUser.Bind(requesterUserId);
                    RowSet departmentByUserRowset = session.Execute(bsDepartmentByUser);

                    List<string> departmentIds = new List<string>();

                    foreach (Row departmentByUserRow in departmentByUserRowset)
                    {
                        departmentIds.Add(departmentByUserRow.GetValue<string>("department_id"));
                    }

                    List<Feed> feedPosts = new List<Feed>();

                    PreparedStatement psFeedPrivacy = null;
                    BoundStatement bsFeedPrivacy = null;
                    DateTime currentDateTime = DateTime.MinValue;

                    bool isSelectCompleted = false;
                    while (isSelectCompleted == false)
                    {
                        // Since will be fetching all to avoid checking for update, then no need for newestTimestamp

                        //if (newestTimestamp == null && oldestTimestamp == null)
                        //{
                        //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp",
                        //        new List<string>(), new List<string> { "company_id", "owner_user_id" }, limit));
                        //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId);
                        //}
                        //else if (oldestTimestamp != null)
                        //{
                        //    if (currentDateTime == DateTime.MinValue)
                        //    {
                        //        currentDateTime = oldestTimestamp.Value;
                        //    }
                        //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp",
                        //        null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId, DateHelper.ConvertDateToLong(currentDateTime));
                        //}
                        //else if (newestTimestamp != null)
                        //{
                        //    if (currentDateTime == DateTime.MinValue)
                        //    {
                        //        currentDateTime = newestTimestamp.Value;
                        //    }
                        //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp",
                        //        null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.GreaterThan, limit));
                        //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId, DateHelper.ConvertDateToLong(currentDateTime));
                        //}

                        if (oldestTimestamp != null)
                        {
                            if (currentDateTime == DateTime.MinValue)
                            {
                                currentDateTime = oldestTimestamp.Value;
                            }
                            psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp_desc",
                                null, new List<string> { "company_id", "is_feed_valid" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                            bsFeedPrivacy = psFeedPrivacy.Bind(companyId, true, DateHelper.ConvertDateToLong(currentDateTime));
                        }
                        else
                        {
                            psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp_desc",
                                new List<string>(), new List<string> { "company_id", "is_feed_valid" }, limit));
                            bsFeedPrivacy = psFeedPrivacy.Bind(companyId, true);
                        }

                        RowSet allFeedRowSet = session.Execute(bsFeedPrivacy);

                        foreach (Row allFeedRow in allFeedRowSet)
                        {
                            bool isPostForCurrentUser = false;

                            int feedType = allFeedRow.GetValue<int>("feed_type");
                            string feedId = allFeedRow.GetValue<string>("feed_id");
                            string ownerUserId = allFeedRow.GetValue<string>("owner_user_id");
                            bool isForEveryone = allFeedRow.GetValue<bool>("is_for_everyone");
                            bool isForDepartment = allFeedRow.GetValue<bool>("is_for_department");
                            bool isForUser = allFeedRow.GetValue<bool>("is_for_user");
                            DateTime feedCreatedOnTimestamp = allFeedRow.GetValue<DateTime>("feed_created_on_timestamp");

                            if (!string.IsNullOrEmpty(searchContent))
                            {
                                string feedTextContent = allFeedRow.GetValue<string>("feed_text_content");
                                bool isMatched = feedTextContent.Contains(searchContent);

                                if (ownerUserId.Equals(requesterUserId))
                                {
                                    if (isMatched)
                                    {
                                        isPostForCurrentUser = true;
                                    }
                                }
                                else
                                {
                                    if (isMatched)
                                    {
                                        isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session);
                                    }
                                }
                            }
                            else
                            {
                                if (ownerUserId.Equals(requesterUserId))
                                {
                                    isPostForCurrentUser = true;
                                }
                                else
                                {
                                    isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session);
                                }
                            }

                            if (isPostForCurrentUser)
                            {
                                User user = new User();
                                Feed feed = GetFeed(feedId, feedType, feedCreatedOnTimestamp, requesterUserId, user.SelectUserBasic(ownerUserId, companyId, false, session).User, session);

                                if (feed != null)
                                {
                                    feedPosts.Add(feed);
                                }

                            }

                            currentDateTime = feedCreatedOnTimestamp;
                        }

                        if (allFeedRowSet.GetRows().Count() == 0)
                        {
                            response.Feeds = feedPosts;
                            response.Success = true;
                            isSelectCompleted = true;
                            break;
                        }
                        else
                        {
                            if (feedPosts.Count == limit || allFeedRowSet.GetRows().Count() < limit)
                            {
                                isSelectCompleted = true;
                                response.Feeds = feedPosts;
                                response.Success = true;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectResponse SelectPersonnelFeedPost(string requesterUserId,
                                                          string ownerUserId,
                                                          string companyId,
                                                          string searchContent,
                                                          DateTime? newestTimestamp,
                                                          DateTime? oldestTimestamp)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Success = false;

            try
            {
                int limit = Int16.Parse(ConfigurationManager.AppSettings["feed_limit"]);

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                Row ownerUserRow = vh.ValidateUser(ownerUserId, companyId, session);
                if (ownerUserRow != null)
                {
                    Row requesterUserRow = vh.ValidateUser(requesterUserId, companyId, session);

                    if (requesterUserRow != null)
                    {
                        PreparedStatement psDepartmentByUser = session.Prepare(CQLGenerator.SelectStatement("department_by_user",
                            new List<string> { "department_id" }, new List<string> { "user_id" }));
                        BoundStatement bsDepartmentByUser = psDepartmentByUser.Bind(requesterUserId);
                        RowSet departmentByUserRowset = session.Execute(bsDepartmentByUser);

                        List<string> departmentIds = new List<string>();

                        foreach (Row departmentByUserRow in departmentByUserRowset)
                        {
                            departmentIds.Add(departmentByUserRow.GetValue<string>("department_id"));
                        }

                        List<Feed> feedPosts = new List<Feed>();

                        PreparedStatement psFeedPrivacy = null;
                        BoundStatement bsFeedPrivacy = null;
                        DateTime currentDateTime = DateTime.MinValue;

                        bool isSelectCompleted = false;
                        while (isSelectCompleted == false)
                        {
                            // Since will be fetching all to avoid checking for update, then no need for newestTimestamp

                            //if (newestTimestamp == null && oldestTimestamp == null)
                            //{
                            //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp",
                            //        new List<string>(), new List<string> { "company_id", "owner_user_id" }, limit));
                            //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId);
                            //}
                            //else if (oldestTimestamp != null)
                            //{
                            //    if (currentDateTime == DateTime.MinValue)
                            //    {
                            //        currentDateTime = oldestTimestamp.Value;
                            //    }
                            //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp",
                            //        null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                            //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId, DateHelper.ConvertDateToLong(currentDateTime));
                            //}
                            //else if (newestTimestamp != null)
                            //{
                            //    if (currentDateTime == DateTime.MinValue)
                            //    {
                            //        currentDateTime = newestTimestamp.Value;
                            //    }
                            //    psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp",
                            //        null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.GreaterThan, limit));
                            //    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId, DateHelper.ConvertDateToLong(currentDateTime));
                            //}

                            if (oldestTimestamp != null)
                            {
                                if (currentDateTime == DateTime.MinValue)
                                {
                                    currentDateTime = oldestTimestamp.Value;
                                }
                                psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp_desc",
                                    null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                                bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId, DateHelper.ConvertDateToLong(currentDateTime));
                            }
                            else
                            {
                                psFeedPrivacy = session.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp_desc",
                                    new List<string>(), new List<string> { "company_id", "owner_user_id" }, limit));
                                bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId);
                            }

                            RowSet allFeedRowSet = session.Execute(bsFeedPrivacy);

                            foreach (Row allFeedRow in allFeedRowSet)
                            {
                                bool isFeedValid = allFeedRow.GetValue<bool>("is_feed_valid");

                                if (isFeedValid)
                                {
                                    bool isPostForCurrentUser = false;

                                    int feedType = allFeedRow.GetValue<int>("feed_type");
                                    string feedId = allFeedRow.GetValue<string>("feed_id");
                                    bool isForEveryone = allFeedRow.GetValue<bool>("is_for_everyone");
                                    bool isForDepartment = allFeedRow.GetValue<bool>("is_for_department");
                                    bool isForUser = allFeedRow.GetValue<bool>("is_for_user");
                                    DateTime feedCreatedOnTimestamp = allFeedRow.GetValue<DateTime>("feed_created_on_timestamp");

                                    if (!string.IsNullOrEmpty(searchContent))
                                    {
                                        string feedTextContent = allFeedRow.GetValue<string>("feed_text_content");
                                        bool isMatched = feedTextContent.Contains(searchContent);

                                        if (isMatched)
                                        {
                                            isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session);
                                        }
                                    }
                                    else
                                    {
                                        isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session);
                                    }

                                    if (isPostForCurrentUser)
                                    {
                                        User user = new User();

                                        Feed feed = GetFeed(feedId, feedType, feedCreatedOnTimestamp, requesterUserId, user.SelectUserBasic(ownerUserId, companyId, false, session).User, session);
                                        if (feed != null)
                                        {
                                            feedPosts.Add(feed);
                                        }

                                    }

                                    currentDateTime = feedCreatedOnTimestamp;
                                }

                            }

                            if (allFeedRowSet.GetRows().Count() == 0)
                            {
                                response.Feeds = feedPosts;
                                response.Success = true;
                                isSelectCompleted = true;
                                break;
                            }
                            else
                            {
                                if (feedPosts.Count == limit || allFeedRowSet.GetRows().Count() < limit)
                                {
                                    isSelectCompleted = true;
                                    response.Feeds = feedPosts;
                                    response.Success = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        Log.Error("Invalid requester user id: " + requesterUserId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid owner user id: " + ownerUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private bool CheckPostForCurrentUser(string userId, string feedId, bool isForEveryone, bool isForDepartment, bool isForUser, List<string> departmentIds, ISession session)
        {

            bool isPostForCurrentUser = false;

            if (isForEveryone)
            {
                isPostForCurrentUser = true;
            }
            else
            {
                if (isForDepartment)
                {
                    foreach (string departmentId in departmentIds)
                    {
                        PreparedStatement psFeedTargetedDepartment = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_department",
                            new List<string>(), new List<string> { "feed_id", "department_id" }));
                        BoundStatement bsFeedTargetedDepartment = psFeedTargetedDepartment.Bind(feedId, departmentId);

                        Row departmentPrivacyRow = session.Execute(bsFeedTargetedDepartment).FirstOrDefault();

                        if (departmentPrivacyRow != null)
                        {
                            isPostForCurrentUser = true;
                            break;
                        }
                    }
                }

                if (!isPostForCurrentUser && isForUser)
                {
                    PreparedStatement psFeedTargetedUser = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_user",
                            new List<string>(), new List<string> { "feed_id", "user_id" }));
                    BoundStatement bsFeedTargetedUser = psFeedTargetedUser.Bind(feedId, userId);

                    Row userPrivacyRow = session.Execute(bsFeedTargetedUser).FirstOrDefault();

                    if (userPrivacyRow != null)
                    {
                        isPostForCurrentUser = true;
                    }
                }
            }

            return isPostForCurrentUser;
        }

        private Dictionary<string, bool> CheckVoteForFeed(string userId, string feedId, ISession session)
        {
            Dictionary<string, bool> resultDict = new Dictionary<string, bool>();

            PreparedStatement psFeedPoint = session.Prepare(CQLGenerator.SelectStatement("feed_point",
                            new List<string>(), new List<string> { "feed_id", "user_id" }));
            BoundStatement bsFeedPoint = psFeedPoint.Bind(feedId, userId);

            Row feedPointRow = session.Execute(bsFeedPoint).FirstOrDefault();

            resultDict.Add("hasVoted", false);
            resultDict.Add("isUpVoted", false);

            if (feedPointRow != null)
            {
                resultDict["hasVoted"] = true;

                int value = feedPointRow.GetValue<int>("value");

                resultDict["isUpVoted"] = value == 1 ? true : false;
            }

            return resultDict;
        }

        private Dictionary<string, bool> CheckVoteForComment(string userId, string commentId, ISession session)
        {
            Dictionary<string, bool> resultDict = new Dictionary<string, bool>();

            PreparedStatement psCommentPoint = session.Prepare(CQLGenerator.SelectStatement("comment_point",
                            new List<string>(), new List<string> { "comment_id", "user_id" }));
            BoundStatement bsCommentPoint = psCommentPoint.Bind(commentId, userId);

            Row commentPointRow = session.Execute(bsCommentPoint).FirstOrDefault();

            resultDict.Add("hasVoted", false);
            resultDict.Add("isUpVoted", false);

            if (commentPointRow != null)
            {
                resultDict["hasVoted"] = true;

                int value = commentPointRow.GetValue<int>("value");

                resultDict["isUpVoted"] = value == 1 ? true : false;
            }

            return resultDict;
        }

        private Dictionary<string, bool> CheckVoteForReply(string userId, string replyId, ISession session)
        {
            Dictionary<string, bool> resultDict = new Dictionary<string, bool>();

            PreparedStatement psReplyPoint = session.Prepare(CQLGenerator.SelectStatement("reply_point",
                            new List<string>(), new List<string> { "reply_id", "user_id" }));
            BoundStatement bsReplyPoint = psReplyPoint.Bind(replyId, userId);

            Row replyPointRow = session.Execute(bsReplyPoint).FirstOrDefault();

            resultDict.Add("hasVoted", false);
            resultDict.Add("isUpVoted", false);

            if (replyPointRow != null)
            {
                resultDict["hasVoted"] = true;

                int value = replyPointRow.GetValue<int>("value");

                resultDict["isUpVoted"] = value == 1 ? true : false;
            }

            return resultDict;
        }

        private Comment GetCommentWithLatestReply(string companyId, string feedId, string commentId, int commentType, string requesterUserId, User commentorUser, ISession session)
        {
            Comment comment = null;
            Reply latestReply = new Reply();

            // Get latest reply
            PreparedStatement psLatestReply = session.Prepare(CQLGenerator.SelectStatementWithLimit("reply_by_comment_timestamp_desc",
                   new List<string> { "reply_id", "reply_type", "commentor_user_id" }, new List<string> { "comment_id", "is_reply_valid" }, 1));
            BoundStatement bsLatestReply = psLatestReply.Bind(commentId, true);
            Row replyRow = session.Execute(bsLatestReply).FirstOrDefault();
            if (replyRow != null)
            {
                string replyId = replyRow.GetValue<string>("reply_id");
                int replyType = replyRow.GetValue<int>("reply_type");
                string replyCommentorUserId = replyRow.GetValue<string>("commentor_user_id");
                User replyCommentorUser = new User().SelectUserBasic(replyCommentorUserId, companyId, false, session).User;

                latestReply = GetReply(companyId, commentId, replyId, replyType, requesterUserId, replyCommentorUser, session);
            }

            // Get counter
            int numberOfReplies = GetNumberOfRepliesForComment(feedId, commentId, session);

            // Get pointers
            Dictionary<string, int> pointDict = GetPointsForComment(feedId, commentId, session);
            int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
            int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

            // Check for vote
            Dictionary<string, bool> voteDict = CheckVoteForComment(requesterUserId, commentId, session);
            bool hasVoted = voteDict["hasVoted"];
            bool isUpVoted = voteDict["isUpVoted"];

            if (commentType == Int16.Parse(FeedTypeCode.TextPost))
            {
                PreparedStatement psCommentText = session.Prepare(CQLGenerator.SelectStatement("feed_comment_text",
                    new List<string> { "user_id", "content", "created_on_timestamp" }, new List<string> { "id", "feed_id", "is_valid" }));
                BoundStatement bsCommentText = psCommentText.Bind(commentId, feedId, true);

                Row commentTextRow = session.Execute(bsCommentText).FirstOrDefault();

                if (commentTextRow != null)
                {
                    string content = commentTextRow.GetValue<string>("content");
                    string commentorUserId = commentTextRow.GetValue<string>("user_id");

                    DateTime createdOnTimestamp = commentTextRow.GetValue<DateTime>("created_on_timestamp");

                    comment = new Comment
                    {
                        CommentId = commentId,
                        CommentType = commentType,
                        CommentText = new CommentText
                        {
                            Content = content
                        },
                        Reply = latestReply,
                        NegativePoints = negativePoints,
                        PositivePoints = positivePoints,
                        NumberOfReplies = numberOfReplies,
                        HasVoted = hasVoted,
                        IsUpVoted = isUpVoted,
                        User = commentorUser,
                        CreatedOnTimestamp = createdOnTimestamp
                    };
                }

            }

            return comment;
        }

        private Reply GetReply(string companyId, string commentId, string replyId, int replyType, string requesterUserId, User commentorUser, ISession session)
        {

            Reply reply = null;

            // Get pointers
            Dictionary<string, int> pointDict = GetPointsForReply(commentId, replyId, session);
            int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
            int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

            // Check for vote
            Dictionary<string, bool> voteDict = CheckVoteForReply(requesterUserId, replyId, session);
            bool hasVoted = voteDict["hasVoted"];
            bool isUpVoted = voteDict["isUpVoted"];

            if (replyType == Int16.Parse(FeedTypeCode.TextPost))
            {
                PreparedStatement psReplyText = session.Prepare(CQLGenerator.SelectStatement("feed_reply_text",
                    new List<string> { "user_id", "content", "created_on_timestamp" }, new List<string> { "id", "comment_id", "is_valid" }));
                BoundStatement bsReplyText = psReplyText.Bind(replyId, commentId, true);

                Row replyTextRow = session.Execute(bsReplyText).FirstOrDefault();

                if (replyTextRow != null)
                {
                    string content = replyTextRow.GetValue<string>("content");
                    DateTime createdOnTimestamp = replyTextRow.GetValue<DateTime>("created_on_timestamp");

                    reply = new Reply
                    {
                        ReplyId = replyId,
                        ReplyType = replyType,
                        ReplyText = new ReplyText
                        {
                            Content = content
                        },
                        NegativePoints = negativePoints,
                        PositivePoints = positivePoints,
                        HasVoted = hasVoted,
                        IsUpVoted = isUpVoted,
                        User = commentorUser,
                        CreatedOnTimestamp = createdOnTimestamp
                    };
                }

            }

            return reply;
        }


        private Feed GetFeed(string feedId, int feedType, DateTime feedCreatedOnTimestamp, string requesterUserId, User ownerUser, ISession session)
        {
            Feed feed = null;

            // Get number of comments
            int numberOfFeed = GetNumberOfCommentsForFeed(feedId, session);

            // Get pointers
            Dictionary<string, int> pointDict = GetPointsForFeed(feedId, session);
            int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
            int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

            // Check for vote
            Dictionary<string, bool> voteDict = CheckVoteForFeed(requesterUserId, feedId, session);
            bool hasVoted = voteDict["hasVoted"];
            bool isUpVoted = voteDict["isUpVoted"];

            if (feedType == Int16.Parse(FeedTypeCode.TextPost))
            {
                PreparedStatement psFeedText = session.Prepare(CQLGenerator.SelectStatement("feed_text",
                    new List<string> { "content" }, new List<string> { "id", "is_valid" }));
                BoundStatement bsFeedText = psFeedText.Bind(feedId, true);

                Row feedTextRow = session.Execute(bsFeedText).FirstOrDefault();

                if (feedTextRow != null)
                {
                    string content = feedTextRow.GetValue<string>("content");

                    feed = new Feed
                    {
                        FeedId = feedId,
                        FeedType = feedType,
                        FeedText = new FeedText
                        {
                            Content = content
                        },
                        NegativePoints = negativePoints,
                        PositivePoints = positivePoints,
                        NumberOfComments = numberOfFeed,
                        HasVoted = hasVoted,
                        IsUpVoted = isUpVoted,
                        User = ownerUser,
                        CreatedOnTimestamp = feedCreatedOnTimestamp
                    };
                }

            }
            else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
            {
                PreparedStatement psFeedImage = session.Prepare(CQLGenerator.SelectStatement("feed_image",
                    new List<string> { "caption" }, new List<string> { "id", "is_valid" }));
                BoundStatement bsFeedImage = psFeedImage.Bind(feedId, true);

                Row feedImageRow = session.Execute(bsFeedImage).FirstOrDefault();

                if (feedImageRow != null)
                {
                    string caption = feedImageRow.GetValue<string>("caption");

                    PreparedStatement psFeedImageUrl = session.Prepare(CQLGenerator.SelectStatement("feed_image_url",
                    new List<string>(), new List<string> { "feed_id", "is_valid" }));
                    BoundStatement bsFeedImageUrl = psFeedImageUrl.Bind(feedId, true);

                    RowSet feedImageUrlRowSet = session.Execute(bsFeedImageUrl);

                    List<Image> images = new List<Image>();
                    foreach (Row feedImageUrlRow in feedImageUrlRowSet.GetRows())
                    {
                        Image image = new Image
                        {
                            ImageId = feedImageUrlRow.GetValue<string>("id"),
                            Url = feedImageUrlRow.GetValue<string>("url"),
                            Order = feedImageUrlRow.GetValue<int>("in_order")
                        };

                        images.Add(image);
                    }

                    feed = new Feed
                    {
                        FeedId = feedId,
                        FeedType = feedType,
                        FeedImage = new FeedImage
                        {
                            Caption = caption,
                            Images = images
                        },
                        NegativePoints = negativePoints,
                        PositivePoints = positivePoints,
                        NumberOfComments = numberOfFeed,
                        HasVoted = hasVoted,
                        IsUpVoted = isUpVoted,
                        User = ownerUser,
                        CreatedOnTimestamp = feedCreatedOnTimestamp
                    };
                }

            }
            else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
            {
                PreparedStatement psFeedVideo = session.Prepare(CQLGenerator.SelectStatement("feed_video",
                    new List<string> { "video_url", "video_thumbnail_url", "caption" }, new List<string> { "id", "is_valid" }));
                BoundStatement bsFeedVideo = psFeedVideo.Bind(feedId, true);

                Row feedVideoRow = session.Execute(bsFeedVideo).FirstOrDefault();

                if (feedVideoRow != null)
                {
                    string caption = feedVideoRow.GetValue<string>("caption");
                    string videoUrl = feedVideoRow.GetValue<string>("video_url");
                    string videoThumbnailUrl = feedVideoRow.GetValue<string>("video_thumbnail_url");

                    feed = new Feed
                    {
                        FeedId = feedId,
                        FeedType = feedType,
                        FeedVideo = new FeedVideo
                        {
                            Caption = caption,
                            VideoUrl = videoUrl,
                            VideoThumbnailUrl = videoThumbnailUrl
                        },
                        NegativePoints = negativePoints,
                        PositivePoints = positivePoints,
                        NumberOfComments = numberOfFeed,
                        HasVoted = hasVoted,
                        IsUpVoted = isUpVoted,
                        User = ownerUser,
                        CreatedOnTimestamp = feedCreatedOnTimestamp
                    };
                }

            }
            else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
            {
                PreparedStatement psFeedSharedUrl = session.Prepare(CQLGenerator.SelectStatement("feed_shared_url",
                    new List<string> { "caption", "url", "url_title", "url_description", "url_site_name", "url_image_url" }, new List<string> { "id", "is_valid" }));
                BoundStatement bsFeedSharedUrl = psFeedSharedUrl.Bind(feedId, true);

                Row feedSharedUrlRow = session.Execute(bsFeedSharedUrl).FirstOrDefault();

                if (feedSharedUrlRow != null)
                {
                    string caption = feedSharedUrlRow.GetValue<string>("caption");
                    string url = feedSharedUrlRow.GetValue<string>("url");
                    string title = feedSharedUrlRow.GetValue<string>("url_title");
                    string description = feedSharedUrlRow.GetValue<string>("url_description");
                    string siteName = feedSharedUrlRow.GetValue<string>("url_site_name");
                    string imageUrl = feedSharedUrlRow.GetValue<string>("url_image_url");

                    feed = new Feed
                    {
                        FeedId = feedId,
                        FeedType = feedType,
                        FeedSharedUrl = new FeedSharedUrl
                        {
                            Caption = caption,
                            Url = url,
                            Title = title,
                            Description = description,
                            SiteName = siteName,
                            ImageUrl = imageUrl
                        },
                        NegativePoints = negativePoints,
                        PositivePoints = positivePoints,
                        NumberOfComments = numberOfFeed,
                        HasVoted = hasVoted,
                        IsUpVoted = isUpVoted,
                        User = ownerUser,
                        CreatedOnTimestamp = feedCreatedOnTimestamp
                    };
                }

            }

            return feed;
        }

        private Dictionary<string, int> GetPointsForFeed(string feedId, ISession session)
        {
            Dictionary<string, int> pointDict = new Dictionary<string, int>();

            try
            {
                PreparedStatement psFeedCounter = session.Prepare(CQLGenerator.SelectStatement("feed_counter",
                    new List<string> { "negative_point", "positive_point" }, new List<string> { "feed_id" }));
                BoundStatement bsFeedCounter = psFeedCounter.Bind(feedId);

                Row feedCounterRow = session.Execute(bsFeedCounter).FirstOrDefault();
                if (feedCounterRow != null)
                {
                    int negativePoint = feedCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)feedCounterRow.GetValue<long>("negative_point");
                    int positivePoint = feedCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)feedCounterRow.GetValue<long>("positive_point");
                    pointDict.Add("negativePoint", negativePoint);
                    pointDict.Add("positivePoint", positivePoint);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return pointDict;
        }

        private Dictionary<string, int> GetPointsForComment(string feedId, string commentId, ISession session)
        {
            Dictionary<string, int> pointDict = new Dictionary<string, int>();

            try
            {
                PreparedStatement psCommentCounter = session.Prepare(CQLGenerator.SelectStatement("comment_counter",
                    new List<string> { "negative_point", "positive_point" }, new List<string> { "feed_id", "comment_id" }));
                BoundStatement bsCommentCounter = psCommentCounter.Bind(feedId, commentId);

                Row commentCounterRow = session.Execute(bsCommentCounter).FirstOrDefault();
                if (commentCounterRow != null)
                {
                    int negativePoint = commentCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)commentCounterRow.GetValue<long>("negative_point");
                    int positivePoint = commentCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)commentCounterRow.GetValue<long>("positive_point");
                    pointDict.Add("negativePoint", negativePoint);
                    pointDict.Add("positivePoint", positivePoint);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return pointDict;
        }

        private Dictionary<string, int> GetPointsForReply(string commentId, string replyId, ISession session)
        {
            Dictionary<string, int> pointDict = new Dictionary<string, int>();

            try
            {
                PreparedStatement psReplyCounter = session.Prepare(CQLGenerator.SelectStatement("reply_counter",
                    new List<string> { "negative_point", "positive_point" }, new List<string> { "comment_id", "reply_id" }));
                BoundStatement bsReplyCounter = psReplyCounter.Bind(commentId, replyId);

                Row replyCounterRow = session.Execute(bsReplyCounter).FirstOrDefault();
                if (replyCounterRow != null)
                {
                    int negativePoint = replyCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)replyCounterRow.GetValue<long>("negative_point");
                    int positivePoint = replyCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)replyCounterRow.GetValue<long>("positive_point");
                    pointDict.Add("negativePoint", negativePoint);
                    pointDict.Add("positivePoint", positivePoint);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return pointDict;
        }

        private int GetNumberOfCommentsForFeed(string feedId, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psFeedCounter = session.Prepare(CQLGenerator.SelectStatement("feed_counter",
                    new List<string> { "comment" }, new List<string> { "feed_id" }));
                BoundStatement bsFeedCounter = psFeedCounter.Bind(feedId);

                Row feedCounterRow = session.Execute(bsFeedCounter).FirstOrDefault();
                if (feedCounterRow != null)
                {
                    number = feedCounterRow.GetValue<Nullable<long>>("comment") == null ? 0 : (int)feedCounterRow.GetValue<long>("comment");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        private int GetNumberOfRepliesForComment(string feedId, string commentId, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psCommentCounter = session.Prepare(CQLGenerator.SelectStatement("comment_counter",
                    new List<string> { "reply" }, new List<string> { "feed_id", "comment_id" }));
                BoundStatement bsCommentCounter = psCommentCounter.Bind(feedId, commentId);

                Row commentCounterRow = session.Execute(bsCommentCounter).FirstOrDefault();
                if (commentCounterRow != null)
                {
                    number = commentCounterRow.GetValue<Nullable<long>>("reply") == null ? 0 : (int)commentCounterRow.GetValue<long>("reply");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        private List<BoundStatement> CreateFeedPrivacy(string ownerUserId,
                                                       string feedId,
                                                       string companyId,
                                                       int feedType,
                                                       string feedTextContent,
                                                       List<string> targetedDepartmentIds,
                                                       List<string> targetedUserIds,
                                                       ISession session)
        {
            List<BoundStatement> boundStatements = new List<BoundStatement>();
            PreparedStatement preparedStatement = null;

            try
            {
                bool isForDepartment = targetedDepartmentIds != null && targetedDepartmentIds.Count != 0 ? true : false;
                bool isForUser = targetedUserIds != null && targetedUserIds.Count != 0 ? true : false;
                bool isForEveryone = !isForDepartment && !isForUser ? true : false;

                if (!isForEveryone)
                {
                    if (isForDepartment)
                    {
                        foreach (string departmentId in targetedDepartmentIds)
                        {
                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_targeted_department",
                                new List<string> { "feed_id", "department_id" }));
                            boundStatements.Add(preparedStatement.Bind(feedId, departmentId));
                        }
                    }

                    if (isForUser)
                    {
                        foreach (string userId in targetedUserIds)
                        {
                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_targeted_user",
                                new List<string> { "feed_id", "user_id" }));
                            boundStatements.Add(preparedStatement.Bind(feedId, userId));
                        }
                    }
                }

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_privacy",
                            new List<string> { "feed_id", "company_id", "owner_user_id", "feed_type", "is_feed_valid", "is_for_everyone", "is_for_department", "is_for_user", "feed_created_on_timestamp", "is_posted_by_admin" }));
                boundStatements.Add(preparedStatement.Bind(feedId, companyId, ownerUserId, feedType, true, isForEveryone, isForDepartment, isForUser, DateHelper.ConvertDateToLong(DateTime.UtcNow), false));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_privacy_by_company_timestamp_desc",
                            new List<string> { "feed_id", "company_id", "owner_user_id", "feed_type", "feed_text_content", "is_feed_valid", "is_for_everyone", "is_for_department", "is_for_user", "feed_created_on_timestamp", "is_posted_by_admin" }));
                boundStatements.Add(preparedStatement.Bind(feedId, companyId, ownerUserId, feedType, feedTextContent, true, isForEveryone, isForDepartment, isForUser, DateHelper.ConvertDateToLong(DateTime.UtcNow), false));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("feed_privacy_by_company_timestamp_asc",
                    new List<string> { "feed_id", "company_id", "owner_user_id", "feed_type", "feed_text_content", "is_feed_valid", "is_for_everyone", "is_for_department", "is_for_user", "feed_created_on_timestamp", "is_posted_by_admin" }));
                boundStatements.Add(preparedStatement.Bind(feedId, companyId, ownerUserId, feedType, feedTextContent, true, isForEveryone, isForDepartment, isForUser, DateHelper.ConvertDateToLong(DateTime.UtcNow), false));

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return boundStatements;
        }

        public void UpdateFeedCommentCounter(string feedId, int operatorValue, ISession session)
        {
            try
            {
                PreparedStatement preparedStatement = null;
                BoundStatement boundStatement = null;

                preparedStatement = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                    new List<string> { "feed_id" }, new List<string> { "comment" }, new List<int> { operatorValue }));
                boundStatement = preparedStatement.Bind(feedId);
                session.Execute(boundStatement);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UpdateCommentReplyCounter(string feedId, string commentId, int operatorValue, ISession session)
        {
            try
            {
                PreparedStatement preparedStatement = null;
                BoundStatement boundStatement = null;

                preparedStatement = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                    new List<string> { "feed_id", "comment_id" }, new List<string> { "reply" }, new List<int> { operatorValue }));
                boundStatement = preparedStatement.Bind(feedId, commentId);
                session.Execute(boundStatement);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public BoundStatement UpdateFeedPointCounter(string userId, string feedId, bool isUpVote, ISession session)
        {
            BoundStatement boundStatement = null;

            try
            {
                string pointColumnName = string.Empty;

                if (isUpVote)
                {
                    pointColumnName = CassandraColumn.FeedPointPositive;
                }
                else
                {
                    pointColumnName = CassandraColumn.FeedPointNegative;
                }

                PreparedStatement psFeedPoint = session.Prepare(CQLGenerator.SelectStatement("feed_point",
                    new List<string> { "value" }, new List<string> { "feed_id", "user_id" }));
                BoundStatement bsFeedPoint = psFeedPoint.Bind(feedId, userId);

                Row feedPointRow = session.Execute(bsFeedPoint).FirstOrDefault();

                if (feedPointRow != null)
                {
                    int value = feedPointRow.GetValue<int>("value");
                    int currentVote = 1;

                    if (!isUpVote)
                    {
                        currentVote = -1;
                    }

                    if (value != currentVote)
                    {
                        // Add 1
                        PreparedStatement psFeedCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                            new List<string> { "feed_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                        BoundStatement bsFeedCounter = psFeedCounter.Bind(feedId);
                        session.Execute(bsFeedCounter);

                        // Remove 1
                        psFeedCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                            new List<string> { "feed_id" }, new List<string> { isUpVote ? CassandraColumn.FeedPointNegative : CassandraColumn.FeedPointPositive }, new List<int> { -1 }));
                        bsFeedCounter = psFeedCounter.Bind(feedId);
                        session.Execute(bsFeedCounter);

                        psFeedPoint = session.Prepare(CQLGenerator.UpdateStatement("feed_point",
                            new List<string> { "feed_id", "user_id" }, new List<string> { "value", "voted_timestamp" }, new List<string>()));
                        bsFeedPoint = psFeedPoint.Bind(isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow), feedId, userId);
                        boundStatement = bsFeedPoint;

                    }
                }
                else
                {
                    PreparedStatement psFeedCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                        new List<string> { "feed_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                    BoundStatement bsFeedCounter = psFeedCounter.Bind(feedId);
                    session.Execute(bsFeedCounter);

                    psFeedPoint = session.Prepare(CQLGenerator.InsertStatement("feed_point",
                        new List<string> { "feed_id", "user_id", "value", "voted_timestamp" }));
                    bsFeedPoint = psFeedPoint.Bind(feedId, userId, isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow));
                    boundStatement = bsFeedPoint;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return boundStatement;
        }


        public BoundStatement UpdateCommentPointCounter(string userId, string feedId, string commentId, bool isUpVote, ISession session)
        {
            BoundStatement boundStatement = null;

            try
            {
                string pointColumnName = string.Empty;

                if (isUpVote)
                {
                    pointColumnName = CassandraColumn.FeedPointPositive;
                }
                else
                {
                    pointColumnName = CassandraColumn.FeedPointNegative;
                }

                PreparedStatement psCommentPoint = session.Prepare(CQLGenerator.SelectStatement("comment_point",
                    new List<string> { "value" }, new List<string> { "comment_id", "user_id" }));
                BoundStatement bsCommentPoint = psCommentPoint.Bind(commentId, userId);

                Row commentPointRow = session.Execute(bsCommentPoint).FirstOrDefault();

                if (commentPointRow != null)
                {
                    int value = commentPointRow.GetValue<int>("value");
                    int currentVote = 1;

                    if (!isUpVote)
                    {
                        currentVote = -1;
                    }

                    if (value != currentVote)
                    {
                        PreparedStatement psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                            new List<string> { "feed_id", "comment_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                        BoundStatement bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                        session.Execute(bsCommentCounter);

                        psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                            new List<string> { "feed_id", "comment_id" }, new List<string> { isUpVote ? CassandraColumn.FeedPointNegative : CassandraColumn.FeedPointPositive }, new List<int> { -1 }));
                        bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                        session.Execute(bsCommentCounter);

                        psCommentPoint = session.Prepare(CQLGenerator.UpdateStatement("comment_point",
                            new List<string> { "comment_id", "user_id" }, new List<string> { "value", "voted_timestamp" }, new List<string>()));
                        bsCommentPoint = psCommentPoint.Bind(isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow), commentId, userId);
                        boundStatement = bsCommentPoint;
                    }
                }
                else
                {
                    PreparedStatement psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                        new List<string> { "feed_id", "comment_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                    BoundStatement bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                    session.Execute(bsCommentCounter);

                    psCommentPoint = session.Prepare(CQLGenerator.InsertStatement("comment_point",
                        new List<string> { "comment_id", "user_id", "value", "voted_timestamp" }));
                    bsCommentPoint = psCommentPoint.Bind(commentId, userId, isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow));
                    boundStatement = bsCommentPoint;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return boundStatement;
        }

        public BoundStatement UpdateReplyPointCounter(string userId, string commentId, string replyId, bool isUpVote, ISession session)
        {
            BoundStatement boundStatement = null;

            try
            {
                string pointColumnName = string.Empty;

                if (isUpVote)
                {
                    pointColumnName = CassandraColumn.FeedPointPositive;
                }
                else
                {
                    pointColumnName = CassandraColumn.FeedPointNegative;
                }

                PreparedStatement psReplyPoint = session.Prepare(CQLGenerator.SelectStatement("reply_point",
                    new List<string> { "value" }, new List<string> { "reply_id", "user_id" }));
                BoundStatement bsReplyPoint = psReplyPoint.Bind(replyId, userId);

                Row replyPointRow = session.Execute(bsReplyPoint).FirstOrDefault();

                if (replyPointRow != null)
                {
                    int value = replyPointRow.GetValue<int>("value");
                    int currentVote = 1;

                    if (!isUpVote)
                    {
                        currentVote = -1;
                    }

                    if (value != currentVote)
                    {
                        PreparedStatement psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter",
                            new List<string> { "comment_id", "reply_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                        BoundStatement bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                        session.Execute(bsReplyCounter);

                        psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter",
                            new List<string> { "comment_id", "reply_id" }, new List<string> { isUpVote ? CassandraColumn.FeedPointNegative : CassandraColumn.FeedPointPositive }, new List<int> { -1 }));
                        bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                        session.Execute(bsReplyCounter);

                        psReplyPoint = session.Prepare(CQLGenerator.UpdateStatement("reply_point",
                            new List<string> { "reply_id", "user_id" }, new List<string> { "value", "voted_timestamp" }, new List<string>()));
                        bsReplyPoint = psReplyPoint.Bind(isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow), replyId, userId);
                        boundStatement = bsReplyPoint;

                    }
                }
                else
                {
                    PreparedStatement psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter",
                        new List<string> { "comment_id", "reply_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                    BoundStatement bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                    session.Execute(bsReplyCounter);

                    psReplyPoint = session.Prepare(CQLGenerator.InsertStatement("reply_point",
                        new List<string> { "reply_id", "user_id", "value", "voted_timestamp" }));
                    bsReplyPoint = psReplyPoint.Bind(replyId, userId, isUpVote ? 1 : -1, DateHelper.ConvertDateToLong(DateTime.UtcNow));
                    boundStatement = bsReplyPoint;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return boundStatement;
        }
    }


    #region Feed type post
    public class FeedText
    {
        [DataMember(EmitDefaultValue = false)]
        public string Content { get; set; }
    }
    public class FeedImage
    {
        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Image> Images { get; set; }
    }
    public class Image
    {
        [DataMember(EmitDefaultValue = false)]
        public string ImageId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Url { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Order { get; set; }
    }
    public class FeedVideo
    {
        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VideoUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VideoThumbnailUrl { get; set; }

    }
    public class FeedSharedUrl
    {
        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Url { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SiteName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ImageUrl { get; set; }
    }
    #endregion

    #region Comment
    public class Comment
    {
        [DataMember(EmitDefaultValue = false)]
        public string CommentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CommentType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public CommentText CommentText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Reply Reply { get; set; }

        [DataMember]
        public int NegativePoints { get; set; }

        [DataMember]
        public int PositivePoints { get; set; }

        [DataMember]
        public int NumberOfReplies { get; set; }

        [DataMember]
        public bool HasVoted { get; set; }

        [DataMember]
        public bool IsUpVoted { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User User { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }
    }

    public class CommentText
    {
        [DataMember(EmitDefaultValue = false)]
        public string Content { get; set; }
    }
    #endregion

    #region Reply
    public class Reply
    {
        [DataMember(EmitDefaultValue = false)]
        public string ReplyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReplyType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ReplyText ReplyText { get; set; }

        [DataMember]
        public int NegativePoints { get; set; }

        [DataMember]
        public int PositivePoints { get; set; }

        [DataMember]
        public bool HasVoted { get; set; }

        [DataMember]
        public bool IsUpVoted { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User User { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }
    }

    public class ReplyText
    {
        [DataMember(EmitDefaultValue = false)]
        public string Content { get; set; }
    }
    #endregion

}