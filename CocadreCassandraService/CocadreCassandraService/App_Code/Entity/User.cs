﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Cassandra;
using CocadreCassandraService.App_GlobalResources;
using CocadreCassandraService.Connection;
using CocadreCassandraService.CQL;
using CocadreCassandraService.Crypto;
using CocadreCassandraService.Date;
using CocadreCassandraService.Error.Classes;
using CocadreCassandraService.IDGenerator;
using CocadreCassandraService.ServiceResponses;
using CocadreCassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using CocadreCassandraService.Utilities.AWS;

namespace CocadreCassandraService.Entity
{
    [DataContract]
    public class User
    {

        private static ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [DataMember(EmitDefaultValue = false)]
        public string UserToken { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Email { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FirstName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProfileImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset LastActiveTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset LastModifiedProfileTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset LastModifiedStatusTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Address { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AddressPostalCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AddressCountryAbb { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AddressCountryName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Phone { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PhoneCountryCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PhoneCountryName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Company Company { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Department> Departments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public AccountStatus Status { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public AccountType Type { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Position { get; set; }

        #region Exp
        [DataMember(EmitDefaultValue = false)]
        public int Level { get; set; }
        #endregion

        [DataContract]
        public class AccountType
        {
            [DataMember(EmitDefaultValue = false)]
            public const int CODE_NORMAL_USER = 1;
            [DataMember(EmitDefaultValue = false)]
            public const int CODE_MODERATER = 2;
            [DataMember(EmitDefaultValue = false)]
            public const int CODE_ADMIN = 3;
            [DataMember(EmitDefaultValue = false)]
            public const int CODE_SUPER_ADMIN = 999;

            [DataMember(EmitDefaultValue = false)]
            public int Code { get; private set; }

            [DataMember(EmitDefaultValue = false)]
            public String Title { get; private set; }

            public AccountType(int code)
            {
                switch (code)
                {
                    case CODE_NORMAL_USER:
                        Code = CODE_NORMAL_USER;
                        Title = "Normal User";
                        break;
                    case CODE_MODERATER:
                        Code = CODE_MODERATER;
                        Title = "Moderater";
                        break;
                    case CODE_ADMIN:
                        Code = CODE_ADMIN;
                        Title = "Admin";
                        break;
                    case CODE_SUPER_ADMIN:
                        Code = CODE_SUPER_ADMIN;
                        Title = "Super Admin";
                        break;

                    default:
                        break;
                }
            }
        }

        [DataContract]
        public class AccountStatus
        {
            [DataMember(EmitDefaultValue = false)]
            public const int CODE_ACTIVE = 1;
            [DataMember(EmitDefaultValue = false)]
            public const int CODE_SUSPENEDED = -1;
            [DataMember(EmitDefaultValue = false)]
            public const int CODE_DELETING = -2;
            [DataMember(EmitDefaultValue = false)]
            public const int CODE_DELETED = -999;

            [DataMember(EmitDefaultValue = false)]
            public int Code { get; private set; }

            [DataMember(EmitDefaultValue = false)]
            public String Title { get; private set; }

            public AccountStatus(int code)
            {
                switch (code)
                {
                    case CODE_ACTIVE:
                        Code = CODE_ACTIVE;
                        Title = "Active";
                        break;
                    case CODE_SUSPENEDED:
                        Code = CODE_SUSPENEDED;
                        Title = "Suspended";
                        break;
                    case CODE_DELETING:
                        Code = CODE_DELETING;
                        Title = "Deleting";
                        break;
                    case CODE_DELETED:
                        Code = CODE_DELETED;
                        Title = "Deleted";
                        break;
                    default:
                        break;
                }
            }
        }

        [DataContract]
        public enum DeviceTokenType
        {
            [EnumMember]
            iOS = 1,

            [EnumMember]
            Android = 2
        }


        public UserSelectBasicResponse SelectUserBasic(string userId,
                                                       string companyId,
                                                       bool checkForValid,
                                                       ISession session = null,
                                                       string startsWithName = null)
        {
            UserSelectBasicResponse response = new UserSelectBasicResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager conn_manager = new ConnectionManager();
                    session = conn_manager.getMainSession();
                }
                PreparedStatement psUser = null;
                BoundStatement bsUser = null;

                if (!checkForValid)
                {
                    psUser = session.Prepare(CQLGenerator.SelectStatement("user_basic",
                        new List<string> { "first_name", "last_name", "profile_image_url" }, new List<string> { "id" }));
                    bsUser = psUser.Bind(userId);
                }
                else
                {
                    PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_account_type",
                        new List<string>(), new List<string> { "user_id", "account_status" }));
                    BoundStatement bsAuthentication = psAuthentication.Bind(userId, User.AccountStatus.CODE_ACTIVE);
                    Row authenticationRow = session.Execute(bsAuthentication).FirstOrDefault();

                    if (authenticationRow != null)
                    {
                        psUser = session.Prepare(CQLGenerator.SelectStatement("user_basic",
                            new List<string> { "first_name", "last_name", "profile_image_url" }, new List<string> { "id" }));
                        bsUser = psUser.Bind(userId);
                    }

                }

                Row userRow = null;

                if (bsUser != null)
                {
                    userRow = session.Execute(bsUser).FirstOrDefault();
                }


                if (userRow != null)
                {
                    string firstName = userRow["first_name"].ToString();

                    bool isFound = true;

                    if (!string.IsNullOrEmpty(startsWithName))
                    {
                        isFound = firstName.ToLower().StartsWith(startsWithName.ToLower());
                    }

                    if (isFound)
                    {
                        string lastName = userRow["last_name"].ToString();
                        string profileImageUrl = userRow["profile_image_url"].ToString();

                        response.User = new User
                        {
                            UserId = userId,
                            FirstName = firstName,
                            LastName = lastName,
                            ProfileImageUrl = profileImageUrl
                        };
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectTokenWithCompanyResponse SelectUserWithTokenAndCompany(String userId, String companyId)
        {
            UserSelectTokenWithCompanyResponse response = new UserSelectTokenWithCompanyResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                Row companyRow = vh.ValidateCompany(companyId, session);

                if (companyRow != null)
                {
                    Row userRow = vh.ValidateUser(userId, companyId, session);

                    if (userRow != null)
                    {
                        Authenticator authenticator = new Authenticator();
                        string userToken = authenticator.CreateAuthenticationToken(companyId, userId, session);

                        if (!string.IsNullOrEmpty(userToken))
                        {
                            string companyTitle = companyRow.GetValue<string>("title");
                            string companyLogoUrl = companyRow.GetValue<string>("logo_url");

                            Company company = new Company
                            {
                                CompanyId = companyId,
                                CompanyTitle = companyTitle,
                                CompanyLogoUrl = companyLogoUrl
                            };

                            string firstName = userRow.GetValue<string>("first_name");
                            string lastName = userRow.GetValue<string>("last_name");
                            string email = userRow.GetValue<string>("email");
                            string profileImageUrl = userRow.GetValue<string>("profile_image_url");

                            List<Department> departments = new Department().GetAllDepartmentByUserId(userId, companyId, session).Departments;

                            User user = new User
                            {
                                UserId = userId,
                                FirstName = firstName,
                                LastName = lastName,
                                Email = email,
                                ProfileImageUrl = profileImageUrl,
                                Company = company,
                                UserToken = userToken,
                                Departments = departments
                            };

                            response.User = user;
                            response.Success = true;

                            Analytic analytic = new Analytic();
                            analytic.UpdateUserLogin(userId, companyId, true);
                        }

                    }
                    else
                    {
                        Log.Error("Invalid userId: " + userId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectAllByDepartmentResponse SelectAllUsersSortedByDepartment(string requesterUserId, string companyId)
        {
            UserSelectAllByDepartmentResponse response = new UserSelectAllByDepartmentResponse();
            response.Departments = new List<Department>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);

                if (companyRow != null)
                {
                    response.Success = true;

                    List<Department> departments = new Department().GetAllDepartment(null, companyId, Department.QUERY_TYPE_BASIC, session).Departments;

                    foreach (Department department in departments)
                    {
                        department.Users = new List<User>();

                        PreparedStatement psColleagues = session.Prepare(CQLGenerator.SelectStatement("user_by_department",
                            new List<string> { "user_id", "position" }, new List<string> { "department_id" }));
                        BoundStatement bsColleagues = psColleagues.Bind(department.Id);
                        RowSet colleagueRowset = session.Execute(bsColleagues);

                        foreach (Row colleagueRow in colleagueRowset)
                        {
                            string userId = colleagueRow.GetValue<string>("user_id");
                            if (!userId.Equals(requesterUserId))
                            {
                                User colleague = SelectUserBasic(userId, companyId, true, session).User;

                                if (colleague != null)
                                {
                                    colleague.Position = colleagueRow.GetValue<string>("position");
                                    department.Users.Add(colleague);
                                }

                            }
                        }
                        response.Departments.Add(department);
                    }
                }
                else
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserSelectAllBasicResponse SelectAllUsersByDepartments(List<Department> departments, string requesterUserId, string companyId, ISession session, string startsWithName = null)
        {
            UserSelectAllBasicResponse response = new UserSelectAllBasicResponse();
            response.Users = new List<User>();
            response.Success = false;
            try
            {
                foreach (Department department in departments)
                {
                    PreparedStatement psColleagues = session.Prepare(CQLGenerator.SelectStatement("user_by_department",
                        new List<string> { "user_id", "position" }, new List<string> { "department_id" }));
                    BoundStatement bsColleagues = psColleagues.Bind(department.Id);
                    RowSet colleagueRowset = session.Execute(bsColleagues);

                    foreach (Row colleagueRow in colleagueRowset)
                    {
                        string userId = colleagueRow.GetValue<string>("user_id");
                        string position = colleagueRow.GetValue<string>("position");
                        if (!response.Users.Any(x => x.UserId == userId) && !userId.Equals(requesterUserId))
                        {
                            User colleague = SelectUserBasic(userId, companyId, true, session, startsWithName).User;
                            if (colleague != null)
                            {
                                colleague.Position = position;
                                response.Users.Add(colleague);
                            }

                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }


        public UserListResponse GetAllUserForAdmin(String adminUserId, String companyId, String departmentId, int userTypeCode, int userStatusCode, ISession session = null)
        {
            UserListResponse response = new UserListResponse();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    #region Step 1. Check data.
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                    #region Step 1.1 Check Admin account's validation.
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                    #endregion
                    #endregion
                }


                #region Step 2. Read database.
                List<User> queryUsers = new List<User>();
                #region Step 2.1 判斷要讀取那些 users.
                PreparedStatement ps_department_by_company;
                BoundStatement bs_department_by_company;
                if (String.IsNullOrEmpty(departmentId))
                {
                    ps_department_by_company = session.Prepare(CQLGenerator.SelectStatement("department_by_company", new List<string> { }, new List<string> { "company_id" }));
                    bs_department_by_company = ps_department_by_company.Bind(companyId);

                }
                else
                {
                    ps_department_by_company = session.Prepare(CQLGenerator.SelectStatement("department_by_company", new List<string> { }, new List<string> { "company_id", "department_id" }));
                    bs_department_by_company = ps_department_by_company.Bind(companyId, departmentId);
                }
                RowSet rowsDepartment = session.Execute(bs_department_by_company);
                if (rowsDepartment != null)
                {
                    foreach (Row rowDepartment in rowsDepartment.GetRows())
                    {
                        PreparedStatement ps_user_by_department = session.Prepare(CQLGenerator.SelectStatement("user_by_department", new List<string> { }, new List<string> { "department_id" })); ;
                        BoundStatement bs_user_by_department = ps_user_by_department.Bind(rowDepartment.GetValue<String>("department_id"));
                        RowSet rowsUser = session.Execute(bs_user_by_department);
                        if (rowsUser != null)
                        {
                            foreach (Row rowUser in rowsUser)
                            {
                                PreparedStatement ps_user_account_type = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" })); ;
                                BoundStatement bs_user_account_type = ps_user_account_type.Bind(rowUser.GetValue<String>("user_id"));
                                Row row = session.Execute(bs_user_account_type).FirstOrDefault();
                                if (row != null)
                                {
                                    User user = new User();
                                    user.Company = new Company { CompanyId = companyId };
                                    user.Departments = new List<Department>();
                                    user.Departments.Add(new Department { Id = rowUser.GetValue<String>("department_id"), Position = rowUser.GetValue<String>("position") });
                                    user.UserId = row.GetValue<String>("user_id");
                                    user.Type = new AccountType(row.GetValue<int>("account_type"));
                                    user.Status = new AccountStatus(row.GetValue<int>("account_status"));
                                    user.LastModifiedStatusTimestamp = row.GetValue<DateTimeOffset>("last_modified_timestamp");

                                    if (userTypeCode != 0 && userStatusCode != 0)
                                    {
                                        if (row.GetValue<int>("account_type") == userTypeCode && row.GetValue<int>("account_status") == userStatusCode)
                                        {
                                            if (!queryUsers.Contains(user))
                                            {
                                                queryUsers.Add(user);
                                            }
                                        }
                                    }
                                    else if (userTypeCode != 0 && userStatusCode == 0)
                                    {
                                        if (row.GetValue<int>("account_type") == userTypeCode)
                                        {
                                            if (!queryUsers.Contains(user))
                                            {
                                                queryUsers.Add(user);
                                            }
                                        }
                                    }
                                    else if (userTypeCode == 0 && userStatusCode != 0)
                                    {
                                        if (row.GetValue<int>("account_status") == userStatusCode)
                                        {
                                            if (!queryUsers.Contains(user))
                                            {
                                                queryUsers.Add(user);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!queryUsers.Contains(user))
                                        {
                                            queryUsers.Add(user);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Step 2.2 Get data of users.
                for (int i = 0; i < queryUsers.Count; i++)
                {
                    PreparedStatement ps_user_basic = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" })); ;
                    BoundStatement bs_user_basic = ps_user_basic.Bind(queryUsers[i].UserId);
                    Row row = session.Execute(bs_user_basic).FirstOrDefault();
                    if (row != null)
                    {
                        queryUsers[i].FirstName = row.GetValue<String>("first_name");
                        queryUsers[i].LastName = row.GetValue<String>("last_name");
                        queryUsers[i].ProfileImageUrl = row.GetValue<String>("profile_image_url");
                        queryUsers[i].Email = row.GetValue<String>("email");
                        queryUsers[i].LastModifiedProfileTimestamp = row.GetValue<DateTimeOffset>("last_modified_timestamp");

                        for (int j = 0; j < queryUsers[i].Departments.Count; j++)
                        {
                            PreparedStatement ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" })); ;
                            BoundStatement bs_department = ps_department.Bind(companyId, queryUsers[i].Departments[j].Id, true);
                            Row row_department = session.Execute(bs_department).FirstOrDefault();
                            if (row_department != null)
                            {
                                queryUsers[i].Departments[j].Title = row_department.GetValue<String>("title");
                            }
                        }
                    }
                }
                response.Users = queryUsers;
                response.Success = true;
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserListResponse GetAdmin(String adminUserId, String companyId)
        {
            UserListResponse response = new UserListResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database.





                RowSet rowSet = session.Execute(@"
    SELECT user_id FROM department_by_user WHERE department_id IN 
        (SELECT department_id FROM department_by_company WHERE company_id ='" + companyId + @"')
    ;");

                //                RowSet rowSet = session.Execute(@"
                //SELECT * FROM user_account_type WHERE account_type = 3 AND user_id IN 
                //    (SELECT user_id FROM department_by_user WHERE department_id IN 
                //        (SELECT department_id FROM department_by_company WHERE company_id ='" + companyId + @"')
                //    );");
                if (rowSet != null)
                {
                    List<User> users = new List<User>();
                    foreach (Row row in rowSet)
                    {
                        users.Add(new User { UserId = row.GetValue<String>("user_id") });
                    }
                    response.Users = users;
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.UserNoAdmin);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserNoAdmin);
                    response.ErrorMessage = ErrorMessage.UserNoAdmin;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserDetailResponse GetUserDetail(String adminUserId, String companyId, String userId)
        {
            UserDetailResponse response = new UserDetailResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion



                #endregion

                #region Step 2. Read database.
                User user = new User();

                #region company information of user
                PreparedStatement ps_company = session.Prepare(CQLGenerator.SelectStatement("company", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_company = ps_company.Bind(companyId);
                Row row_company = session.Execute(bs_company).FirstOrDefault();
                if (row_company != null)
                {
                    Company company = new Company();
                    company.CompanyId = companyId;
                    company.CompanyTitle = row_company.GetValue<String>("title");
                    company.CompanyLogoUrl = row_company.GetValue<String>("logo_url");
                    user.Company = company;
                }
                else
                {
                    Log.Error(ErrorMessage.CompanyInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }
                #endregion


                #region departments of user
                List<Department> departments = new List<Department>();
                PreparedStatement ps_department_by_user = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" })); ;
                BoundStatement bs_department_by_user = ps_department_by_user.Bind(userId);
                RowSet rowSet = session.Execute(bs_department_by_user);
                foreach (Row r in rowSet)
                {
                    Department department = new Department();
                    PreparedStatement ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" })); ;
                    BoundStatement bs_department = ps_department.Bind(companyId, r.GetValue<String>("department_id"), true);
                    Row row_department = session.Execute(bs_department).FirstOrDefault();
                    if (row_department == null)
                    {
                        Log.Error(ErrorMessage.DepartmentIsInvalid);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentIsInvalid);
                        response.ErrorMessage = ErrorMessage.DepartmentIsInvalid;
                        return response;
                    }
                    else
                    {
                        department.Id = row_department.GetValue<String>("id");
                        department.Title = row_department.GetValue<String>("title");
                    }

                    PreparedStatement ps_user_by_department = session.Prepare(CQLGenerator.SelectStatement("user_by_department", new List<string> { }, new List<string> { "department_id", "user_id" })); ;
                    BoundStatement bs_user_by_department = ps_user_by_department.Bind(r.GetValue<String>("department_id"), userId);
                    Row row_user_by_department = session.Execute(bs_user_by_department).FirstOrDefault();
                    if (row_user_by_department != null)
                    {
                        department.Position = row_user_by_department.GetValue<String>("position");
                    }
                    departments.Add(department);
                }
                user.Departments = departments;
                #endregion

                #region account type of user
                PreparedStatement ps_user_account_type = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs_user_account_type = ps_user_account_type.Bind(userId);
                Row row_user_account_type = session.Execute(bs_user_account_type).FirstOrDefault();
                if (row_user_account_type != null)
                {
                    user.Type = new AccountType(row_user_account_type.GetValue<int>("account_type"));
                    user.Status = new AccountStatus(row_user_account_type.GetValue<int>("account_status"));

                    Log.Debug("last_modified_timestamp: " + row_user_account_type.GetValue<DateTimeOffset>("last_modified_timestamp").ToString("yyyyMMdd"));

                    user.LastModifiedStatusTimestamp = row_user_account_type.GetValue<DateTimeOffset>("last_modified_timestamp");
                }
                else
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion


                #region basic information of user
                PreparedStatement ps_user_basic = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_user_basic = ps_user_basic.Bind(userId);
                Row row_user_basic = session.Execute(bs_user_basic).FirstOrDefault();
                if (row_user_basic != null)
                {
                    user.UserId = userId;
                    user.Email = row_user_basic.GetValue<String>("email");
                    user.FirstName = row_user_basic.GetValue<String>("first_name");
                    user.LastName = row_user_basic.GetValue<String>("last_name");
                    user.ProfileImageUrl = row_user_basic.GetValue<String>("profile_image_url");
                    user.LastModifiedProfileTimestamp = row_user_basic.GetValue<DateTimeOffset>("last_modified_timestamp");
                }
                else
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion

                #region contact information of user
                PreparedStatement ps_user_contact = session.Prepare(CQLGenerator.SelectStatement("user_contact", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_user_contact = ps_user_contact.Bind(userId);
                Row row_user_contact = session.Execute(bs_user_contact).FirstOrDefault();
                if (row_user_contact != null)
                {
                    user.Address = row_user_contact.GetValue<String>("address");
                    user.AddressCountryName = row_user_contact.GetValue<String>("address_country_name");
                    user.AddressPostalCode = row_user_contact.GetValue<String>("address_postal_code");
                    user.Phone = row_user_contact.GetValue<String>("phone");
                    user.PhoneCountryCode = row_user_contact.GetValue<String>("phone_country_code");
                    user.PhoneCountryName = row_user_contact.GetValue<String>("phone_country_name");
                }
                #endregion
                response.User = user;
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserSelectAllBasicResponse SelectAllUsersByTopicId(string topicId, string categoryId, string requesterUserId, string companyId, string startsWithName = null)
        {
            UserSelectAllBasicResponse response = new UserSelectAllBasicResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                List<Department> targetedDepartments = new Department().GetAllDepartmentByTopicId(topicId, companyId, session).Departments;
                response.Users = SelectAllUsersByDepartments(targetedDepartments, requesterUserId, companyId, session, startsWithName).Users;
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserCreateResponse CreateAdmin(String companyId, String companyTitle, String companyLogoUrl, String adminUserId, String plainPassword, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode)
        {
            UserCreateResponse response = new UserCreateResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement company_ps = null;

                PreparedStatement user_authentication_ps = null;
                PreparedStatement department_ps = null;
                PreparedStatement department_by_company_ps = null;
                PreparedStatement department_by_user_ps = null;

                PreparedStatement user_basic_ps = null;
                PreparedStatement user_contact_ps = null;
                PreparedStatement user_account_type_ps = null;
                PreparedStatement user_by_department_ps = null;

                BatchStatement batch_statement = new BatchStatement();

                if (string.IsNullOrEmpty(email))
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.UserMissingEmail);
                    response.ErrorMessage = ErrorMessage.UserMissingEmail;
                    return response;
                }
                #endregion

                if (string.IsNullOrEmpty(adminUserId))
                {
                    adminUserId = UUIDGenerator.GenerateUniqueIDForUser();
                }

                #region Step 2. Write data to database.
                #region Company

                if (string.IsNullOrEmpty(companyId))
                {
                    companyId = UUIDGenerator.GenerateUniqueIDForCompany();
                }
                company_ps = session.Prepare(CQLGenerator.InsertStatement("company", new List<string> { "id", "title", "logo_url", "admin_title", "admin_profile_image_url", "is_valid", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                batch_statement.Add(company_ps.Bind(companyId, companyTitle, companyLogoUrl, "Admin", companyLogoUrl, true, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));
                #endregion
                #region user_authentication.
                if (string.IsNullOrEmpty(plainPassword))
                {
                    plainPassword = UUIDGenerator.GenerateNumberPasswordForUser();
                }

                String salt = CryptoLib.GenerateRandomSalt(plainPassword.Length);
                String hashedPassword = CryptoLib.EncryptTextWithSalt(plainPassword, salt);

                user_authentication_ps = session.Prepare(CQLGenerator.InsertStatement("user_authentication", new List<string> { "email", "user_id", "company_id", "created_by_user_id", "created_on_timestamp", "hashed_password", "last_modified_by_user_id", "last_modified_timestamp", "salt", "is_default" }));
                batch_statement.Add(user_authentication_ps.Bind(email, adminUserId, companyId, adminUserId, DateTime.UtcNow, hashedPassword, adminUserId, DateTime.UtcNow, salt, true));
                #endregion

                #region department, department_by_company
                string departmentId = UUIDGenerator.GenerateUniqueIDForDepartment();
                string departmentTitle = ConfigurationManager.AppSettings["default_department_title"].ToString();
                department_ps = session.Prepare(CQLGenerator.InsertStatement("department", new List<string> { "company_id", "id", "created_by_user_id", "created_on_timestamp", "is_valid", "last_modified_by_user_id", "last_modified_timestamp", "title" }));
                batch_statement.Add(department_ps.Bind(companyId, departmentId, adminUserId, DateTime.UtcNow, true, adminUserId, DateTime.UtcNow, departmentTitle));

                department_by_company_ps = session.Prepare(CQLGenerator.InsertStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                batch_statement.Add(department_by_company_ps.Bind(companyId, departmentId));
                #endregion

                #region department_by_user, user_by_department
                department_by_user_ps = session.Prepare(CQLGenerator.InsertStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                batch_statement.Add(department_by_user_ps.Bind(adminUserId, departmentId));

                user_by_department_ps = session.Prepare(CQLGenerator.InsertStatement("user_by_department", new List<string> { "department_id", "user_id", "position", "user_status" }));
                batch_statement.Add(user_by_department_ps.Bind(departmentId, adminUserId, position, User.AccountStatus.CODE_ACTIVE));
                #endregion

                #region user_basic
                if (String.IsNullOrEmpty(profileImageUrl))
                {
                    profileImageUrl = null;
                }
                user_basic_ps = session.Prepare(CQLGenerator.InsertStatement("user_basic", new List<string> { "id", "created_by_user_id", "created_on_timestamp", "email", "first_name", "last_modified_by_user_id", "last_modified_timestamp", "last_name", "profile_image_url" }));
                batch_statement.Add(user_basic_ps.Bind(adminUserId, adminUserId, DateTime.UtcNow, email, firstName, adminUserId, DateTime.UtcNow, lastName, profileImageUrl));
                #endregion

                #region user_contact
                user_contact_ps = session.Prepare(CQLGenerator.InsertStatement("user_contact", new List<string> { "id", "address", "address_country_name", "address_postal_code", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp", "phone", "phone_country_code", "phone_country_name" }));
                batch_statement.Add(user_contact_ps.Bind(adminUserId, address, addressCountryName, postalCode, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow, phoneNumber, phoneCountryCode, phoneCountryName));
                #endregion

                #region user_account_type
                user_account_type_ps = session.Prepare(CQLGenerator.InsertStatement("user_account_type", new List<string> { "user_id", "account_status", "account_type", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                batch_statement.Add(user_account_type_ps.Bind(adminUserId, User.AccountStatus.CODE_ACTIVE, User.AccountType.CODE_ADMIN, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));
                #endregion

                session.Execute(batch_statement);
                #endregion

                #region Step 3. Create bucket on AWS
                try
                {
                    String bucketName = "cocadre-" + companyId.ToLower();
                    String snsTopic = "arn:aws:sns:ap-southeast-1:433645821931:CocadreS3UploadEvent";
                    String filter = "original.jpg";
                    String topicEvent = "s3:ObjectCreated:*";
                    String id = "CocadreSNSS3PutEvent";

                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(ConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), ConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        PutBucketRequest putRequest = new PutBucketRequest();
                        putRequest.BucketName = bucketName;
                        PutBucketResponse putResponse = s3Client.PutBucket(putRequest);

                        List<TopicConfiguration> topicConfigurations = new List<TopicConfiguration>();

                        topicConfigurations.Add(new TopicConfiguration()
                        {
                            Id = id,
                            Events = new List<EventType> { topicEvent },
                            Topic = snsTopic,
                            Filter = new Filter 
                            { 
                                S3KeyFilter = new S3KeyFilter 
                                { 
                                    FilterRules = new List<FilterRule> 
                                    { 
                                        new FilterRule { Name = "Suffix", Value = filter } 
                                    } 
                                } 
                            }
                        });

                        PutBucketNotificationRequest request = new PutBucketNotificationRequest
                        {
                            BucketName = bucketName,
                            TopicConfigurations = topicConfigurations,
                        };

                        PutBucketNotificationResponse notificationResponse = s3Client.PutBucketNotification(request);
                    }
                }
                catch (AmazonS3Exception amazonS3Exception)
                {
                    if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                    {
                        Log.Error("Check the provided AWS Credentials.");
                    }
                    else
                    {
                        Log.Error(string.Format("Error occurred. Message:'{0}' when enabling notifications.", amazonS3Exception.Message));
                    }
                }

                #endregion

                #region Step 3. Send email to user. Using AWS SES.
                try
                {
                    EmailTemplate emailTemplate = new EmailTemplate(EmailTemplate.Type.CREATED_ADMIN, plainPassword);
                    List<string> receivers = new List<string>();
                    receivers.Add(email);
                    Destination destination = new Destination(receivers);
                    Message message = new Message();
                    message.Subject = new Content(emailTemplate.Tilte);
                    Body mBody = new Body();
                    if (emailTemplate.IsHtmlFormat)
                    {
                        mBody.Html = new Content(emailTemplate.Body);
                    }
                    else
                    {
                        mBody.Text = new Content(emailTemplate.Body);
                    }
                    message.Body = mBody;
                    SendEmailRequest request = new SendEmailRequest("admin@cocadre.com", destination, message);
                    AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient("AKIAIRFRL6Z2OYHO4R3Q", "HfL9OECxbpWqEH7KjHtl8T5/dJM6UiVQ66ywHXzY", RegionEndpoint.USWest2);
                    SendEmailResult result = client.SendEmail(request);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString(), ex);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemErrorSendEmailFailed);
                    response.ErrorMessage = ErrorMessage.SystemErrorSendEmailFailed;

                    #region Step 1. Delete data on database.
                    BatchStatement bs = new BatchStatement();

                    company_ps = session.Prepare(CQLGenerator.DeleteStatement("company", new List<string> { "id" }));
                    bs.Add(company_ps.Bind(companyId));

                    user_authentication_ps = session.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "email", "user_id" }));
                    bs.Add(user_authentication_ps.Bind(email, adminUserId));

                    department_ps = session.Prepare(CQLGenerator.DeleteStatement("department", new List<string> { "company_id", "id" }));
                    bs.Add(department_ps.Bind(companyId, departmentId));

                    department_by_company_ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                    bs.Add(department_by_company_ps.Bind(companyId, departmentId));

                    department_by_user_ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                    bs.Add(department_by_user_ps.Bind(adminUserId, departmentId));

                    user_by_department_ps = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "department_id", "user_id" }));
                    bs.Add(user_by_department_ps.Bind(departmentId, adminUserId));

                    user_basic_ps = session.Prepare(CQLGenerator.DeleteStatement("user_basic", new List<string> { "id" }));
                    bs.Add(user_basic_ps.Bind(adminUserId));

                    user_contact_ps = session.Prepare(CQLGenerator.DeleteStatement("user_contact", new List<string> { "id" }));
                    bs.Add(user_contact_ps.Bind(adminUserId));

                    user_account_type_ps = session.Prepare(CQLGenerator.DeleteStatement("user_account_type", new List<string> { "user_id" }));
                    bs.Add(user_account_type_ps.Bind(adminUserId));

                    session.Execute(bs);
                    #endregion

                    #region Step 2. Delete folder on S3.
                    String bucketName = "cocadre-" + companyId.ToLower();

                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(ConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), ConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        s3Client.DeleteObject(deleteFolderRequest);
                    }
                    #endregion

                    return response;
                }
                #endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserCreateResponse Create(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentId, String departmentTitle)
        {
            UserCreateResponse response = new UserCreateResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                String plainPassword = String.Empty;

                PreparedStatement user_authentication_ps = null;
                PreparedStatement department_ps = null;
                PreparedStatement department_by_company_ps = null;
                PreparedStatement department_by_user_ps = null;

                PreparedStatement user_basic_ps = null;
                PreparedStatement user_contact_ps = null;
                PreparedStatement user_account_type_ps = null;
                PreparedStatement query_department_ps = null;
                PreparedStatement query_user_department_ps = null;
                PreparedStatement user_by_department_ps = null;

                BatchStatement batch_statement = new BatchStatement();

                if (string.IsNullOrEmpty(email))
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.UserMissingEmail);
                    response.ErrorMessage = ErrorMessage.UserMissingEmail;
                    return response;
                }

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check Email
                PreparedStatement ps_user_authentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "email", "company_id" }));
                BoundStatement bs_user_authentication = ps_user_authentication.Bind(email, companyId);
                Row row_user_authentication = session.Execute(bs_user_authentication).FirstOrDefault();
                if (row_user_authentication != null)
                {
                    Log.Error(ErrorMessage.UserDuplicatedEmail);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserDuplicatedEmail);
                    response.ErrorMessage = ErrorMessage.UserDuplicatedEmail;
                    return response;
                }
                #endregion
                #endregion


                #region Step 2. Write data to database.
                #region user_authentication.
                if (string.IsNullOrEmpty(plainPassword))
                {
                    plainPassword = UUIDGenerator.GenerateNumberPasswordForUser();
                }

                String salt = CryptoLib.GenerateRandomSalt(plainPassword.Length);
                String hashedPassword = CryptoLib.EncryptTextWithSalt(plainPassword, salt);

                user_authentication_ps = session.Prepare(CQLGenerator.InsertStatement("user_authentication", new List<string> { "email", "user_id", "company_id", "created_by_user_id", "created_on_timestamp", "hashed_password", "last_modified_by_user_id", "last_modified_timestamp", "salt", "is_default" }));
                batch_statement.Add(user_authentication_ps.Bind(email, userId, companyId, adminUserId, DateTime.UtcNow, hashedPassword, adminUserId, DateTime.UtcNow, salt, true));
                #endregion

                #region department, department_by_company
                Row row_department = null;
                bool isNewDepartment = false;

                if (!String.IsNullOrEmpty(departmentId))
                {
                    PreparedStatement ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" }));
                    BoundStatement bs_department = ps_department.Bind(companyId, departmentId, true);
                    row_department = session.Execute(bs_department).FirstOrDefault();
                }

                if (row_department == null)
                {
                    isNewDepartment = true;
                    departmentId = UUIDGenerator.GenerateUniqueIDForDepartment();
                    // It is a new department. Check department title again.
                    PreparedStatement ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "is_valid" }));
                    BoundStatement bs_department = ps_department.Bind(companyId, true);
                    RowSet rowset_department = session.Execute(bs_department);
                    foreach (Row row in rowset_department)
                    {
                        if (row.GetValue<string>("title").ToLower().Trim().Equals(departmentTitle.ToLower().Trim()))
                        {
                            departmentId = row.GetValue<string>("id");
                            isNewDepartment = false;
                            break;
                        }
                    }
                }

                if (isNewDepartment)
                {
                    department_ps = session.Prepare(CQLGenerator.InsertStatement("department", new List<string> { "company_id", "id", "created_by_user_id", "created_on_timestamp", "is_valid", "last_modified_by_user_id", "last_modified_timestamp", "title" }));
                    batch_statement.Add(department_ps.Bind(companyId, departmentId, adminUserId, DateTime.UtcNow, true, adminUserId, DateTime.UtcNow, departmentTitle));

                    department_by_company_ps = session.Prepare(CQLGenerator.InsertStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                    batch_statement.Add(department_by_company_ps.Bind(companyId, departmentId));
                }
                #endregion

                #region department_by_user, user_by_department
                department_by_user_ps = session.Prepare(CQLGenerator.InsertStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                batch_statement.Add(department_by_user_ps.Bind(userId, departmentId));

                user_by_department_ps = session.Prepare(CQLGenerator.InsertStatement("user_by_department", new List<string> { "department_id", "user_id", "position", "user_status" }));
                batch_statement.Add(user_by_department_ps.Bind(departmentId, userId, position, User.AccountStatus.CODE_ACTIVE));
                #endregion

                #region user_basic
                if (String.IsNullOrEmpty(profileImageUrl))
                {
                    profileImageUrl = null;
                }
                user_basic_ps = session.Prepare(CQLGenerator.InsertStatement("user_basic", new List<string> { "id", "created_by_user_id", "created_on_timestamp", "email", "first_name", "last_modified_by_user_id", "last_modified_timestamp", "last_name", "profile_image_url" }));
                batch_statement.Add(user_basic_ps.Bind(userId, adminUserId, DateTime.UtcNow, email, firstName, adminUserId, DateTime.UtcNow, lastName, profileImageUrl));
                #endregion

                #region user_contact
                user_contact_ps = session.Prepare(CQLGenerator.InsertStatement("user_contact", new List<string> { "id", "address", "address_country_name", "address_postal_code", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp", "phone", "phone_country_code", "phone_country_name" }));
                batch_statement.Add(user_contact_ps.Bind(userId, address, addressCountryName, postalCode, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow, phoneNumber, phoneCountryCode, phoneCountryName));
                #endregion

                #region user_account_type
                user_account_type_ps = session.Prepare(CQLGenerator.InsertStatement("user_account_type", new List<string> { "user_id", "account_status", "account_type", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                batch_statement.Add(user_account_type_ps.Bind(userId, User.AccountStatus.CODE_ACTIVE, User.AccountType.CODE_NORMAL_USER, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));
                #endregion

                session.Execute(batch_statement);
                #endregion

                #region Step 3. Send email to user. Using AWS SES.
                try
                {
                    EmailTemplate emailTemplate = new EmailTemplate(EmailTemplate.Type.CREATED_USER, plainPassword);
                    Destination destination = new Destination(new List<String> { email });
                    Message message = new Message();
                    message.Subject = new Content(emailTemplate.Tilte);
                    Body mBody = new Body();
                    if (emailTemplate.IsHtmlFormat)
                    {
                        mBody.Html = new Content(emailTemplate.Body);
                    }
                    else
                    {
                        mBody.Text = new Content(emailTemplate.Body);
                    }
                    message.Body = mBody;
                    SendEmailRequest request = new SendEmailRequest("admin@cocadre.com", destination, message);
                    AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient("AKIAIRFRL6Z2OYHO4R3Q", "HfL9OECxbpWqEH7KjHtl8T5/dJM6UiVQ66ywHXzY", RegionEndpoint.USWest2);
                    client.SendEmail(request);
                    Log.Debug("Sent to " + email + " done.");
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString(), ex);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemErrorSendEmailFailed);
                    response.ErrorMessage = ErrorMessage.SystemErrorSendEmailFailed;

                    #region Step 1. Delete data on database.
                    BatchStatement bs = new BatchStatement();

                    user_authentication_ps = session.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "email", "user_id" }));
                    bs.Add(user_authentication_ps.Bind(email, userId));

                    department_ps = session.Prepare(CQLGenerator.DeleteStatement("department", new List<string> { "company_id", "id" }));
                    bs.Add(department_ps.Bind(companyId, departmentId));

                    department_by_company_ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                    bs.Add(department_by_company_ps.Bind(companyId, departmentId));

                    department_by_user_ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                    bs.Add(department_by_user_ps.Bind(userId, departmentId));

                    user_by_department_ps = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "department_id", "user_id" }));
                    bs.Add(user_by_department_ps.Bind(departmentId, userId));

                    user_basic_ps = session.Prepare(CQLGenerator.DeleteStatement("user_basic", new List<string> { "id" }));
                    bs.Add(user_basic_ps.Bind(userId));

                    user_contact_ps = session.Prepare(CQLGenerator.DeleteStatement("user_contact", new List<string> { "id" }));
                    bs.Add(user_contact_ps.Bind(userId));

                    user_account_type_ps = session.Prepare(CQLGenerator.DeleteStatement("user_account_type", new List<string> { "user_id" }));
                    bs.Add(user_account_type_ps.Bind(userId));

                    session.Execute(bs);
                    #endregion

                    #region Step 2. Delete file on S3.

                    String bucketName = "cocadre-" + companyId.ToLower() + "/users/" + userId;
                    String fileName = profileImageUrl.Substring(profileImageUrl.LastIndexOf("/"), profileImageUrl.Length - profileImageUrl.LastIndexOf("/")).Replace("/", "");
                    String format = fileName.Substring(fileName.LastIndexOf("."), fileName.Length - fileName.LastIndexOf("."));

                    List<String> keys = new List<string>();
                    keys.Add(fileName.Replace(format, "") + "_original" + format);
                    keys.Add(fileName.Replace(format, "") + "_large" + format);
                    keys.Add(fileName.Replace(format, "") + "_medium" + format);
                    keys.Add(fileName.Replace(format, "") + "_small" + format);

                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(ConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), ConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        for (int i = 0; i < keys.Count; i++)
                        {
                            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                            {
                                BucketName = bucketName,
                                Key = keys[i]
                            };
                            try
                            {
                                s3Client.DeleteObject(deleteObjectRequest);
                                Log.Debug("Delete file: " + bucketName + "/" + keys[i]);
                            }
                            catch (AmazonS3Exception s3Exception)
                            {
                                Log.Error(s3Exception.ToString(), s3Exception);
                            }
                        }
                    }
                    #endregion

                    return response;
                }
                #endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserSelectDeviceTokenResponse SelectUserDeviceToken(string userId)
        {
            UserSelectDeviceTokenResponse response = new UserSelectDeviceTokenResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psDeviceToken = session.Prepare(CQLGenerator.SelectStatement("user_device_token",
                           new List<string> { "device_token", "device_type", "device_type_name" }, new List<string> { "user_id" }));
                BoundStatement bsDeviceToken = psDeviceToken.Bind(userId);
                Row deviceRow = session.Execute(bsDeviceToken).FirstOrDefault();

                if (deviceRow != null)
                {
                    response.DeviceToken = deviceRow.GetValue<string>("device_token");
                    response.DeviceType = deviceRow.GetValue<int>("device_type");
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserUpdateDeviceTokenResponse UpdateUserDeviceToken(string userId, string companyId, string deviceToken, int deviceType)
        {
            UserUpdateDeviceTokenResponse response = new UserUpdateDeviceTokenResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psDeviceToken = null;
                BoundStatement bsDeviceToken = null;

                Row companyRow = vh.ValidateCompany(companyId, session);

                if (companyRow != null)
                {
                    Row userRow = vh.ValidateUser(userId, companyId, session);

                    if (userRow != null)
                    {
                        string deviceTypeName = "iOS";

                        if (deviceType == (int)DeviceTokenType.Android)
                        {
                            deviceTypeName = "Android";
                        }

                        psDeviceToken = session.Prepare(CQLGenerator.SelectStatement("user_device_token",
                            new List<string> { "device_token", "device_type", "device_type_name" }, new List<string> { "user_id" }));
                        bsDeviceToken = psDeviceToken.Bind(userId);
                        Row deviceRow = session.Execute(bsDeviceToken).FirstOrDefault();

                        // Device token already recorded before
                        if (deviceRow != null)
                        {
                            psDeviceToken = session.Prepare(CQLGenerator.UpdateStatement("user_device_token",
                                new List<string> { "user_id" }, new List<string> { "device_token", "device_type", "device_type_name" }, new List<string>()));
                            bsDeviceToken = psDeviceToken.Bind(deviceToken, deviceType, deviceTypeName, userId);
                            session.Execute(bsDeviceToken);

                            response.OutdatedDeviceToken = deviceRow.GetValue<string>("device_token");
                            response.OutdatedDeviceType = deviceRow.GetValue<int>("device_type");
                        }
                        // Device token not recorded
                        else
                        {
                            psDeviceToken = session.Prepare(CQLGenerator.InsertStatement("user_device_token",
                                new List<string> { "user_id", "device_token", "device_type", "device_type_name" }));
                            bsDeviceToken = psDeviceToken.Bind(userId, deviceToken, deviceType, deviceTypeName);
                            session.Execute(bsDeviceToken);
                        }


                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Invalid userId: " + userId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                    }

                }
                else
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                }
            }

            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserDeleteResponse Delete(string adminUserId, string companyId, string userId)
        {
            UserDeleteResponse response = new UserDeleteResponse();
            response.Success = true;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                BatchStatement deleteBatchStatement = new BatchStatement();
                BatchStatement updateBatchStatement = new BatchStatement();

                PreparedStatement psUser = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string>(), new List<string> { "id" }));
                Row userRow = session.Execute(psUser.Bind(userId)).FirstOrDefault();

                string email = userRow.GetValue<string>("email");

                PreparedStatement psDepartmentByUser = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string>(), new List<string> { "user_id" }));
                RowSet departmentByUserRowset = session.Execute(psDepartmentByUser.Bind(userId));

                string mainDepartmentId = string.Empty;
                foreach (Row departmentByUserRow in departmentByUserRowset)
                {
                    string departmentId = departmentByUserRow.GetValue<string>("department_id");

                    if (string.IsNullOrEmpty(mainDepartmentId))
                    {
                        mainDepartmentId = departmentId;
                    }

                    PreparedStatement psUserByDepartment = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "user_id", "department_id" }));
                    deleteBatchStatement = deleteBatchStatement.Add(psUserByDepartment.Bind(userId, departmentId));
                }

                psDepartmentByUser = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id" }));
                deleteBatchStatement = deleteBatchStatement.Add(psDepartmentByUser.Bind(userId));

                // Deleted
                PreparedStatement psUserToken = session.Prepare(CQLGenerator.DeleteStatement("user_token", new List<string> { "user_id" }));
                deleteBatchStatement = deleteBatchStatement.Add(psUserToken.Bind(userId));

                PreparedStatement psUserDeviceToken = session.Prepare(CQLGenerator.DeleteStatement("user_device_token", new List<string> { "user_id" }));
                deleteBatchStatement = deleteBatchStatement.Add(psUserToken.Bind(userId));

                PreparedStatement psUserAuthentication = session.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "user_id", "email" }));
                deleteBatchStatement = deleteBatchStatement.Add(psUserAuthentication.Bind(userId, email));

                PreparedStatement psModeratorAccessRight = session.Prepare(CQLGenerator.DeleteStatement("moderator_access_rights", new List<string> { "user_id" }));
                deleteBatchStatement = deleteBatchStatement.Add(psModeratorAccessRight.Bind(userId));

                // Update
                PreparedStatement psAccountType = session.Prepare(CQLGenerator.UpdateStatement("user_account_type",
                    new List<string> { "user_id" }, new List<string> { "account_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                updateBatchStatement = updateBatchStatement.Add(psAccountType.Bind(User.AccountStatus.CODE_DELETED, adminUserId, DateTime.UtcNow, userId));

                session.Execute(deleteBatchStatement);
                session.Execute(updateBatchStatement);

                // Analytic
                Analytic analytic = new Analytic();
                ISession analyticSession = cm.getAnalyticSession();
                analytic.RemoveFromLeaderboard(true, false, companyId, analyticSession, userId, mainDepartmentId, null);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserUpdateResponse Update(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentId, String departmentTitle)
        {
            UserUpdateResponse response = new UserUpdateResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement user_authentication_ps = null;
                PreparedStatement department_ps = null;
                PreparedStatement department_by_company_ps = null;
                PreparedStatement department_by_user_ps = null;

                PreparedStatement user_basic_ps = null;
                PreparedStatement user_contact_ps = null;
                PreparedStatement user_account_type_ps = null;
                PreparedStatement query_department_ps = null;
                PreparedStatement query_user_department_ps = null;
                PreparedStatement user_by_department_ps = null;

                BatchStatement batch_statement = new BatchStatement();
                BatchStatement bsDelete = new BatchStatement();

                int origStatus = 0;

                if (string.IsNullOrEmpty(email))
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.UserMissingEmail);
                    response.ErrorMessage = ErrorMessage.UserMissingEmail;
                    return response;
                }

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check user status. (user_account_type)
                PreparedStatement ps_user_account_type = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string>(), new List<string> { "user_id" }));
                BoundStatement bs_user_account_type = ps_user_account_type.Bind(userId);
                Row row_user_account_type = session.Execute(bs_user_account_type).FirstOrDefault();
                if (row_user_account_type == null || row_user_account_type.GetValue<int>("account_status") == AccountStatus.CODE_DELETED)
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                else
                {
                    origStatus = row_user_account_type.GetValue<int>("account_status");
                }
                #endregion


                #region Step 1.3 Check Email
                String origEmail = String.Empty;
                PreparedStatement ps_user_basic = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string>(), new List<string> { "id" }));
                BoundStatement bs_user_basic = ps_user_basic.Bind(userId);
                Row row_user_basic = session.Execute(bs_user_basic).FirstOrDefault();
                if (row_user_basic == null)
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                origEmail = row_user_basic.GetValue<String>("email");

                if (!origEmail.Equals(email))
                {
                    PreparedStatement ps_user_authentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "email", "company_id" }));
                    BoundStatement bs_user_authentication = ps_user_authentication.Bind(email, companyId);
                    Row row_user_authentication = session.Execute(bs_user_authentication).FirstOrDefault();
                    if (row_user_authentication != null && !row_user_authentication.GetValue<String>("user_id").Equals(userId))
                    {
                        Log.Error(ErrorMessage.UserDuplicatedEmail);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.UserDuplicatedEmail);
                        response.ErrorMessage = ErrorMessage.UserDuplicatedEmail;
                        return response;
                    }
                    else if (row_user_authentication == null)
                    {

                        ps_user_authentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "email", "company_id" }));
                        bs_user_authentication = ps_user_authentication.Bind(origEmail, companyId);
                        row_user_authentication = session.Execute(bs_user_authentication).FirstOrDefault();

                        #region user_authentication. Delete > Insert
                        user_authentication_ps = session.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "email", "user_id" }));
                        batch_statement.Add(user_authentication_ps.Bind(origEmail, userId));

                        user_authentication_ps = session.Prepare(CQLGenerator.InsertStatement("user_authentication", new List<string> { "email", "user_id", "company_id", "created_by_user_id", "created_on_timestamp", "hashed_password", "is_default", "last_modified_by_user_id", "last_modified_timestamp", "salt" }));
                        batch_statement.Add(user_authentication_ps.Bind(email, userId, companyId, row_user_authentication.GetValue<String>("created_by_user_id"), row_user_authentication.GetValue<DateTimeOffset>("created_on_timestamp"), row_user_authentication.GetValue<String>("hashed_password"), row_user_authentication.GetValue<Boolean>("is_default"), adminUserId, DateTime.UtcNow, row_user_authentication.GetValue<String>("salt")));
                        #endregion
                    }
                }
                #endregion
                #endregion

                #region Step 2. Write data to database.
                #region Department
                PreparedStatement ps_department_by_user = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs_department_by_user = ps_department_by_user.Bind(userId);
                RowSet rowSet_department_by_user = session.Execute(bs_department_by_user);
                List<String> origDepartments = new List<String>();
                foreach (Row row in rowSet_department_by_user)
                {
                    origDepartments.Add(row.GetValue<String>("department_id"));
                }

                if (!origDepartments[0].Equals(departmentId)) // Department has changed
                {
                    #region department, department_by_company
                    Row row_department = null;
                    bool isNewDepartment = false;

                    if (!String.IsNullOrEmpty(departmentId))
                    {
                        PreparedStatement ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" }));
                        BoundStatement bs_department = ps_department.Bind(companyId, departmentId, true);
                        row_department = session.Execute(bs_department).FirstOrDefault();
                    }

                    if (row_department == null)
                    {
                        isNewDepartment = true;
                        departmentId = UUIDGenerator.GenerateUniqueIDForDepartment();
                        // It is a new department. Check department title again.
                        PreparedStatement ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "is_valid" }));
                        BoundStatement bs_department = ps_department.Bind(companyId, true);
                        RowSet rowset_department = session.Execute(bs_department);
                        foreach (Row row in rowset_department)
                        {
                            if (row.GetValue<string>("title").ToLower().Trim().Equals(departmentTitle.ToLower().Trim()))
                            {
                                departmentId = row.GetValue<string>("id");
                                isNewDepartment = false;
                                break;
                            }
                        }
                    }

                    if (isNewDepartment)
                    {
                        department_ps = session.Prepare(CQLGenerator.InsertStatement("department", new List<string> { "company_id", "id", "created_by_user_id", "created_on_timestamp", "is_valid", "last_modified_by_user_id", "last_modified_timestamp", "title" }));
                        batch_statement.Add(department_ps.Bind(companyId, departmentId, adminUserId, DateTime.UtcNow, true, adminUserId, DateTime.UtcNow, departmentTitle));

                        department_by_company_ps = session.Prepare(CQLGenerator.InsertStatement("department_by_company", new List<String> { "company_id", "department_id" }));
                        batch_statement.Add(department_by_company_ps.Bind(companyId, departmentId));
                    }
                    #endregion

                    #region department_by_user, user_by_department. Delete > Insert
                    department_by_user_ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id" }));
                    bsDelete.Add(department_by_user_ps.Bind(userId));

                    department_by_user_ps = session.Prepare(CQLGenerator.InsertStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                    batch_statement.Add(department_by_user_ps.Bind(userId, departmentId));

                    user_by_department_ps = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "department_id", "user_id" }));
                    batch_statement.Add(user_by_department_ps.Bind(origDepartments[0], userId));

                    user_by_department_ps = session.Prepare(CQLGenerator.InsertStatement("user_by_department", new List<string> { "department_id", "user_id", "position", "user_status" }));
                    batch_statement.Add(user_by_department_ps.Bind(departmentId, userId, position, origStatus));

                    /* For Kevin  /You can use "orig department Id : origDepartments[0], new department Id : departmentId, user id: userId "/     */
                    Analytic analytic = new Analytic();
                    ISession analyticSession = cm.getAnalyticSession();
                    analytic.SwitchLeaderboardForDepartment(userId, companyId, origDepartments[0], departmentId);
                    /* For Kevin /End/ */

                    #endregion
                }
                #endregion

                #region user_basic
                user_basic_ps = session.Prepare(CQLGenerator.UpdateStatement("user_basic", new List<string> { "id" }, new List<string> { "email", "first_name", "last_modified_by_user_id", "last_modified_timestamp", "last_name", "profile_image_url" }, new List<string>()));
                batch_statement.Add(user_basic_ps.Bind(email, firstName, adminUserId, DateTime.UtcNow, lastName, profileImageUrl, userId));
                #endregion

                #region user_contact
                user_contact_ps = session.Prepare(CQLGenerator.UpdateStatement("user_contact", new List<string> { "id" }, new List<string> { "address", "address_country_name", "address_postal_code", "last_modified_by_user_id", "last_modified_timestamp", "phone", "phone_country_code", "phone_country_name" }, new List<string>()));
                batch_statement.Add(user_contact_ps.Bind(address, addressCountryName, postalCode, adminUserId, DateTime.UtcNow, phoneNumber, phoneCountryCode, phoneCountryName, userId));
                #endregion

                //#region user_account_type
                //user_account_type_ps = session.Prepare(CQLGenerator.InsertStatement("user_account_type", new List<string> { "user_id", "account_status", "account_type", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                //batch_statement.Add(user_account_type_ps.Bind(userId, User.AccountStatus.CODE_ACTIVE, User.AccountType.CODE_NORMAL_USER, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));
                //#endregion

                session.Execute(bsDelete);
                session.Execute(batch_statement);
                #endregion

                //#region Step 3. Send email to user. Using AWS SES.
                //try
                //{
                //    EmailTemplate emailTemplate = new EmailTemplate(EmailTemplate.Type.CREATED_USER, "密碼");
                //    Destination destination = new Destination(new List<String> { email });
                //    Message message = new Message();
                //    message.Subject = new Content(emailTemplate.Tilte);
                //    Body mBody = new Body();
                //    if (emailTemplate.IsHtmlFormat)
                //    {
                //        mBody.Html = new Content(emailTemplate.Body);
                //    }
                //    else
                //    {
                //        mBody.Text = new Content(emailTemplate.Body);
                //    }
                //    message.Body = mBody;
                //    SendEmailRequest request = new SendEmailRequest("admin@cocadre.com", destination, message);
                //    AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient("AKIAIRFRL6Z2OYHO4R3Q", "HfL9OECxbpWqEH7KjHtl8T5/dJM6UiVQ66ywHXzY", RegionEndpoint.USWest2);
                //    client.SendEmail(request);
                //    Log.Debug("Sent to " + email + " done.");
                //}
                //catch (Exception ex)
                //{
                //    Log.Error(ex.ToString(), ex);
                //    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemErrorSendEmailFailed);
                //    response.ErrorMessage = ErrorMessage.SystemErrorSendEmailFailed;

                //    #region Step 1. Delete data on database.
                //    BatchStatement bs = new BatchStatement();

                //    user_authentication_ps = session.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "email", "user_id" }));
                //    bs.Add(user_authentication_ps.Bind(email, userId));

                //    department_ps = session.Prepare(CQLGenerator.DeleteStatement("department", new List<string> { "company_id", "id" }));
                //    bs.Add(department_ps.Bind(companyId, departmentId));

                //    department_by_company_ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                //    bs.Add(department_by_company_ps.Bind(companyId, departmentId));

                //    department_by_user_ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                //    bs.Add(department_by_user_ps.Bind(userId, departmentId));

                //    user_by_department_ps = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "department_id", "user_id" }));
                //    bs.Add(user_by_department_ps.Bind(departmentId, userId));

                //    user_basic_ps = session.Prepare(CQLGenerator.DeleteStatement("user_basic", new List<string> { "id" }));
                //    bs.Add(user_basic_ps.Bind(userId));

                //    user_contact_ps = session.Prepare(CQLGenerator.DeleteStatement("user_contact", new List<string> { "id" }));
                //    bs.Add(user_contact_ps.Bind(userId));

                //    user_account_type_ps = session.Prepare(CQLGenerator.DeleteStatement("user_account_type", new List<string> { "user_id" }));
                //    bs.Add(user_account_type_ps.Bind(userId));

                //    session.Execute(bs);
                //    #endregion

                //    #region Step 2. Delete file on S3.

                //    String bucketName = "cocadre-" + companyId.ToLower() + "/users/" + userId;
                //    String fileName = profileImageUrl.Substring(profileImageUrl.LastIndexOf("/"), profileImageUrl.Length - profileImageUrl.LastIndexOf("/")).Replace("/", "");
                //    String format = fileName.Substring(fileName.LastIndexOf("."), fileName.Length - fileName.LastIndexOf("."));

                //    List<String> keys = new List<string>();
                //    keys.Add(fileName.Replace(format, "") + "_original" + format);
                //    keys.Add(fileName.Replace(format, "") + "_large" + format);
                //    keys.Add(fileName.Replace(format, "") + "_medium" + format);
                //    keys.Add(fileName.Replace(format, "") + "_small" + format);

                //    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(ConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), ConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                //    {
                //        for (int i = 0; i < keys.Count; i++)
                //        {
                //            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                //            {
                //                BucketName = bucketName,
                //                Key = keys[i]
                //            };
                //            try
                //            {
                //                s3Client.DeleteObject(deleteObjectRequest);
                //                Log.Debug("Delete file: " + bucketName + "/" + keys[i]);
                //            }
                //            catch (AmazonS3Exception s3Exception)
                //            {
                //                Log.Error(s3Exception.ToString(), s3Exception);
                //            }
                //        }
                //    }
                //    #endregion

                //    return response;
                //}
                //#endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserUpdateStatusResponse UpdateStatus(String adminUserId, String companyId, String userId, int statusCode)
        {
            UserUpdateStatusResponse response = new UserUpdateStatusResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                BatchStatement bs = new BatchStatement();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check UserId.
                if (string.IsNullOrEmpty(userId.Trim()))
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Update user account status.
                #region user_account_type
                PreparedStatement ps_user_account_type = session.Prepare(CQLGenerator.UpdateStatement("user_account_type", new List<string> { "user_id" }, new List<string> { "account_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                bs.Add(ps_user_account_type.Bind(statusCode, adminUserId, DateTime.UtcNow, userId));
                #endregion

                #region user_by_department
                PreparedStatement ps_department_by_user = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" })); ;
                BoundStatement bs_department_by_user = ps_department_by_user.Bind(userId);
                RowSet rowsUser = session.Execute(bs_department_by_user);
                foreach (Row row in rowsUser)
                {
                    PreparedStatement ps_user_by_department = session.Prepare(CQLGenerator.UpdateStatement("user_by_department", new List<string> { "department_id", "user_id" }, new List<string> { "user_status" }, new List<string>()));
                    bs.Add(ps_user_by_department.Bind(statusCode, row.GetValue<String>("department_id"), userId));
                }
                #endregion

                session.Execute(bs);
                response.Success = true;

                #region Analytics
                Analytic analytic = new Analytic();
                ISession analyticSession = cm.getAnalyticSession();

                string mainDepartmentId = new Department().GetAllDepartmentByUserId(userId, companyId, session).Departments[0].Id;

                if (statusCode == AccountStatus.CODE_ACTIVE)
                {
                    analytic.UnhideFromLeaderboard(true, false, companyId, analyticSession, userId, mainDepartmentId, null);
                }
                else if (statusCode == AccountStatus.CODE_DELETING || statusCode == AccountStatus.CODE_SUSPENEDED)
                {
                    analytic.HideFromLeaderboard(true, false, companyId, analyticSession, userId, mainDepartmentId, null);
                }
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserUpdateTypeResponse UpdateType(String adminUserId, String companyId, String userId, int typeCode)
        {
            UserUpdateTypeResponse response = new UserUpdateTypeResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                BatchStatement bs = new BatchStatement();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check UserId.
                if (string.IsNullOrEmpty(userId.Trim()))
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Update user account status.
                #region user_account_type
                PreparedStatement ps_user_account_type = session.Prepare(CQLGenerator.UpdateStatement("user_account_type", new List<string> { "user_id" }, new List<string> { "account_type", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                bs.Add(ps_user_account_type.Bind(typeCode, adminUserId, DateTime.UtcNow, userId));
                #endregion

                #region moderator_access_rights
                PreparedStatement ps_moderator_access_rights;
                if (typeCode == User.AccountType.CODE_NORMAL_USER)
                {
                    ps_moderator_access_rights = session.Prepare(CQLGenerator.DeleteStatement("moderator_access_rights", new List<string> { "user_id" }));
                    bs.Add(ps_moderator_access_rights.Bind(userId));
                }
                else if (typeCode == User.AccountType.CODE_ADMIN || typeCode == User.AccountType.CODE_MODERATER)
                {
                    ps_moderator_access_rights = session.Prepare(CQLGenerator.InsertStatement("moderator_access_rights", new List<string> { "user_id", "assigned_by_user_id", "assigned_on_timestamp", "is_access_report", "is_manage_event", "is_manage_feed", "is_manage_topic", "last_modified_by_user_id", "last_modified_timestamp" }));
                    bs.Add(ps_moderator_access_rights.Bind(userId, adminUserId, DateTime.UtcNow, true, true, true, true, adminUserId, DateTime.UtcNow));
                }
                #endregion

                session.Execute(bs);
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

    }
}