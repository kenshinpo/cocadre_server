﻿using CocadreCassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class ChallengeCreateResponse : ServiceResponse
    {
        public Topic Topic;
        public List<User> Players;
        public string ChallengeId;
    }
}