﻿using CocadreCassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class ChallengeInvalidateResponse : ServiceResponse
    {
        public string InitiatedUserId { get; set; }
        public string ChallengedUserId { get; set; }
    }
}