﻿using CocadreCassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class CategorySelectAllWithTopicResponse : ServiceResponse
    {
        public List<TopicCategory> TopicCategories;
    }
}