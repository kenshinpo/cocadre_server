﻿using System.Collections.Generic;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class CommentSelectResponse : ServiceResponse
    {
        public List<Comment> Comments { get; set; }
    }
}