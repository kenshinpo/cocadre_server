﻿using System.Runtime.Serialization;
using CocadreCassandraService.Entity;
using CocadreCassandraService.ServiceResponses;
using System.Collections.Generic;

namespace CocadreCassandraService.ServiceResponses
{
    public class QuestionSelectAllResponse : ServiceResponse
    {
        public List<ChallengeQuestion> Questions;
    }
}