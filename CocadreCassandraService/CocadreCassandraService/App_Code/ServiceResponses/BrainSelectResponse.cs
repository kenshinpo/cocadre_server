﻿using System.Collections.Generic;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class BrainSelectResponse : ServiceResponse
    {
        public Analytic.Exp UserExp { get; set; }
        public List<Topic> RecentlyPlayedTopics { get; set; }
        public List<User> RecentOpponents { get; set; }
        public List<Challenge> LatestChallenges { get; set; }
        public int ChallengeCount { get; set; }
    }
}