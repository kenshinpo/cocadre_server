﻿using System.Runtime.Serialization;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    [DataContract]
    public class DepartmentCreateResponse : ServiceResponse
    {
        [DataMember]
        public Department Department;
    }
}