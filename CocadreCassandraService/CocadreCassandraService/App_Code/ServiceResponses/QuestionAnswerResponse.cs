﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class QuestionAnswerResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string OpponentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CorrectAnswer { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsCorrect { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsInitiator { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsLive { get; set; }

        // Remove channel if last round
        [DataMember(EmitDefaultValue = false)]
        public bool IsLastRound { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AnswerOfOpponent { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public float TimeTakenByOpponent { get; set; }
    }
}