﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class TopicCreateResponse : ServiceResponse
    {
        public String TopicId { get; set; }
    }
}