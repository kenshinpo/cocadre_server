﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    [DataContract]
    public class UserUpdateLoginResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string DeviceToken { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int DeviceType { get; set; } 
    }
}