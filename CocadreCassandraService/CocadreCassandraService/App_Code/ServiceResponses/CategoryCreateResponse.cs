﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class CategoryCreateResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string NewCategoryId { get; set; }
    }
}