﻿using System.Runtime.Serialization;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class UserDetailResponse : ServiceResponse
    {
        [DataMember]
        public User User;
    }
}