﻿using CocadreCassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class ChallengeOfflineSelectOpponentAnswerResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string OpponentUserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasOpponentAnswered { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsOpponentAnswerCorrect { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public float TimeTakenByOpponent { get; set; }
    }
}