﻿
using System;
using System.Collections.Generic;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class LoginByAdminResponse : ServiceResponse
    {
        public List<Company> Companies { get; set; }
    }
}