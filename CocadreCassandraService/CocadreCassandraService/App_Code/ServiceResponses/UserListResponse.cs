﻿using System.Collections.Generic;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class UserListResponse : ServiceResponse
    {
        public List<User> Users;
    }
}