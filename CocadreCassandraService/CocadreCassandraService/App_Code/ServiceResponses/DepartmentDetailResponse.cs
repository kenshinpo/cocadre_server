﻿using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class DepartmentDetailResponse : ServiceResponse
    {
        public Department Department { get; set; }
    }
}