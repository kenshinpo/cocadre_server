﻿using System.Runtime.Serialization;
using CocadreCassandraService.Entity;
using CocadreCassandraService.ServiceResponses;

namespace CocadreCassandraService.ServiceResponses
{
    public class UserSelectBasicResponse : ServiceResponse
    {
        public User User;
    }
}