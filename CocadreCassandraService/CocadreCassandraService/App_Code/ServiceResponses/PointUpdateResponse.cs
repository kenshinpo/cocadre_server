﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class PointUpdateResponse : ServiceResponse
    {
        public int NegativePoints { get; set; }
        public int PositivePoints { get; set; }
        public bool IsUpVoted { get; set; }
    }
}