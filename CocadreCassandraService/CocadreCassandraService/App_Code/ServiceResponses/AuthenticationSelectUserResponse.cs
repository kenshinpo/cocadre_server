﻿using CocadreCassandraService.Entity;
using CocadreCassandraService.ServiceResponses;
using System.Collections.Generic;

namespace CocadreCassandraService.ServiceResponses
{
    public class AuthenticationSelectUserResponse : ServiceResponse
    {
        public List<Company> Companies { get; set; }
    }
}