﻿using CocadreCassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class ChallengeSelectStatsResponse : ServiceResponse
    {
        public Analytic.Exp Exp;
        public int OpponentLevel;
        public List<User> TopPlayers;
    }
}