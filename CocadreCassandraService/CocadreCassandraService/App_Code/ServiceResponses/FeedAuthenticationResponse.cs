﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class FeedAuthenticationResponse : ServiceResponse
    {
        public bool IsAuthenticatedToPost { get; set; }

        public string FeedId { get; set; }
    }
}