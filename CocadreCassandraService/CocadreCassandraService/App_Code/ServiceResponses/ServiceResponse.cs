﻿using System.Runtime.Serialization;

namespace CocadreCassandraService.ServiceResponses
{
    [DataContract]
    public class ServiceResponse
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ErrorCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ErrorMessage { get; set; }
    }
}