﻿using System.Runtime.Serialization;
using CocadreCassandraService.Entity;
using CocadreCassandraService.ServiceResponses;
using System.Collections.Generic;

namespace CocadreCassandraService.ServiceResponses
{
    public class UserSelectAllByDepartmentResponse : ServiceResponse
    {
        public List<Department> Departments;
    }
}