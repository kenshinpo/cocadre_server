﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class AccountKindListResponse : ServiceResponse
    {
        public List<AccountKind> AccountKinds { get; set; }
    }
}