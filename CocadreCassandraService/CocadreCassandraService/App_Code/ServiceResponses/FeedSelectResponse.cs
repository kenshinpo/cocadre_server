﻿using System.Collections.Generic;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class FeedSelectResponse : ServiceResponse
    {
        public List<Feed> Feeds { get; set; }
    }
}