﻿using System.Collections.Generic;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class CountryListResponse : ServiceResponse
    {
        public List<Country> Countries { get; set; }
    }
}