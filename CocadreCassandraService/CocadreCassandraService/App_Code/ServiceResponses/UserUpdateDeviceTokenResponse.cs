﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    [DataContract]
    public class UserUpdateDeviceTokenResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string OutdatedDeviceToken { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int OutdatedDeviceType { get; set; }
    }
}