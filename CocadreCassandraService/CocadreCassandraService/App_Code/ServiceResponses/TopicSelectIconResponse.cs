﻿using CocadreCassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class TopicSelectIconResponse : ServiceResponse
    {
        public List<string> TopicIconUrls;
    }
}