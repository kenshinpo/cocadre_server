﻿using System.Runtime.Serialization;
using CocadreCassandraService.Entity;
using CocadreCassandraService.ServiceResponses;

namespace CocadreCassandraService.ServiceResponses
{
    public class UserSelectTokenWithCompanyResponse : ServiceResponse
    {
        public User User;
    }
}