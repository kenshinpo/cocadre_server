﻿using System.Collections.Generic;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class ReplySelectResponse : ServiceResponse
    {
        public List<Reply> Replies { get; set; }
    }
}