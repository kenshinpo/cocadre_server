﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class TopicUpdateResponse : ServiceResponse
    {
        public string NewTopicCategoryId { get; set; }
    }
}