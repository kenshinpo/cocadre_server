﻿using System.Collections.Generic;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.ServiceResponses
{
    public class DepartmentListResponse : ServiceResponse
    {
        public List<Department> Departments { get; set; }
    }
}