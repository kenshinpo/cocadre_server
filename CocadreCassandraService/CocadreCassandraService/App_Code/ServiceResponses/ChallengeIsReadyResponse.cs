﻿using CocadreCassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.ServiceResponses
{
    public class ChallengeIsReadyResponse : ServiceResponse
    {
        public bool HaveBothPlayersAccepted { get; set; }
        public bool IsLive { get; set; }
    }
}