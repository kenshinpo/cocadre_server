﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreCassandraService.Date
{
    public class DateHelper
    {
        static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long ConvertDateToLong(DateTime date_time_utc)
        {
            long date_in_long = (long)Math.Round((date_time_utc - epoch).TotalMilliseconds);

            // Remove milliseconds
            date_in_long = (date_in_long / 1000) * 1000;

            return date_in_long;
        }

        public static DateTime ConvertLongToDate(long date_in_long)
        {
            DateTime date_time_utc = epoch.AddMilliseconds(date_in_long).ToUniversalTime();

            return date_time_utc;
        }
    }
}