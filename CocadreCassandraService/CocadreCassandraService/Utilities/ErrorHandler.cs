﻿using System;
using System.Configuration;
using System.Runtime.Serialization;
using CocadreCassandraService.App_GlobalResources;

namespace CocadreCassandraService.Utilities
{
    [DataContract]
    public class ErrorHandler
    {
        public class ErrorStatus
        {
            public int ErrorCode { get; set; }

            public string ErrorMessage { get; set; }
        }

        public static ErrorStatus SystemError
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.SystemError),
                    ErrorMessage = ErrorMessage.SystemError
                };
            }
        }

        public static ErrorStatus DuplicatedEmail
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.UserDuplicatedEmail),
                    ErrorMessage = ErrorCode.UserDuplicatedEmail
                };
            }
        }

        public static ErrorStatus MismatchEmailOrPassword
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.UserMismatchEmailOrPassword),
                    ErrorMessage = ErrorMessage.UserMismatchEmailOrPassword
                };
            }
        }

        public static ErrorStatus EmptyEmailOrPassword
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.UserEmptyEmailOrPassword),
                    ErrorMessage = ErrorMessage.UserEmptyEmailOrPassword
                };
            }
        }


        public static ErrorStatus InvalidUserAccount
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.UserInvalid),
                    ErrorMessage = ErrorCode.UserInvalid
                };
            }
        }


        public static ErrorStatus InvalidCompany
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid),
                    ErrorMessage = ErrorCode.CompanyInvalid
                };
            }
        }


        public static ErrorStatus InvalidDepartment
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.DepartmentIsInvalid),
                    ErrorMessage = ErrorMessage.DepartmentIsInvalid
                };
            }
        }


        public static ErrorStatus InvalidProfileImageBasedString
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.UserInvalidProfileImageBasedString),
                    ErrorMessage = ErrorMessage.UserInvalidProfileImageBasedString
                };
            }
        }

        public static ErrorStatus InvalidChallenge
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.ChallengeInvalid),
                    ErrorMessage = ErrorMessage.ChallengeInvalid
                };
            }
        }


        public static ErrorStatus PlayerNotFoundForChallenge
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.ChallengeInvalidPlayer),
                    ErrorMessage = ErrorMessage.ChallengeInvalidPlayer
                };
            }
        }


        public static ErrorStatus AccountIsNotAdmin
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.UserIsNotAdmin),
                    ErrorMessage = ErrorMessage.UserIsNotAdmin
                };
            }
        }


        public static ErrorStatus TopicTitleIsEmpty
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.TopicMissingTitle),
                    ErrorMessage = ErrorMessage.TopicMissingTitle
                };
            }
        }


        public static ErrorStatus TopicCategoryTitleIsEmpty
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.CategoryMissingTitle),
                    ErrorMessage = ErrorCode.CategoryMissingTitle
                };
            }
        }


        public static ErrorStatus TopicLessSelectedQuestions
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.TopicLessActiveQuestions),
                    ErrorMessage = ErrorMessage.TopicLessActiveQuestions
                };
            }
        }


        public static ErrorStatus TopicInsufficientActiveQuestions
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.TopicLessActiveQuestions),
                    ErrorMessage = ErrorMessage.TopicLessActiveQuestions
                };
            }
        }


        public static ErrorStatus TopicCategoryNotTally
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid),
                    ErrorMessage = ErrorMessage.CategoryInvalid
                };
            }
        }


        public static ErrorStatus InvalidTopicCategory
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid),
                    ErrorMessage = ErrorMessage.CategoryInvalid
                };
            }
        }


        public static ErrorStatus InvalidTopic
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.TopicInvalid),
                    ErrorMessage = ErrorMessage.TopicInvalid
                };
            }
        }


        public static ErrorStatus TopicQuestionIsEmpty
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.TopicQuestionMissing),
                    ErrorMessage = ErrorMessage.TopicQuestionMissing
                };
            }
        }


        public static ErrorStatus TopicQuestionChoiceIsEmpty
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.TopicChoiceMissing),
                    ErrorMessage = ErrorMessage.TopicChoiceMissing
                };
            }
        }


        public static ErrorStatus InvalidTopicRound
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.ChallengeInvalidRound),
                    ErrorMessage = ErrorMessage.ChallengeInvalidRound
                };
            }
        }


        public static ErrorStatus TopicDeleteFailedAsQuestionStillExists
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.TopicQuestionStillExists),
                    ErrorMessage = ErrorMessage.TopicQuestionStillExists
                };
            }
        }

        public static ErrorStatus FeedPermissionNotGranted
        {
            get
            {
                return new ErrorStatus
                {
                    ErrorCode = Int16.Parse(ErrorCode.FeedPermissionNotGranted),
                    ErrorMessage = ErrorMessage.FeedPermissionNotGranted
                };
            }
        }
    }
}