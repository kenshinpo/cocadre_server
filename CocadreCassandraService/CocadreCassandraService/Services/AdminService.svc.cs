﻿using System;
using CocadreCassandraService.Entity;
using CocadreCassandraService.ServiceResponses;
using System.Collections.Generic;
using System.ServiceModel;

namespace CocadreCassandraService.WcfService.Services
{
    // 注意: 您可以使用 [重構] 功能表上的 [重新命名] 命令同時變更程式碼、svc 和組態檔中的類別名稱 "AdminService"。
    // 注意: 若要啟動 WCF 測試用戶端以便測試此服務，請在 [方案總管] 中選取 AdminService.svc 或 AdminService.svc.cs，然後開始偵錯。
    public class AdminService : IAdminService
    {
        Authenticator authenticator = new Authenticator();
        Department department = new Department();
        User user = new User();
        Country country = new Country();
        AccountKind accountKind = new AccountKind();

        #region Preload Data
        public void WriteExpAToTable()
        {
            Analytic analytic = new Analytic();
            analytic.WriteExpAToTable();
        }
        #endregion

        public LoginByAdminResponse LoginByAdmin(String email, String password)
        {
            return authenticator.LoginByAdmin(email, password);
        }

        public AccountKindListResponse GetAllAccountKind(String adminUserId, String companyId)
        {
            return accountKind.GetAllAccountKind(adminUserId, companyId);
        }

        #region For Department
        public DepartmentDetailResponse GetDepartmentDetail(String adminUserId, String companyId, String departmentId)
        {
            return department.GetDepartmentDetail(adminUserId, companyId, departmentId, Department.QUERY_TYPE_DETAIL);
        }

        public DepartmentListResponse GetAllDepartment(String adminUserId, String companyId)
        {
            return department.GetAllDepartment(adminUserId, companyId, Department.QUERY_TYPE_DETAIL);
        }

        public DepartmentCreateResponse CreateDepartment(String adminUserId, String companyId, String departmentTitle)
        {
            return department.Create(adminUserId, companyId, departmentTitle);
        }

        public DepartmentUpdateResponse UpdateDepartment(String adminUserId, String companyId, String departmentId, String departmentTitle)
        {
            return department.Update(adminUserId, companyId, departmentId, departmentTitle);
        }

        public DepartmentDeleteResponse DeleteDepartment(String adminUserId, String companyId, String departmentId)
        {
            return department.Delete(adminUserId, companyId, departmentId);
        }
        #endregion

        #region For User
        public UserCreateResponse CreateAdmin(String companyId, String companyTitle, String companyLogoUrl, String adminUserId, String plainPassword, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode)
        {
            return new User().CreateAdmin(companyId, companyTitle, companyLogoUrl, adminUserId, plainPassword, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode);
        }
        public UserListResponse GetAllUser(String adminUserId, String companyId, String departmentId, int userTypeCode, int userStatusCode)
        {
            return user.GetAllUserForAdmin(adminUserId, companyId, departmentId, userTypeCode, userStatusCode);
        }

        public UserListResponse GetAdmin(String adminUserId, String companyId)
        {
            return user.GetAdmin(adminUserId, companyId);
        }

        public UserDetailResponse GetUserDetail(String adminUserId, String companyId, String userId)
        {
            return user.GetUserDetail(adminUserId, companyId, userId);
        }

        public UserCreateResponse CreateUser(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentId, String departmentTitle)
        {
            return user.Create(adminUserId, companyId, userId, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode, departmentId, departmentTitle);
        }

        public UserUpdateResponse UpdateUser(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentId, String departmentTitle)
        {
            #region for debug
            //if (String.IsNullOrEmpty(adminUserId))
            //    adminUserId = "Udb0e704ee7244d21aea7930c27709fee";
            //if (String.IsNullOrEmpty(companyId))
            //    companyId = "C8b28091502514318bdcf48bec7c69129";
            //if (String.IsNullOrEmpty(userId))
            //    userId = "Ub82e69f8f1bb450c9d5c000e3bf0fe34";
            //if (String.IsNullOrEmpty(firstName))
            //    firstName = "Poh Chih";
            //if (String.IsNullOrEmpty(lastName))
            //    lastName = "Chan";
            //if (String.IsNullOrEmpty(email))
            //    email = "apo@aporigin.com";
            //if (String.IsNullOrEmpty(profileImageUrl))
            //    profileImageUrl = "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xap1/v/t1.0-1/p160x160/11136721_10153143570701390_2244157874794600609_n.jpg?oh=f992cf31e3dce0e38d0d2920967f4db8&oe=56488BDF&__gda__=1451022090_e420366741f6fa670544d259dfbc922f";
            //if (String.IsNullOrEmpty(position))
            //    position = "Software Engineer";
            //if (String.IsNullOrEmpty(phoneNumber))
            //    phoneNumber = "12345678";
            //if (String.IsNullOrEmpty(phoneCountryCode))
            //    phoneCountryCode = "886";
            //if (String.IsNullOrEmpty(phoneCountryName))
            //    phoneCountryName = "Taiwan";
            //if (String.IsNullOrEmpty(address))
            //    address = "香蕉路1號";
            //if (String.IsNullOrEmpty(addressCountryName))
            //    addressCountryName = "Taiwan";
            //if (String.IsNullOrEmpty(postalCode))
            //    postalCode = "5566";
            //if (String.IsNullOrEmpty(departmentId))
            //    departmentId = "D9231307fe82a411793de77f62c33c42f";
            //if (String.IsNullOrEmpty(departmentTitle))
            //    departmentTitle = "Development";
            #endregion

            return user.Update(adminUserId, companyId, userId, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode, departmentId, departmentTitle);
        }

        public UserUpdateStatusResponse UpdateUserStatus(String adminUserId, String companyId, String userId, int statusCode)
        {
            return user.UpdateStatus(adminUserId, companyId, userId, statusCode);
        }

        public UserUpdateTypeResponse UpdateUserType(String adminUserId, String companyId, String userId, int typeCode)
        {
            return user.UpdateType(adminUserId, companyId, userId, typeCode);
        }

        public UserDeleteResponse Delete(string adminUserId, string companyId, string userId)
        {
            return user.Delete(adminUserId, companyId, userId);
        }
        #endregion

        #region Country
        public void AddCountryToTable()
        {
            new Country().AddCountryToTable();
        }

        public void AddCountryIpToTable()
        {
            new Country().AddCountryIpToTable();
        }

        public CountryListResponse GetAllCountry(String adminUserId, String companyId)
        {
            return country.GetAllCountry(adminUserId, companyId);
        }
        #endregion

        #region Feed

        public FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId, string adminUserId)
        {
            return new Feed().DeleteFeed(feedId, companyId, ownerUserId, adminUserId);
        }
        public FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId, string commentorUserId, string adminUserId)
        {
            return new Feed().DeleteComment(feedId, commentId, companyId, commentorUserId, adminUserId);
        }
        public FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId, string commentorUserId, string adminUserId)
        {
            return new Feed().DeleteReply(feedId, commentId, replyId, companyId, commentorUserId, adminUserId);
        }

        #endregion

        #region Challenge

        public TopicSelectIconResponse SelectAllTopicIcons(string adminUserId,
                                                           string companyId)
        {
            return new Topic().SelectAllTopicIcons(adminUserId, companyId);
        }

        public CategoryCreateResponse CreateCategory(String adminUserId, String companyId, String categoryTitle)
        {
            return new TopicCategory().Create(adminUserId, companyId, categoryTitle);
        }

        public CategoryUpdateResponse UpdateCategory(String adminUserId, String companyId, String categoryId, String categoryTitle)
        {
            return new TopicCategory().Update(adminUserId, companyId, categoryId, categoryTitle);
        }

        public CategoryDeleteResponse DeleteCategory(String adminUserId, String companyId, String categoryId)
        {
            return new TopicCategory().Delete(adminUserId, companyId, categoryId);
        }

        public List<Topic.TopicStatus> SelectTopicStatusForDropdown()
        {
            return new Topic().SelectTopicStatusForDropdown();
        }

        public List<ChallengeQuestion.QuestionStatus> SelectQuestionStatusForDropdown()
        {
            return new ChallengeQuestion().SelectQuestionStatusForDropdown();
        }

        public List<Topic.DropdownNumberOfQuestions> SelectNumberOfQuestionsForDropdown()
        {
            return new Topic().SelectNumberOfQuestionsForDropdown();
        }

        public CategorySelectAllResponse SelectAllCategories(string adminUserId, string companyId)
        {
            TopicCategory category = new TopicCategory();

            return category.SelectAllCategories(adminUserId, companyId);
        }

        public CategorySelectAllResponse SelectAllCategoriesForDropdown(string adminUserId, string companyId)
        {
            TopicCategory category = new TopicCategory();

            return category.SelectAllCategoriesForDropdown(adminUserId, companyId);
        }

        public TopicCreateResponse CreateTopic(string adminUserId,
                                               string companyId,
                                               string topicTitle,
                                               string topicLogoUrl,
                                               string topicDescription,
                                               string categoryId,
                                               string categoryTitle,
                                               List<string> targetedDepartmentIds,
                                               int numberOfSelectedQuestions)
        {
            Topic topic = new Topic();
            return topic.CreateTopic(adminUserId, companyId, topicTitle, topicLogoUrl, topicDescription, categoryId, categoryTitle, targetedDepartmentIds, numberOfSelectedQuestions);
        }

        public QuestionCreateResponse CreateNewQuestion(string adminUserId,
                                                        string companyId,
                                                        string questionId,
                                                        string topicId,
                                                        string categoryId,
                                                        int questionType,
                                                        string questionContent,
                                                        string questionContentImageUrl,
                                                        string questionContentImageMd5,
                                                        string questionContentImageBackgroundColorCode,
                                                        int choiceType,
                                                        string firstChoiceContent,
                                                        string firstChoiceImageUrl,
                                                        string firstChoiceImageMd5,
                                                        string secondChoiceContent,
                                                        string secondChoiceImageUrl,
                                                        string secondChoiceImageMd5,
                                                        string thirdChoiceContent,
                                                        string thirdChoiceImageUrl,
                                                        string thirdChoiceImageMd5,
                                                        string fourthChoiceContent,
                                                        string fourthChoiceImageUrl,
                                                        string fourthChoiceImageMd5,
                                                        float timeAssignedForReading,
                                                        float timeAssignedForAnswering,
                                                        int difficultyLevel,
                                                        int scoreMultiplier,
                                                        double frequency,
                                                        int questionStatus)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.CreateNewQuestion(adminUserId,
                                             companyId,
                                             questionId,
                                             topicId,
                                             categoryId,
                                             questionType,
                                             questionContent,
                                             questionContentImageUrl,
                                             questionContentImageMd5,
                                             questionContentImageBackgroundColorCode,
                                             choiceType,
                                             firstChoiceContent,
                                             firstChoiceImageUrl,
                                             firstChoiceImageMd5,
                                             secondChoiceContent,
                                             secondChoiceImageUrl,
                                             secondChoiceImageMd5,
                                             thirdChoiceContent,
                                             thirdChoiceImageUrl,
                                             thirdChoiceImageMd5,
                                             fourthChoiceContent,
                                             fourthChoiceImageUrl,
                                             fourthChoiceImageMd5,
                                             timeAssignedForReading,
                                             timeAssignedForAnswering,
                                             difficultyLevel,
                                             scoreMultiplier,
                                             frequency,
                                             questionStatus);
        }

        public TopicSelectAllBasicResponse SelectTopicBasicByCategoryAndDepartment(string adminUserId,
                                                                                    string companyId,
                                                                                    string selectedTopicCategoryId,
                                                                                    string selectedDepartmentId)
        {
            Topic topic = new Topic();
            return topic.SelectAllTopicBasicByCategoryAndDepartment(adminUserId, companyId, selectedTopicCategoryId, selectedDepartmentId);
        }


        public TopicSelectResponse SelectTopicDetail(string topicId,
                                                     string adminUserId,
                                                     string companyId)
        {
            Topic topic = new Topic();
            return topic.SelectTopicDetail(topicId,
                                           adminUserId,
                                           companyId);
        }

        public QuestionSelectResponse SelectQuestion(string questionId, string topicId, string companyId, string adminUserId)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.SelectQuestion(questionId,
                                           topicId,
                                           companyId,
                                           adminUserId);
        }

        public TopicUpdateResponse UpdateTopicStatus(string adminUserId,
                                                     string companyId,
                                                     string topicId,
                                                     string categoryId,
                                                     int status)
        {
            Topic topic = new Topic();
            return topic.UpdateTopicStatus(adminUserId,
                                           companyId,
                                           topicId,
                                           categoryId,
                                           status);
        }

        public QuestionUpdateResponse UpdateQuestionStatus(string adminUserId,
                                                           string companyId,
                                                           string topicId,
                                                           string categoryId,
                                                           string questionId,
                                                           int status)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.UpdateQuestionStatus(adminUserId, companyId, topicId, categoryId, questionId, status);
        }

        public TopicUpdateResponse UpdateTopic(string adminUserId,
                                               string companyId,
                                               string topicId,
                                               string newTitle,
                                               string newLogoUrl,
                                               string newDescription,
                                               string newCategoryId,
                                               string newCategoryTitle,
                                               int newStatus,
                                               List<string> newTargetedDepartmentIds,
                                               int newNumberOfSelectedQuestions)
        {
            Topic topic = new Topic();
            return topic.UpdateTopic(adminUserId, companyId, topicId, newTitle, newLogoUrl, newDescription, newCategoryId, newCategoryTitle, newStatus, newTargetedDepartmentIds, newNumberOfSelectedQuestions);
        }

        public QuestionUpdateResponse UpdateQuestion(string adminUserId,
                                                    string companyId,
                                                    string questionId,
                                                    string topicId,
                                                    string categoryId,
                                                    int questionType,
                                                    string questionContent,
                                                    string questionContentImageUrl,
                                                    string questionContentImageMd5,
                                                    string questionContentImageBackgroundColorCode,
                                                    int choiceType,
                                                    string firstChoiceContent,
                                                    string firstChoiceImageUrl,
                                                    string firstChoiceImageMd5,
                                                    string secondChoiceContent,
                                                    string secondChoiceImageUrl,
                                                    string secondChoiceImageMd5,
                                                    string thirdChoiceContent,
                                                    string thirdChoiceImageUrl,
                                                    string thirdChoiceImageMd5,
                                                    string fourthChoiceContent,
                                                    string fourthChoiceImageUrl,
                                                    string fourthChoiceImageMd5,
                                                    float timeAssignedForReading,
                                                    float timeAssignedForAnswering,
                                                    int difficultyLevel,
                                                    int scoreMultiplier,
                                                    double frequency,
                                                    int questionStatus)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.UpdateQuestion(adminUserId, companyId, questionId, topicId, categoryId, questionType, questionContent, questionContentImageUrl, questionContentImageMd5, questionContentImageBackgroundColorCode, choiceType, firstChoiceContent, firstChoiceImageUrl, firstChoiceImageMd5, secondChoiceContent, secondChoiceImageUrl, secondChoiceImageMd5, thirdChoiceContent, thirdChoiceImageUrl, thirdChoiceImageMd5, fourthChoiceContent, fourthChoiceImageUrl, fourthChoiceImageMd5, timeAssignedForReading, timeAssignedForAnswering, difficultyLevel, scoreMultiplier, frequency, questionStatus);
        }
        #endregion
    }
}
