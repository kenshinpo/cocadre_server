﻿using System;
using System.Collections.Generic;
using CocadreCassandraService.Connection;
using CocadreCassandraService.Entity;
using CocadreCassandraService.ServiceResponses;
using Cassandra;
using CocadreCassandraService.Validation;


namespace CocadreCassandraService.Services
{
    // 注意: 您可以使用 [重構] 功能表上的 [重新命名] 命令同時變更程式碼、svc 和組態檔中的類別名稱 "ClientService"。
    // 注意: 若要啟動 WCF 測試用戶端以便測試此服務，請在 [方案總管] 中選取 ClientService.svc 或 ClientService.svc.cs，然後開始偵錯。
    public class ClientService : IClientService
    {
        #region Device Token
        public UserUpdateDeviceTokenResponse UpdateUserDeviceToken(string userId,
                                                                   string companyId,
                                                                   string deviceToken,
                                                                   int deviceType)
        {
            User user = new User();
            return user.UpdateUserDeviceToken(userId, companyId, deviceToken, deviceType);
        }
        #endregion
        #region Authentication
        public AuthenticationSelectUserResponse AuthenticateUserForLogin(string email,
                                                                         string encryptedPassword)
        {
            Authenticator authenticator = new Authenticator();
            return authenticator.SelectAuthenticatedUser(email, encryptedPassword);
        }

        public UserSelectTokenWithCompanyResponse SelectUserTokenWithCompany(string userId,
                                                                             string companyId)
        {
            User user = new User();
            return user.SelectUserWithTokenAndCompany(userId, companyId);
        }

        #endregion

        #region Colleagues
        public UserSelectAllByDepartmentResponse SelectAllUsersByDepartment(string requesterUserId, string companyId)
        {
            User user = new User();
            return user.SelectAllUsersSortedByDepartment(requesterUserId, companyId);
        }
        #endregion

        #region DAU
        public void UpdateUserActivity(string userId,
                                       string companyId,
                                       bool isActive)
        {
            Analytic analytic = new Analytic();
            analytic.UpdateUserActivity(userId, companyId, isActive);
        }

        public UserUpdateLoginResponse UpdateUserLogin(string userId,
                                                       string companyId,
                                                       bool isLogin)
        {
            Analytic analytic = new Analytic();
            return analytic.UpdateUserLogin(userId, companyId, isLogin);
        }
        #endregion 

        #region
        public BrainSelectResponse SelectBrain(string requesterUserId, string companyId)
        {
            return new Brain().SelectBrain(requesterUserId, companyId);
        }
        #endregion

        #region Feed
        public FeedAuthenticationResponse AuthenticateUserForPostingFeed(string companyId,
                                                                         string userId)
        {
            Feed feed = new Feed();
            FeedAuthenticationResponse response = feed.AuthenticateUserForPostingFeed(companyId,
                                                                                      userId);


            return response;
        }

        public FeedCreateResponse CreateFeedTextPost(string feedId,
                                                     string creatorUserId,
                                                     string companyId,
                                                     string content,
                                                     List<string> targetedDepartmentIds,
                                                     List<string> targetedUserIds)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedTextPost(feedId,
                                                                  creatorUserId,
                                                                  companyId,
                                                                  content,
                                                                  targetedDepartmentIds,
                                                                  targetedUserIds);


            return response;
        }

        public FeedCreateResponse CreateFeedImagePost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      List<string> imageUrls,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedImagePost(feedId,
                                                                    creatorUserId,
                                                                    companyId,
                                                                    caption,
                                                                    imageUrls,
                                                                    targetedDepartmentIds,
                                                                    targetedUserIds);


            return response;
        }

        public FeedCreateResponse CreateFeedVideoPost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      string videoUrl,
                                                      string videoThumbnailUrl,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedVideoPost(feedId,
                                                                    creatorUserId,
                                                                    companyId,
                                                                    caption,
                                                                    videoUrl,
                                                                    videoThumbnailUrl,
                                                                    targetedDepartmentIds,
                                                                    targetedUserIds);

            return response;
        }

        public FeedCreateResponse CreateFeedSharedUrlPost(string feedId,
                                                          string creatorUserId,
                                                          string companyId,
                                                          string caption,
                                                          string url,
                                                          string urlTitle,
                                                          string urlDescription,
                                                          string urlSiteName,
                                                          string urlImageUrl,
                                                          List<string> targetedDepartmentIds,
                                                          List<string> targetedUserIds)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedSharedUrlPost(feedId,
                                                                       creatorUserId,
                                                                       companyId,
                                                                       caption,
                                                                       url,
                                                                       urlTitle,
                                                                       urlDescription,
                                                                       urlSiteName,
                                                                       urlImageUrl,
                                                                       targetedDepartmentIds,
                                                                       targetedUserIds);

            return response;
        }

        public CommentCreateResponse CreateCommentText(string creatorUserId,
                                                       string companyId,
                                                       string feedId,
                                                       string content)
        {
            Feed feed = new Feed();
            CommentCreateResponse response = feed.CreateCommentText(creatorUserId,
                                                                     companyId,
                                                                     feedId,
                                                                     content);

            return response;
        }

        public ReplyCreateResponse CreateReplyText(string creatorUserId,
                                                   string companyId,
                                                   string feedId,
                                                   string commentId,
                                                   string content)
        {
            Feed feed = new Feed();
            ReplyCreateResponse response = feed.CreateReplyText(creatorUserId,
                                                                companyId,
                                                                feedId,
                                                                commentId,
                                                                content);

            return response;
        }


        public FeedSelectResponse SelectCompanyFeedPost(string requesterUserId,
                                                        string companyId,
                                                        string searchContent,
                                                        int numberOfPostsLoaded,
                                                        DateTime? newestTimestamp,
                                                        DateTime? oldestTimestamp)
        {
            Feed feed = new Feed();
            FeedSelectResponse response = feed.SelectCompanyFeedPost(requesterUserId,
                                                                      companyId,
                                                                      searchContent,
                                                                      numberOfPostsLoaded,
                                                                      newestTimestamp,
                                                                      oldestTimestamp);

            return response;
        }

        public FeedSelectResponse SelectPersonnelFeedPost(string requesterUserId,
                                                          string ownerUserId,
                                                          string companyId,
                                                          string searchContent,
                                                          DateTime? newestTimestamp,
                                                          DateTime? oldestTimestamp)
        {
            Feed feed = new Feed();
            FeedSelectResponse response = feed.SelectPersonnelFeedPost(requesterUserId,
                                                                        ownerUserId,
                                                                        companyId,
                                                                        searchContent,
                                                                        newestTimestamp,
                                                                        oldestTimestamp);

            return response;
        }

        public CommentSelectResponse SelectFeedComment(string requesterUserId,
                                                       string feedId,
                                                       string companyId)
        {
            Feed feed = new Feed();
            CommentSelectResponse response = feed.SelectFeedComment(requesterUserId,
                                                                     feedId,
                                                                     companyId);

            return response;
        }

        public ReplySelectResponse SelectCommentReply(string requesterUserId,
                                                      string feedId,
                                                      string commentId,
                                                      string companyId)
        {
            Feed feed = new Feed();
            ReplySelectResponse response = feed.SelectCommentReply(requesterUserId,
                                                                    feedId,
                                                                    commentId,
                                                                    companyId);

            return response;
        }

        public PointUpdateResponse UpdateFeedPoint(string voterUserId,
                                                   string feedId,
                                                   string companyId,
                                                   bool isUpVote)
        {
            Feed feed = new Feed();
            PointUpdateResponse response = feed.UpdateFeedPoint(voterUserId,
                                                                feedId,
                                                                companyId,
                                                                isUpVote);

            return response;

        }

        public PointUpdateResponse UpdateCommentPoint(string voterUserId,
                                                      string feedId,
                                                      string commentId,
                                                      string companyId,
                                                      bool isUpVote)
        {
            Feed feed = new Feed();
            PointUpdateResponse response = feed.UpdateCommentPoint(voterUserId,
                                                                   feedId,
                                                                   commentId,
                                                                   companyId,
                                                                   isUpVote);

            return response;
        }

        public PointUpdateResponse UpdateReplyPoint(string voterUserId,
                                                    string feedId,
                                                    string commentId,
                                                    string replyId,
                                                    string companyId,
                                                    bool isUpVote)
        {
            Feed feed = new Feed();
            PointUpdateResponse response = feed.UpdateReplyPoint(voterUserId,
                                                                  feedId,
                                                                  commentId,
                                                                  replyId,
                                                                  companyId,
                                                                  isUpVote);

            return response;
        }

        public FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId)
        {
            return new Feed().DeleteFeed(feedId, companyId, ownerUserId, null);
        }

        public FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId, string commentorUserId)
        {
            return new Feed().DeleteComment(feedId, commentId, companyId, commentorUserId, null);
        }

        public FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId, string commentorUserId)
        {
            return new Feed().DeleteReply(feedId, commentId, replyId, companyId, commentorUserId, null);
        }
        #endregion

        #region Challenge
        public ChallengeInvalidateResponse InvalidateChallenge(string userId, string companyId, string challengeId, int reason)
        {
            return new Topic().InvalidateChallenge(userId, companyId, challengeId, reason);
        }

        public CategorySelectAllWithTopicResponse SelectAllTopicBasicByUserAndCategory(string requesterUserId,
                                                                                       string companyId)
        {
            Topic topic = new Topic();
            return topic.SelectAllTopicBasicByUserAndCategory(requesterUserId, companyId);
        }

        public TopicSelectAllBasicResponse SelectAllTopicBasicByOpponent(string initiatorUserId,
                                                                         string challengedUserId,
                                                                         string companyId,
                                                                         string topicStartsWithName)
        {
            Topic topic = new Topic();
            return topic.SelectAllTopicBasicByOpponent(initiatorUserId, challengedUserId, companyId, topicStartsWithName);
        }

        public UserSelectAllBasicResponse SelectAllUsersByTopicId(string topicId, string categoryId, string requesterUserId, string companyId, string startsWithName)
        {
            User user = new User();
            return user.SelectAllUsersByTopicId(topicId, categoryId, requesterUserId, companyId, startsWithName);
        }

        public ChallengeCreateResponse CreateChallengeWithTopicId(string companyId, 
                                                                  string initiatorUserId,
                                                                  string challengedUserId,
                                                                  string topicId,
                                                                  string categoryId)
        {
            Topic topic = new Topic();
            return topic.CreateChallengeWithTopicId(companyId, initiatorUserId, challengedUserId, topicId, categoryId);
        }

        public QuestionSelectAllResponse SelectAllQuestionsForChallenge(string requesterUserId,
                                                                        string companyId,
                                                                        string challengeId)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.SelectAllQuestionsForChallenge(requesterUserId, companyId, challengeId);
        }

        public QuestionAnswerResponse AnswerChallengeQuestion(string challengeId,
                                                              string companyId,
                                                              string userId,
                                                              string answer,
                                                              float timeTaken,
                                                              string questionId,
                                                              string topicId,
                                                              int round)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.AnswerChallengeQuestion(challengeId,
                                                    companyId,
                                                    userId,
                                                    answer,
                                                    timeTaken,
                                                    questionId,
                                                    topicId,
                                                    round);
        }

        public ChallengeCreateResponse SelectChallengeWithChallengeId(string companyId, 
                                                                      string challengeId,
                                                                      string requesterUserId)
        {
            Topic topic = new Topic();
            return topic.SelectChallengeWithChallengeId(companyId, challengeId, requesterUserId);
        }

        public ChallengeStartWithoutOpponentResponse StartChallengeWithoutOpponent(string challengeId, string requesterUserId, string companyId)
        {
            Topic topic = new Topic();
            return topic.StartChallengeWithoutOpponent(challengeId, requesterUserId, companyId);
        }

        public ChallengeOfflineSelectOpponentAnswerResponse SelectOpponentAnswerForOfflineGame(string challengeId, string requesterUserId, string companyId, int round)
        {
            Topic topic = new Topic();
            return topic.SelectOpponentAnswerForOfflineGame(challengeId, requesterUserId, companyId, round);
        }

        public ChallengeIsReadyResponse SetPlayerReadyForChallenge(string requesterUserId,
                                                                   string companyId,
                                                                   string challengeId)
        {
            Topic topic = new Topic();
            return topic.SetPlayerReadyForChallenge(requesterUserId, companyId, challengeId);
        }
        #endregion

        #region Challenge stats
        public ChallengeSelectStatsResponse SelectStatsForChallenge(string topicId, string requesterUserId, string opponentUserId, string companyId)
        {
            Analytic analytic = new Analytic();
            return analytic.SelectStatsForChallenge(topicId, requesterUserId, opponentUserId, companyId);
        }
        #endregion
    }
}
