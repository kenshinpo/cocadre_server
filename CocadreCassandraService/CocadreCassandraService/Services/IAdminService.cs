﻿using System;
using System.ServiceModel;
using CocadreCassandraService.ServiceResponses;
using System.Collections.Generic;
using CocadreCassandraService.Entity;

namespace CocadreCassandraService.WcfService.Services
{
    // 注意: 您可以使用 [重構] 功能表上的 [重新命名] 命令同時變更程式碼和組態檔中的介面名稱 "IAdminService"。
    [ServiceContract]
    public interface IAdminService
    {
        #region Preload Data
        [OperationContract]
        void WriteExpAToTable();

        #endregion

        [OperationContract]
        UserCreateResponse CreateAdmin(String companyId, String companyTitle, String companyLogoUrl, String adminUserId, String plainPassword, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode);

        [OperationContract]
        LoginByAdminResponse LoginByAdmin(String email, String password);

        [OperationContract]
        AccountKindListResponse GetAllAccountKind(String adminUserId, String companyId);

        [OperationContract]
        DepartmentDetailResponse GetDepartmentDetail(String adminUserId, String companyId, String departmentId);

        [OperationContract]
        DepartmentListResponse GetAllDepartment(String adminUserId, String companyId);

        [OperationContract]
        DepartmentCreateResponse CreateDepartment(String adminUserId, String companyId, String departmentTitle);

        [OperationContract]
        DepartmentUpdateResponse UpdateDepartment(String adminUserId, String companyId, String departmentId, String departmentTitle);

        [OperationContract]
        DepartmentDeleteResponse DeleteDepartment(String adminUserId, String companyId, String departmentId);

        [OperationContract]
        UserListResponse GetAllUser(String adminUserId, String companyId, String departmentId, int userTypeCode, int userStatusCode);

        [OperationContract]
        UserListResponse GetAdmin(String adminUserId, String companyId);

        [OperationContract]
        UserDetailResponse GetUserDetail(String adminUserId, String companyId, String userId);

        [OperationContract]
        UserCreateResponse CreateUser(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentId, String departmentTitle);

        [OperationContract]
        UserUpdateResponse UpdateUser(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentId, String departmentTitle);

        [OperationContract]
        UserUpdateStatusResponse UpdateUserStatus(String adminUserId, String companyId, String userId, int statusCode);

        [OperationContract]
        UserUpdateTypeResponse UpdateUserType(String adminUserId, String companyId, String userId, int typeCode);

        [OperationContract]
        UserDeleteResponse Delete(string adminUserId, string companyId, string userId);

        #region Country
        [OperationContract]
        void AddCountryToTable();

        [OperationContract]
        void AddCountryIpToTable();

        [OperationContract]
        CountryListResponse GetAllCountry(String adminUserId, String companyId);
        #endregion

        #region Feed
        [OperationContract]
        FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId, string adminUserId);

        [OperationContract]
        FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId, string commentorUserId, string adminUserId);

        [OperationContract]
        FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId, string commentorUserId, string adminUserId);
        #endregion

        #region Challenge
        [OperationContract]
        TopicSelectIconResponse SelectAllTopicIcons(string adminUserId, string companyId);

        [OperationContract]
        CategoryCreateResponse CreateCategory(String adminUserId, String companyId, String categoryTitle);

        [OperationContract]
        CategoryUpdateResponse UpdateCategory(String adminUserId, String companyId, String categoryId, String categoryTitle);

        [OperationContract]
        CategoryDeleteResponse DeleteCategory(String adminUserId, String companyId, String categoryId);

        [OperationContract]
        List<Topic.TopicStatus> SelectTopicStatusForDropdown();

        [OperationContract]
        List<ChallengeQuestion.QuestionStatus> SelectQuestionStatusForDropdown();

        [OperationContract]
        List<Topic.DropdownNumberOfQuestions> SelectNumberOfQuestionsForDropdown();

        [OperationContract]
        CategorySelectAllResponse SelectAllCategories(string adminUserId, string companyId);

        [OperationContract]
        CategorySelectAllResponse SelectAllCategoriesForDropdown(string adminUserId, string companyId);

        [OperationContract]
        TopicCreateResponse CreateTopic(string adminUserId,
                                        string companyId,
                                        string topicTitle,
                                        string topicLogoUrl,
                                        string topicDescription,
                                        string categoryId,
                                        string categoryTitle,
                                        List<string> targetedDepartmentIds,
                                        int numberOfSelectedQuestions);

        [OperationContract]
        QuestionCreateResponse CreateNewQuestion(string adminUserId,
                                                string companyId,
                                                string questionId,
                                                string topicId,
                                                string categoryId,
                                                int questionType,
                                                string questionContent,
                                                string questionContentImageUrl,
                                                string questionContentImageMd5,
                                                string questionContentImageBackgroundColorCode,
                                                int choiceType,
                                                string firstChoiceContent,
                                                string firstChoiceImageUrl,
                                                string firstChoiceImageMd5,
                                                string secondChoiceContent,
                                                string secondChoiceImageUrl,
                                                string secondChoiceImageMd5,
                                                string thirdChoiceContent,
                                                string thirdChoiceImageUrl,
                                                string thirdChoiceImageMd5,
                                                string fourthChoiceContent,
                                                string fourthChoiceImageUrl,
                                                string fourthChoiceImageMd5,
                                                float timeAssignedForReading,
                                                float timeAssignedForAnswering,
                                                int difficultyLevel,
                                                int scoreMultiplier,
                                                double frequency,
                                                int questionStatus);

        [OperationContract]
        TopicSelectAllBasicResponse SelectTopicBasicByCategoryAndDepartment(string adminUserId,
                                                                            string companyId,
                                                                            string selectedTopicCategoryId,
                                                                            string selectedDepartmentId);

        [OperationContract]
        TopicSelectResponse SelectTopicDetail(string topicId,
                                              string adminUserId,
                                              string companyId);

        [OperationContract]
        QuestionSelectResponse SelectQuestion(string questionId,
                                              string topicId,
                                              string companyId,
                                              string adminUserId);

        [OperationContract]
        TopicUpdateResponse UpdateTopicStatus(string adminUserId,
                                             string companyId,
                                             string topicId,
                                             string categoryId,
                                             int status);

        [OperationContract]
        QuestionUpdateResponse UpdateQuestionStatus(string adminUserId,
                                                    string companyId,
                                                    string topicId,
                                                    string categoryId,
                                                    string questionId,
                                                    int status);

        [OperationContract]
        TopicUpdateResponse UpdateTopic(string adminUserId,
                                        string companyId,
                                        string topicId,
                                        string newTitle,
                                        string newLogoUrl,
                                        string newDescription,
                                        string newCategoryId,
                                        string newCategoryTitle,
                                        int newStatus,
                                        List<string> newTargetedDepartmentIds,
                                        int newNumberOfSelectedQuestions);

        [OperationContract]
        QuestionUpdateResponse UpdateQuestion(string adminUserId,
                                            string companyId,
                                            string questionId,
                                            string topicId,
                                            string categoryId,
                                            int questionType,
                                            string questionContent,
                                            string questionContentImageUrl,
                                            string questionContentImageMd5,
                                            string questionContentImageBackgroundColorCode,
                                            int choiceType,
                                            string firstChoiceContent,
                                            string firstChoiceImageUrl,
                                            string firstChoiceImageMd5,
                                            string secondChoiceContent,
                                            string secondChoiceImageUrl,
                                            string secondChoiceImageMd5,
                                            string thirdChoiceContent,
                                            string thirdChoiceImageUrl,
                                            string thirdChoiceImageMd5,
                                            string fourthChoiceContent,
                                            string fourthChoiceImageUrl,
                                            string fourthChoiceImageMd5,
                                            float timeAssignedForReading,
                                            float timeAssignedForAnswering,
                                            int difficultyLevel,
                                            int scoreMultiplier,
                                            double frequency,
                                            int questionStatus);
        #endregion
    }
}
