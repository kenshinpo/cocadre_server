﻿using CocadreCassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace CocadreCassandraService.Services
{
    // 注意: 您可以使用 [重構] 功能表上的 [重新命名] 命令同時變更程式碼和組態檔中的介面名稱 "IClientService"。
    [ServiceContract]
    public interface IClientService
    {
        #region Device Token
        [OperationContract]
        UserUpdateDeviceTokenResponse UpdateUserDeviceToken(string userId,
                                                            string companyId,
                                                            string deviceToken,
                                                            int deviceType);
        #endregion

        #region Authentication
        [OperationContract]
        AuthenticationSelectUserResponse AuthenticateUserForLogin(string email,
                                                                  string encryptedPassword);

        [OperationContract]
        UserSelectTokenWithCompanyResponse SelectUserTokenWithCompany(string userId,
                                                                      string companyId);
        #endregion

        #region Colleagues
        [OperationContract]
        UserSelectAllByDepartmentResponse SelectAllUsersByDepartment(string requesterUserId, string companyId);
        #endregion

        #region DAU
        [OperationContract]
        void UpdateUserActivity(string userId,
                                string companyId,
                                bool isActive);

        [OperationContract]
        UserUpdateLoginResponse UpdateUserLogin(string userId,
                                                string companyId,
                                                bool isLogin);
        #endregion

        #region Brain
        [OperationContract]
        BrainSelectResponse SelectBrain(string requesterUserId, string companyId);
        #endregion

        #region Feed
        [OperationContract]
        FeedAuthenticationResponse AuthenticateUserForPostingFeed(string companyId,
                                                                  string userId);

        [OperationContract]
        FeedCreateResponse CreateFeedTextPost(string feedId,
                                              string creatorUserId,
                                              string companyId,
                                              string content,
                                              List<string> targetedDepartmentIds,
                                              List<string> targetedUserIds);

        [OperationContract]
        FeedCreateResponse CreateFeedImagePost(string feedId,
                                               string creatorUserId,
                                               string companyId,
                                               string caption,
                                               List<string> imageUrls,
                                               List<string> targetedDepartmentIds,
                                               List<string> targetedUserIds);

        [OperationContract]
        FeedCreateResponse CreateFeedVideoPost(string feedId,
                                               string creatorUserId,
                                               string companyId,
                                               string caption,
                                               string videoUrl,
                                               string videoThumbnailUrl,
                                               List<string> targetedDepartmentIds,
                                               List<string> targetedUserIds);

        [OperationContract]
        FeedCreateResponse CreateFeedSharedUrlPost(string feedId,
                                                   string creatorUserId,
                                                   string companyId,
                                                   string caption,
                                                   string url,
                                                   string urlTitle,
                                                   string urlDescription,
                                                   string urlSiteName,
                                                   string urlImageUrl,
                                                   List<string> targetedDepartmentIds,
                                                   List<string> targetedUserIds);
        [OperationContract]
        CommentCreateResponse CreateCommentText(string creatorUserId,
                                                string companyId,
                                                string feedId,
                                                string content);

        [OperationContract]
        ReplyCreateResponse CreateReplyText(string creatorUserId,
                                            string companyId,
                                            string feedId,
                                            string commentId,
                                            string content);

        [OperationContract]
        FeedSelectResponse SelectCompanyFeedPost(string requestorUserId,
                                                 string companyId,
                                                 string searchContent,
                                                 int numberOfPostsLoaded,
                                                 DateTime? newestTimestamp,
                                                 DateTime? oldestTimestamp);

        [OperationContract]
        FeedSelectResponse SelectPersonnelFeedPost(string requesterUserId,
                                                   string ownerUserId,
                                                   string companyId,
                                                   string searchContent,
                                                   DateTime? newestTimestamp,
                                                   DateTime? oldestTimestamp);

        [OperationContract]
        ReplySelectResponse SelectCommentReply(string requestorUserId,
                                               string feedId,
                                               string commentId,
                                               string companyId);

        [OperationContract]
        CommentSelectResponse SelectFeedComment(string requestorUserId,
                                                string feedId,
                                                string companyId);

        [OperationContract]
        PointUpdateResponse UpdateFeedPoint(string voterUserId,
                                            string feedId,
                                            string companyId,
                                            bool isUpVote);
        [OperationContract]
        PointUpdateResponse UpdateCommentPoint(string voterUserId,
                                               string feedId,
                                               string commentId,
                                               string companyId,
                                               bool isUpVote);
        [OperationContract]
        PointUpdateResponse UpdateReplyPoint(string voterUserId,
                                            string feedId,
                                            string commentId,
                                            string replyId,
                                            string companyId,
                                            bool isUpVote);


        [OperationContract]
        FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId);

        [OperationContract]
        FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId, string commentorUserId);

        [OperationContract]
        FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId, string commentorUserId);

        #endregion

        #region Challenge
        [OperationContract]
        ChallengeInvalidateResponse InvalidateChallenge(string userId, string companyId, string challengeId, int reason);

        [OperationContract]
        CategorySelectAllWithTopicResponse SelectAllTopicBasicByUserAndCategory(string requesterUserId,
                                                                                string companyId);

        [OperationContract]
        TopicSelectAllBasicResponse SelectAllTopicBasicByOpponent(string initiatorUserId,
                                                                  string challengedUserId,
                                                                  string companyId,
                                                                  string topicStartsWithName);

        [OperationContract]
        UserSelectAllBasicResponse SelectAllUsersByTopicId(string topicId, string categoryId, string requesterUserId, string companyId, string startsWithName);

        [OperationContract]
        ChallengeCreateResponse CreateChallengeWithTopicId(string companyId,
                                                           string initiatorUserId,
                                                           string challengedUserId,
                                                           string topicId,
                                                           string categoryId);
        [OperationContract]
        QuestionSelectAllResponse SelectAllQuestionsForChallenge(string requesterUserId,
                                                                 string companyId,
                                                                 string challengeId);

        [OperationContract]
        QuestionAnswerResponse AnswerChallengeQuestion(string challengeId,
                                                       string companyId,
                                                       string userId,
                                                       string answer,
                                                       float timeTaken,
                                                       string questionId,
                                                       string topicId,
                                                       int round);
        [OperationContract]
        ChallengeCreateResponse SelectChallengeWithChallengeId(string companyId,
                                                               string challengeId,
                                                               string requesterUserId);

        [OperationContract]
        ChallengeStartWithoutOpponentResponse StartChallengeWithoutOpponent(string challengeId, 
                                                                            string requesterUserId, 
                                                                            string companyId);

        [OperationContract]
        ChallengeOfflineSelectOpponentAnswerResponse SelectOpponentAnswerForOfflineGame(string challengeId,
                                                                                        string requesterUserId,
                                                                                        string companyId,
                                                                                        int round);
        [OperationContract]
        ChallengeIsReadyResponse SetPlayerReadyForChallenge(string requesterUserId,
                                                            string companyId,
                                                            string challengeId);
        #endregion

        #region Challenge stats
        [OperationContract]
        ChallengeSelectStatsResponse SelectStatsForChallenge(string topicId, string requesterUserId, string opponentUserId, string companyId);
        #endregion
    }
}
