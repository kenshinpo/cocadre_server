﻿using log4net;
using PubNubMessaging.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Utilities
{
    public class PubnubCallback
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        public static void AddChannelToGroupMessage(string message)
        {
            Log.Debug("Add channel to group success: " + message);
        }

        public static void AddChannelToGroupErrorMessage(PubnubClientError error)
        {
            Log.Error("Add channel to group error: " + error.Message);
        }

        public static void GetChannelsFromGroupMessage(string message)
        {
            Log.Debug("Get channels from group success: " + message);
        }

        public static void GetChannelsFromGroupErrorMessage(PubnubClientError error)
        {
            Log.Error("Get channels from group error: " + error.Message);
        }

        public static void GrantServerAccessToChannelMessage(string message)
        {
            Log.Debug("Grant server access to channel success: " + message);
        }

        public static void GrantServerAccessToChannelErrorMessage(PubnubClientError error)
        {
            Log.Error("Grant server access to channel error: " + error.Message);
        }

        public static void GrantClientAccessToChannelMessage(string message)
        {
            Log.Debug("Grant client access to channel success: " + message);
        }

        public static void GrantClientAccessToChannelErrorMessage(PubnubClientError error)
        {
            Log.Error("Grant client access to channel error: " + error.Message);
        }

        public static void RemoveChannelMessage(string message)
        {
            Log.Debug("Remove channel is success: " + message);
        }

        public static void RemoveChannelErrorMessage(PubnubClientError error)
        {
            Log.Error("Remove channel error: " + error.Message);
        }

        public static void RemoveGroupMessage(string message)
        {
            Log.Debug("Remove group is success: " + message);
        }

        public static void RemoveGroupErrorMessage(PubnubClientError error)
        {
            Log.Error("Remove group error: " + error.Message);
        }

        public static void RegisterDeviceForPushMessage(string message)
        {
            Log.Debug("Register device for push success: " + message);
        }

        public static void RegisterDeviceForPushErrorMessage(PubnubClientError error)
        {
            Log.Error("Register device for push error: " + error.Message);
        }

        public static void UnregisterDeviceForPushMessage(string message)
        {
            Log.Debug("Unregister device for push success: " + message);
        }

        public static void UnregisterDeviceForPushErrorMessage(PubnubClientError error)
        {
            Log.Error("Unregister device for push error: " + error.Message);
        }

        public static void PushNotificationErrorMessage(PubnubClientError error)
        {
            Log.Debug("Push notification message error: " + error.Message);
        }

        public static void PushNotificationMessage(string message)
        {
            Log.Debug("Push notification message: " + message);
        }

        public static void PublishToNotifyErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish notification message error: " + error.Message);
        }

        public static void PublishToNotifyMessage(string message)
        {
            Log.Debug("Publish notification message: " + message);
        }

        public static void PresenceCheckErrorMessage(PubnubClientError error)
        {
            Log.Debug("Presence check error: " + error.Message);
        }

        public static void PublishToJoinChallengeMessage(string message)
        {
            Log.Debug("Publish to join channel to challenge success: " + message);
        }

        public static void PublishToJoinChallengeErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to join channel to challenge error: " + error.Message);
        }

        public static void PublishToCancelChallengeMessage(string message)
        {
            Log.Debug("Publish to cancel challenge success: " + message);
        }

        public static void PublishToCancelChallengeErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to cancel challenge error: " + error.Message);
        }

        public static void PublishToKillChallengeMessage(string message)
        {
            Log.Debug("Publish to kill challenge success: " + message);
        }

        public static void PublishToCancelKillErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to kill challenge error: " + error.Message);
        }

        public static void PublishOpponentAnswerMessage(string message)
        {
            Log.Debug("Publish opponent answer success: " + message);
        }

        public static void PublishOpponentAnswerErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish opponent answer error: " + error.Message);
        }

        public static void UnsubscribeGameChannelFromGroupMessage(string message)
        {
            Log.Debug("Unsubscribe game challenge success: " + message);
        }

        public static void UnsubscribeGameChannelFromGroupErrorMessage(PubnubClientError error)
        {
            Log.Debug("Unsubscribe game challenge error: " + error.Message);
        }

        public static void RemoveGameChannelFromGroupMessage(string message)
        {
            Log.Debug("Remove game challenge success: " + message);
        }

        public static void RemoveGameChannelFromGroupErrorMessage(PubnubClientError error)
        {
            Log.Debug("Remove game challenge error: " + error.Message);
        }

        public static void PublishToUserImageProgressMessage(string message)
        {
            Log.Debug("Publish to user image progress channel success: " + message);
        }

        public static void PublishToUserImageProgressErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to user image progress channel error: " + error.Message);
        }

        public static void PublishToFeedVideoProgressMessage(string message)
        {
            Log.Debug("Publish to feed video progress channel success: " + message);
        }

        public static void PublishToFeedVideoProgressErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to feed video progress channel error: " + error.Message);
        }

        public static void PublishToFeedImageProgressMessage(string message)
        {
            Log.Debug("Publish to feed image progress channel success: " + message);
        }

        public static void PublishToFeedImageProgressErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to feed image progress channel error: " + error.Message);
        }

        public static void PublishToFeedImageProgressCompletedMessage(string message)
        {
            Log.Debug("Publish to feed image progress completed channel success: " + message);
        }

        public static void PublishToFeedImageProgressCompletedErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to feed image progress completed channel error: " + error.Message);
        }
    }
}