﻿using log4net;
using PubNubMessaging.Core;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Utilities
{
    public class PushNotification
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);
        private static Pubnub pubnub = PubnubConfig.ConfigPubnub();

        [DataContract]
        public class AppleNotification
        {
            [DataMember]
            public Aps aps { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string FeedId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string CommentId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ReplyId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ChallengeId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string AchievementId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string PulseId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string AppraisalId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ReportId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int NotificationType { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int NotificationSubType { get; set; }

            [DataMember]
            public string EventName { get; set; }
        }

        [DataContract]
        public class Aps
        {
            [DataMember]
            public string alert { get; set; }

            [DataMember]
            public int badge { get; set; }

            [DataMember]
            public string sound { get; set; }
        }

        [DataContract]
        public class GoogleNotification
        {
            [DataMember]
            public Data data { get; set; }
        }

        [DataContract]
        public class Data
        {
            [DataMember]
            public string EventName { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string FeedId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string CommentId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ReplyId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ChallengeId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string AchievementId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string PulseId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string AppraisalId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ReportId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int NotificationType { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int NotificationSubType { get; set; }

            [DataMember]
            public string Alert { get; set; }

            [DataMember]
            public int Badge { get; set; }

            [DataMember]
            public string Sound { get; set; }
        }

        [DataContract]
        public class Notify
        {
            [DataMember]
            public string EventName { get; set; }

            [DataMember]
            public string NotificationText { get; set; }

            [DataMember]
            public int NumberOfNewNotification { get; set; }

            [DataMember]
            public int NotificationType { get; set; }

            [DataMember]
            public int NotificationSubType { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string FeedId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string CommentId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ReplyId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ChallengeId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string AchievementId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string PulseId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string AppraisalId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ReportId { get; set; }
        }

        public static void PushFeedNotification(string targetedUserId, string notificationText, int numberOfNewNotification, int notificationType, int notificationSubType, string feedId, string commentId = null, string replyId = null)
        {
            if (!string.IsNullOrEmpty(notificationText))
            {
                string targetedClientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, targetedUserId);

                pubnub.EnableDebugForPushPublish = true;

                Dictionary<string, object> notificationDict = GeneratedPushNotificationDict(PubnubEvent.Notification, numberOfNewNotification, notificationText, notificationType, notificationSubType, feedId, commentId, replyId);

                pubnub.Publish<string>(targetedClientChannel, notificationDict, PubnubCallback.PushNotificationMessage, PubnubCallback.PushNotificationErrorMessage);
            }
        }

        public static void PushChallengeJoinNotification(string targetedUserId, string notificationText, int numberOfNewNotification, int notificationType, int notificationSubType, string challengeId)
        {
            if (!string.IsNullOrEmpty(notificationText))
            {
                string targetedClientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, targetedUserId);

                pubnub.EnableDebugForPushPublish = true;

                Dictionary<string, object> notificationDict = GeneratedPushNotificationDict(PubnubEvent.ChallengePrepareToJoin, numberOfNewNotification, notificationText, notificationType, notificationSubType, null, null, null, challengeId);

                pubnub.Publish<string>(targetedClientChannel, notificationDict, PubnubCallback.PushNotificationMessage, PubnubCallback.PushNotificationErrorMessage);
            }
        }

        public static void PushChallengeCompletedNotification(string targetedUserId, string notificationText, int numberOfNewNotification, int notificationType, int notificationSubType, string challengeId)
        {
            if (!string.IsNullOrEmpty(notificationText))
            {
                string targetedClientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, targetedUserId);

                pubnub.EnableDebugForPushPublish = true;

                Dictionary<string, object> notificationDict = GeneratedPushNotificationDict(PubnubEvent.Notification, numberOfNewNotification, notificationText, notificationType, notificationSubType, null, null, null, challengeId);

                pubnub.Publish<string>(targetedClientChannel, notificationDict, PubnubCallback.PushNotificationMessage, PubnubCallback.PushNotificationErrorMessage);
            }
        }

        public static void PushAchievementUnlockedNotification(string targetedUserId, string notificationText, int numberOfNewNotification, int notificationType, int notificationSubType)
        {
            if (!string.IsNullOrEmpty(notificationText))
            {
                string targetedClientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, targetedUserId);

                pubnub.EnableDebugForPushPublish = true;

                Dictionary<string, object> notificationDict = GeneratedPushNotificationDict(PubnubEvent.Notification, numberOfNewNotification, notificationText, notificationType, notificationSubType, null, null, null, null);

                pubnub.Publish<string>(targetedClientChannel, notificationDict, PubnubCallback.PushNotificationMessage, PubnubCallback.PushNotificationErrorMessage);
            }
        }

        public static void PushAnnouncementNotification(string targetedUserId, string notificationText, int numberOfNewNotification, int notificationType, int notificationSubType, string pulseId)
        {
            if (!string.IsNullOrEmpty(notificationText))
            {
                string targetedClientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, targetedUserId);

                pubnub.EnableDebugForPushPublish = true;

                Dictionary<string, object> notificationDict = GeneratedPushNotificationDict(PubnubEvent.Notification, numberOfNewNotification, notificationText, notificationType, notificationSubType, null, null, null, null, null, pulseId);

                pubnub.Publish<string>(targetedClientChannel, notificationDict, PubnubCallback.PushNotificationMessage, PubnubCallback.PushNotificationErrorMessage);
            }
        }

        public static void Push360Notification(string targetedUserId, string notificationText, int numberOfNewNotification, int notificationType, int notificationSubType, string pulseId, string appraisalId)
        {
            if (!string.IsNullOrEmpty(notificationText))
            {
                string targetedClientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, targetedUserId);

                pubnub.EnableDebugForPushPublish = true;

                Dictionary<string, object> notificationDict = GeneratedPushNotificationDict(PubnubEvent.Notification, numberOfNewNotification, notificationText, notificationType, notificationSubType, null, null, null, null, null, pulseId, appraisalId);

                pubnub.Publish<string>(targetedClientChannel, notificationDict, PubnubCallback.PushNotificationMessage, PubnubCallback.PushNotificationErrorMessage);
            }
        }

        public static void PushReportNotification(string targetedUserId, string notificationText, int numberOfNewNotification, int notificationType, int notificationSubType, string reportId)
        {
            if (!string.IsNullOrEmpty(notificationText))
            {
                string targetedClientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, targetedUserId);

                pubnub.EnableDebugForPushPublish = true;

                Dictionary<string, object> notificationDict = GeneratedPushNotificationDict(PubnubEvent.Notification, numberOfNewNotification, notificationText, notificationType, notificationSubType, null, null, null, null, null, null, null, reportId);

                pubnub.Publish<string>(targetedClientChannel, notificationDict, PubnubCallback.PushNotificationMessage, PubnubCallback.PushNotificationErrorMessage);
            }
        }

        private static Dictionary<string, object> GeneratedPushNotificationDict(string eventName, int numberOfNewNotification, string notificationText, int notificationType, int notificationSubType, string feedId = null, string commentId = null, string replyId = null, string challengeId = null, string achievementId = null, string pulseId = null, string appraisalId = null, string reportId = null)
        {
            AppleNotification appleNotification = GenerateApnsNotification(eventName, numberOfNewNotification, notificationText, notificationType, notificationSubType, feedId, commentId, replyId, challengeId, achievementId, pulseId, appraisalId, reportId);

            GoogleNotification googleNotification = GenerateGcmNotification(eventName, numberOfNewNotification, notificationText, notificationType, notificationSubType, feedId, commentId, replyId, challengeId, achievementId, pulseId, appraisalId, reportId);

            Dictionary<string, object> notificationDict = new Dictionary<string, object>();
            notificationDict.Add("pn_apns", appleNotification);
            notificationDict.Add("pn_gcm", googleNotification);

            Notify notify = new Notify
            {
                EventName = PubnubEvent.Notification,
                NotificationText = notificationText,
                NumberOfNewNotification = numberOfNewNotification,
                FeedId = feedId,
                CommentId = commentId,
                ReplyId = replyId,
                ChallengeId = challengeId,
                AchievementId = achievementId,
                PulseId = pulseId,
                AppraisalId = appraisalId,
                ReportId = reportId,
                NotificationType = notificationType,
                NotificationSubType = notificationSubType
            };

            notificationDict.Add("event", notify);

            return notificationDict;
        }

        private static AppleNotification GenerateApnsNotification(string eventName, int numberOfNewNotification, string notificationText, int notificationType, int notificationSubType, string feedId = null, string commentId = null, string replyId = null, string challengeId = null, string achievementId = null, string pulseId = null, string appraisalId = null, string reportId = null)
        {
            string alertMessage = WebConfigurationManager.AppSettings["notification_alert"];
            string sound = WebConfigurationManager.AppSettings["notification_alert_sound"];
            int badge = numberOfNewNotification;

            AppleNotification appleNotification = new AppleNotification();
            Aps aps = new Aps();

            aps.alert = notificationText;
            aps.badge = badge;
            aps.sound = sound;

            appleNotification.aps = aps;
            appleNotification.EventName = eventName;
            appleNotification.NotificationType = notificationType;
            appleNotification.NotificationSubType = notificationSubType;
            appleNotification.FeedId = feedId;
            appleNotification.CommentId = commentId;
            appleNotification.ReplyId = replyId;
            appleNotification.ChallengeId = challengeId;
            appleNotification.AchievementId = achievementId;
            appleNotification.PulseId = pulseId;
            appleNotification.AppraisalId = appraisalId;
            appleNotification.ReportId = reportId;

            return appleNotification;
        }

        private static GoogleNotification GenerateGcmNotification(string eventName, int numberOfNewNotification, string notificationText, int notificationType, int notificationSubType, string feedId = null, string commentId = null, string replyId = null, string challengeId = null, string achievementId = null, string pulseId = null, string appraisalId = null, string reportId = null)
        {
            string alertMessage = WebConfigurationManager.AppSettings["notification_alert"];
            string sound = WebConfigurationManager.AppSettings["notification_alert_sound"];
            int badge = numberOfNewNotification;

            GoogleNotification googleNotification = new GoogleNotification();

            Data data = new Data();

            data.Alert = notificationText;
            data.Badge = badge;
            data.Sound = sound;
            data.EventName = eventName;
            data.FeedId = feedId;
            data.CommentId = commentId;
            data.ReplyId = replyId;
            data.ChallengeId = challengeId;
            data.AchievementId = achievementId;
            data.PulseId = pulseId;
            data.AppraisalId = appraisalId;
            data.ReportId = reportId;
            data.NotificationType = notificationType;
            data.NotificationSubType = notificationSubType;

            googleNotification.data = data;

            return googleNotification;
        }
    }
}