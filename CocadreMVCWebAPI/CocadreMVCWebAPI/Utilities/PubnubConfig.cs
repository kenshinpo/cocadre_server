﻿using PubNubMessaging.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Utilities
{
    public class PubnubConfig
    {
        private static string pubnubPublishKey = WebConfigurationManager.AppSettings["pubnub_publish_key"].ToString();
        private static string pubnubSubscribeKey = WebConfigurationManager.AppSettings["pubnub_subscribe_key"].ToString();
        private static string pubnubSecretKey = WebConfigurationManager.AppSettings["pubnub_secret_key"].ToString();

        public static Pubnub ConfigPubnub()
        {
            return new Pubnub(pubnubPublishKey, pubnubSubscribeKey, pubnubSecretKey);
        }
    }
}