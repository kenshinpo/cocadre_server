﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreMVCWebAPI.Utilities
{
    [DataContract]
    public class ErrorResponse
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public int ErrorCode { get; set; }

        [DataMember]
        public String ErrorMessage { get; set; }
    }
}