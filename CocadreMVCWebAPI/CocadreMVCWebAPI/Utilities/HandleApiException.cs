﻿using CocadreMVCWebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Http.Filters;
using System.Web.Http.Routing;

namespace CocadreMVCWebAPI.Utilities
{
    public class HandleApiException : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            var request = context.ActionContext.Request;

            var response = new
            {
                Success = false,
                ErrorCode = CassandraService.GlobalResources.ErrorCode.UserInvalid,
                ErrorMessage = CassandraService.GlobalResources.ErrorMessage.UpdateCocadre
            };

            context.Response = request.CreateResponse(HttpStatusCode.BadRequest, response);
        }
    }
}