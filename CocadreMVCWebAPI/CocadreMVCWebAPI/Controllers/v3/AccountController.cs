﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using CocadreMVCWebAPI.Models.v3;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v3
{
    public class AccountController : ApiController
    {
        private Account accountModel = new Account();

        [AcceptVerbs("GET", "POST")]
        public UserCreateResponse CreateAdmin(Account.CreateAdminRequest request)
        {
            return accountModel.CreateAdmin(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserSelectWithCompanyResponse GetAccount(Account.AccountRequest request)
        {
            return accountModel.GetAccount(request);
        }

        [AcceptVerbs("GET", "POST")]
        public Account.GetCognitoTokenResponse GetCognitoToken(Account.AccountRequest request)
        {
            return accountModel.GetCognitoToken(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserSelectAllByDepartmentResponse GetColleagues(Account.AccountRequest request)
        {
            return accountModel.GetColleagues(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AuthenticationSelectUserResponse GoLoginAccount(Account.LoginRequest request)
        {
            return accountModel.GoLoginAccount(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserUpdateLoginResponse GoLogoutAccount(Account.AccountRequest request)
        {
            return accountModel.GoLogoutAccount(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserUpdateDeviceTokenResponse UpdateDeviceToken(Account.UpdateDeviceTokenRequest request)
        {
            return accountModel.UpdateDeviceToken(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AuthenticationUpdateResponse UpdatePassword(Account.UpdatePasswordRequest request)
        {
            return accountModel.UpdatePassword(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AuthenticationUpdateResponse ForgetPassword(Account.UpdatePasswordRequest request)
        {
            return accountModel.ForgetPassword(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserUploadProfileImageResponse UploadProfileImage(Account.UploadProfileImageRequest request)
        {
            return accountModel.UploadProfileImage(request);
        }
    }
}
