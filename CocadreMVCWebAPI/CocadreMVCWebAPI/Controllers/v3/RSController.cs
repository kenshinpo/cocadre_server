﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v3;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v3
{
    public class RSController : ApiController
    {
        private ResponsiveSurvey rsModel = new ResponsiveSurvey();

        [AcceptVerbs("GET", "POST")]
        public RSTopicSelectResponse GetRSTopic(ResponsiveSurvey.TopicRequest request)
        {
            return rsModel.GetRSTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public RSCardAnswerResponse AnswerCard(ResponsiveSurvey.AnswerCardRequest request)
        {
            return rsModel.AnswerCard(request);
        }
    }
}
