﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using CocadreMVCWebAPI.Models.v4;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v4
{
    public class AccountController : ApiController
    {
        private Account accountModel = new Account();
        private Aws awsModel = new Aws();

        [AcceptVerbs("GET", "POST")]
        public UserSelectWithCompanyResponse GetAccount(Account.AccountRequest request)
        {
            return accountModel.GetAccount(request);
        }

        [AcceptVerbs("GET", "POST")]
        public Aws.GetCognitoTokenResponse GetCognitoToken(Aws.AwsRequest request)
        {
            return awsModel.GetCognitoToken(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserSelectAllByDepartmentResponse GetColleagues(Account.AccountRequest request)
        {
            return accountModel.GetColleagues(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AuthenticationSelectUserResponse GoLoginAccount(Account.LoginRequest request)
        {
            return accountModel.GoLoginAccount(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AuthenticationSelectUserByCompanyResponse GoLoginCompany(Account.LoginRequest request)
        {
            return accountModel.GoLoginCompany(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AuthenticationSelectCompanyResponse GoSwitchCompany(Account.AccountRequest request)
        {
            return accountModel.GoSwitchCompany(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserUpdateLoginResponse GoLogoutAccount(Account.AccountRequest request)
        {
            return accountModel.GoLogoutAccount(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserUpdateDeviceTokenResponse UpdateDeviceToken(Account.UpdateDeviceTokenRequest request)
        {
            return accountModel.UpdateDeviceToken(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AuthenticationUpdateResponse UpdatePassword(Account.UpdatePasswordRequest request)
        {
            return accountModel.UpdatePassword(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AuthenticationUpdateResponse ForgetPassword(Account.UpdatePasswordRequest request)
        {
            return accountModel.ForgetPassword(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserUploadProfileImageResponse UploadProfileImage(Account.UploadProfileImageRequest request)
        {
            return accountModel.UploadProfileImage(request);
        }
    }
}
