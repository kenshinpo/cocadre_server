﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v4
{
    public class EventController : ApiController
    {
        private Event eventModel = new Event();

        //[AcceptVerbs("GET", "POST")]
        //public EventCreateResponse CreateEvent()
        //{
        //    return eventModel2.CreateEvent();
        //}

        //[AcceptVerbs("GET", "POST")]
        //public EventSelectAllResponse SelectAllEvents()
        //{
        //    return eventModel2.GetAllEvents();
        //}

        [AcceptVerbs("GET", "POST")]
        public EventSelectResponse GetEvent(Event.EventRequest request)
        {
            return eventModel.GetEvent(request);
        }
    }
}
