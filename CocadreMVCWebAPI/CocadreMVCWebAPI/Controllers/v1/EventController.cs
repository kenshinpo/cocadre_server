﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v1
{
    public class EventController : ApiController
    {
        #region v1
        private Event eventModel = new Event();

        //[AcceptVerbs("GET", "POST")]
        //public EventCreateResponse CreateEvent()
        //{
        //    return eventModel.CreateEvent();
        //}

        //[AcceptVerbs("GET", "POST")]
        //public EventSelectAllResponse SelectAllEvents()
        //{
        //    return eventModel.GetAllEvents();
        //}

        [AcceptVerbs("GET", "POST")]
        public EventSelectResponse GetEvent(Event.EventRequest request)
        {
            return eventModel.GetEvent(request);
        }
        #endregion
    }
}
