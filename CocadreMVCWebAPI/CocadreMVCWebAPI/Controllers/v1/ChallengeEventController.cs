﻿using CocadreMVCWebAPI.Models.v1;
using log4net;
using Resources;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v1
{
    public class ChallengeEventController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ChallengeEvent eventModel = new ChallengeEvent();

        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage SetupChallengeEvent(ChallengeEvent.ChallengeEventRequest request)
        {
            string eventName = request.EventName;
            HttpResponseMessage message = new HttpResponseMessage();

            if (eventName == PubnubEvent.ChallengeCreate)
            {
                Log.Debug("Create challenge");
                Log.Info("Processing create challenge event");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.CreateChallenge(request));
            }
            else if (eventName == PubnubEvent.ChallengeFetchQuestions)
            {
                Log.Debug("Prepare to fetch questions");
                Log.Info("Processing fetched questions event");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.FetchQuestions(request));
            }
            else if (eventName == PubnubEvent.ChallengeIsReady)
            {
                Log.Debug("Ready for challenge");
                Log.Info("Processing challenge ready event");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.PlayerIsReady(request));
            }
            else if (eventName == PubnubEvent.ChallengeQuestionAnswered)
            {
                Log.Debug("Question answered");
                Log.Info("Processing question answered event");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.AnswerQuestion(request));
            }
            else if (eventName == PubnubEvent.ChallengeStartWithoutOpponent)
            {
                Log.Debug("Start game without opponent");
                Log.Info("Processing Start game without opponent event");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.ChallengeStartWithoutOpponent(request));
            }
            else if (eventName == PubnubEvent.ChallengePrepareToJoin)
            {
                Log.Debug("Prepared to join");
                Log.Info("Processing preparing to join");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.GetChallengeForOffline(request));
            }
            else if (eventName == PubnubEvent.ChallengeOfflineRoundStart)
            {
                Log.Debug("Prepared to start next round offline");
                Log.Info("Processing start next round offline");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.GetOpponentAnswer(request));
            }
            else if (eventName == PubnubEvent.ChallengeKill)
            {
                Log.Debug("Prepared to kill challenge");
                Log.Info("Processing kill challenge");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.KillChallenge(request));
            }
            else if (eventName == PubnubEvent.ChallengeCancel)
            {
                Log.Debug("Prepared to cancel challenge");
                Log.Info("Processing cancel challenge");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.CancelChallenge(request));
            }

            return message;
        }

        //[AcceptVerbs("GET", "POST")]
        //[Route("~/Quiz/PubnubChallengeEvent")]
        //[Route("~/Quiz/PubnubChallengeEvent.ashx")]
        //[Route("~/Quiz/v1.0.0/PubnubChallengeEvent.ashx")]
        //[Route("PubnubChallengeEvent")]
        //public _0._0.ChallengeEvent.CreateChallengeResponse CreateChallenge(_0._0.ChallengeEvent.CreateChallengeRequest request)
        //{
        //    return eventModel.CreateChallenge(request);
        //}

        //[AcceptVerbs("GET", "POST")]
        //[Route("~/Quiz/PubnubChallengeEvent")]
        //[Route("~/Quiz/PubnubChallengeEvent.ashx")]
        //[Route("~/Quiz/v1.0.0/PubnubChallengeEvent.ashx")]
        //[Route("PubnubChallengeEvent")]
        //public _0._0.ChallengeEvent.CreateChallengeResponse GetChallengeForOffline(_0._0.ChallengeEvent.PrepareToJoinRequest request)
        //{
        //    return eventModel.GetChallengeForOffline(request);
        //}

        //[AcceptVerbs("GET", "POST")]
        //[Route("~/Quiz/PubnubChallengeEvent")]
        //[Route("~/Quiz/PubnubChallengeEvent.ashx")]
        //[Route("~/Quiz/v1.0.0/PubnubChallengeEvent.ashx")]
        //[Route("PubnubChallengeEvent")]
        //public QuestionSelectAllResponse FetchQuestions(_0._0.ChallengeEvent.GetQuestionsRequest request)
        //{
        //    return eventModel.FetchQuestions(request);
        //}

        //[AcceptVerbs("GET", "POST")]
        //[Route("~/Quiz/PubnubChallengeEvent")]
        //[Route("~/Quiz/PubnubChallengeEvent.ashx")]
        //[Route("~/Quiz/v1.0.0/PubnubChallengeEvent.ashx")]
        //[Route("PubnubChallengeEvent")]
        //public ChallengeIsReadyResponse PlayerIsReady(_0._0.ChallengeEvent.PlayerIsReadyRequest request)
        //{
        //    return eventModel.PlayerIsReady(request);
        //}

        //[AcceptVerbs("GET", "POST")]
        //[Route("~/Quiz/PubnubChallengeEvent")]
        //[Route("~/Quiz/PubnubChallengeEvent.ashx")]
        //[Route("~/Quiz/v1.0.0/PubnubChallengeEvent.ashx")]
        //[Route("PubnubChallengeEvent")]
        //public _0._0.ChallengeEvent.CorrectAnswerResponse AnswerQuestion(_0._0.ChallengeEvent.AnswerQuestionRequest request)
        //{
        //    return eventModel.AnswerQuestion(request);
        //}

        //[AcceptVerbs("GET", "POST")]
        //[Route("~/Quiz/PubnubChallengeEvent")]
        //[Route("~/Quiz/PubnubChallengeEvent.ashx")]
        //[Route("~/Quiz/v1.0.0/PubnubChallengeEvent.ashx")]
        //[Route("PubnubChallengeEvent")]
        //public _0._0.ChallengeEvent.SinglePlayerToStartResponse ChallengeStartWithoutOpponent(_0._0.ChallengeEvent.StartGameWithoutOpponentRequest request)
        //{
        //    return eventModel.ChallengeStartWithoutOpponent(request);
        //}

        //[AcceptVerbs("GET", "POST")]
        //[Route("~/Quiz/PubnubChallengeEvent")]
        //[Route("~/Quiz/PubnubChallengeEvent.ashx")]
        //[Route("~/Quiz/v1.0.0/PubnubChallengeEvent.ashx")]
        //[Route("PubnubChallengeEvent")]
        //public ChallengeOfflineSelectOpponentAnswerResponse GetOpponentAnswer(_0._0.ChallengeEvent.GetOpponentAnswerRequest request)
        //{
        //    return eventModel.GetOpponentAnswer(request);
        //}

        //[AcceptVerbs("GET", "POST")]
        //[Route("~/Quiz/PubnubChallengeEvent")]
        //[Route("~/Quiz/PubnubChallengeEvent.ashx")]
        //[Route("~/Quiz/v1.0.0/PubnubChallengeEvent.ashx")]
        //[Route("PubnubChallengeEvent")]
        //public ChallengeInvalidateResponse KillChallenge(_0._0.ChallengeEvent.InvalidateChallengeRequest request)
        //{
        //    return eventModel.KillChallenge(request);
        //}

        //[AcceptVerbs("GET", "POST")]
        //[Route("~/Quiz/PubnubChallengeEvent")]
        //[Route("~/Quiz/PubnubChallengeEvent.ashx")]
        //[Route("~/Quiz/v1.0.0/PubnubChallengeEvent.ashx")]
        //[Route("PubnubChallengeEvent")]
        //public ChallengeInvalidateResponse CancelChallenge(_0._0.ChallengeEvent.InvalidateChallengeRequest request)
        //{
        //    return eventModel.CancelChallenge(request);
        //}
    }
}
