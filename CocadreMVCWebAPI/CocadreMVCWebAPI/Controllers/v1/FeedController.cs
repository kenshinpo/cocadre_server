﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v1;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v1
{
    public class FeedController : ApiController
    {
        private Feed feedModel = new Feed();

        [AcceptVerbs("GET", "POST")]
        public FeedSelectResponse GetCompanyFeed(Feed.GetCompanyFeedRequest request)
        {
            return feedModel.GetCompanyFeed(request);
        }

        [AcceptVerbs("GET", "POST")]
        public FeedSelectResponse GetPersonnelFeed(Feed.GetPersonnelFeedRequest request)
        {
            return feedModel.GetPersonnelFeed(request);
        }

        [AcceptVerbs("GET", "POST")]
        public CommentSelectResponse GetComment(Feed.FeedRequest request)
        {
            return feedModel.GetComment(request);
        }

        [AcceptVerbs("GET", "POST")]
        public ReplySelectResponse GetReply(Feed.FeedRequest request)
        {
            return feedModel.GetReply(request);
        }

        [AcceptVerbs("GET", "POST")]
        public FeedAuthenticationResponse NewPostAuthentication(Feed.FeedRequest request)
        {
            return feedModel.NewPostAuthentication(request);
        }

        [AcceptVerbs("GET", "POST")]
        public FeedCreateResponse PostFeed(Feed.PostFeedRequest request)
        {
            return feedModel.PostFeed(request);
        }

        [AcceptVerbs("GET", "POST")]
        public CommentCreateResponse PostComment(Feed.PostCommentRequest request)
        {
            return feedModel.PostComment(request);
        }

        [AcceptVerbs("GET", "POST")]
        public ReplyCreateResponse PostReply(Feed.PostReplyRequest request)
        {
            return feedModel.PostReply(request);
        }

        [AcceptVerbs("GET", "POST")]
        public FeedDeleteResponse DeleteFeed(Feed.FeedRequest request)
        {
            return feedModel.DeleteFeed(request);
        }

        [AcceptVerbs("GET", "POST")]
        public FeedDeleteResponse DeleteComment(Feed.FeedRequest request)
        {
            return feedModel.DeleteComment(request);
        }

        [AcceptVerbs("GET", "POST")]
        public FeedDeleteResponse DeleteReply(Feed.FeedRequest request)
        {
            return feedModel.DeleteReply(request);
        }

        [AcceptVerbs("GET", "POST")]
        public DashboardReportResponse ReportFeed(Feed.ReportFeedRequest request)
        {
            return feedModel.ReportFeed(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PointUpdateResponse UpdateVote(Feed.VoteFeedRequest request)
        {
            return feedModel.UpdateVote(request);
        }
    }
}
