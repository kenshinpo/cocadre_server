﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v1;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v1
{
    public class BrainController : ApiController
    {
        private Brain brainModel = new Brain();

        [AcceptVerbs("GET", "POST")]
        public BrainSelectResponse GetBrain(Brain.BrainRequest request)
        {
            return brainModel.GetBrain(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserSelectStatsResponse GetUserChallengeStats(Brain.BrainRequest request)
        {
            return brainModel.GetUserChallengeStats(request);
        }
    }
}
