﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v5;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v5
{
    public class RSController : ApiController
    {
        private ResponsiveSurvey rsModel = new ResponsiveSurvey();

        [AcceptVerbs("GET", "POST")]
        public RSTopicSelectResponse GetRSTopic(ResponsiveSurvey.TopicRequest request)
        {
            return rsModel.GetRSTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public RSCardAnswerResponse AnswerCard(ResponsiveSurvey.AnswerCardRequest request)
        {
            return rsModel.AnswerCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        public RSUpdateActivityResponse UpdateBounce(ResponsiveSurvey.TopicRequest request)
        {
            return rsModel.UpdateBounce(request);
        }

        [AcceptVerbs("GET", "POST")]
        public RSUpdateActivityResponse UpdateCompletion(ResponsiveSurvey.TopicRequest request)
        {
            return rsModel.UpdateCompletion(request);
        }

        [AcceptVerbs("GET", "POST")]
        public RSTopicUpdateFeedbackResponse CreateFeedback(ResponsiveSurvey.TopicRequest request)
        {
            return rsModel.CreateFeedback(request);
        }
    }
}
