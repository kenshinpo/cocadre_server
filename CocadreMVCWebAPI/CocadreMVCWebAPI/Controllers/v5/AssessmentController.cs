﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using CocadreMVCWebAPI.Models.v5;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v5
{
    public class AssessmentController : ApiController
    {
        private Assessment assessModel = new Assessment();

        [AcceptVerbs("GET", "POST")]
        public AssessmentSelectResponse SelectAssessmentByUser(Assessment.AssessmentRequest request)
        {
            return assessModel.SelectAssessmentByUser(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AssessmentSelectResponse TakeAssessment(Assessment.AssessmentRequest request)
        {
            return assessModel.TakeAssessment(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AssessmentSelectResultByUserResponse SelectAssessmentResultByUser(Assessment.AssessmentRequest request)
        {
            return assessModel.SelectAssessmentResultByUser(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AssessmentAnswerCardResponse AnswerAssessmentCard(Assessment.AssessmentRequest request)
        {
            return assessModel.AnswerAssessmentCard(request);
        }
    }
}
