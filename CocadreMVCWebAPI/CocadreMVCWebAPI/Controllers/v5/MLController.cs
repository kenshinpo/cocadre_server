﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v5;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v5
{
    public class MLController : ApiController
    {
        private MobileLearning mlModel = new MobileLearning();

        [AcceptVerbs("GET", "POST")]
        public MLTopicSelectResponse GetMLTopic(MobileLearning.Request request)
        {
            return mlModel.GetMLTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLCardAnswerResponse AnswerCard(MobileLearning.Request request)
        {
            return mlModel.AnswerCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLSelectUserResultResponse SelectResult(MobileLearning.Request request)
        {
            return mlModel.SelectResult(request);
        }


        [AcceptVerbs("GET", "POST")]
        public MLEducationDetailResponse GetMLEducation(MobileLearning.Request request)
        {
            return mlModel.GetMLEducation(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLEducationCardAnswerResponse AnswerEducationCard(MobileLearning.Request request)
        {
            return mlModel.AnswerEducationCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLEducationCompletedResponse CompletedEducation(MobileLearning.Request request)
        {
            return mlModel.CompletedEducation(request);
        }
    }
}
