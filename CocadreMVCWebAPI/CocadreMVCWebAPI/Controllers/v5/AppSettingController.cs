﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v5;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v5
{
    public class AppSettingController : ApiController
    {
        private AppSetting appSettingModel = new AppSetting();

        [AcceptVerbs("GET", "POST")]
        public SelectAppSettingResponse GetAppSetting(AppSetting.AppSettingRequest request)
        {
            return appSettingModel.GetAppSetting(request);
        }

        [AcceptVerbs("GET", "POST")]
        public SelectAppNotificationResponse GetNotificationSetting(AppSetting.AppSettingRequest request)
        {
            return appSettingModel.GetNotificationSetting(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UpdateAppSettingResponse UpdateSoundEffect(AppSetting.UpdateAppSettingRequest request)
        {
            return appSettingModel.UpdateAppSoundEffect(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UpdateAppSettingResponse UpdateGameMusic(AppSetting.UpdateAppSettingRequest request)
        {
            return appSettingModel.UpdateInGameMusic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UpdateAppSettingResponse UpdateNotification(AppSetting.UpdateAppSettingRequest request)
        {
            return appSettingModel.UpdateNotification(request);
        }

        [AcceptVerbs("GET", "POST")]
        public CountryListResponse GetCountriesForPhone()
        {
            return appSettingModel.GetCountriesForPhone();
        }
    }
}
