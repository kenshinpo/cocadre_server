﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v5;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v5
{
    public class EventController : ApiController
    {
        private Event eventModel = new Event();

        //[AcceptVerbs("GET", "POST")]
        //public EventCreateResponse CreateEvent()
        //{
        //    return eventModel2.CreateEvent();
        //}

        //[AcceptVerbs("GET", "POST")]
        //public EventSelectAllResponse SelectAllEvents()
        //{
        //    return eventModel2.GetAllEvents();
        //}

        [AcceptVerbs("GET", "POST")]
        public EventSelectResponse GetEvent(Event.EventRequest request)
        {
            return eventModel.GetEvent(request);
        }
    }
}
