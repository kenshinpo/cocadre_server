﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v5;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v5
{
    public class StatsController : ApiController
    {
        private Stats statsModel = new Stats();

        [AcceptVerbs("GET", "POST")]
        public AnalyticsSelectAllGameHistoriesResponse GetGameHistories(Stats.StatsRequest request)
        {
            return statsModel.GetGameHistories(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticsSelectGameResultResponse GetGameResult(Stats.StatsRequest request)
        {
            return statsModel.GetGameResult(request);
        }

    }
}
