﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v6
{
    public class AchievementController : ApiController
    {
        private Achievement achievementModel = new Achievement();

        [AcceptVerbs("GET", "POST")]
        public GamificationSelectAllAchievementResponse GetAchievements(Achievement.AchievementRequest request)
        {
            return achievementModel.GetAchievements(request);
        }

        [AcceptVerbs("GET", "POST")]
        public GamificationSelectAchievementResponse GetAchievement(Achievement.AchievementRequest request)
        {
            return achievementModel.GetAchievement(request);
        }

        [AcceptVerbs("GET", "POST")]
        public GamificationUpdateBadgeResponse SetBadge(Achievement.AchievementRequest request)
        {
            return achievementModel.SetBadge(request);
        }

        [AcceptVerbs("GET", "POST")]
        public GamificationUpdateBadgeResponse RemoveBadge(Achievement.AchievementRequest request)
        {
            return achievementModel.RemoveBadge(request);
        }


        [AcceptVerbs("GET", "POST")]
        public GamificationUpdateBadgeSeenResponse UpdateSeenBadge(Achievement.AchievementRequest request)
        {
            return achievementModel.UpdateSeenBadge(request);
        }
    }
}
