﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v6
{
    public class AppraisalController : ApiController
    {
        private Appraisal model = new Appraisal();

        [AcceptVerbs("GET", "POST")]
        public AppraisalSelectAllTemplatesResponse GetTemplates(Appraisal.AppraisalRequest request)
        {
            return model.GetTemplates(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalSelectTemplateResponse GetTemplateCards(Appraisal.AppraisalRequest request)
        {
            return model.GetTemplateCards(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalCategorySelectAllResponse GetCreatedCategoriesByTemplate(Appraisal.AppraisalRequest request)
        {
            return model.GetCreatedCategoriesForTemplate(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalCardCreateTemplateResponse CreateCard(Appraisal.AppraisalRequest request)
        {
            return model.CreateCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalCategorySelectResponse GetCreatedCardsForCategory(Appraisal.AppraisalRequest request)
        {
            return model.SelectCreatedCardsByCategory(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalCreateResponse CreateAppraisal(Appraisal.AppraisalRequest request)
        {
            return model.CreateAppraisal(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalSelectResponse GetAppraisal(Appraisal.AppraisalRequest request)
        {
            return model.SelectAppraisal(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalSelectResponse GetCreatedAppraisal(Appraisal.AppraisalRequest request)
        {
            return model.SelectCreatedAppraisal(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalSelectAllResponse GetCreatedAppraisals(Appraisal.AppraisalRequest request)
        {
            return model.SelectCreatedAppraisals(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalAnswerCardResponse AnswerCard(Appraisal.AppraisalRequest request)
        {
            return model.AnswerCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalSelectResponse GetResult(Appraisal.AppraisalRequest request)
        {
            return model.SelectResult(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalUpdateCategoryResponse UpdateCategory(Appraisal.AppraisalRequest request)
        {
            return model.UpdateCategory(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalUpdateCardResponse UpdateCard(Appraisal.AppraisalRequest request)
        {
            return model.UpdateCardByCategory(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalUpdateCardResponse RemoveCard(Appraisal.AppraisalRequest request)
        {
            return model.RemoveCardByCategory(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalUpdateResponse UpdateAppraisal(Appraisal.AppraisalRequest request)
        {
            return model.UpdateAppraisal(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalExportResponse ExportAppraisal(Appraisal.AppraisalRequest request)
        {
            return model.ExportAppraisal(request);
        }
    }
}
