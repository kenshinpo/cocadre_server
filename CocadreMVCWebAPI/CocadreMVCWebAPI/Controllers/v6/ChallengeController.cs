﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v6
{
    public class ChallengeController : ApiController
    {
        private Challenge challengeModel = new Challenge();

        [AcceptVerbs("GET", "POST")]
        public ChallengeSelectStatsResponse GetChallengeStats(Challenge.GetChallengeStatsRequest request)
        {
            return challengeModel.GetChallengeStats(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserSelectAllBasicResponse GetOpponentsForTopic(Challenge.GetOpponentsForTopicRequest request)
        {
            return challengeModel.GetOpponentsForTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserSelectBasicResponse GetRandomOpponentForTopic(Challenge.GetOpponentsForTopicRequest request)
        {
            return challengeModel.GetRandomOpponentForTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public TopicSelectAllBasicResponse GetTopicsForOpponent(Challenge.GetTopicsForOpponentRequest request)
        {
            return challengeModel.GetTopicsForOpponent(request);
        }

        [AcceptVerbs("GET", "POST")]
        public CategorySelectAllWithTopicResponse GetTopics(Challenge.ChallengeRequest request)
        {
            return challengeModel.GetTopics(request);
        }

        [AcceptVerbs("GET", "POST")]
        public ChallengeSelectIncompleteResponse GetIncompletedChallenges(Challenge.ChallengeRequest request)
        {
            return challengeModel.GetIncompletedChallenges(request);
        }
    }
}
