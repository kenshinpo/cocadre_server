﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v6
{
    public class NotificationController : ApiController
    {
        private Notification notificationModel = new Notification();

        [AcceptVerbs("GET", "POST")]
        public NotificationSelectAllResponse GetNotification(Notification.GetNotificationRequest request)
        {
            return notificationModel.GetNotification(request);
        }

        [AcceptVerbs("GET", "POST")]
        public NotificationSelectNumberResponse GetNotificationNumber(Notification.NotificationRequest request)
        {
            return notificationModel.GetNotificationNumber(request);
        }

        [AcceptVerbs("GET", "POST")]
        public NotificationUpdateSeenResponse UpdateSeenNotification(Notification.UpdateNotificationSeenRequest request)
        {
            return notificationModel.UpdateSeenNotification(request);
        }
    }
}
