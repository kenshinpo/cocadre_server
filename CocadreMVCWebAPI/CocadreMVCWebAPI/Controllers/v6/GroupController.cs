﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v6
{
    public class GroupController : ApiController
    {
        private Group groupModel = new Group();

        [AcceptVerbs("GET", "POST")]
        public GroupCreateResponse CreateGroup(Group.GroupRequest request)
        {
            return groupModel.CreateGroup(request);
        }

        [AcceptVerbs("GET", "POST")]
        public GroupSelectResponse SelectGroup(Group.GroupRequest request)
        {
            return groupModel.SelectGroup(request);
        }

        [AcceptVerbs("GET", "POST")]
        public GroupSelectAllResponse SelectAllGroupsByUser(Group.GroupRequest request)
        {
            return groupModel.SelectAllGroupsByUser(request);
        }

        [AcceptVerbs("GET", "POST")]
        public GroupUpdateResponse DeleteGroup(Group.GroupRequest request)
        {
            return groupModel.DeleteGroup(request);
        }

        [AcceptVerbs("GET", "POST")]
        public GroupUpdateResponse UpdateGroup(Group.GroupRequest request)
        {
            return groupModel.UpdateGroup(request);
        }
    }
}
