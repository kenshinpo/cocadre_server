﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v6
{
    public class BrainController : ApiController
    {
        #region v6
        private Brain brainModel = new Brain();

        [AcceptVerbs("GET", "POST")]
        public BrainSelectResponse GetBrain(Brain.BrainRequest request)
        {
            return brainModel.GetBrain(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserSelectStatsResponse GetUserChallengeStats(Brain.BrainRequest request)
        {
            return brainModel.GetUserChallengeStats(request);
        }
        #endregion
    }
}
