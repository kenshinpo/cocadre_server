﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v6
{
    public class LeaderboardController : ApiController
    {
        private Leaderboard leaderboardModel = new Leaderboard();

        [AcceptVerbs("GET", "POST")]
        public AnalyticsSelectLeaderboardByCompanyResponse GetLeaderboardByCompany(Leaderboard.LeaderboardRequest request)
        {
            return leaderboardModel.GetLeaderboardByCompany(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticsSelectLeaderboardByDepartmentResponse GetLeaderboardByDepartment(Leaderboard.LeaderboardRequest request)
        {
            return leaderboardModel.GetLeaderboardByDepartment(request);
        }
    }
}
