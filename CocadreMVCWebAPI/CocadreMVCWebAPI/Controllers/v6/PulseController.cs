﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v6
{
    public class PulseController : ApiController
    {
        private Pulse pulseModel = new Pulse();

        [AcceptVerbs("GET", "POST")]
        public PulseSelectAllResponse GetPulses(Pulse.PulseRequest request)
        {
            return pulseModel.GetPulses(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseUpdateSingleStatsResponse UpdateAction(Pulse.PulseRequest request)
        {
            return pulseModel.UpdateAction(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseUpdateImpressionResponse UpdateImpression(Pulse.PulseRequest request)
        {
            return pulseModel.UpdateImpression(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectAllResponse GetInbox(Pulse.PulseRequest request)
        {
            return pulseModel.GetInbox(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectAllResponse GetTrashbin(Pulse.PulseRequest request)
        {
            return pulseModel.GetTrashbin(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseUpdateResponse MoveToTrashBin(Pulse.PulseRequest request)
        {
            return pulseModel.MoveToTrashBin(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseUpdateResponse RestoreToInbox(Pulse.PulseRequest request)
        {
            return pulseModel.RestoreToInbox(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseUpdateResponse RemoveFromTrashBin(Pulse.PulseRequest request)
        {
            return pulseModel.RemoveFromTrashBin(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseAnswerCardResponse AnswerCard(Pulse.DeckRequest request)
        {
            return pulseModel.AnswerCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectResponse GetPulse(Pulse.PulseRequest request)
        {
            return pulseModel.GetPulse(request);
        }
    }
}
