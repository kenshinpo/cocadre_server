﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.v6;
using log4net;
using Resources;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.v6
{
    public class QuizController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ChallengeEvent eventModel = new ChallengeEvent();
        private Challenge challengeModel = new Challenge();

        [AcceptVerbs("GET", "POST")]
        public ChallengeSelectStatsResponse GetChallengeStats(Challenge.GetChallengeStatsRequest request)
        {
            return challengeModel.GetChallengeStats(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserSelectAllBasicResponse GetOpponentsForTopic(Challenge.GetOpponentsForTopicRequest request)
        {
            //if (System.Runtime.Caching.MemoryCache.Default.Contains(request.CompanyId + "_" + request.TopicId))
            //{
            //    return (UserSelectAllBasicResponse)System.Runtime.Caching.MemoryCache.Default[request.CompanyId + "_" + request.TopicId];
            //}
            //else
            //{
            //    System.Runtime.Caching.MemoryCache.Default.Add(
            //        new System.Runtime.Caching.CacheItem(request.CompanyId + "_" + request.TopicId, challengeModel.GetOpponentsForTopic(request)),
            //        new System.Runtime.Caching.CacheItemPolicy() { AbsoluteExpiration = System.DateTimeOffset.Now.AddMinutes(5) });
            //    return (UserSelectAllBasicResponse)System.Runtime.Caching.MemoryCache.Default[request.CompanyId + "_" + request.TopicId];
            //}

            return challengeModel.GetOpponentsForTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public TopicSelectAllBasicResponse GetTopicsForOpponent(Challenge.GetTopicsForOpponentRequest request)
        {
            return challengeModel.GetTopicsForOpponent(request);
        }

        [AcceptVerbs("GET", "POST")]
        public CategorySelectAllWithTopicResponse GetTopics(Challenge.ChallengeRequest request)
        {
            return challengeModel.GetTopics(request);
        }

        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage PubnubChallengeEvent(ChallengeEvent.ChallengeEventRequest request)
        {
            string eventName = request.EventName;

            Log.Debug("Event name: " + eventName);

            HttpResponseMessage message = new HttpResponseMessage();

            if (eventName == PubnubEvent.ChallengeCreate)
            {
                Log.Debug("Create challenge");
                Log.Info("Processing create challenge event");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.CreateChallenge(request));
            }
            else if (eventName == PubnubEvent.ChallengeFetchQuestions)
            {
                Log.Debug("Prepare to fetch questions");
                Log.Info("Processing fetched questions event");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.FetchQuestions(request));
            }
            else if (eventName == PubnubEvent.ChallengeIsReady)
            {
                Log.Debug("Ready for challenge");
                Log.Info("Processing challenge ready event");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.PlayerIsReady(request));
            }
            else if (eventName == PubnubEvent.ChallengeQuestionAnswered)
            {
                Log.Debug("Question answered");
                Log.Info("Processing question answered event");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.AnswerQuestion(request));
            }
            else if (eventName == PubnubEvent.ChallengeStartWithoutOpponent)
            {
                Log.Debug("Start game without opponent");
                Log.Info("Processing Start game without opponent event");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.ChallengeStartWithoutOpponent(request));
            }
            else if (eventName == PubnubEvent.ChallengePrepareToJoin)
            {
                Log.Debug("Prepared to join");
                Log.Info("Processing preparing to join");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.GetChallengeForOffline(request));
            }
            else if (eventName == PubnubEvent.ChallengeOfflineRoundStart)
            {
                Log.Debug("Prepared to start next round offline");
                Log.Info("Processing start next round offline");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.GetOpponentAnswer(request));
            }
            else if (eventName == PubnubEvent.ChallengeKill)
            {
                Log.Debug("Prepared to kill challenge");
                Log.Info("Processing kill challenge");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.KillChallenge(request));
            }
            else if (eventName == PubnubEvent.ChallengeCancel)
            {
                Log.Debug("Prepared to cancel challenge");
                Log.Info("Processing cancel challenge");

                message = Request.CreateResponse(HttpStatusCode.OK, eventModel.CancelChallenge(request));
            }

            return message;
        }

    }
}
