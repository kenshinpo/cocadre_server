﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    public class PubnubWebhookController : ApiController
    {
        private PubnubActivity pubnubActivityModel = new PubnubActivity();

        [AcceptVerbs("GET", "POST")]
        public void UpdateActivity(PubnubActivity.PubnubActivityRequest request)
        {
            pubnubActivityModel.UpdatePubnubActivity(request);
        }
    }
}
