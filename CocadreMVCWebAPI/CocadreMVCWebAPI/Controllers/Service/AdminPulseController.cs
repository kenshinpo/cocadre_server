﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using CocadreMVCWebAPI.Models.v6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    [RoutePrefix("AdminPulse")]
    public class AdminPulseController : ApiController
    {
        private AdminPulse adminPulse = new AdminPulse();
        private Pulse pulseModel = new Pulse();

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminPulse/CreateBanner.ashx")]
        [Route("CreateBanner")]
        public PulseCreateResponse CreateBanner(AdminPulse.PulseRequest request)
        {
            return adminPulse.CreateBanner(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminPulse/CreateBanner.ashx")]
        [Route("CreateBanner")]
        public PulseCreateResponse CreateAnnouncement(AdminPulse.PulseRequest request)
        {
            return adminPulse.CreateAnnouncement(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminPulse/SelectAllAnnouncementBasic.ashx")]
        [Route("SelectAllFeedBasic")]
        public PulseSelectAllResponse SelectAllAnnouncementBasic(AdminPulse.PulseRequest request)
        {
            return adminPulse.SelectAllAnnouncementBasic(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminPulse/SelectAllBannerBasic.ashx")]
        [Route("SelectAllFeedBasic")]
        public PulseSelectAllResponse SelectAllBannerBasic(AdminPulse.PulseRequest request)
        {
            return adminPulse.SelectAllBannerBasic(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminPulse/SelectFullDetailBanner.ashx")]
        [Route("SelectAllFeedBasic")]
        public PulseSelectResponse SelectFullDetailBanner(AdminPulse.PulseRequest request)
        {
            return adminPulse.SelectFullDetailBanner(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminPulse/SelectFullDetailAnnouncement.ashx")]
        [Route("SelectAllFeedBasic")]
        public PulseSelectResponse SelectFullDetailAnnouncement(AdminPulse.PulseRequest request)
        {
            return adminPulse.SelectFullDetailAnnouncement(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminPulse/SelectAllByUser.ashx")]
        [Route("SelectAllByUser")]
        public PulseSelectAllResponse SelectAllByUser(AdminPulse.PulseRequest request)
        {
            return adminPulse.SelectAllByUser(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectSingleAnalyticResponse SelectFeedAnalytic(AdminPulse.PulseRequest request)
        {
            return adminPulse.SelectFeedAnalytic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseUpdateSingleStatsResponse UpdateAction(Pulse.PulseRequest request)
        {
            return pulseModel.UpdateAction(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseUpdateImpressionResponse UpdateImpression(Pulse.PulseRequest request)
        {
            return pulseModel.UpdateImpression(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectAllResponse GetInbox(Pulse.PulseRequest request)
        {
            return pulseModel.GetInbox(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectAllResponse GetTrashbin(Pulse.PulseRequest request)
        {
            return pulseModel.GetTrashbin(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseDeckCreateResponse CreateDeck(AdminPulse.DeckRequest request)
        {
            return adminPulse.CreateDeck(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectAllDecksResponse SelectAllDeckBasic(AdminPulse.DeckRequest request)
        {
            return adminPulse.SelectAllDeckBasic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseCreateResponse CreateDynamicNormal(AdminPulse.DeckRequest request)
        {
            return adminPulse.CreateDynamic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectDeckResponse SelectFullDetailDeck(AdminPulse.DeckRequest request)
        {
            return adminPulse.SelectFullDetailDeck(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseUpdateResponse UpdateDeckStatus(AdminPulse.DeckRequest request)
        {
            return adminPulse.UpdateDeckStatus(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseAnswerCardResponse AnswerCard(Pulse.DeckRequest request)
        {
            return pulseModel.AnswerCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectDeckAnalyticResponse SelectDeckAnalytic(AdminPulse.DeckRequest request)
        {
            return adminPulse.SelectDeckAnalytic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectDeckCardAnalyticResponse SelectDeckCardAnalytic(AdminPulse.DeckRequest request)
        {
            return adminPulse.SelectDeckCardAnalytic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectDeckCardOptionAnalyticResponse SelectDeckCardOptionAnalytic(AdminPulse.DeckRequest request)
        {
            return adminPulse.SelectDeckCardOptionAnalytic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectDeckCardCustomAnswerAnalyticResponse SelectDeckCardCustomAnswerAnalytic(AdminPulse.DeckRequest request)
        {
            return adminPulse.SelectDeckCardCustomAnswerAnalytic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseSelectDeckAnalyticByUserResponse SelectDeckAnalyticByUser(AdminPulse.DeckRequest request)
        {
            return adminPulse.SelectDeckAnalyticByUser(request);
        }

        [AcceptVerbs("GET", "POST")]
        public PulseUpdateResponse ImportDeck(AdminPulse.DeckRequest request)
        {
            return adminPulse.ImportDeck(request);
        }
    }
}
