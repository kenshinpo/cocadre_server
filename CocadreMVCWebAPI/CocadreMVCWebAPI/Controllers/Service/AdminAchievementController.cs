﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    [RoutePrefix("AdminAchievement")]
    public class AdminAchievementController : ApiController
    {
        private AdminAchievement adminA = new AdminAchievement();

        public GamificationCreateAchievementResponse CreateAchievement(AdminAchievement.Request request)
        {
            return adminA.CreateAchievement(request);
        }

        public GamificationSelectAllAchievementResponse SelectAllAchievement(AdminAchievement.Request request)
        {
            return adminA.SelectAllAchievement(request);
        }

        public GamificationSelectAchievementResponse SelectAchievement(AdminAchievement.Request request)
        {
            return adminA.SelectAchievement(request);
        }

        public GamificationSelectTopicResponse SelectTopicsForAchievement(AdminAchievement.Request request)
        {
            return adminA.SelectTopicsForAchievement(request);
        }

        public GamificationSelectAchievementResultResponse SelectAchievementResult(AdminAchievement.Request request)
        {
            return adminA.SelectAchievementResult(request);
        }

        public GamificationSelectUserAchievementResultResponse SelectUserAchievementResult(AdminAchievement.Request request)
        {
            return adminA.SelectUserAchievementResult(request);
        }
    }
}
