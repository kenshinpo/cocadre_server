﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    [RoutePrefix("AdminDashboard")]
    public class AdminDashboardController : ApiController
    {
        private AdminDashboard adminDashboardModel = new AdminDashboard();

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminDashboard/GetFeedbackPosts.ashx")]
        [Route("GetFeedbackPosts")]
        public DashboardSelectFeedbackResponse GetFeedbackPosts(AdminDashboard.DashboardRequest request)
        {
            return adminDashboardModel.GetFeedbackPosts(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminDashboard/GetReportedPosts.ashx")]
        [Route("GetReportedPosts")]
        public DashboardSelectReportedFeedResponse GetReportedPosts(AdminDashboard.DashboardRequest request)
        {
            return adminDashboardModel.GetReportedPosts(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminDashboard/PostFeedbackComment.ashx")]
        [Route("PostFeedbackComment")]
        public DashboardCreateFeedbackCommentResponse PostFeedbackComment(AdminDashboard.DashboardCreateFeedbackCommentRequest request)
        {
            return adminDashboardModel.PostFeedbackComment(request);
        }
    }
}
