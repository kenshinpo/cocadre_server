﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System.Web.Http;
namespace CocadreMVCWebAPI.Controllers.Service
{
    public class SchedulerController : ApiController
    {
        private Scheduler scheduler = new Scheduler();

        [AcceptVerbs("GET", "POST")]
        public TriggerScheduledNotificationResponse ScheduledNotification(Scheduler.ApiRequest request)
        {
            return scheduler.TriggerScheduledNotification(request);
        }

        [AcceptVerbs("GET", "POST")]
        public HttpRequestGetScheduleResponse GetSchedules()
        {
            return scheduler.GetSchedules();
        }
    }
}
