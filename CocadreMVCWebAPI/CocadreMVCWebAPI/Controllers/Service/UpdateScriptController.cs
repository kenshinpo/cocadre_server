﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    [RoutePrefix("UpdateScript")]
    public class UpdateScriptController : ApiController
    {
        private UpdateScript updateScriptModel = new UpdateScript();

        [AcceptVerbs("GET", "POST")]
        [Route("InsertFeedValidStatus")]
        public UpdateScriptResponse InsertFeedValidStatus(UpdateScript.UpdateScriptRequest request)
        {
            return updateScriptModel.InsertValidStatusColumnToFeed(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateChallenge")]
        public UpdateScriptResponse UpdateChallenge()
        {
            return updateScriptModel.InsertNewColumnsAndTablesToChallenge();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateChallengeForEvent")]
        public UpdateScriptResponse UpdateChallengeForEvent()
        {
            return updateScriptModel.InsertTableChallengeByTopic();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateUserToken")]
        public UpdateScriptResponse UpdateUserToken()
        {
            return updateScriptModel.CreateNewTablesForUserToken();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateNotification")]
        public UpdateScriptResponse UpdateNotification()
        {
            return updateScriptModel.CreateNewTablesFoNotification();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateUser")]
        public UpdateScriptResponse UpdateUser()
        {
            return updateScriptModel.CreateNewColumnsForUser();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateCompany")]
        public UpdateScriptResponse UpdateCompany()
        {
            return updateScriptModel.CreateNewColumnsForCompany();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateMatchUp")]
        public UpdateScriptResponse UpdateMatchUp()
        {
            return updateScriptModel.CreateMatchUpActivity();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateMatchUpAttempt")]
        public UpdateScriptResponse UpdateMatchUpAttempt()
        {
            return updateScriptModel.CreateMatchUpTopicAttempt();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateQuestionAttempt")]
        public UpdateScriptResponse UpdateQuestionAttempt()
        {
            return updateScriptModel.CreateMatchUpQuestionAttempt();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateOptionAttempt")]
        public UpdateScriptResponse UpdateOptionAttempt()
        {
            return updateScriptModel.CreateMatchUpOptionAttempt();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateOptionAttempt")]
        public UpdateScriptResponse UpdateFeedUser()
        {
            return updateScriptModel.UpdateFeedUser();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateSurveyCategory")]
        public UpdateScriptResponse UpdateSurveyCategory()
        {
            return updateScriptModel.UpdateSurveyCategory();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateSurvey")]
        public UpdateScriptResponse UpdateSurvey()
        {
            return updateScriptModel.UpdateSurvey();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateSurveyCard")]
        public UpdateScriptResponse UpdateSurveyCard()
        {
            return updateScriptModel.UpdateSurveyCard();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateMatchupDifficulty")]
        public UpdateScriptResponse UpdateMatchupDifficulty()
        {
            return updateScriptModel.UpdateMatchupDifficulty();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateMatchupDifficulty")]
        public UpdateScriptResponse UpdateDynamicPulseCustomized()
        {
            return updateScriptModel.UpdateDynamicPulseCustomized();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateUserEmailLower")]
        public UpdateScriptResponse UpdateUserEmailLower()
        {
            return updateScriptModel.UpdateUserEmailLower();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdatePhoneCountryForUser")]
        public UpdateScriptResponse UpdatePhoneCountryForUser(UpdateScript.UpdateScriptRequest request)
        {
            return updateScriptModel.UpdatePhoneCountryForUser(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateFeedPoint")]
        public UpdateScriptResponse UpdateFeedPoint()
        {
            return updateScriptModel.UpdateFeedPoint();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdatePrivacyForFeed")]
        public UpdateScriptResponse UpdatePrivacyForFeed()
        {
            return updateScriptModel.UpdatePrivacyForFeed();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateLogin")]
        public UpdateScriptResponse UpdateLogin()
        {
            return updateScriptModel.UpdateLogin();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("OptimizeNotification")]
        public UpdateScriptResponse OptimizeNotification()
        {
            return updateScriptModel.OptimizeNotification();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateCompanyJobs")]
        public UpdateScriptResponse UpdateCompanyJobs()
        {
            return updateScriptModel.UpdateCompanyJobs();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateUserJob")]
        public UpdateScriptResponse UpdateUserJob()
        {
            return updateScriptModel.UpdateUserJob();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateAppraisalCommentRating")]
        public UpdateScriptResponse UpdateAppraisalCommentRating()
        {
            return updateScriptModel.UpdateAppraisalCommentRating();
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateDeletedUserJob")]
        public UpdateScriptResponse UpdateDeletedUserJob()
        {
            return updateScriptModel.UpdateDeletedUserJob();
        }
    }
}
