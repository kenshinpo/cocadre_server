﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    public class FixBugController : ApiController
    {
        private FixBug fixBugModel = new FixBug();

        [AcceptVerbs("GET", "POST")]
        public FixBugResponse FixSurveyCustomAnswerGrouping(FixBugRequest request)
        {
            return fixBugModel.FixSurveyCustomAnswerGrouping(request);
        }

        [AcceptVerbs("GET", "POST")]
        public FixBugResponse FixSurveyCompletion()
        {
            return fixBugModel.FixSurveyCompletion();
        }

        [AcceptVerbs("GET", "POST")]
        public FixBugResponse FixImpressionTally()
        {
            return fixBugModel.FixImpressionTally();
        }

        [AcceptVerbs("GET", "POST")]
        public FixBugResponse RemoveAppraisalPulses()
        {
            return fixBugModel.RemoveAppraisalPulses();
        }

        [AcceptVerbs("GET", "POST")]
        public FixBugResponse UpdateMissingDau(FixBugRequest request)
        {
            return fixBugModel.UpdateMissingDau(request);
        }
    }
}
