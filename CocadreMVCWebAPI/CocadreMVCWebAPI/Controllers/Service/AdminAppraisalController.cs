﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    public class AdminAppraisalController : ApiController
    {
        private AdminAppraisal model = new AdminAppraisal();

        [AcceptVerbs("GET", "POST")]
        public AppraisalCreateTemplateResponse CreateTemplate(AdminAppraisal.AppraisalRequest request)
        {
            return model.CreateTemplate(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalSelectAllTemplatesResponse SelectTemplates(AdminAppraisal.AppraisalRequest request)
        {
            return model.SelectTemplates(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalSelectTemplateResponse SelectTemplate(AdminAppraisal.AppraisalRequest request)
        {
            return model.SelectTemplate(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalUpdateResponse UpdateStatus(AdminAppraisal.AppraisalRequest request)
        {
            return model.UpdateStatus(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalCategoryCreateResponse CreateCategory(AdminAppraisal.AppraisalRequest request)
        {
            return model.CreateCategory(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalCardCreateTemplateResponse CreateCard(AdminAppraisal.AppraisalRequest request)
        {
            return model.CreateCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalSelectResponse GetResult(AdminAppraisal.AppraisalRequest request)
        {
            return model.SelectResult(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AppraisalSelectOverallReportsResponse GetReports(AdminAppraisal.AppraisalRequest request)
        {
            return model.SelectReports(request);
        }
    }
}
