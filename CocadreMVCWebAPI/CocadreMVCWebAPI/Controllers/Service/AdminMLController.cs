﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    [RoutePrefix("AdminML")]
    public class AdminMLController : ApiController
    {
        private AdminML adminML = new AdminML();
        private MobileLearning clientML = new MobileLearning();

        [AcceptVerbs("GET", "POST")]
        public MLCategorySelectAllResponse GetMLCategories(AdminML.SelectAllMLCategoriesRequest request)
        {
            return adminML.SelectAllMLCategories(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLTopicCreateResponse CreateMLTopic(AdminML.CreateMLTopicRequest request)
        {
            return adminML.CreateMLTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLTopicSelectAllBasicResponse SelectAllMLTopics(AdminML.SelectAllMLTopicsRequest request)
        {
            return adminML.SelectAllBasicByCategory(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLCardCreateResponse CreateMLCard(AdminML.CreateCardRequest request)
        {
            return adminML.CreateCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLTopicSelectResponse SelectMLTopic(AdminML.SelectMLTopicRequest request)
        {
            return adminML.SelectTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLCardSelectResponse SelectMLCard(AdminML.SelectMLCardRequest request)
        {
            return adminML.SelectCard(request);
        }


        [AcceptVerbs("GET", "POST")]
        public MLTopicUpdateResponse UpdateMLTopicStatus(AdminML.UpdateCardRequest request)
        {
            return adminML.UpdateStatus(request);
        }


        [AcceptVerbs("GET", "POST")]
        public MLSelectCandidateResultResponse SelectCandidateResult(AdminML.PreviewRequest request)
        {
            return adminML.SelectCandidateResult(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLSelectQuestionResultResponse SelectQuestionResult(AdminML.PreviewRequest request)
        {
            return adminML.SelectQuestionResult(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLSelectCardQuestionResultResponse SelectCardQuestionResult(AdminML.PreviewRequest request)
        {
            return adminML.SelectCardQuestionResult(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLSelectUserAttemptHistoryResponse SelectUserAttemptResult(AdminML.PreviewRequest request)
        {
            return adminML.SelectUserAttemptResult(request);
        }


        [AcceptVerbs("GET", "POST")]
        public MLSelectSortedCardAttemptHistoryResponse SelectSortedCardHistoryResult(AdminML.PreviewRequest request)
        {
            return adminML.SelectSortedCardHistoryResult(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLTopicSelectAllByCategoryResponse SelectAllMLTopicsByUser(MobileLearning.Request request)
        {
            return clientML.GetAllMLTopics(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLTopicSelectResponse GetMLTopic(MobileLearning.Request request)
        {
            return clientML.GetMLTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        public MLEducationCardCreateResponse CreateMLEducationCard(AdminML.CreateEducationCardRequest request)
        {
            return adminML.CreateEducationCard(request);
        }
    }
}
