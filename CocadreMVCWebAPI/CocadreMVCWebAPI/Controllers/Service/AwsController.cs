﻿using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Web.Http;
using Amazon.Runtime;
using Amazon.S3;
using CassandraService.Entity;
using CassandraService.Utilities;
using CocadreMVCWebAPI.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Resources;
using Aws = CocadreMVCWebAPI.Models.Service.Aws;

namespace CocadreMVCWebAPI.Controllers.Service
{
    [RoutePrefix("Aws")]
    public class AwsController : ApiController
    {
        private Aws awsModel = new Aws();
        private static Object progressLock = new Object();
        private static IAmazonS3 s3Client;

        [AcceptVerbs("GET", "POST")]
        public Aws.GetCognitoTokenResponse GetCognitoToken(Aws.AwsRequest request)
        {
            return awsModel.GetCognitoToken(request);
        }

        [AcceptVerbs("GET", "POST")]
        public Aws.SignedUrlResponse GetSignedUrl(Aws.SignedUrlRequest request)
        {
            return awsModel.GetSignedUrl(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserImageProgressResponse LambdaUpdateUserThumbnailingProgress([FromBody]JToken jsonbody)
        {
            var imageObj = jsonbody["imageObj"];
            var userId = imageObj["imageFolderID"];
            var imageName = imageObj["imageName"];
            var companyId = imageObj["companyID"];
            var env = imageObj["env"];

            var progressState = jsonbody["progressState"];
            float progress = progressState["progress"].ToObject<float>();

            return awsModel.LambdaUpdateUserThumbnailingProgress(userId.ToString(), imageName.ToString(), progress, env.ToString(), companyId.ToString());
        }

        public UserImageProgressResponse LambdaSetUserThumbnailingAsCompleted([FromBody] JToken jsonbody)
        {
            var imageObj = jsonbody["imageObj"];
            var userId = imageObj["imageFolderID"];
            var imageName = imageObj["imageName"];
            var companyId = imageObj["companyID"];
            var env = imageObj["env"];

            return awsModel.LambdaSetUserThumbnailingAsCompleted(userId.ToString(), imageName.ToString(), env.ToString(), companyId.ToString()); ;
        }

        [AcceptVerbs("GET", "POST")]
        public FeedImageProgressResponse LambdaVideoTranscodingNotification()
        {
            FeedImageProgressResponse feedImageProgressResponse = new FeedImageProgressResponse();
            feedImageProgressResponse.Success = true;

            var content = Request.Content;
            string jsonContent = content.ReadAsStringAsync().Result;

            AWSSNSNotification AwsSNSNotification = JsonConvert.DeserializeObject<AWSSNSNotification>(jsonContent);

            if(AwsSNSNotification.Type.Equals("Notification"))
            { 
                awsModel.LambdaVideoTranscodingNotification(AwsSNSNotification);
            } else if (AwsSNSNotification.Type.Equals("SubscriptionConfirmation"))
            {
                awsModel.LambdaVideoTranscodingConfirmation(AwsSNSNotification);
            }

            return feedImageProgressResponse;
        }

        [AcceptVerbs("GET", "POST")]
        public FeedImageProgressResponse LambdaUpdateFeedThumbnailingProgress([FromBody]JToken jsonbody)
        {
            var imageObj = jsonbody["imageObj"];
            var feedId = imageObj["imageFolderID"];
            var imageName = imageObj["imageName"];
            var companyId = imageObj["companyID"];
            var env = imageObj["env"];

            var progressState = jsonbody["progressState"];
            float progress = progressState["progress"].ToObject<float>();
            int expectedCount = 0; // lambda's expected count is always 0

            return awsModel.LambdaUpdateFeedThumbnailingProgress(feedId.ToString(), imageName.ToString(), progress, expectedCount, env.ToString(), companyId.ToString());
        }

        [AcceptVerbs("POST")]
        public FeedImageProgressResponse LambdaSetFeedThumbnailingAsCompleted([FromBody] JToken jsonbody)
        {
            var imageObj = jsonbody["imageObj"];
            var feedId = imageObj["imageFolderID"];
            var imageName = imageObj["imageName"];
            var companyId = imageObj["companyID"];
            var env = imageObj["env"];

            return awsModel.LambdaSetFeedThumbnailingAsCompleted(feedId.ToString(), imageName.ToString(), env.ToString(), companyId.ToString()); ;
        }
    }
}
