﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    [RoutePrefix("AdminGami")]
    public class AdminGamiController : ApiController
    {
        private AdminGami adminGami = new AdminGami();

        [AcceptVerbs("GET", "POST")]
        [Route("BackupExp")]
        public GamificationBackupResponse BackupExp(AdminGami.GamificationRequest request)
        {
            return adminGami.SelectExpBackup(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("BackupLeaderboard")]
        public GamificationBackupResponse BackupLeaderboard(AdminGami.GamificationRequest request)
        {
            return adminGami.SelectLeaderboardBackup(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("ResetExp")]
        public GamificationResetResponse ResetExp(AdminGami.GamificationRequest request)
        {
            return adminGami.ResetExp(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("ResetExp")]
        public GamificationResetResponse ResetLeaderboard(AdminGami.GamificationRequest request)
        {
            return adminGami.ResetLeaderboard(request);
        }
    }
}
