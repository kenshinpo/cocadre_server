﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using RazorEngine;
using RazorEngine.Templating;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
namespace CocadreMVCWebAPI.Controllers.Service
{
    public class PasswordController : ApiController
    {
        private ForgetPassword passwordModel = new ForgetPassword();

        [AcceptVerbs("GET", "POST")]
        public HttpResponseMessage Reset(string token)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

            AuthenticationUpdateResponse serviceResponse = passwordModel.ResetPasswordWithToken(token);

            string viewPath = HttpContext.Current.Server.MapPath(@"~/Views/Password/ResetConfirmation.cshtml");
            string template = File.ReadAllText(viewPath);

            var result = Engine.Razor.RunCompile(template,"ResetConfirmationTemplate", null, new { Success = serviceResponse.Success, ErrorMessage = serviceResponse.ErrorMessage });

            response.Content = new StringContent(result);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("text/html");
            return response;
        }
    }
}
