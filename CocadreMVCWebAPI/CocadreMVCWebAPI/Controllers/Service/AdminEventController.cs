﻿using CassandraService.ServiceResponses;
using CassandraService.Entity;
using CocadreMVCWebAPI.Models.Service;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Collections.Generic;
using iTextSharp.tool.xml;

namespace CocadreMVCWebAPI.Controllers.Service
{
    public class AdminEventController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminEvent model = new AdminEvent();

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.Route("GetEventResult")]
        public HttpResponseMessage GetEventResult(string adminUserId, string companyId, string eventId)
        {
            var result = Request.CreateResponse(HttpStatusCode.OK);
            AnalyticSelectEventResultResponse eventResponse = model.GetEventResult(adminUserId, companyId, eventId);

            if(eventResponse.Success)
            {
                byte[] pdfBytes;
                Document doc = new Document(PageSize.A4);

                using (var mem = new MemoryStream())
                {
                    using (PdfWriter wri = PdfWriter.GetInstance(doc, mem))
                    {
                        doc.Open();
                        doc.Add(GenerateHeaderTitle("Event Result"));
                        doc.Add(GenerateHeader(eventResponse.Event));
                        doc.Add(GenerateBody(eventResponse.Score, wri));
                        doc.Add(GenerateFooter());
                        doc.Close();
                    }

                    pdfBytes = mem.ToArray();
                }

                MemoryStream output = new MemoryStream();
                output.Write(pdfBytes, 0, pdfBytes.Length);
                output.Position = 0;
                
                result.Content = new StreamContent(output);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                //result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "EventResult.pdf" };
            }
            
            return result;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.Route("DownloadEventResult")]
        public HttpResponseMessage DownloadEventResult(string adminUserId, string companyId, string eventId)
        {
            var result = Request.CreateResponse(HttpStatusCode.OK);
            AnalyticSelectEventResultResponse eventResponse = model.GetEventResult(adminUserId, companyId, eventId);

            if (eventResponse.Success)
            {
                byte[] pdfBytes;
                Document doc = new Document(PageSize.A4);

                using (var mem = new MemoryStream())
                {
                    using (PdfWriter wri = PdfWriter.GetInstance(doc, mem))
                    {
                        doc.Open();
                        doc.Add(GenerateHeaderTitle("Event Result"));
                        doc.Add(GenerateHeader(eventResponse.Event));
                        doc.Add(GenerateBody(eventResponse.Score, wri));
                        doc.Add(GenerateFooter());
                        doc.Close();
                    }

                    pdfBytes = mem.ToArray();
                }

                MemoryStream output = new MemoryStream();
                output.Write(pdfBytes, 0, pdfBytes.Length);
                output.Position = 0;

                result.Content = new StreamContent(output);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "EventResult.pdf" };
            }

            return result;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.Route("ConvertHtml")]
        public HttpResponseMessage ConvertHtml()
        {
            var result = Request.CreateResponse(HttpStatusCode.OK);

            byte[] pdfBytes;
            Document doc = new Document(PageSize.A4);

            using (var mem = new MemoryStream())
            {
                using (PdfWriter wri = PdfWriter.GetInstance(doc, mem))
                {
                    doc.Open();
                    XMLWorkerHelper worker = XMLWorkerHelper.GetInstance();
                    string str = "<html><head></head><body>" +
                      "<a href='http://www.rgagnon.com/howto.html'><b>Real's HowTo</b></a>" +
                      "<h1>Show your support</h1>" +
                      "<p>It DOES cost a lot to produce this site - in ISP storage and transfer fees, " +
                      "in personal hardware and software costs to set up test environments, and above all," +
                      "the huge amounts of time it takes for one person to design and write the actual content.</p>" +
                      "<p>If you feel that effort has been useful to you, perhaps you will consider giving something back?</p>" +
                      "<p>Donate using PayPal to real@rgagnon.com.</p>" +
                      "<p>Contributions via PayPal are accepted in any amount</p>" +
                      "<P><br/><table border='1'><tr><td>Java HowTo</td></tr><tr>" +
                      "<td style='background-color:red;'>Javascript HowTo</td></tr>" +
                      "<tr><td>Powerbuilder HowTo</td></tr></table></p>" +
                      "</body></html>";
                    worker.ParseXHtml(wri, doc, new StringReader(str));
                    doc.Close();
                }

                pdfBytes = mem.ToArray();
            }

            MemoryStream output = new MemoryStream();
            output.Write(pdfBytes, 0, pdfBytes.Length);
            output.Position = 0;

            result.Content = new StreamContent(output);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            return result;
        }

        private static BaseFont bfTimes = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
        private static Font header = new Font(bfTimes, 13, Font.BOLD, BaseColor.BLACK);
        private static Font normal = new Font(bfTimes, 10, Font.NORMAL, BaseColor.BLACK);
        private static Font italicSmall = new Font(bfTimes, 8, Font.ITALIC, BaseColor.BLACK);

        private Paragraph GenerateHeaderTitle(string headerText)
        {
            Paragraph paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_LEFT;
            paragraph.SpacingAfter = 5;
            paragraph.Add(new Phrase(headerText, header));
            return paragraph;
        }

        private PdfPTable GenerateHeader(Event selectedEvent)
        {        
            var headerTable = new PdfPTable(2);
            headerTable.HorizontalAlignment = 0;
            headerTable.SpacingBefore = 20;
            headerTable.SpacingAfter = 50;
            headerTable.DefaultCell.Border = 0;
            headerTable.SetWidths(new int[] { 2, 6 });
            headerTable.DefaultCell.PaddingTop = 5;
            headerTable.DefaultCell.PaddingBottom = 5;

            headerTable.AddCell(new Phrase("Title:", normal));
            headerTable.AddCell(new Phrase(selectedEvent.Title, normal));
            headerTable.AddCell(new Phrase("Description:", normal));
            headerTable.AddCell(new Phrase(selectedEvent.Description, normal));
            headerTable.AddCell(new Phrase("Start Date:", normal));
            headerTable.AddCell(new Phrase(selectedEvent.StartTimestamp.ToString("dd-MM-yyyy hh:mm:ss"), normal));
            headerTable.AddCell(new Phrase("End Date:", normal));
            headerTable.AddCell(new Phrase(selectedEvent.EndTimestamp.ToString("dd-MM-yyyy hh:mm:ss"), normal));

            return headerTable;
        }

        private PdfPTable GenerateBody(List<AnalyticQuiz.AnalyticEventUserScore> scores, PdfWriter wri)
        {
            var bodyTable = new PdfPTable(6);
            bodyTable.HorizontalAlignment = 0;
            bodyTable.SpacingAfter = 5;
            bodyTable.DefaultCell.Border = 0;
            bodyTable.SetWidths(new int[] { 2, 2, 6, 6, 2, 6 });
            bodyTable.WidthPercentage = 100;
            bodyTable.DefaultCell.FixedHeight = 30f;
            bodyTable.DefaultCell.Border = 0;
            bodyTable.DefaultCell.PaddingTop = 5;
            bodyTable.DefaultCell.PaddingBottom = 5;

            bodyTable.AddCell(new Phrase("Rank", normal));
            bodyTable.AddCell(new Phrase());
            bodyTable.AddCell(new Phrase("Staff", normal));
            bodyTable.AddCell(new Phrase("Department", normal));
            bodyTable.AddCell(new Phrase("Score", normal));
            bodyTable.AddCell(new Phrase("Last Updated Timestamp", normal));
            int rank = 1;
            foreach (AnalyticQuiz.AnalyticEventUserScore score in scores)
            {
                iTextSharp.text.Image userImage = iTextSharp.text.Image.GetInstance(new Uri(score.User.ProfileImageUrl));
                float w = userImage.ScaledWidth;
                float h = userImage.ScaledHeight;
                PdfTemplate t = wri.DirectContent.CreateTemplate(w, h);
                t.Ellipse(0, 0, w, h);
                t.Clip();
                t.NewPath();
                t.AddImage(userImage, w, 0, 0, h, 0, 0);
                iTextSharp.text.Image clipped = iTextSharp.text.Image.GetInstance(t);

                bodyTable.AddCell(new Phrase(rank.ToString(), normal));
                bodyTable.AddCell(clipped);
                bodyTable.AddCell(new Phrase(string.Format("{0} {1}", score.User.FirstName, score.User.LastName), normal));
                bodyTable.AddCell(new Phrase(score.User.Departments[0].Title, normal));
                bodyTable.AddCell(new Phrase(score.Score.ToString(), normal));
                bodyTable.AddCell(new Phrase(score.TimestampString, normal));
                rank++;
            }

            return bodyTable;
        }

        private Paragraph GenerateFooter()
        {
            Paragraph paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Add(new Phrase("This is an auto generated result by the system", italicSmall));
            return paragraph;
        }
    }
}
