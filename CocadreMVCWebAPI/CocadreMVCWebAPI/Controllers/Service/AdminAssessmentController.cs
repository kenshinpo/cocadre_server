﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using CocadreMVCWebAPI.Models.v6;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    public class AdminAssessmentController : ApiController
    {
        private AdminAssessment assessAdminModel = new AdminAssessment();
        private Assessment assessModel = new Assessment();

        [AcceptVerbs("GET", "POST")]
        public AssessmentCreateResponse CreateAssessment(AdminAssessment.CreateAssessmentRequest request)
        {
            return assessAdminModel.CreateAssessment(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AssessmentSelectAllResponse SelectAllAssessments(AdminAssessment.AssessmentRequest request)
        {
            return assessAdminModel.SelectAllAssessment(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AssessmentSelectResponse SelectFullDetailAssessment(AdminAssessment.AssessmentRequest request)
        {
            return assessAdminModel.SelectFullDetailAssessment(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AssessmentCreateCardResponse CreateAssessmentCard(AdminAssessment.CreateCardRequest request)
        {
            return assessAdminModel.CreateAssessmentCard(request);
        }
        [AcceptVerbs("GET", "POST")]
        public AssessmentSelectCardResponse SelectFullDetailAssessmentCard(AdminAssessment.AssessmentRequest request)
        {
            return assessAdminModel.SelectFullDetailCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        public SubscriptionCreateResponse CreateAssessmentSubscription(AdminAssessment.SubscriptionRequest request)
        {
            return assessAdminModel.CreateAssessmentSubscription(request);
        }

        [AcceptVerbs("GET", "POST")]
        public SubscriptionSelectAllResponse SelectAllAssessmentSubscription(AdminAssessment.SubscriptionRequest request)
        {
            return assessAdminModel.SelectAllAssessmentSubscription(request);
        }

        [AcceptVerbs("GET", "POST")]
        public SubscriptionSelectResponse SelectFullDetailAssessmentSubscription(AdminAssessment.SubscriptionRequest request)
        {
            return assessAdminModel.SelectFullDetailAssessmentSubscription(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AssessmentSelectResponse SelectAssessmentByUser(Assessment.AssessmentRequest request)
        {
            return assessModel.SelectAssessmentByUser(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AssessmentSelectResponse TakeAssessment(Assessment.AssessmentRequest request)
        {
            return assessModel.TakeAssessment(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AssessmentUpdateResponse UpdateAssessmentStatus(AdminAssessment.AssessmentRequest request)
        {
            return assessAdminModel.UpdateAssessmentStatus(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AssessmentSelectResultByUserResponse SelectAssessmentResultByUser(Assessment.AssessmentRequest request)
        {
            return assessModel.SelectAssessmentResultByUser(request);
        }


        [AcceptVerbs("GET", "POST")]
        public AssessmentAnswerCardResponse AnswerAssessmentCard(Assessment.AssessmentRequest request)
        {
            return assessModel.AnswerAssessmentCard(request);
        }
    }
}
