﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using CocadreMVCWebAPI.Models.v6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    public class AdminController : ApiController
    {
        private Admin adminModel = new Admin();
        private Account accountModel = new Account();
        private AdminService adminService = new AdminService();


        [AcceptVerbs("GET", "POST")]
        public UserCreateResponse CreateAdmin(Admin.CreateAdminRequest request)
        {
            return adminModel.CreateAdmin(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UserSelectProfileResponse UpdateUserProfile(Account.UpdateUserProfileRequest request)
        {
            return accountModel.UpdateUserProfile(request);
        }

        [AcceptVerbs("GET", "POST")]
        public UpdateCopmaniesSetting UpdateAllCompaniesS3CorsSetting()
        {
            return adminService.UpdateAllCompaniesS3CorsSetting();
        }
    }
}
