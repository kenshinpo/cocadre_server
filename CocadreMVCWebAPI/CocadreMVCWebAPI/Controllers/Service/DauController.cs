﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    [RoutePrefix("Dau")]
    public class DauController : ApiController
    {
        private Dau dauModel = new Dau();

        [AcceptVerbs("GET", "POST")]
        public AnalyticsUpdateDailyActiveUserResponse CreateDAU()
        {
            return dauModel.CreateDAU();
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticsUpdateDailyActiveUserResponse CreateThirtyDaysDAU()
        {
            return dauModel.CreateThirtyDaysDAU();
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticsSelectBasicResponse SelectSummaryReport()
        {
            return dauModel.SelectSummaryReport();
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticsSelectDetailDauResponse SelectDauReport(Dau.DauRequest request)
        {
            return dauModel.SelectDauReport(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticSelectMatchUpOverallResponse SelectMatchUpOverall(Dau.MatchupRequest request)
        {
            return dauModel.SelectMatchUpOverall(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticSelectTopicAttemptResponse SelectTopicAttempts(Dau.MatchupRequest request)
        {
            return dauModel.SelectTopicAttempts(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticSelectTopicAttemptResponse SelectDailyTopicActivities(Dau.MatchupRequest request)
        {
            return dauModel.SelectDailyTopicActivities(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticSelectTopicOverallResponse SelectTopicOverall(Dau.MatchupRequest request)
        {
            return dauModel.SelectTopicOverall(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticSelectQuestionAttemptResponse SelectQuestionAttempts(Dau.MatchupRequest request)
        {
            return dauModel.SelectQuestionAttempts(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticSelectOptionAttemptResponse SelectOptionAttempts(Dau.MatchupRequest request)
        {
            return dauModel.SelectOptionAttempts(request);
        }

        [AcceptVerbs("GET", "POST")]
        public AnalyticActivityResponse GetUserActivityPerDay(Dau.DauRequest request)
        {
            return dauModel.SelectUserActivityPerDay(request);
        }
    }
}
