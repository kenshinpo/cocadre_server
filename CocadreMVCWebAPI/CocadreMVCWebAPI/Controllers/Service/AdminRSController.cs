﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    [RoutePrefix("AdminRS")]
    public class AdminRSController : ApiController
    {
        private AdminRS adminRS = new AdminRS();

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/GetRSCategories.ashx")]
        [Route("GetRSCategories")]
        public RSCategorySelectAllResponse GetRSCategories(AdminRS.SelectAllRSCategoriesRequest request)
        {
            return adminRS.SelectAllRSCategories(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/CreateRSTopic.ashx")]
        [Route("CreateRSTopic")]
        public RSTopicCreateResponse CreateRSTopic(AdminRS.CreateRSTopicRequest request)
        {
            return adminRS.CreateRSTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/SelectAllRSTopics.ashx")]
        [Route("SelectAllRSTopics")]
        public RSTopicSelectAllBasicResponse SelectAllRSTopics(AdminRS.SelectAllRSTopicsRequest request)
        {
            return adminRS.SelectAllBasicByCategory(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/CreateCard.ashx")]
        [Route("CreateCard")]
        public RSCardCreateResponse CreateCard(AdminRS.CreateCardRequest request)
        {
            return adminRS.CreateCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/SelectRSTopic.ashx")]
        [Route("SelectRSTopic")]
        public RSTopicSelectResponse SelectRSTopic(AdminRS.SelectRSTopicRequest request)
        {
            return adminRS.SelectTopic(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/SelectRSCard.ashx")]
        [Route("SelectRSCard")]
        public RSCardSelectResponse SelectRSCard(AdminRS.SelectRSCardRequest request)
        {
            return adminRS.SelectCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/Preview.ashx")]
        [Route("Preview")]
        public RSCardSelectAllResponse Preview(AdminRS.PreviewRequest request)
        {
            return adminRS.Preview(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/UpdateCard.ashx")]
        [Route("UpdateCard")]
        public RSCardUpdateResponse UpdateCard(AdminRS.CreateCardRequest request)
        {
            return adminRS.UpdateCard(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/SelectResultOverview.ashx")]
        [Route("SelectResultOverview")]
        public AnalyticSelectRSResultOverviewResponse SelectResultOverview(AdminRS.PreviewRequest request)
        {
            return adminRS.SelectResultOverview(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/SelectCardResult.ashx")]
        [Route("SelectCardResult")]
        public AnalyticSelectRSCardResultResponse SelectCardResult(AdminRS.PreviewRequest request)
        {
            return adminRS.SelectCardResult(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/SelectCardResultByUser.ashx")]
        [Route("SelectCardResultByUser")]
        public AnalyticSelectRSCardResultByUserResponse SelectCardResultByUser(AdminRS.PreviewRequest request)
        {
            return adminRS.SelectCardResultByUser(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/SelectCustomAnswersResult.ashx")]
        [Route("SelectCustomAnswersResult")]
        public AnalyticSelectRSCustomAnswersResponse SelectCustomAnswersResult(AdminRS.PreviewRequest request)
        {
            return adminRS.SelectCustomAnswersResult(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/SelectRespondersReport.ashx")]
        [Route("SelectRespondersReport")]
        public AnalyticSelectRSResponderReportResponse SelectRespondersReport(AdminRS.PreviewRequest request)
        {
            return adminRS.SelectRespondersReport(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/SelectOptionResult.ashx")]
        [Route("SelectOptionResult")]
        public AnalyticSelectRSOptionResultResponse SelectOptionResult(AdminRS.PreviewRequest request)
        {
            return adminRS.SelectOptionResult(request);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/SelectFeedback.ashx")]
        [Route("SelectFeedback")]
        public RSTopicSelectFeedbackResponse SelectFeedback(AdminRS.PreviewRequest request)
        {
            return adminRS.SelectFeedback(request);
        }


        [AcceptVerbs("GET", "POST")]
        [Route("~/AdminRS/ExportRSResult.ashx")]
        [Route("ExportRSResult")]
        public AnalyticExportRSResultResponse ExportRSResult(AdminRS.PreviewRequest request)
        {
            return adminRS.ExportRSResult(request);
        }
    }
}
