﻿using CassandraService.Entity;
using CocadreMVCWebAPI.Models.Service;
using System.Collections.Generic;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    public class AdminQuizController : ApiController
    {
        private AdminQuiz adminModel = new AdminQuiz();

        [AcceptVerbs("GET", "POST")]
        public List<ChallengeQuestion> GetRandomQuestions(AdminQuiz.QuizRequest request)
        {
            return adminModel.GetRandomQuestions(request);
        }
    }
}
