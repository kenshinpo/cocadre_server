﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    public class TestLoadController : ApiController
    {
        private TestLoad testLoadModel = new TestLoad();

        [AcceptVerbs("GET", "POST")]
        public UserSelectWithCompanyResponse TestWithoutLoop(TestLoadRequest request)
        {
            return testLoadModel.TestWithoutLoop(request);
        }

        [AcceptVerbs("GET", "POST")]
        public CategorySelectAllWithTopicResponse TestWithLoop(TestLoadRequest request)
        {
            return testLoadModel.TestWithLoop(request);
        }

        [AcceptVerbs("GET", "POST")]
        public BrainSelectResponse TestWithMultipleKeyspaces(TestLoadRequest request)
        {
            return testLoadModel.TestWithMultipleKeyspaces(request);
        } 
    }
}
