﻿using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Models.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CocadreMVCWebAPI.Controllers.Service
{
    [RoutePrefix("CleanUp")]
    public class CleanUpController : ApiController
    {
        private Cleanup cleanUpModel = new Cleanup();

        [AcceptVerbs("GET", "POST")]
        [Route("CleanUpUser")]
        public UserCleanUpResponse CleanUpUser()
        {
            return cleanUpModel.CleanUpUser();
        }
    }
}
