﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v4
{
    public class ResponsiveSurvey
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public RSTopicSelectResponse GetRSTopic(TopicRequest request)
        {
            RSTopicSelectResponse response = new RSTopicSelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectRSTopicByUser(request.RequesterUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public RSCardAnswerResponse AnswerCard(AnswerCardRequest request)
        {
            RSCardAnswerResponse response = new RSCardAnswerResponse();
            response.Success = false;

            try
            {
                response = client.AnswerCard(request.AnsweredUserId, request.CompanyId, request.TopicId, request.CategoryId, request.CardId, request.RangeSelected, request.OptionIds, request.CustomAnswer, request.IsSkipped);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public RSUpdateActivityResponse UpdateBounce(TopicRequest request)
        {
            RSUpdateActivityResponse response = new RSUpdateActivityResponse();
            response.Success = false;

            try
            {
                response = client.UpdateBounceActivity(request.RequesterUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public RSUpdateActivityResponse UpdateCompletion(TopicRequest request)
        {
            RSUpdateActivityResponse response = new RSUpdateActivityResponse();
            response.Success = false;

            try
            {
                response = client.UpdateCompletionActivity(request.RequesterUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }
        public RSTopicUpdateFeedbackResponse CreateFeedback(TopicRequest request)
        {
            RSTopicUpdateFeedbackResponse response = new RSTopicUpdateFeedbackResponse();
            response.Success = false;

            try
            {
                response = client.CreateFeedback(request.RequesterUserId, request.CompanyId, request.TopicId, request.CategoryId, request.Feedback);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class TopicRequest
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
            public string TopicId { get; set; }
            public string CategoryId { get; set; }
            public string Feedback { get; set; }
        }

        public class AnswerCardRequest
        {
            public string CompanyId { get; set; }
            public string AnsweredUserId { get; set; }
            public string TopicId { get; set; }
            public string CategoryId { get; set; }
            public string CardId { get; set; }
            public int RangeSelected { get; set; }
            public List<string> OptionIds { get; set; }
            public RSOption CustomAnswer { get; set; }
            public bool IsSkipped { get; set; }
        }
    }
}