﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.Http;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PubNubMessaging.Core;
using Resources;
using Aws = CocadreMVCWebAPI.Models.Service.Aws;
using FeedTypeCode = CassandraService.GlobalResources.FeedTypeCode;

namespace CocadreMVCWebAPI.Models.v4
{
    public class Feed
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public FeedSelectResponse GetCompanyFeed(GetCompanyFeedRequest request)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string searchContent = request.SearchContent;
                DateTime? newestTimestamp = request.NewestTimestamp;
                DateTime? oldestTimestamp = request.OldestTimestamp;

                response = client.SelectCompanyFeedPost(userId, companyId, searchContent, 0, newestTimestamp, oldestTimestamp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectResponse GetPersonnelFeed(GetPersonnelFeedRequest request)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Success = false;

            try
            {
                string requesterUserId = request.RequesterUserId;
                string ownerUserId = request.OwnerUserId;
                string companyId = request.CompanyId;
                string searchContent = request.SearchContent;
                DateTime? newestTimestamp = request.NewestTimestamp;
                DateTime? oldestTimestamp = request.OldestTimestamp;

                response = client.SelectPersonnelFeedPost(requesterUserId, ownerUserId, companyId, searchContent, newestTimestamp, oldestTimestamp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public CommentSelectResponse GetComment(FeedRequest request)
        {
            CommentSelectResponse response = new CommentSelectResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;

                response = client.SelectFeedComment(userId, feedId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public ReplySelectResponse GetReply(FeedRequest request)
        {
            ReplySelectResponse response = new ReplySelectResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string commentId = request.CommentId;
                string feedId = request.FeedId;

                response = client.SelectCommentReply(userId, feedId, commentId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedAuthenticationResponse NewPostAuthentication(FeedRequest request)
        {
            FeedAuthenticationResponse response = new FeedAuthenticationResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = client.AuthenticateUserForPostingFeed(companyId, userId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }
            return response;
        }

        public FeedCreateResponse PostFeed(PostFeedRequest request)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.Success = false;

            try
            {
                string feedId = request.FeedId;
                int feedType = request.FeedType;
                string userId = request.UserId;
                string companyId = request.CompanyId;
                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                List<string> targetedGroupIds = request.TargetedGroupIds;
                bool isForAdmin = request.IsForAdmin;

                if (targetedDepartmentIds != null)
                {
                    Log.Debug("Count for targetedDepartmentIds: " + targetedDepartmentIds.Count);
                }

                if (targetedUserIds != null)
                {
                    Log.Debug("Count for targetedUserIds: " + targetedUserIds.Count);
                }

                if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                {
                    string content = request.Content;
                    response = client.CreateFeedTextPost(feedId, userId, companyId, content, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isForAdmin);
                }
                else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                {
                    string caption = request.Caption;
                    List<string> imageUrls = request.ImageUrls;

                    response = client.CreateFeedImagePost(feedId, userId, companyId, caption, imageUrls, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isForAdmin);

                }
                else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                {
                    string caption = request.Caption;
                    string videoUrl = request.VideoUrl;
                    string videoThumbnailUrl = request.VideoThumbnailUrl;
                    response = client.CreateFeedVideoPost(feedId, userId, companyId, caption, videoUrl, videoThumbnailUrl, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isForAdmin);
                }
                else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                {
                    string caption = request.Caption;
                    string url = request.Url;
                    string urlTitle = request.UrlTitle;
                    string urlDescription = request.UrlDescription;
                    string urlSiteName = request.UrlSiteName;
                    string urlImageUrl = request.UrlImageUrl;
                    response = client.CreateFeedSharedUrlPost(feedId, userId, companyId, caption, url, urlTitle, urlDescription, urlSiteName, urlImageUrl, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isForAdmin);
                }

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification notification in response.TargetedNotifications)
                    {
                        string targetedUserId = notification.TaggedUserId;
                        string notificationText = notification.NotificationText;
                        int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                        int notificationSubType = notification.SubType;
                        PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, response.FeedId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public CommentCreateResponse PostComment(PostCommentRequest request)
        {
            CommentCreateResponse response = new CommentCreateResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string content = request.Content;

                response = client.CreateCommentText(userId, companyId, feedId, content);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification serviceNotification in response.Notifications)
                    {
                        PushNotification.PushFeedNotification(serviceNotification.TaggedUserId, serviceNotification.NotificationText, serviceNotification.NumberOfNotificationForTargetedUser, serviceNotification.Type, serviceNotification.SubType, serviceNotification.TaggedFeedId, serviceNotification.TaggedCommentId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public ReplyCreateResponse PostReply(PostReplyRequest request)
        {
            ReplyCreateResponse response = new ReplyCreateResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string content = request.Content;

                response = client.CreateReplyText(userId, companyId, feedId, commentId, content);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification serviceNotification in response.Notifications)
                    {
                        PushNotification.PushFeedNotification(serviceNotification.TaggedUserId, serviceNotification.NotificationText, serviceNotification.NumberOfNotificationForTargetedUser, serviceNotification.Type, serviceNotification.SubType, serviceNotification.TaggedFeedId, serviceNotification.TaggedCommentId, serviceNotification.TaggedReplyId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteFeed(FeedRequest request)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;

                response = client.DeleteFeed(feedId, companyId, userId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteComment(FeedRequest request)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;

                response = client.DeleteComment(feedId, commentId, companyId, userId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteReply(FeedRequest request)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string replyId = request.ReplyId;

                response = client.DeleteReply(feedId, commentId, replyId, companyId, userId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public DashboardReportResponse ReportFeed(ReportFeedRequest request)
        {
            DashboardReportResponse response = new DashboardReportResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string replyId = request.ReplyId;
                string reason = request.Reason;

                bool isReportPost = request.IsReportPost;
                bool isReportComment = request.IsReportComment;
                bool isReportReply = request.IsReportReply;

                if (isReportPost)
                {
                    response = client.ReportFeed(userId, reason, companyId, feedId, null, null, (int)Dashboard.ReportType.Feed);
                }
                else if (isReportComment)
                {
                    response = client.ReportFeed(userId, reason, companyId, feedId, commentId, null, (int)Dashboard.ReportType.Comment);
                }
                else if (isReportReply)
                {
                    response = client.ReportFeed(userId, reason, companyId, feedId, commentId, replyId, (int)Dashboard.ReportType.Reply);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public PointUpdateResponse UpdateVote(VoteFeedRequest request)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;

                bool isUpVote = request.IsUpVote;

                bool isVoteFeed = request.IsVoteFeed;
                bool isVoteComment = request.IsVoteComment;
                bool isVoteReply = request.IsVoteReply;

                if (isVoteFeed)
                {
                    string feedId = request.FeedId;
                    response = client.UpdateFeedPoint(userId, feedId, companyId, isUpVote);
                }
                else if (isVoteComment)
                {
                    string feedId = request.FeedId;
                    string commentId = request.CommentId;
                    response = client.UpdateCommentPoint(userId, feedId, commentId, companyId, isUpVote);
                }
                else if (isVoteReply)
                {
                    string feedId = request.FeedId;
                    string commentId = request.CommentId;
                    string replyId = request.ReplyId;
                    response = client.UpdateReplyPoint(userId, feedId, commentId, replyId, companyId, isUpVote);
                }

                if (response.Success)
                {
                    string targetedUserId = response.Notification.TaggedUserId;
                    string notificationText = response.Notification.NotificationText;
                    int numberOfNotification = response.Notification.NumberOfNotificationForTargetedUser;
                    int notificationSubType = response.Notification.SubType;
                    string feedId = response.Notification.TaggedFeedId;
                    string commentId = response.Notification.TaggedCommentId;
                    string replyId = response.Notification.TaggedReplyId;
                    PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, feedId, commentId, replyId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectPrivacyResponse GetFeedPrivacy(FeedPrivacyRequest request)
        {
            FeedSelectPrivacyResponse response = new FeedSelectPrivacyResponse();
            response.Success = false;

            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;

                response = client.SelectFeedPrivacy(requesterUserId, companyId, feedId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToFeedTextPost(FeedUpdateRequest request)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.Success = false;
            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string content = request.Content;

                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                List<string> targetedGroupIds = request.TargetedGroupIds;
                bool isSpecificNotification = false;

                response = client.UpdateToFeedTextPost(requesterUserId, companyId, feedId, content, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification notification in response.TargetedNotifications)
                    {
                        string targetedUserId = notification.TaggedUserId;
                        string notificationText = notification.NotificationText;
                        int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                        int notificationSubType = notification.SubType;
                        PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, feedId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToFeedImagePost(FeedUpdateRequest request)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.Success = false;
            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string caption = request.Caption;
                List<string> updatedImageUrls = request.UpdatedImageUrls;

                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                List<string> targetedGroupIds = request.TargetedGroupIds;
                bool isSpecificNotification = false;

                response = client.UpdateToFeedImagePost(requesterUserId, companyId, feedId, caption, updatedImageUrls, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification notification in response.TargetedNotifications)
                    {
                        string targetedUserId = notification.TaggedUserId;
                        string notificationText = notification.NotificationText;
                        int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                        int notificationSubType = notification.SubType;
                        PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, feedId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToFeedVideoPost(FeedUpdateRequest request)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.Success = false;
            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string caption = request.Caption;

                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                List<string> targetedGroupIds = request.TargetedGroupIds;
                bool isSpecificNotification = false;

                response = client.UpdateToFeedVideoPost(requesterUserId, companyId, feedId, caption, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification notification in response.TargetedNotifications)
                    {
                        string targetedUserId = notification.TaggedUserId;
                        string notificationText = notification.NotificationText;
                        int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                        int notificationSubType = notification.SubType;
                        PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, feedId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToSharedUrlPost(FeedUpdateRequest request)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.Success = false;
            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string caption = request.Caption;

                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                List<string> targetedGroupIds = request.TargetedGroupIds;
                bool isSpecificNotification = false;

                response = client.UpdateToSharedUrlPost(requesterUserId, companyId, feedId, caption, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification notification in response.TargetedNotifications)
                    {
                        string targetedUserId = notification.TaggedUserId;
                        string notificationText = notification.NotificationText;
                        int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                        int notificationSubType = notification.SubType;
                        PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, feedId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectUpVotersResponse SelectVoters(FeedRequest request)
        {
            FeedSelectUpVotersResponse response = new FeedSelectUpVotersResponse();
            response.Success = false;

            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string replyId = request.ReplyId;

                response = client.SelectUpVoters(requesterUserId, companyId, feedId, commentId, replyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedVideoProgressResponse InitFeedVideoProgress(PostFeedRequest request)
        {
            string userId = request.UserId;
            string feedId = request.FeedId;
            string companyId = request.CompanyId;
            List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
            List<string> targetedGroupIds = request.TargetedGroupIds;
            List<string> targetedUserIds = request.TargetedUserIds;
            string caption = request.Caption;
            bool isSpecificNotification = false;
            bool isForAdmin = request.IsForAdmin;
            int feedType = request.FeedType;

            FeedVideoProgressResponse feedVideoProgressResponse = new FeedVideoProgressResponse();
            feedVideoProgressResponse = client.SelectFeedVideoProgress(feedId);
            // this is where you check if db already has the FeedVideoProgress
            // if yes: update 
            // if no: create it 
            FeedVideoProgress feedVideoProgress = feedVideoProgressResponse.feedVideoProgress;

            if (feedVideoProgress != null)
            {
                // update
                feedVideoProgress.UserId = userId;
                feedVideoProgress.Caption = caption;
                feedVideoProgress.TargetedDepartmentIds = targetedDepartmentIds != null ? JsonConvert.SerializeObject(targetedDepartmentIds) : null;
                feedVideoProgress.TargetedGroupIds = targetedGroupIds != null ? JsonConvert.SerializeObject(targetedGroupIds) : null;
                feedVideoProgress.TargetedUserIds = targetedUserIds != null ? JsonConvert.SerializeObject(targetedUserIds) : null;
                feedVideoProgress.IsForAdmin = isForAdmin;
                feedVideoProgress.FeedType = feedType;

                feedVideoProgressResponse = client.UpdateFeedVideoProgress(feedVideoProgress);
            }
            else
            {
                // create
                bool completed = false;
                bool uploaded = true;
                double progress = 0;
                string environment = null;
                string playlistName = null;
                string s3SrcKey = null;
                string s3OutputKeyPrefix = null;

                feedVideoProgressResponse = client.CreateFeedVideoProgress(
                    feedId,
                    playlistName,
                    progress,
                    completed,
                    uploaded,
                    environment,
                    companyId,
                    feedType,
                    s3SrcKey,
                    s3OutputKeyPrefix,
                    targetedDepartmentIds != null ? JsonConvert.SerializeObject(targetedDepartmentIds) : null,
                    targetedGroupIds != null ? JsonConvert.SerializeObject(targetedGroupIds) : null,
                    targetedUserIds != null ? JsonConvert.SerializeObject(targetedUserIds) : null,
                    caption,
                    isSpecificNotification,
                    isForAdmin,
                    userId);
            }

            CalculateVideoProgressUpdatePubNub(feedId);

            return feedVideoProgressResponse;
        }

        public FeedImageProgressResponse InitFeedImageProgress(PostFeedRequest request, bool isVideoThumbnail = false)
        {
            string userId = request.UserId;
            string feedId = request.FeedId;
            List<string> originalImageUrls = request.ImageUrls;
            int expectedCount = request.ExpectedCount;
            string companyId = request.CompanyId;
            List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
            List<string> targetedGroupIds = request.TargetedGroupIds;
            List<string> targetedUserIds = request.TargetedUserIds;
            string caption = request.Caption;
            bool isSpecificNotification = false;
            bool isForAdmin = request.IsForAdmin;
            int feedType = request.FeedType;

            FeedImageProgressResponse feedImageProgressResponse = new FeedImageProgressResponse();

            string regexPattern = @"^(?:.*/feeds/F\w+/)(\d+)(?:_original)";
            var regex = new Regex(regexPattern);

            // for each image
            foreach (var originalImageUrl in originalImageUrls)
            {
                var match = regex.Match(originalImageUrl);
                string imageName = match.Groups[1].Value;

                feedImageProgressResponse = client.SelectFeedImageProgress(feedId, imageName);

                // this is where you check if db already has the FeedImageObject
                // if yes: update expectedCount
                // if no: create it 
                FeedImageProgress feedImageProgress = feedImageProgressResponse.feedImageProgress;

                if (feedImageProgress != null)
                {
                    feedImageProgress.ExpectedCount = expectedCount;
                    feedImageProgress.UserId = userId;
                    feedImageProgress.TargetedDepartmentIds = targetedDepartmentIds != null ? JsonConvert.SerializeObject(targetedDepartmentIds) : null;
                    feedImageProgress.TargetedGroupIds = targetedGroupIds != null ? JsonConvert.SerializeObject(targetedGroupIds) : null;
                    feedImageProgress.TargetedUserIds = targetedUserIds != null ? JsonConvert.SerializeObject(targetedUserIds) : null;
                    feedImageProgress.Caption = caption;
                    feedImageProgress.IsSpecificNotification = isSpecificNotification;
                    feedImageProgress.IsForAdmin = isForAdmin;
                    feedImageProgress.FeedType = feedType;

                    feedImageProgressResponse = client.UpdateFeedImageProgress(feedImageProgress);
                }
                else
                {
                    bool completed = false;
                    bool uploaded = true;
                    double progress = 0;
                    string environment = null;
                    bool isEdit = false;
                    int ordering = 0;

                    // create new
                    feedImageProgressResponse = client.CreateFeedImageProgress(
                        feedId,
                        imageName,
                        progress,
                        completed,
                        uploaded,
                        expectedCount,
                        environment,
                        companyId,
                        feedType,
                        targetedDepartmentIds != null ? JsonConvert.SerializeObject(targetedDepartmentIds) : null,
                        targetedGroupIds != null ? JsonConvert.SerializeObject(targetedGroupIds) : null,
                        targetedUserIds != null ? JsonConvert.SerializeObject(targetedUserIds) : null,
                        caption,
                        isSpecificNotification,
                        isForAdmin,
                        isVideoThumbnail,
                        isEdit,
                        ordering,
                        userId);
                }
            }

            CalculateImageProgressUpdatePubNub(feedId);

            return feedImageProgressResponse;
        }

        public void CalculateVideoProgressUpdatePubNub(string feedId)
        {
            FeedVideoProgressResponse feedVideoProgressResponse = client.SelectFeedVideoProgress(feedId);
            FeedVideoProgress feedVideoProgress =
                feedVideoProgressResponse.feedVideoProgress;

            if (feedVideoProgress != null && feedVideoProgress.UserId != null)
            {
                if (feedVideoProgress.Progress >= 95.0)
                {
                    AWSCredentials credentials = new BasicAWSCredentials(AwsConfig.AccessKey, AwsConfig.SecretKey);
                    IAmazonS3 s3Client = new AmazonS3Client(credentials, Amazon.RegionEndpoint.APSoutheast1);

                    // copy objects from transcoder output to company feed bucket
                    string srcBucket = string.Format("cocadre-elastic-transcoder-output/{0}",
                        feedVideoProgress.S3OutputKeyPrefix);
                    string dstBucket = string.Format("cocadre-{0}/feeds/{1}", feedVideoProgress.CompanyId.ToLower(),
                        feedVideoProgress.FeedId);

                    if (Aws.CopyFolder(srcBucket, dstBucket))
                    {
                        // set video url property
                        feedVideoProgress.VideoUrl = string.Format("{0}/{1}/{2}.m3u8",
                            "http://s3-ap-southeast-1.amazonaws.com",
                            dstBucket,
                            feedVideoProgress.PlaylistName);

                        client.UpdateFeedVideoProgress(feedVideoProgress);

                        // copy image from cocadre-elastic-transcoder-output to cocadre-images-resize-input
                        srcBucket = "cocadre-elastic-transcoder-output";
                        string srcKey = string.Format("{0}{1}_00001.jpg", feedVideoProgress.S3OutputKeyPrefix,
                            feedVideoProgress.PlaylistName);
                        dstBucket = "cocadre-images-resize-input";
                        string dstKey = string.Format("{0}/{1}/feeds/{2}/{3}.jpg", feedVideoProgress.Environment,
                            feedVideoProgress.CompanyId.ToLower(), feedVideoProgress.FeedId,
                            feedVideoProgress.PlaylistName);

                        CopyObjectRequest request = new CopyObjectRequest
                        {
                            SourceBucket = srcBucket,
                            SourceKey = srcKey,
                            DestinationBucket = dstBucket,
                            DestinationKey = dstKey,
                            CannedACL = S3CannedACL.PublicRead
                        };
                        CopyObjectResponse response = s3Client.CopyObject(request);

                        if (response.HttpStatusCode.Equals(HttpStatusCode.OK))
                        {
                            // delete contents from cocadre-elastic-transcoder-output 
                            srcBucket = string.Format("cocadre-elastic-transcoder-output/{0}",
                                feedVideoProgress.S3OutputKeyPrefix);
                            Aws.DeleteFolder(srcBucket);

                            List<string> imageUrls = new List<string>();
                            imageUrls.Add(string.Format("{0}/{1}", dstBucket, dstKey));

                            // send video thumbnail for thumbnailing
                            InitFeedImageProgress(new Feed.PostFeedRequest
                            {
                                UserId = feedVideoProgress.UserId,
                                FeedId = feedVideoProgress.FeedId,
                                Caption = feedVideoProgress.Caption,
                                CompanyId = feedVideoProgress.CompanyId,
                                ExpectedCount = 1,
                                ImageUrls = imageUrls,
                                TargetedDepartmentIds =
                                    feedVideoProgress.TargetedDepartmentIds != null
                                        ? JsonConvert.DeserializeObject<List<string>>(
                                            feedVideoProgress.TargetedDepartmentIds)
                                        : null,
                                TargetedUserIds =
                                    feedVideoProgress.TargetedUserIds != null
                                        ? JsonConvert.DeserializeObject<List<string>>(
                                            feedVideoProgress.TargetedUserIds)
                                        : null,
                                TargetedGroupIds =
                                    feedVideoProgress.TargetedGroupIds != null
                                        ? JsonConvert.DeserializeObject<List<string>>(
                                            feedVideoProgress.TargetedGroupIds)
                                        : null,
                                IsForAdmin = feedVideoProgress.IsForAdmin
                            }, true);
                        }
                    }
                }

                // pubnub live progress update
                Pubnub pubnub = PubnubConfig.ConfigPubnub();

                string channel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, feedVideoProgress.UserId);
                FeedVideoProgressPubnubResponse msg = new FeedVideoProgressPubnubResponse
                {
                    EventName = "FEED.VIDEO_PROGRESS",
                    FeedId = feedId,
                    Progress = (int)feedVideoProgress.Progress,
                    Success = true
                };

                pubnub.Publish<string>(channel, msg, PubnubCallback.PublishToFeedVideoProgressMessage,
                    PubnubCallback.PublishToFeedVideoProgressErrorMessage);
            }
        }

        public void CalculateImageProgressUpdatePubNub(string feedId)
        {
            // retrieve all feedImageProgresses belonging to current feedId
            // using expectedCount, calculate progress
            FeedImageProgressesResponse feedImageProgressesResponse = new FeedImageProgressesResponse();

            feedImageProgressesResponse = client.SelectFeedImageProgresses(feedId);

            IEnumerable<FeedImageProgress> feedImageProgresses = feedImageProgressesResponse.feedImageProgresses;

            var progresses = feedImageProgresses as IList<FeedImageProgress> ?? feedImageProgresses.ToList();
            if (progresses.Any())
            {
                string userId = null;
                string companyId = null;
                string environment = null;
                string caption = null;
                List<string> targetedDepartmentIds = null;
                List<string> targetedUserIds = null;
                List<string> targetedGroupIds = null;
                bool isSpecificNotification = false;
                bool isForAdmin = false;
                bool isVideoThumbnail = false;

                double sumProgress = 0;
                double finalProgress = 0;
                List<string> imageNamesList = new List<string>();
                string eventName = "FEED.THUMBNAIL_PROGRESS";

                // add up all the progresses and divide by expected Count
                foreach (var feedImageProgress in progresses)
                {
                    sumProgress += feedImageProgress.Progress;

                    // final progress
                    finalProgress = (sumProgress / feedImageProgress.ExpectedCount) - 5; // final Progress will be at max 95%, 5% comes from s3 moving and creation of entity in db

                    if (finalProgress < 0)
                    {
                        finalProgress = 0;
                    }

                    // check if userId exists (required to publish to pubnub user channel)
                    if (feedImageProgress.UserId != null)
                    {
                        userId = feedImageProgress.UserId;
                    }

                    // retrieve properties
                    companyId = feedImageProgress.CompanyId;
                    environment = feedImageProgress.Environment;
                    caption = feedImageProgress.Caption;
                    isSpecificNotification = feedImageProgress.IsSpecificNotification;
                    isForAdmin = feedImageProgress.IsForAdmin;
                    isVideoThumbnail = feedImageProgress.IsVideoThumbnail;

                    if (!string.IsNullOrEmpty(feedImageProgress.TargetedDepartmentIds))
                    {
                        targetedDepartmentIds =
                            JsonConvert.DeserializeObject<List<string>>(feedImageProgress.TargetedDepartmentIds);
                    }

                    if (!string.IsNullOrEmpty(feedImageProgress.TargetedGroupIds))
                    {
                        targetedGroupIds =
                            JsonConvert.DeserializeObject<List<string>>(feedImageProgress.TargetedGroupIds);
                    }

                    if (!string.IsNullOrEmpty(feedImageProgress.TargetedUserIds))
                    {
                        targetedUserIds =
                            JsonConvert.DeserializeObject<List<string>>(feedImageProgress.TargetedUserIds);
                    }

                    imageNamesList.Add(feedImageProgress.ImageName);
                }

                if (userId != null)
                {
                    // pubnub live progress update
                    Pubnub pubnub = PubnubConfig.ConfigPubnub();

                    string channel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);
                    FeedImageProgressPubnubResponse msg = new FeedImageProgressPubnubResponse
                    {
                        EventName = "FEED.THUMBNAIL_PROGRESS",
                        FeedId = feedId,
                        ImageNamesList = imageNamesList,
                        Progress = (int)finalProgress,
                        Success = true
                    };

                    pubnub.Publish<string>(channel, msg, PubnubCallback.PublishToFeedImageProgressMessage,
                        PubnubCallback.PublishToFeedImageProgressErrorMessage);

                    if (finalProgress >= 95.0)
                    {
                        bool moveImages = true;

                        // make sure all are completed before moving
                        foreach (var feedImageProgress in progresses)
                        {
                            if (feedImageProgress.Completed != true)
                            {
                                moveImages = false;
                                break;
                            }
                        }

                        if (moveImages)
                        {
                            MoveFeedImagesToBucket(imageNamesList, environment, companyId, feedId, userId, caption, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isForAdmin, isSpecificNotification, finalProgress, isVideoThumbnail);
                        }
                    }
                }
            }
        }

        private void MoveFeedImagesToBucket(List<string> imageNamesList, string environment, string companyId, string feedId, string userId, string caption, List<string> targetedDepartmentIds, List<string> targetedUserIds, List<string> targetedGroupIds, bool isForAdmin, bool isSpecificNotification, double finalProgress, bool isVideoThumbnail = false)
        {
            AWSCredentials credentials = new BasicAWSCredentials(AwsConfig.AccessKey, AwsConfig.SecretKey);
            IAmazonS3 s3Client = new AmazonS3Client(credentials, Amazon.RegionEndpoint.APSoutheast1);

            try
            {
                string[] suffix = { "small", "medium", "large" };
                List<string> imageUrls = new List<string>();

                // move images from s3 cocadre-images-resize-output/env:/companyId:/feeds/feedId:/ to
                // cocadre-companyId:/feeds/feedId:/
                for (var h = 0; h < imageNamesList.Count; h++)
                {
                    string imageName = imageNamesList[h];

                    for (var i = 0; i < suffix.Length; i++)
                    {
                        string srcBucket = "cocadre-images-resize-output";
                        string srcKey = string.Format("{0}/{1}/feeds/{2}/{3}_{4}.jpg", environment, companyId.ToLower(),
                            feedId, imageName, suffix[i]);
                        string dstBucket = string.Format("cocadre-{0}", companyId.ToLower());
                        string dstKey = string.Format("feeds/{0}/{1}_{2}.jpg", feedId, imageName, suffix[i]);

                        CopyObjectRequest request = new CopyObjectRequest
                        {
                            SourceBucket = srcBucket,
                            SourceKey = srcKey,
                            DestinationBucket = dstBucket,
                            DestinationKey = dstKey,
                            CannedACL = S3CannedACL.PublicRead
                        };
                        CopyObjectResponse response = s3Client.CopyObject(request);

                        if (response.HttpStatusCode.Equals(HttpStatusCode.OK))
                        {
                            // successful

                            // delete 
                            DeleteObjectRequest deleteRequest = new DeleteObjectRequest
                            {
                                BucketName = srcBucket,
                                Key = srcKey
                            };
                            s3Client.DeleteObject(deleteRequest);

                            if (i >= suffix.Length - 1) // last
                            {
                                // copy original from input bucket
                                srcBucket = "cocadre-images-resize-input";
                                srcKey = string.Format("{0}/{1}/feeds/{2}/{3}_{4}.jpg", environment, companyId.ToLower(), feedId, imageName, "original");
                                dstKey = string.Format("feeds/{0}/{1}_{2}.jpg", feedId, imageName, "original");

                                imageUrls.Add(string.Format("{0}/{1}/{2}", "http://s3-ap-southeast-1.amazonaws.com", dstBucket, dstKey));

                                request = new CopyObjectRequest
                                {
                                    SourceBucket = srcBucket,
                                    SourceKey = srcKey,
                                    DestinationBucket = dstBucket,
                                    DestinationKey = dstKey,
                                    CannedACL = S3CannedACL.PublicRead
                                };
                                response = s3Client.CopyObject(request);

                                // delete 
                                deleteRequest = new DeleteObjectRequest
                                {
                                    BucketName = srcBucket,
                                    Key = srcKey
                                };
                                s3Client.DeleteObject(deleteRequest);

                                if (response.HttpStatusCode.Equals(HttpStatusCode.OK) && h >= imageNamesList.Count - 1)
                                {
                                    // reached the last image (not last suffix), create the feed image post
                                    if (!isVideoThumbnail)
                                    {
                                        FeedCreateResponse feedCreateResponse = client.CreateFeedImagePost(feedId,
                                        userId, companyId, caption, imageUrls, targetedDepartmentIds, targetedUserIds,
                                        targetedGroupIds, isForAdmin, isSpecificNotification);

                                        if (feedCreateResponse.Success)
                                        {
                                            finalProgress = 100.0;

                                            // pubnub live progress update
                                            Pubnub pubnub = PubnubConfig.ConfigPubnub();

                                            string channel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                                            FeedImageProgressPubnubResponse msg = new FeedImageProgressPubnubResponse
                                            {
                                                EventName = "FEED.THUMBNAIL_PROGRESS",
                                                FeedId = feedId,
                                                ImageNamesList = imageNamesList,
                                                Progress = (int)finalProgress,
                                                Success = true
                                            };

                                            pubnub.Publish<string>(channel, msg,
                                            PubnubCallback.PublishToFeedImageProgressMessage,
                                            PubnubCallback.PublishToFeedImageProgressErrorMessage);
                                        }
                                    }

                                    else
                                    {
                                        FeedVideoProgressResponse feedVideoProgressResponse = client.SelectFeedVideoProgress(feedId);
                                        FeedVideoProgress feedVideoProgress =
                                            feedVideoProgressResponse.feedVideoProgress;

                                        if (feedVideoProgress != null)
                                        {
                                            FeedCreateResponse feedCreateResponse = client.CreateFeedVideoPost(feedId, userId, companyId, caption, feedVideoProgress.VideoUrl, imageUrls[0], targetedDepartmentIds, targetedUserIds, targetedGroupIds, isForAdmin, isSpecificNotification);

                                            if (feedCreateResponse.Success)
                                            {
                                                finalProgress = 100.0;
                                                feedVideoProgress.Progress = finalProgress;

                                                // pubnub live progress update
                                                Pubnub pubnub = PubnubConfig.ConfigPubnub();

                                                string channel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                                                FeedVideoProgressPubnubResponse msg = new FeedVideoProgressPubnubResponse
                                                {
                                                    EventName = "FEED.VIDEO_PROGRESS",
                                                    FeedId = feedId,
                                                    Progress = (int)finalProgress,
                                                    Success = true
                                                };

                                                pubnub.Publish<string>(channel, msg,
                                                PubnubCallback.PublishToFeedVideoProgressMessage,
                                                PubnubCallback.PublishToFeedVideoProgressErrorMessage);

                                                client.UpdateFeedVideoProgress(feedVideoProgress);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (AmazonS3Exception s3Exception)
            {
                Console.WriteLine(s3Exception.Message,
                                  s3Exception.InnerException);
            }
        }

        // Requests
        public class FeedRequest
        {
            public string UserId { get; set; }
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string ReplyId { get; set; }
        }

        public class GetCompanyFeedRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string SearchContent { get; set; }
            public int NumberOfPostsLoaded { get; set; }
            public DateTime? NewestTimestamp { get; set; }
            public DateTime? OldestTimestamp { get; set; }
        }

        public class GetPersonnelFeedRequest
        {
            public string RequesterUserId { get; set; }
            public string OwnerUserId { get; set; }
            public string CompanyId { get; set; }
            public string SearchContent { get; set; }
            public DateTime? NewestTimestamp { get; set; }
            public DateTime? OldestTimestamp { get; set; }
        }

        public class PostFeedRequest
        {
            public string FeedId { get; set; }
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public List<string> TargetedDepartmentIds { get; set; }
            public List<string> TargetedUserIds { get; set; }
            public List<string> TargetedGroupIds { get; set; }
            public bool IsForAdmin { get; set; }
            public int FeedType { get; set; }

            public string Content { get; set; }

            public string Caption { get; set; }

            public List<string> ImageUrls { get; set; }
            public int ExpectedCount { get; set; }

            public string VideoUrl { get; set; }
            public string VideoThumbnailUrl { get; set; }

            public string Url { get; set; }
            public string UrlTitle { get; set; }
            public string UrlDescription { get; set; }
            public string UrlSiteName { get; set; }
            public string UrlImageUrl { get; set; }
        }

        public class PostCommentRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string Content { get; set; }
        }

        public class PostReplyRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string Content { get; set; }
        }

        public class ReportFeedRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string ReplyId { get; set; }
            public string Reason { get; set; }

            public bool IsReportPost { get; set; }
            public bool IsReportComment { get; set; }
            public bool IsReportReply { get; set; }
        }

        public class VoteFeedRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public bool IsVoteFeed { get; set; }
            public bool IsVoteComment { get; set; }
            public bool IsVoteReply { get; set; }
            public bool IsUpVote { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string ReplyId { get; set; }
        }

        public class FeedPrivacyRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
        }

        public class FeedUpdateRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string Content { get; set; }
            public string Caption { get; set; }
            public int FeedType { get; set; }
            public List<string> TargetedDepartmentIds { get; set; }
            public List<string> TargetedUserIds { get; set; }
            public List<string> TargetedGroupIds { get; set; }
            public List<string> UpdatedImageUrls { get; set; }
        }
    }
}