﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v5
{
    public class Pulse
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public PulseSelectAllResponse GetPulses(PulseRequest request)
        {
            PulseSelectAllResponse response = new PulseSelectAllResponse();
            response.Success = false;

            try
            {
                response = client.SelectAllByUser(request.RequesterUserId,
                                                  request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public PulseUpdateSingleStatsResponse UpdateAction(PulseRequest request)
        {
            PulseUpdateSingleStatsResponse response = new PulseUpdateSingleStatsResponse();
            response.Success = false;

            try
            {
                response = client.UpdatePulseSingleAction(request.RequesterUserId,
                                                        request.CompanyId,
                                                        request.PulseId,
                                                        request.Action);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public PulseUpdateImpressionResponse UpdateImpression(PulseRequest request)
        {
            PulseUpdateImpressionResponse response = new PulseUpdateImpressionResponse();
            response.Success = false;

            try
            {
                response = client.UpdatePulseImpression(request.RequesterUserId,
                                                        request.CompanyId,
                                                        request.PulseId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public PulseSelectAllResponse GetInbox(PulseRequest request)
        {
            PulseSelectAllResponse response = new PulseSelectAllResponse();
            response.Success = false;

            try
            {
                response = client.SelectFromInbox(request.RequesterUserId,
                                                  request.CompanyId,
                                                  request.LastDrawnTimestamp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public PulseSelectAllResponse GetTrashbin(PulseRequest request)
        {
            PulseSelectAllResponse response = new PulseSelectAllResponse();
            response.Success = false;

            try
            {
                response = client.SelectFromTrashbin(request.RequesterUserId,
                                                     request.CompanyId,
                                                     request.LastDrawnTimestamp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public PulseUpdateResponse MoveToTrashBin(PulseRequest request)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;

            try
            {
                response = client.MoveToTrashBin(request.PulseId,
                                                 request.PulseType,
                                                 request.RequesterUserId,
                                                 request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public PulseUpdateResponse RestoreToInbox(PulseRequest request)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;

            try
            {
                response = client.RestoreToInbox(request.PulseId,
                                                 request.PulseType,
                                                 request.RequesterUserId,
                                                 request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public PulseUpdateResponse RemoveFromTrashBin(PulseRequest request)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;

            try
            {
                response = client.RemoveFromTrashBin(request.PulseId,
                                                     request.PulseType,
                                                     request.RequesterUserId,
                                                     request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }
        public PulseAnswerCardResponse AnswerCard(DeckRequest request)
        {
            try
            {
                PulseAnswerCardResponse response = new PulseAnswerCardResponse();
                response.Success = false;

                try
                {
                    response = client.AnswerCard(request.AnsweredByUserId,
                                                 request.CompanyId,
                                                 request.DeckId,
                                                 request.PulseId,
                                                 request.SelectedRange,
                                                 request.SelectedOptionId,
                                                 request.CustomAnswer);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        // Requests
        public class PulseRequest
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
            public string PulseId { get; set; }
            public int PulseType { get; set; }
            public int Action { get; set; }
            public DateTime? LastDrawnTimestamp { get; set; }
        }

        public class DeckRequest
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }           
            public string PulseId { get; set; }
            public string DeckId { get; set; }
           
            public string AnsweredByUserId { get; set; }
            public int SelectedRange { get; set; }
            public string SelectedOptionId { get; set; }
            public string CustomAnswer { get; set; }
        }
    }
}