﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v5
{
    public class Event
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public EventSelectResponse GetEvent(EventRequest request)
        {
            EventSelectResponse response = new EventSelectResponse();
            response.Success = false;

            try
            {
                Log.Debug("V2");
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string eventId = request.EventId;

                response = client.SelectEventDetail(requesterUserId, eventId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public class EventRequest
        {
            public string EventId { get; set; }
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
        }
    }
}