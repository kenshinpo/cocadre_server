﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using PubNubMessaging.Core;
using Resources;
using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Web.Configuration;
using CocadreMVCWebAPI.Models.Service;

namespace CocadreMVCWebAPI.Models.v5
{
    public class Account
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public UserSelectWithCompanyResponse GetAccount(AccountRequest request)
        {
            UserSelectWithCompanyResponse response = new UserSelectWithCompanyResponse();
            response.Success = false;

            try
            {
                Pubnub pubnub = PubnubConfig.ConfigPubnub();
                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = client.SelectUserWithCompany(userId, companyId);

                if (response.Success)
                {
                    string groupChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, userId);
                    string companyChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixCompany, companyId);
                    string clientChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    pubnub.ChannelGroupGrantPresenceAccess<string>(groupChannelName, true, true, PubnubCallback.GrantClientAccessToChannelMessage, PubnubCallback.GrantClientAccessToChannelErrorMessage);
                    pubnub.ChannelGroupGrantAccess<string>(groupChannelName, true, true, PubnubCallback.GrantClientAccessToChannelMessage, PubnubCallback.GrantClientAccessToChannelErrorMessage);

                    foreach (Department department in response.User.Departments)
                    {
                        string departmentChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixDepartment, department.Id);
                        pubnub.AddChannelsToChannelGroup<string>(new string[] { departmentChannelName }, groupChannelName, PubnubCallback.AddChannelToGroupMessage, PubnubCallback.AddChannelToGroupErrorMessage);
                    }

                    pubnub.AddChannelsToChannelGroup<string>(new string[] { companyChannelName, clientChannelName }, groupChannelName, PubnubCallback.AddChannelToGroupMessage, PubnubCallback.AddChannelToGroupErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        //public GetCognitoTokenResponse GetCognitoToken(AccountRequest request)
        //{
        //    GetCognitoTokenResponse response = new GetCognitoTokenResponse();
        //    response.Success = false;

        //    try
        //    {
        //        if (String.IsNullOrEmpty(request.CompanyId))
        //        {
        //            Log.Warn("CompanyId parameter is null or empty.");
        //            response.ErrorCode = 999;
        //            response.ErrorMessage = "CompanyId parameter is null or empty.";
        //            return response;
        //        }

        //        if (String.IsNullOrEmpty(request.UserId))
        //        {
        //            Log.Warn("UserId parameter is null or empty.");
        //            response.ErrorCode = 999;
        //            response.ErrorMessage = "UserId parameter is null or empty.";
        //            return response;
        //        }

        //        AWSCredentials credentials = new BasicAWSCredentials(AwsConfig.AccessKey, AwsConfig.SecretKey);
        //        AmazonCognitoIdentityClient client = new AmazonCognitoIdentityClient(credentials, RegionEndpoint.APNortheast1);

        //        Dictionary<string, string> dic = new Dictionary<string, string>();
        //        dic.Add(AwsConfig.CognitoLinkedLogin, request.CompanyId + ":" + request.UserId);

        //        GetOpenIdTokenForDeveloperIdentityRequest identityRequest = new GetOpenIdTokenForDeveloperIdentityRequest();
        //        identityRequest.IdentityPoolId = AwsConfig.CognitoIdentityPoolId;
        //        identityRequest.Logins = dic;
        //        identityRequest.TokenDuration = Convert.ToInt64(AwsConfig.CognitoTokenDuration);
        //        GetOpenIdTokenForDeveloperIdentityResponse identityResponse = client.GetOpenIdTokenForDeveloperIdentity(identityRequest);

        //        response = new GetCognitoTokenResponse
        //        {
        //            Success = true,
        //            Token = identityResponse.Token,
        //            RoleArn = WebConfigurationManager.AppSettings["AwsCognitoRoleArn"].ToString(),
        //            IdentityId = identityResponse.IdentityId
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex.ToString(), ex);
        //        response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
        //        response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
        //    }

        //    return response;
        //}

        public UserSelectAllByDepartmentResponse GetColleagues(AccountRequest request)
        {
            UserSelectAllByDepartmentResponse response = new UserSelectAllByDepartmentResponse();
            response.Success = false;

            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;

                response = client.SelectAllUsersByDepartment(requesterUserId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationSelectUserResponse GoLoginAccount(LoginRequest request)
        {
            AuthenticationSelectUserResponse response = new AuthenticationSelectUserResponse();
            response.Success = false;

            try
            {
                string email = request.Email;
                string encryptedPassword = request.EncryptedPassword;

                response = client.AuthenticateEmailForLogin(email, encryptedPassword);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationSelectUserByCompanyResponse GoLoginCompany(LoginRequest request)
        {
            AuthenticationSelectUserByCompanyResponse response = new AuthenticationSelectUserByCompanyResponse();
            response.Success = false;

            try
            {
                string encryptedPassword = request.EncryptedPassword;
                string companyId = request.CompanyId;
                string userId = request.UserId;
                string currentToken = request.CurrentAuthenticationToken;

                response = client.AuthenticateLoginByCompany(companyId, userId, currentToken, encryptedPassword);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserUpdateLoginResponse GoLogoutAccount(AccountRequest request)
        {
            UserUpdateLoginResponse response = new UserUpdateLoginResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = client.UpdateUserLogin(userId, companyId, false);

                if (response.Success)
                {
                    Pubnub pubnub = PubnubConfig.ConfigPubnub();

                    string groupChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, userId);
                    string companyChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixCompany, companyId);
                    string clientChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { companyChannelName, clientChannelName }, groupChannelName, PubnubCallback.RemoveChannelMessage, PubnubCallback.RemoveChannelErrorMessage);
                    pubnub.RemoveChannelGroup<string>(string.Empty, groupChannelName, PubnubCallback.RemoveGroupMessage, PubnubCallback.RemoveGroupErrorMessage);

                    if (!string.IsNullOrEmpty(response.DeviceToken) && response.DeviceType != 0)
                    {
                        PushTypeService pushType = PushTypeService.APNS;

                        int deviceType = response.DeviceType;
                        string deviceToken = response.DeviceToken;

                        if (deviceType == 2)
                        {
                            pushType = PushTypeService.GCM;
                        }

                        pubnub.UnregisterDeviceForPush<string>(pushType, deviceToken, PubnubCallback.UnregisterDeviceForPushMessage, PubnubCallback.UnregisterDeviceForPushErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserUpdateDeviceTokenResponse UpdateDeviceToken(UpdateDeviceTokenRequest request)
        {
            UserUpdateDeviceTokenResponse response = new UserUpdateDeviceTokenResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string newDeviceToken = request.DeviceToken;
                int newDeviceType = request.DeviceType;

                response = client.UpdateUserDeviceToken(userId, companyId, newDeviceToken, newDeviceType);

                if (response.Success)
                {
                    Pubnub pubnub = PubnubConfig.ConfigPubnub();

                    string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    PushTypeService pushType = PushTypeService.APNS;

                    User user = new User();

                    if (newDeviceType == 2)
                    {
                        pushType = PushTypeService.GCM;
                    }

                    pubnub.RegisterDeviceForPush<string>(clientChannel, pushType, newDeviceToken, PubnubCallback.RegisterDeviceForPushMessage, PubnubCallback.RegisterDeviceForPushErrorMessage);

                    if (!string.IsNullOrEmpty(response.OutdatedDeviceToken) && !response.OutdatedDeviceToken.Equals(request.DeviceToken))
                    {
                        PushTypeService outdatedPushType = PushTypeService.APNS;

                        int outdatedDeviceType = response.OutdatedDeviceType;
                        string outdatedDeviceToken = response.OutdatedDeviceToken;

                        if (outdatedDeviceType == 2)
                        {
                            outdatedPushType = PushTypeService.GCM;
                        }

                        pubnub.UnregisterDeviceForPush<string>(outdatedPushType, outdatedDeviceToken, PubnubCallback.UnregisterDeviceForPushMessage, PubnubCallback.UnregisterDeviceForPushErrorMessage);
                    }

                    response.OutdatedDeviceToken = null;
                    response.OutdatedDeviceType = 0;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationUpdateResponse UpdatePassword(UpdatePasswordRequest request)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string newPassword = request.NewPassword;
                string email = request.Email;

                if (!string.IsNullOrEmpty(userId))
                {
                    response = client.UpdatePasswordForUser(userId, email, companyId, newPassword);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationUpdateResponse ForgetPassword(UpdatePasswordRequest request)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = false;

            try
            {
                string email = request.Email;

                response = client.ResetPasswordForEmail(email);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserUploadProfileImageResponse UploadProfileImage(UploadProfileImageRequest request)
        {
            UserUploadProfileImageResponse response = new UserUploadProfileImageResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string newProfileImageUrl = request.NewProfileImageUrl;

                response = client.UploadProfileImage(userId, companyId, newProfileImageUrl, (int)Dashboard.ProfileApprovalState.Pending);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserImageProgressResponse UpdateProfileImage(UploadProfileImageRequest request)
        {
            UserImageProgressResponse userImageProgressResponse = new UserImageProgressResponse();
            userImageProgressResponse.Success = false;

            // here check if db already has the UserImageProgress
            userImageProgressResponse = client.SelectUserImageProgress(request.RequesterUserId,
                request.ImageName);

            UserImageProgress userImageProgress = userImageProgressResponse.userImageProgress;

            try
            {
                if (userImageProgress != null)
                {
                    // existing
                    // update userid, company id
                    userImageProgress.UserId = request.RequesterUserId;
                    userImageProgress.CompanyId = request.CompanyId;

                    userImageProgressResponse = client.UpdateUserImageProgress(userImageProgress);
                }
                else
                {
                    // create new
                    double progress = 0.0;
                    bool uploaded = true;
                    bool completed = false;

                    userImageProgressResponse = client.CreateUserImageProgress(request.RequesterUserId, request.ImageName,
                        progress, completed, uploaded, null, request.CompanyId);
                }

                userImageProgress = userImageProgressResponse.userImageProgress;
                CalculateUserProgressUpdatePubNub(userImageProgress.UserId, userImageProgress.ImageName);

                #region

                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                userImageProgressResponse.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                userImageProgressResponse.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return userImageProgressResponse;
        }

        public UserSelectProfileResponse UpdateUserProfile(UpdateUserProfileRequest request)
        {
            UserSelectProfileResponse response = new UserSelectProfileResponse();
            response.Success = false;

            try
            {
                response = client.UpdateForClient(request.CompanyId, request.UserId, request.FirstName, request.LastName, request.Email, request.Position, request.PhoneNumber, request.PhoneCountryCode, request.PhoneCountryAbb, request.Address, request.AddressCountryName, request.PostalCode, request.DepartmentTitle, request.Gender, request.Birthday);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public void CalculateUserProgressUpdatePubNub(string userId, string imageName)
        {
            UserImageProgressResponse userImageProgressResponse = client.SelectUserImageProgress(userId, imageName);
            UserImageProgress userImageProgress = userImageProgressResponse.userImageProgress;

            if (userImageProgress != null && userImageProgress.UserId != null)
            {
                // pubnub live progress update
                Pubnub pubnub = PubnubConfig.ConfigPubnub();

                string channel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userImageProgress.UserId);
                UserImageProgressPubnubResponse msg = new UserImageProgressPubnubResponse
                {
                    EventName = "PROFILE.THUMBNAIL_PROGRESS",
                    Progress = userImageProgress.Progress >= 100.0 ? (int)(userImageProgress.Progress - 5.0) : (int)userImageProgress.Progress, // save 5% for moving the images in s3
                    Success = true
                };

                pubnub.Publish<string>(channel, msg, PubnubCallback.PublishToFeedVideoProgressMessage,
                    PubnubCallback.PublishToFeedVideoProgressErrorMessage);

                if (userImageProgress.Progress >= 100.0 && userImageProgress.Completed)
                {
                    // here, we need to 
                    // 1) move the images to the right place and
                    // 2) publish 100% to pub nub
                    string srcBucket = string.Format("{0}/{1}/{2}/users/{3}/", "cocadre-images-resize-output",
                        userImageProgress.Environment, userImageProgress.CompanyId.ToLower(),
                        userImageProgress.UserId);
                    string dstBucket = string.Format("cocadre-{0}/users/{1}", userImageProgress.CompanyId.ToLower(),
                        userImageProgress.UserId);

                    if (Aws.CopyFolder(srcBucket, dstBucket, true))
                    {
                        // copy original from input bucket
                        srcBucket = string.Format("{0}/{1}/{2}/users/{3}/", "cocadre-images-resize-input",
                            userImageProgress.Environment, userImageProgress.CompanyId.ToLower(),
                            userImageProgress.UserId);

                        Aws.CopyFolder(srcBucket, dstBucket, true);

                        string newProfileImageUrl = string.Format("{0}/{1}/{2}_original.jpg",
                            "http://s3-ap-southeast-1.amazonaws.com", dstBucket, userImageProgress.ImageName);

                        UserUploadProfileImageResponse response = client.UploadProfileImage(userImageProgress.UserId, userImageProgress.CompanyId,
                            newProfileImageUrl, (int)Dashboard.ProfileApprovalState.Pending);

                        msg = new UserImageProgressPubnubResponse
                        {
                            EventName = "PROFILE.THUMBNAIL_PROGRESS",
                            Progress = (int)100.0,
                            Success = true,
                            UserDetails = response.UserDetail
                        };

                        pubnub.Publish<string>(channel, msg, PubnubCallback.PublishToUserImageProgressMessage,
                            PubnubCallback.PublishToUserImageProgressErrorMessage);
                    }
                }
            }
        }

        public AuthenticationSelectCompanyResponse GoSwitchCompany(AccountRequest request)
        {
            AuthenticationSelectCompanyResponse response = new AuthenticationSelectCompanyResponse();
            response.Success = false;

            try
            {
                string companyId = request.CompanyId;
                string userId = request.UserId;

                response = client.SwitchCompanyByUser(userId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class AccountRequest
        {
            public string RequesterUserId { get; set; }
            public string UserId { get; set; }
            public string CompanyId { get; set; }
        }

        public class LoginRequest
        {
            public string Email { get; set; }
            public string EncryptedPassword { get; set; }
            public string CompanyId { get; set; }
            public string UserId { get; set; }
            public string CurrentAuthenticationToken { get; set; }
        }

        public class UpdateDeviceTokenRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string DeviceToken { get; set; }
            public int DeviceType { get; set; }
        }

        public class UpdatePasswordRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string NewPassword { get; set; }
            public string Email { get; set; }
        }

        public class UploadProfileImageRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string NewProfileImageUrl { get; set; }
            public string ImageName { get; set; }
        }

        public class UpdateUserProfileRequest
        {
            public string CompanyId { get; set; }
            public string UserId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string DepartmentTitle { get; set; }
            public string Position { get; set; }
            public string PhoneNumber { get; set; }
            public string PhoneCountryCode { get; set; }
            public string PhoneCountryAbb { get; set; }
            public string Address { get; set; }
            public string AddressCountryName { get; set; }
            public string PostalCode { get; set; }
            public int Gender { get; set; }
            public DateTime Birthday { get; set; }

        }

        // Responses
        [DataContract]
        public class GetCognitoTokenResponse : ErrorResponse
        {
            [DataMember]
            public String Token { get; set; }
            [DataMember]
            public String RoleArn { get; set; }
            [DataMember]
            public String IdentityId { get; set; }
            [DataMember]
            public String IdentityPoolId { get; set; }
        }
    }
}