﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v3
{
    public class Leaderboard
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public AnalyticsSelectLeaderboardByCompanyResponse GetLeaderboardByCompany(LeaderboardRequest request)
        {
            AnalyticsSelectLeaderboardByCompanyResponse response = new AnalyticsSelectLeaderboardByCompanyResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = client.SelectLeaderboardByCompany(userId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsSelectLeaderboardByDepartmentResponse GetLeaderboardByDepartment(LeaderboardRequest request)
        {
            AnalyticsSelectLeaderboardByDepartmentResponse response = new AnalyticsSelectLeaderboardByDepartmentResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = client.SelectLeaderboardByDepartment(userId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class LeaderboardRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
        }
    }
}