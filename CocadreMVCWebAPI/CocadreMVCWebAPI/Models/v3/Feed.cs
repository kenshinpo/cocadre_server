﻿using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v3
{
    public class Feed
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public FeedSelectResponse GetCompanyFeed(GetCompanyFeedRequest request)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string searchContent = request.SearchContent;
                DateTime? newestTimestamp = request.NewestTimestamp;
                DateTime? oldestTimestamp = request.OldestTimestamp;

                response = client.SelectCompanyFeedPost(userId, companyId, searchContent, 0, newestTimestamp, oldestTimestamp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectResponse GetPersonnelFeed(GetPersonnelFeedRequest request)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Success = false;

            try
            {
                string requesterUserId = request.RequesterUserId;
                string ownerUserId = request.OwnerUserId;
                string companyId = request.CompanyId;
                string searchContent = request.SearchContent;
                DateTime? newestTimestamp = request.NewestTimestamp;
                DateTime? oldestTimestamp = request.OldestTimestamp;

                response = client.SelectPersonnelFeedPost(requesterUserId, ownerUserId, companyId, searchContent, newestTimestamp, oldestTimestamp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public CommentSelectResponse GetComment(FeedRequest request)
        {
            CommentSelectResponse response = new CommentSelectResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;

                response = client.SelectFeedComment(userId, feedId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public ReplySelectResponse GetReply(FeedRequest request)
        {
            ReplySelectResponse response = new ReplySelectResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string commentId = request.CommentId;
                string feedId = request.FeedId;

                response = client.SelectCommentReply(userId, feedId, commentId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedAuthenticationResponse NewPostAuthentication(FeedRequest request)
        {
            FeedAuthenticationResponse response = new FeedAuthenticationResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = client.AuthenticateUserForPostingFeed(companyId, userId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }
            return response;
        }

        public FeedCreateResponse PostFeed(PostFeedRequest request)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.Success = false;

            try
            {
                string feedId = request.FeedId;
                int feedType = request.FeedType;
                string userId = request.UserId;
                string companyId = request.CompanyId;
                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                List<string> targetedGroupIds = request.TargetedGroupIds;
                bool isForAdmin = request.IsForAdmin;

                if (targetedDepartmentIds != null)
                {
                    Log.Debug("Count for targetedDepartmentIds: " + targetedDepartmentIds.Count);
                }

                if (targetedUserIds != null)
                {
                    Log.Debug("Count for targetedUserIds: " + targetedUserIds.Count);
                }

                if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                {
                    string content = request.Content;
                    response = client.CreateFeedTextPost(feedId, userId, companyId, content, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isForAdmin);
                }
                else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                {
                    string caption = request.Caption;
                    List<string> imageUrls = request.ImageUrls;

                    response = client.CreateFeedImagePost(feedId, userId, companyId, caption, imageUrls, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isForAdmin);

                }
                else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                {
                    string caption = request.Caption;
                    string videoUrl = request.VideoUrl;
                    string videoThumbnailUrl = request.VideoThumbnailUrl;
                    response = client.CreateFeedVideoPost(feedId, userId, companyId, caption, videoUrl, videoThumbnailUrl, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isForAdmin);
                }
                else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                {
                    string caption = request.Caption;
                    string url = request.Url;
                    string urlTitle = request.UrlTitle;
                    string urlDescription = request.UrlDescription;
                    string urlSiteName = request.UrlSiteName;
                    string urlImageUrl = request.UrlImageUrl;
                    response = client.CreateFeedSharedUrlPost(feedId, userId, companyId, caption, url, urlTitle, urlDescription, urlSiteName, urlImageUrl, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isForAdmin);
                }

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification notification in response.TargetedNotifications)
                    {
                        string targetedUserId = notification.TaggedUserId;
                        string notificationText = notification.NotificationText;
                        int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                        int notificationSubType = notification.SubType;
                        PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, response.FeedId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public CommentCreateResponse PostComment(PostCommentRequest request)
        {
            CommentCreateResponse response = new CommentCreateResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string content = request.Content;

                response = client.CreateCommentText(userId, companyId, feedId, content);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification serviceNotification in response.Notifications)
                    {
                        PushNotification.PushFeedNotification(serviceNotification.TaggedUserId, serviceNotification.NotificationText, serviceNotification.NumberOfNotificationForTargetedUser, serviceNotification.Type, serviceNotification.SubType, serviceNotification.TaggedFeedId, serviceNotification.TaggedCommentId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public ReplyCreateResponse PostReply(PostReplyRequest request)
        {
            ReplyCreateResponse response = new ReplyCreateResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string content = request.Content;

                response = client.CreateReplyText(userId, companyId, feedId, commentId, content);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification serviceNotification in response.Notifications)
                    {
                        PushNotification.PushFeedNotification(serviceNotification.TaggedUserId, serviceNotification.NotificationText, serviceNotification.NumberOfNotificationForTargetedUser, serviceNotification.Type, serviceNotification.SubType, serviceNotification.TaggedFeedId, serviceNotification.TaggedCommentId, serviceNotification.TaggedReplyId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteFeed(FeedRequest request)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;

                response = client.DeleteFeed(feedId, companyId, userId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteComment(FeedRequest request)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;

                response = client.DeleteComment(feedId, commentId, companyId, userId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteReply(FeedRequest request)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string replyId = request.ReplyId;

                response = client.DeleteReply(feedId, commentId, replyId, companyId, userId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public DashboardReportResponse ReportFeed(ReportFeedRequest request)
        {
            DashboardReportResponse response = new DashboardReportResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string replyId = request.ReplyId;
                string reason = request.Reason;

                bool isReportPost = request.IsReportPost;
                bool isReportComment = request.IsReportComment;
                bool isReportReply = request.IsReportReply;

                if (isReportPost)
                {
                    response = client.ReportFeed(userId, reason, companyId, feedId, null, null, (int)Dashboard.ReportType.Feed);
                }
                else if (isReportComment)
                {
                    response = client.ReportFeed(userId, reason, companyId, feedId, commentId, null, (int)Dashboard.ReportType.Comment);
                }
                else if (isReportReply)
                {
                    response = client.ReportFeed(userId, reason, companyId, feedId, commentId, replyId, (int)Dashboard.ReportType.Reply);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public PointUpdateResponse UpdateVote(VoteFeedRequest request)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;

                bool isUpVote = request.IsUpVote;

                bool isVoteFeed = request.IsVoteFeed;
                bool isVoteComment = request.IsVoteComment;
                bool isVoteReply = request.IsVoteReply;

                if (isVoteFeed)
                {
                    string feedId = request.FeedId;
                    response = client.UpdateFeedPoint(userId, feedId, companyId, isUpVote);
                }
                else if (isVoteComment)
                {
                    string feedId = request.FeedId;
                    string commentId = request.CommentId;
                    response = client.UpdateCommentPoint(userId, feedId, commentId, companyId, isUpVote);
                }
                else if (isVoteReply)
                {
                    string feedId = request.FeedId;
                    string commentId = request.CommentId;
                    string replyId = request.ReplyId;
                    response = client.UpdateReplyPoint(userId, feedId, commentId, replyId, companyId, isUpVote);
                }

                if (response.Success)
                {
                    string targetedUserId = response.Notification.TaggedUserId;
                    string notificationText = response.Notification.NotificationText;
                    int numberOfNotification = response.Notification.NumberOfNotificationForTargetedUser;
                    int notificationSubType = response.Notification.SubType;
                    string feedId = response.Notification.TaggedFeedId;
                    string commentId = response.Notification.TaggedCommentId;
                    string replyId = response.Notification.TaggedReplyId;
                    PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, feedId, commentId, replyId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectPrivacyResponse GetFeedPrivacy(FeedPrivacyRequest request)
        {
            FeedSelectPrivacyResponse response = new FeedSelectPrivacyResponse();
            response.Success = false;

            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;

                response = client.SelectFeedPrivacy(requesterUserId, companyId, feedId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToFeedTextPost(FeedUpdateRequest request)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.Success = false;
            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string content = request.Content;

                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                List<string> targetedGroupIds = request.TargetedGroupIds;
                bool isSpecificNotification = false;

                response = client.UpdateToFeedTextPost(requesterUserId, companyId, feedId, content, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification notification in response.TargetedNotifications)
                    {
                        string targetedUserId = notification.TaggedUserId;
                        string notificationText = notification.NotificationText;
                        int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                        int notificationSubType = notification.SubType;
                        PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, feedId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToFeedImagePost(FeedUpdateRequest request)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.Success = false;
            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string caption = request.Caption;
                List<string> updatedImageUrls = request.UpdatedImageUrls;

                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                List<string> targetedGroupIds = request.TargetedGroupIds;
                bool isSpecificNotification = false;

                response = client.UpdateToFeedImagePost(requesterUserId, companyId, feedId, caption, updatedImageUrls, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification notification in response.TargetedNotifications)
                    {
                        string targetedUserId = notification.TaggedUserId;
                        string notificationText = notification.NotificationText;
                        int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                        int notificationSubType = notification.SubType;
                        PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, feedId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToFeedVideoPost(FeedUpdateRequest request)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.Success = false;
            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string caption = request.Caption;

                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                List<string> targetedGroupIds = request.TargetedGroupIds;
                bool isSpecificNotification = false;

                response = client.UpdateToFeedVideoPost(requesterUserId, companyId, feedId, caption, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification notification in response.TargetedNotifications)
                    {
                        string targetedUserId = notification.TaggedUserId;
                        string notificationText = notification.NotificationText;
                        int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                        int notificationSubType = notification.SubType;
                        PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, feedId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToSharedUrlPost(FeedUpdateRequest request)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.Success = false;
            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string caption = request.Caption;

                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                List<string> targetedGroupIds = request.TargetedGroupIds;
                bool isSpecificNotification = false;

                response = client.UpdateToSharedUrlPost(requesterUserId, companyId, feedId, caption, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);

                if (response.Success)
                {
                    foreach (CassandraService.Entity.Notification notification in response.TargetedNotifications)
                    {
                        string targetedUserId = notification.TaggedUserId;
                        string notificationText = notification.NotificationText;
                        int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                        int notificationSubType = notification.SubType;
                        PushNotification.PushFeedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, notificationSubType, feedId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }
        
        // Requests
        public class FeedRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string ReplyId { get; set; }
        }

        public class GetCompanyFeedRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string SearchContent { get; set; }
            public int NumberOfPostsLoaded { get; set; }
            public DateTime? NewestTimestamp { get; set; }
            public DateTime? OldestTimestamp { get; set; }
        }

        public class GetPersonnelFeedRequest
        {
            public string RequesterUserId { get; set; }
            public string OwnerUserId { get; set; }
            public string CompanyId { get; set; }
            public string SearchContent { get; set; }
            public DateTime? NewestTimestamp { get; set; }
            public DateTime? OldestTimestamp { get; set; }
        }

        public class PostFeedRequest
        {
            public string FeedId { get; set; }
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public List<string> TargetedDepartmentIds { get; set; }
            public List<string> TargetedUserIds { get; set; }
            public List<string> TargetedGroupIds { get; set; }
            public bool IsForAdmin { get; set; }
            public int FeedType { get; set; }

            public string Content { get; set; }

            public string Caption { get; set; }

            public List<string> ImageUrls { get; set; }

            public string VideoUrl { get; set; }
            public string VideoThumbnailUrl { get; set; }

            public string Url { get; set; }
            public string UrlTitle { get; set; }
            public string UrlDescription { get; set; }
            public string UrlSiteName { get; set; }
            public string UrlImageUrl { get; set; }
        }

        public class PostCommentRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string Content { get; set; }
        }

        public class PostReplyRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string Content { get; set; }
        }

        public class ReportFeedRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string ReplyId { get; set; }
            public string Reason { get; set; }

            public bool IsReportPost { get; set; }
            public bool IsReportComment { get; set; }
            public bool IsReportReply { get; set; }
        }

        public class VoteFeedRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public bool IsVoteFeed { get; set; }
            public bool IsVoteComment { get; set; }
            public bool IsVoteReply { get; set; }
            public bool IsUpVote { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string ReplyId { get; set; }
        }

        public class FeedPrivacyRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
        }

        public class FeedUpdateRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string Content { get; set; }
            public string Caption { get; set; }
            public int FeedType { get; set; }
            public List<string> TargetedDepartmentIds { get; set; }
            public List<string> TargetedUserIds { get; set; }
            public List<string> TargetedGroupIds { get; set; }
            public List<string> UpdatedImageUrls { get; set; }
        }
    }
}