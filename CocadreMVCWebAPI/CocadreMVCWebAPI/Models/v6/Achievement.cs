﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v6
{
    public class Achievement
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public GamificationSelectAllAchievementResponse GetAchievements(AchievementRequest request)
        {
            GamificationSelectAllAchievementResponse response = new GamificationSelectAllAchievementResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;

                response = client.SelectAllAchievements(userId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationSelectAchievementResponse GetAchievement(AchievementRequest request)
        {
            GamificationSelectAchievementResponse response = new GamificationSelectAchievementResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string achievementId = request.AchievementId;

                response = client.SelectAchievement(userId, companyId, achievementId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationUpdateBadgeResponse SetBadge(AchievementRequest request)
        {
            GamificationUpdateBadgeResponse response = new GamificationUpdateBadgeResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string achievementId = request.AchievementId;

                response = client.UpdateBadge(achievementId, userId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationUpdateBadgeResponse RemoveBadge(AchievementRequest request)
        {
            GamificationUpdateBadgeResponse response = new GamificationUpdateBadgeResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string achievementId = request.AchievementId;

                response = client.UpdateBadge(achievementId, userId, companyId, true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationUpdateBadgeSeenResponse UpdateSeenBadge(AchievementRequest request)
        {
            GamificationUpdateBadgeSeenResponse response = new GamificationUpdateBadgeSeenResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string achievementId = request.AchievementId;

                response = client.UpdateSeenBadge(achievementId, userId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class AchievementRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string AchievementId { get; set; }
        }
    }
}