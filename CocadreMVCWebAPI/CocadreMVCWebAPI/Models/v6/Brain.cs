﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v6
{
    public class Brain
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public BrainSelectResponse GetBrain(BrainRequest request)
        {
            BrainSelectResponse response = new BrainSelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectBrainy(request.RequesterUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectStatsResponse GetUserChallengeStats(BrainRequest request)
        {
            UserSelectStatsResponse response = new UserSelectStatsResponse();
            response.Success = false;

            try
            {
                response = client.SelectStatsForUser(request.RequesterUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class BrainRequest
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
        }
    }
}