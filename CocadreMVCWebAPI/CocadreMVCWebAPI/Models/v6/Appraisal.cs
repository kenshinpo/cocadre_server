﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v6
{
    public class Appraisal
    {
        private delegate AppraisalAnswerCardResponse AnswerCardDelegate(string answeredByUserId, string companyId, string appraisalId, string cardId, int selectedRange, int selectedOption, string comment);

        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public AppraisalSelectAllTemplatesResponse GetTemplates(AppraisalRequest request)
        {
            AppraisalSelectAllTemplatesResponse response = new AppraisalSelectAllTemplatesResponse();
            response.Success = false;

            try
            {
                response = client.SelectAppraisalTemplates(request.RequesterUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalSelectTemplateResponse GetTemplateCards(AppraisalRequest request)
        {
            AppraisalSelectTemplateResponse response = new AppraisalSelectTemplateResponse();
            response.Success = false;

            try
            {
                response = client.SelectAppraisalTemplateCards(request.RequesterUserId, request.CompanyId, request.TemplateAppraisalId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalCategorySelectAllResponse GetCreatedCategoriesForTemplate(AppraisalRequest request)
        {
            AppraisalCategorySelectAllResponse response = new AppraisalCategorySelectAllResponse();
            response.Success = false;

            try
            {
                response = client.SelectCreatedCategoriesForTemplate(request.CompanyId, request.CreatorUserId, request.TemplateAppraisalId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalCardCreateTemplateResponse CreateCard(AppraisalRequest request)
        {
            AppraisalCardCreateTemplateResponse response = new AppraisalCardCreateTemplateResponse();
            response.Success = false;

            try
            {
                response = client.CreateAppraisalCardForTemplate(request.CreatorUserId, request.CompanyId, request.TemplateAppraisalId, request.CategoryId, request.CategoryTitle, request.CardContent);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalCategorySelectResponse SelectCreatedCardsByCategory(AppraisalRequest request)
        {
            AppraisalCategorySelectResponse response = new AppraisalCategorySelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectCreatedApprasialCardsByCategory(request.CompanyId, request.CreatorUserId, request.TemplateAppraisalId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalCreateResponse CreateAppraisal(AppraisalRequest request)
        {
            AppraisalCreateResponse response = new AppraisalCreateResponse();
            response.Success = false;

            try
            {
                response = client.CreateCustomAppraisal(request.CreatorUserId, request.CompanyId, request.TemplateAppraisalId, request.Title, request.Description, request.IsSelfAssessmentNeeded, request.IsAnonymous, request.TeamType, request.ParticipantIds, request.StartDate, request.EndDate, request.Cards);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalSelectResponse SelectAppraisal(AppraisalRequest request)
        {
            AppraisalSelectResponse response = new AppraisalSelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectFullCustomAppraisalByUser(request.RequesterUserId, request.CompanyId, request.AppraisalId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalSelectResponse SelectCreatedAppraisal(AppraisalRequest request)
        {
            AppraisalSelectResponse response = new AppraisalSelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectCreatedCustomAppraisalByUser(request.RequesterUserId, request.CompanyId, request.AppraisalId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalSelectAllResponse SelectCreatedAppraisals(AppraisalRequest request)
        {
            AppraisalSelectAllResponse response = new AppraisalSelectAllResponse();
            response.Success = false;

            try
            {
                response = client.SelectCreatedAppraisalsByCreator(request.CreatorUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalAnswerCardResponse AnswerCard(AppraisalRequest request)
        {
            AppraisalAnswerCardResponse response = new AppraisalAnswerCardResponse();
            response.Success = false;
            try
            {
                AnswerCardDelegate dTrigger = new AnswerCardDelegate(client.AnswerCard);
                dTrigger.BeginInvoke(request.AnsweredByUserId, request.CompanyId, request.AppraisalId, request.CardId, request.SelectedRange, request.SelectedOption, request.Comment, new AsyncCallback(TriggerAnswerCardOnCallBack), dTrigger);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        private void TriggerAnswerCardOnCallBack(IAsyncResult iar)
        {
            Log.Debug("Get answer card on callback");

            // Cast the state object back to the delegate type
            AnswerCardDelegate dTrigger = (AnswerCardDelegate)iar.AsyncState;

            // Call EndInvoke on the delegate to get the results
            AppraisalAnswerCardResponse response = dTrigger.EndInvoke(iar);

            if (response.Success)
            {
                if(!string.IsNullOrEmpty(response.CompletionNotification.NotificationText))
                {
                    PushNotification.PushReportNotification(response.CompletionNotification.TaggedUserId, response.CompletionNotification.NotificationText, response.CompletionNotification.NumberOfNotificationForTargetedUser, response.CompletionNotification.Type, response.CompletionNotification.SubType, response.CompletionNotification.TaggedReportId);
                }
            }
        }

        public AppraisalSelectResponse SelectResult(AppraisalRequest request)
        {
            AppraisalSelectResponse response = new AppraisalSelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectAppraisalResult(request.CreatorUserId, request.CompanyId, request.AppraisalId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalUpdateCategoryResponse UpdateCategory(AppraisalRequest request)
        {
            AppraisalUpdateCategoryResponse response = new AppraisalUpdateCategoryResponse();
            response.Success = false;

            try
            {
                response = client.UpdateTemplateCategoryTitleByUser(request.RequesterUserId, request.CompanyId, request.TemplateAppraisalId, request.CategoryId, request.CategoryTitle);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }


        public AppraisalUpdateCardResponse UpdateCardByCategory(AppraisalRequest request)
        {
            AppraisalUpdateCardResponse response = new AppraisalUpdateCardResponse();
            response.Success = false;

            try
            {
                response = client.UpdateTemplateAppraisalCardByUser(request.RequesterUserId, request.CompanyId, request.TemplateAppraisalId, request.CategoryId, request.CardId, request.CardContent);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalUpdateCardResponse RemoveCardByCategory(AppraisalRequest request)
        {
            AppraisalUpdateCardResponse response = new AppraisalUpdateCardResponse();
            response.Success = false;

            try
            {
                response = client.RemoveTemplateAppraisalCardByUser(request.RequesterUserId, request.CompanyId, request.TemplateAppraisalId, request.CategoryId, request.CardId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalUpdateResponse UpdateAppraisal(AppraisalRequest request)
        {
            AppraisalUpdateResponse response = new AppraisalUpdateResponse();
            response.Success = false;

            try
            {
                response = client.UpdateCustomAppraisal(request.AppraisalId, request.CreatorUserId, request.CompanyId, request.TemplateAppraisalId, request.Title, request.Description, request.IsSelfAssessmentNeeded, request.IsAnonymous, request.TeamType, request.ParticipantIds, request.StartDate, request.EndDate, request.Cards);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalExportResponse ExportAppraisal(AppraisalRequest request)
        {
            AppraisalExportResponse response = new AppraisalExportResponse();
            response.Success = false;

            try
            {
                response = client.ExportAppraisalResult(request.CreatorUserId, request.CompanyId, request.AppraisalId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class AppraisalRequest
        {
            public string RequesterUserId { get; set; }
            public string AnsweredByUserId { get; set; }
            public string CreatorUserId { get; set; }
            public string CompanyId { get; set; }
            public string CardId { get; set; }
            public int CardType { get; set; }
            public string CategoryId { get; set; }
            public string CategoryTitle { get; set; }
            public string CardContent { get; set; }
            public string TemplateAppraisalId { get; set; }
            public int SelectedRange { get; set; }
            public int SelectedOption { get; set; }
            public string Comment { get; set; }

            public string Title { get; set; }
            public string Description { get; set; }
            public bool IsSelfAssessmentNeeded { get; set; }
            public bool IsAnonymous { get; set; }
            public List<string> ParticipantIds { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public List<AppraisalCard> Cards { get; set; }
            public string AppraisalId { get; set; }
            public int TeamType { get; set; }
        }
    }
}