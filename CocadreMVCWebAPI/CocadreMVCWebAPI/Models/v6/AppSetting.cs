﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v6
{
    public class AppSetting
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public SelectAppSettingResponse GetAppSetting(AppSettingRequest request)
        {
            SelectAppSettingResponse response = new SelectAppSettingResponse();
            response.Success = false;

            try
            {
                response = client.SelectAppSetting(request.RequesterUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public SelectAppNotificationResponse GetNotificationSetting(AppSettingRequest request)
        {
            SelectAppNotificationResponse response = new SelectAppNotificationResponse();
            response.Success = false;

            try
            {
                response = client.SelectNotificationSetting(request.RequesterUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateAppSettingResponse UpdateAppSoundEffect(UpdateAppSettingRequest request)
        {
            UpdateAppSettingResponse response = new UpdateAppSettingResponse();
            response.Success = false;

            try
            {
                response = client.UpdateAppSoundEffect(request.RequesterUserId, request.CompanyId, request.IsOn);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateAppSettingResponse UpdateInGameMusic(UpdateAppSettingRequest request)
        {
            UpdateAppSettingResponse response = new UpdateAppSettingResponse();
            response.Success = false;

            try
            {
                response = client.UpdateInGameMusic(request.RequesterUserId, request.CompanyId, request.IsOn);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateAppSettingResponse UpdateNotification(UpdateAppSettingRequest request)
        {
            UpdateAppSettingResponse response = new UpdateAppSettingResponse();
            response.Success = false;

            try
            {
                response = client.UpdateNotification(request.RequesterUserId, request.CompanyId, request.GameNotification, request.EventNotification, request.FeedNotification);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public CountryListResponse GetCountriesForPhone()
        {
            CountryListResponse response = new CountryListResponse();
            response.Success = false;

            try
            {
                response = client.GetCountriesForPhone();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class AppSettingRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
        }

        public class UpdateAppSettingRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public bool IsOn { get; set; }

            public int GameNotification { get; set; }
            public int EventNotification { get; set; }
            public int FeedNotification { get; set; }
        }
    }
}