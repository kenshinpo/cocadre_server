﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v6
{
    public class Group
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public GroupCreateResponse CreateGroup(GroupRequest request)
        {
            GroupCreateResponse response = new GroupCreateResponse();
            response.Success = false;

            try
            {
                response = client.CreateGroup(request.RequesterUserId, request.CompanyId, request.GroupName, request.GroupMemberIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public GroupSelectResponse SelectGroup(GroupRequest request)
        {
            GroupSelectResponse response = new GroupSelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectGroup(request.RequesterUserId, request.CompanyId, request.GroupId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public GroupSelectAllResponse SelectAllGroupsByUser(GroupRequest request)
        {
            GroupSelectAllResponse response = new GroupSelectAllResponse();
            response.Success = false;

            try
            {
                response = client.SelectAllGroupsByUser(request.RequesterUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public GroupUpdateResponse DeleteGroup(GroupRequest request)
        {
            GroupUpdateResponse response = new GroupUpdateResponse();
            response.Success = false;

            try
            {
                response = client.DeleteGroup(request.RequesterUserId, request.CompanyId, request.GroupId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public GroupUpdateResponse UpdateGroup(GroupRequest request)
        {
            GroupUpdateResponse response = new GroupUpdateResponse();
            response.Success = false;

            try
            {
                response = client.UpdateGroup(request.RequesterUserId, request.CompanyId, request.GroupId, request.GroupName, request.GroupMemberIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class GroupRequest
        {
            public string RequesterUserId { get; set; }
            public string GroupId { get; set; }
            public string CompanyId { get; set; }
            public string GroupName { get; set; }
            public List<string> GroupMemberIds { get; set; }
        }
    }
}