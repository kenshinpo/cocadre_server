﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v6
{
    public class MobileLearning
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public MLTopicSelectAllByCategoryResponse GetAllMLTopics(Request request)
        {
            MLTopicSelectAllByCategoryResponse response = new MLTopicSelectAllByCategoryResponse();
            response.Success = false;

            try
            {
                response = client.SelectAllMLTopicByUser(request.RequesterUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public MLTopicSelectResponse GetMLTopic(Request request)
        {
            MLTopicSelectResponse response = new MLTopicSelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectMLTopicByUser(request.RequesterUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public MLCardAnswerResponse AnswerCard(Request request)
        {
            MLCardAnswerResponse response = new MLCardAnswerResponse();
            response.Success = false;

            try
            {
                response = client.AnswerMLCard(request.AnsweredByUserId, request.CompanyId, request.TopicId, request.CategoryId, request.CardId, request.OptionIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public MLSelectUserResultResponse SelectResult(Request request)
        {
            MLSelectUserResultResponse response = new MLSelectUserResultResponse();
            response.Success = false;

            try
            {
                response = client.SelectMLUserResult(request.AnsweredByUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public MLEducationDetailResponse GetMLEducation(Request request)
        {
            MLEducationDetailResponse response = new MLEducationDetailResponse();
            response.Success = false;

            try
            {
                response = client.SelectMLEducationByUser(request.RequesterUserId, request.CompanyId, request.EducationId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public MLEducationCardAnswerResponse AnswerEducationCard(Request request)
        {
            MLEducationCardAnswerResponse response = new MLEducationCardAnswerResponse();
            response.Success = false;

            try
            {
                response = client.AnswerEducationCard(request.RequesterUserId, request.CompanyId, request.EducationId, request.CategoryId, request.CardId, request.OptionIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public MLEducationCompletedResponse CompletedEducation(Request request)
        {
            MLEducationCompletedResponse response = new MLEducationCompletedResponse();
            response.Success = false;

            try
            {
                response = client.CompletedEducation(request.RequesterUserId, request.CompanyId, request.EducationId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }


        // Requests
        public class Request
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
            public string AnsweredByUserId { get; set; }
            public string TopicId { get; set; }
            public string CategoryId { get; set; }
            public string CardId { get; set; }
            public List<string> OptionIds { get; set; }
            public string Feedback { get; set; }
            public string EducationId { get; set; }
        }
    }
}