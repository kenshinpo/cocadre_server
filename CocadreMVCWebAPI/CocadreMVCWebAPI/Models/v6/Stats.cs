﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v6
{
    public class Stats
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public AnalyticsSelectAllGameHistoriesResponse GetGameHistories(StatsRequest request)
        {
            AnalyticsSelectAllGameHistoriesResponse response = new AnalyticsSelectAllGameHistoriesResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                int numberOfHistoryLoaded = request.NumberOfHistoryLoaded;
                DateTime? newestTimestamp = request.NewestTimestamp;
                DateTime? oldestTimestamp = request.OldestTimestamp;

                response = client.SelectAllGameHistoriesForUser(userId, companyId, numberOfHistoryLoaded, newestTimestamp, oldestTimestamp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsSelectGameResultResponse GetGameResult(StatsRequest request)
        {
            AnalyticsSelectGameResultResponse response = new AnalyticsSelectGameResultResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string challengeId = request.ChallengeId;

                response = client.SelectGameResultForChallenge(userId, companyId, challengeId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class StatsRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string ChallengeId { get; set; }
            public int NumberOfHistoryLoaded { get; set; }
            public DateTime? NewestTimestamp { get; set; }
            public DateTime? OldestTimestamp { get; set; }
        }
    }
}