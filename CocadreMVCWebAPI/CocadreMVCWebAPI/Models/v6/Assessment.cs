﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v6
{
    public class Assessment
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public AssessmentSelectResponse SelectAssessmentByUser(AssessmentRequest request)
        {
            AssessmentSelectResponse response = new AssessmentSelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectAssessmentByUser(request.RequesterUserId, request.CompanyId, request.AssessmentId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentSelectResponse TakeAssessment(AssessmentRequest request)
        {
            AssessmentSelectResponse response = new AssessmentSelectResponse();
            response.Success = false;

            try
            {
                response = client.TakeAssessmentByUser(request.RequesterUserId, request.CompanyId, request.AssessmentId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentSelectResultByUserResponse SelectAssessmentResultByUser(AssessmentRequest request)
        {
            AssessmentSelectResultByUserResponse response = new AssessmentSelectResultByUserResponse();
            response.Success = false;

            try
            {
                response = client.SelectAssessmentResultByUser(request.RequesterUserId, request.CompanyId, request.AssessmentId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentAnswerCardResponse AnswerAssessmentCard(AssessmentRequest request)
        {
            AssessmentAnswerCardResponse response = new AssessmentAnswerCardResponse();
            response.Success = false;

            try
            {
                response = client.AnswerAssessmentCard(request.AnsweredByUserId, request.CompanyId, request.AssessmentId, request.CardId, request.Options);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentSelectResultByUserResponse SelectColorBrainResultByUser(AssessmentRequest request)
        {
            AssessmentSelectResultByUserResponse response = new AssessmentSelectResultByUserResponse();
            response.Success = false;

            try
            {
                response = client.SelectColorBrainResultByUser(request.RequesterUserId, request.TargetedUserId, request.CompanyId, request.AssessmentId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }


        // Requests
        public class AssessmentRequest
        {
            public string AdminUserId { get; set; }
            public string RequesterUserId { get; set; }
            public string TargetedUserId { get; set; }
            public string AssessmentId { get; set; }
            public string CardId { get; set; }
            public string CompanyId { get; set; }

            public string AnsweredByUserId { get; set; }
            public List<AssessmentCardOption> Options { get; set; }
        }
    }
}