﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class AdminGami
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService client = new AdminService();

        public GamificationBackupResponse SelectExpBackup(GamificationRequest request)
        {
            GamificationBackupResponse response = new GamificationBackupResponse();
            response.Success = false;

            try
            {
                response = client.SelectExperienceBackup(request.AdminUserId, request.CompanyId, request.UserIds, request.DepartmentIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public GamificationBackupResponse SelectLeaderboardBackup(GamificationRequest request)
        {
            GamificationBackupResponse response = new GamificationBackupResponse();
            response.Success = false;

            try
            {
                response = client.SelectLeaderboardBackup(request.AdminUserId, request.CompanyId, request.UserIds, request.DepartmentIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public GamificationResetResponse ResetExp(GamificationRequest request)
        {
            GamificationResetResponse response = new GamificationResetResponse();
            response.Success = false;

            try
            {
                response = client.ResetExperience(request.AdminUserId, request.CompanyId, request.UserIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public GamificationResetResponse ResetLeaderboard(GamificationRequest request)
        {
            GamificationResetResponse response = new GamificationResetResponse();
            response.Success = false;

            try
            {
                response = client.ResetLeaderboard(request.AdminUserId, request.CompanyId, request.TopicIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        // Requests
        public class GamificationRequest
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public List<string> UserIds { get; set; }
            public List<string> TopicIds { get; set; }
            public List<string> DepartmentIds { get; set; }
        }
    }
}