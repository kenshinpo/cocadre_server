﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class AdminRS
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService client = new AdminService();

        public RSCategorySelectAllResponse SelectAllRSCategories(SelectAllRSCategoriesRequest request)
        {
            RSCategorySelectAllResponse response = new RSCategorySelectAllResponse();
            response.Success = false;

            try
            {
                response = client.SelectAllRSCategories(request.AdminUserId, request.CompanyId, request.QueryType);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public RSTopicCreateResponse CreateRSTopic(CreateRSTopicRequest request)
        {
            RSTopicCreateResponse response = new RSTopicCreateResponse();
            response.Success = false;

            try
            {
                response = client.CreateRSTopic(request.AdminUserId,
                                                request.CompanyId,
                                                request.Title,
                                                request.Introduction,
                                                request.ClosingWords,
                                                request.CategoryId,
                                                request.CategoryTitle,
                                                request.TopicIconUrl,
                                                request.Status,
                                                request.TargetedDepartmentIds,
                                                request.TargetedUserIds,
                                                request.StartDate,
                                                request.EndDate,
                                                request.AnonymityCount);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public RSTopicSelectAllBasicResponse SelectAllBasicByCategory(SelectAllRSTopicsRequest request)
        {
            RSTopicSelectAllBasicResponse response = new RSTopicSelectAllBasicResponse();
            response.Success = false;

            try
            {
                response = client.SelectAllRSTopicByCategory(request.AdminUserId,
                                                             request.CompanyId,
                                                             request.SelectedCategoryId,
                                                             request.ContainsName);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public RSCardCreateResponse CreateCard(CreateCardRequest request)
        {
            RSCardCreateResponse response = new RSCardCreateResponse();
            response.Success = false;

            try
            {
                //response = client.CreateCard(request.CardId,
                //                            request.Type,
                //                            request.Content,
                //                            request.HasImage,
                //                            request.Images,
                //                            request.ToBeSkipped,
                //                            request.HasCustomAnswer,
                //                            request.CustomAnswerInstruction,
                //                            request.IsOptionRandomized,
                //                            request.AllowMultipleLines,
                //                            request.HasPageBreak,
                //                            request.BackgroundImageType,
                //                            request.Note,
                //                            request.CategoryId,
                //                            request.TopicId,
                //                            request.AdminUserId,
                //                            request.CompanyId,
                //                            request.Options,
                //                            request.MiniOptionToSelect,
                //                            request.MaxOptionToSelect,
                //                            request.MaxRange,
                //                            request.MinRange);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public RSTopicSelectResponse SelectTopic(SelectRSTopicRequest request)
        {
            RSTopicSelectResponse response = new RSTopicSelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectFullDetailRSTopic(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public RSCardSelectResponse SelectCard(SelectRSCardRequest request)
        {
            RSCardSelectResponse response = new RSCardSelectResponse();
            response.Success = false;

            try
            {
                response = client.SelectFullDetailCard(request.AdminUserId, request.CompanyId, request.CardId, request.TopicId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public RSCardSelectAllResponse Preview(PreviewRequest request)
        {
            RSCardSelectAllResponse response = new RSCardSelectAllResponse();
            response.Success = false;

            try
            {
                response = client.Preview(request.TopicId, request.CategoryId, request.AdminUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public RSCardUpdateResponse UpdateCard(CreateCardRequest request)
        {
            RSCardUpdateResponse response = new RSCardUpdateResponse();
            response.Success = false;

            try
            {
                //response = client.UpdateCard(request.CardId,
                //                            request.Type,
                //                            request.Content,
                //                            request.HasImage,
                //                            request.Images,
                //                            request.ToBeSkipped,
                //                            request.HasCustomAnswer,
                //                            request.CustomAnswerInstruction,
                //                            request.IsOptionRandomized,
                //                            request.AllowMultipleLines,
                //                            request.HasPageBreak,
                //                            request.BackgroundImageType,
                //                            request.Note,
                //                            request.CategoryId,
                //                            request.TopicId,
                //                            request.AdminUserId,
                //                            request.CompanyId,
                //                            request.Options,
                //                            request.MiniOptionToSelect,
                //                            request.MaxOptionToSelect,
                //                            request.MaxRange,
                //                            request.MinRange);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectRSResultOverviewResponse SelectResultOverview(PreviewRequest request)
        {
            AnalyticSelectRSResultOverviewResponse response = new AnalyticSelectRSResultOverviewResponse();
            response.Success = false;

            try
            {
                response = client.SelectResultOverview(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectRSCardResultResponse SelectCardResult(PreviewRequest request)
        {
            AnalyticSelectRSCardResultResponse response = new AnalyticSelectRSCardResultResponse();
            response.Success = false;

            try
            {
                response = client.SelectCardResult(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectRSCardResultByUserResponse SelectCardResultByUser(PreviewRequest request)
        {
            AnalyticSelectRSCardResultByUserResponse response = new AnalyticSelectRSCardResultByUserResponse();
            response.Success = false;

            try
            {
                response = client.SelectCardResultByUser(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId, request.AnsweredByUserId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectRSCustomAnswersResponse SelectCustomAnswersResult(PreviewRequest request)
        {
            AnalyticSelectRSCustomAnswersResponse response = new AnalyticSelectRSCustomAnswersResponse();
            response.Success = false;

            try
            {
                response = client.SelectCustomAnswersFromCard(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId, request.CardId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectRSResponderReportResponse SelectRespondersReport(PreviewRequest request)
        {
            AnalyticSelectRSResponderReportResponse response = new AnalyticSelectRSResponderReportResponse();
            response.Success = false;

            try
            {
                response = client.SelectRespondersReport(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectRSOptionResultResponse SelectOptionResult(PreviewRequest request)
        {
            AnalyticSelectRSOptionResultResponse response = new AnalyticSelectRSOptionResultResponse();
            response.Success = false;

            try
            {
                response = client.SelectOptionResult(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId, request.CardId, request.OptionId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public RSTopicSelectFeedbackResponse SelectFeedback(PreviewRequest request)
        {
            RSTopicSelectFeedbackResponse response = new RSTopicSelectFeedbackResponse();
            response.Success = false;
            try
            {
                response = client.SelectFeedback(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticExportRSResultResponse ExportRSResult(PreviewRequest request)
        {
            AnalyticExportRSResultResponse response = new AnalyticExportRSResultResponse();
            response.Success = false;
            try
            {
                response = client.ExportRSResult(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        // Requests
        public class SelectAllRSCategoriesRequest
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public int QueryType { get; set; }
        }

        public class CreateRSTopicRequest
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public string Title { get; set; }
            public string Introduction { get; set; }
            public string ClosingWords { get; set; }
            public string CategoryId { get; set; }
            public string CategoryTitle { get; set; }
            public string TopicIconUrl { get; set; }
            public int Status { get; set; }
            public List<string> TargetedDepartmentIds { get; set; }
            public List<string> TargetedUserIds { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public int AnonymityCount { get; set; }
        }

        public class SelectAllRSTopicsRequest
        {
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
            public string SelectedCategoryId { get; set; }
            public string ContainsName { get; set; }
        }

        public class CreateCardRequest
        {
            public string CardId { get; set; }
            public int Type { get; set; }
            public string Content { get; set; }
            public string Caption { get; set; }
            public bool HasImage { get; set; }
            public List<RSImage> Images { get; set; }
            public bool ToBeSkipped { get; set; }
            public bool HasCustomAnswer { get; set; }
            public string CustomAnswerInstruction { get; set; }
            public bool IsOptionRandomized { get; set; }
            public bool AllowMultipleLines { get; set; }
            public bool HasPageBreak { get; set; }
            public int Ordering { get; set; }
            public int BackgroundImageType { get; set; }
            public string Note { get; set; }
            public int MaxRange { get; set; }
            public int MinRange { get; set; }

            public string CategoryId { get; set; }
            public string TopicId { get; set; }
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
            public List<RSOption> Options { get; set; }
            public int MiniOptionToSelect { get; set; }
            public int MaxOptionToSelect { get; set; }
        }

        public class SelectRSTopicRequest
        {
            public string TopicId { get; set; }
            public string CategoryId { get; set; }
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
        }

        public class SelectRSCardRequest
        {
            public string TopicId { get; set; }
            public string CardId { get; set; }
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
        }

        public class PreviewRequest
        {
            public string TopicId { get; set; }
            public string CategoryId { get; set; }
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }

            public string CardId { get; set; }
            public string OptionId { get; set; }
            public string AnsweredByUserId { get; set; }
        }

        public class UpdateCardRequest
        {
            public string CardId { get; set; }
            public string TopicId { get; set; }
            public string CategoryId { get; set; }
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
            public List<RSImage> Images { get; set; }
            public List<RSOption> Options { get; set; }
            public string Note { get; set; }
            public bool HasPageBreak { get; set; }
            public string Content { get; set; }
            public bool HasImage { get; set; }
            public int MiniOptionToSelect { get; set; }
            public int MaxOptionToSelect { get; set; }
        }
    }
}