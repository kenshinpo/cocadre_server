﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class AdminDashboard
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public DashboardSelectFeedbackResponse GetFeedbackPosts(DashboardRequest request)
        {
            DashboardSelectFeedbackResponse response = new DashboardSelectFeedbackResponse();
            response.Success = false;

            try
            {
                string companyId = request.CompanyId;
                string adminUserId = request.AdminUserId;
                int reportState = request.ReportState;

                response = client.SelectFeedbackFeedPosts(adminUserId, companyId, reportState);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public DashboardSelectReportedFeedResponse GetReportedPosts(DashboardRequest request)
        {
            DashboardSelectReportedFeedResponse response = new DashboardSelectReportedFeedResponse();
            response.Success = false;

            try
            {
                string companyId = request.CompanyId;
                string adminUserId = request.AdminUserId;
                int reportState = request.ReportState;

                response = client.SelectReportedFeedPosts(adminUserId, companyId, reportState);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public DashboardCreateFeedbackCommentResponse PostFeedbackComment(DashboardCreateFeedbackCommentRequest request)
        {
            DashboardCreateFeedbackCommentResponse response = new DashboardCreateFeedbackCommentResponse();
            response.Success = false;

            try
            {
                string companyId = request.CompanyId;
                string adminUserId = request.AdminUserId;
                string feedId = request.FeedId;
                string comment = request.Comment;

                response = client.CreateFeedbackComment(adminUserId, companyId, feedId, comment);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        // Requests
        public class DashboardRequest
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public int ReportState { get; set; }
        }

        public class DashboardCreateFeedbackCommentRequest
        {
            public string AdminUserId { get; set; }
            public string Comment { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
        }
    }
}