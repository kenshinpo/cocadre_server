﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class AdminAssessment
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService admin = new AdminService();
        private ClientService client = new ClientService();

        public AssessmentCreateResponse CreateAssessment(CreateAssessmentRequest request)
        {
            AssessmentCreateResponse response = new AssessmentCreateResponse();
            response.Success = false;

            try
            {
                response = admin.CreateAssessment(request.AdminUserId, request.Title, request.Introduction, request.ClosingWords, request.IconUrl, request.CoachId, request.CoachName, request.IsDisplayCoachName, request.IsAnonymous, request.ImageBanners, request.Tabulations, request.Answers, request.VideoUrl, request.VideoTitle, request.VideoDescription, request.CssArea);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentSelectAllResponse SelectAllAssessment(AssessmentRequest request)
        {
            AssessmentSelectAllResponse response = new AssessmentSelectAllResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAllAssessments(request.AdminUserId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentSelectResponse SelectFullDetailAssessment(AssessmentRequest request)
        {
            AssessmentSelectResponse response = new AssessmentSelectResponse();
            response.Success = false;

            try
            {
                response = admin.SelectFullDetailAssessment(request.AdminUserId, request.AssessmentId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentCreateCardResponse CreateAssessmentCard(CreateCardRequest request)
        {
            AssessmentCreateCardResponse response = new AssessmentCreateCardResponse();
            response.Success = false;

            try
            {
                response = admin.CreateAssessmentCard(request.CardId, request.Type, request.Content, request.Images, request.ToBeSkipped, request.IsOptionRandomized, request.HasPageBreak, request.BackgroundImageType, request.Note, request.AssessmentId, request.AdminUserId, request.Options, request.TotalPointsAllocation, request.MiniOptionToSelect, request.MaxOptionToSelect, request.StartRangePosition, request.MaxRange, request.MaxRangeLabel, request.MidRange, request.MidRangeLabel, request.MinRange, request.MinRangeLabel);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentSelectCardResponse SelectFullDetailCard(AssessmentRequest request)
        {
            AssessmentSelectCardResponse response = new AssessmentSelectCardResponse();
            response.Success = false;

            try
            {
                response = admin.SelectFullDetailAssessmentCard(request.AssessmentId, request.CardId, request.AdminUserId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public SubscriptionSelectAllResponse SelectAllAssessmentSubscription(SubscriptionRequest request)
        {
            SubscriptionSelectAllResponse response = new SubscriptionSelectAllResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAllAssessmentSubscription(request.AdminUserId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public SubscriptionSelectResponse SelectFullDetailAssessmentSubscription(SubscriptionRequest request)
        {
            SubscriptionSelectResponse response = new SubscriptionSelectResponse();
            response.Success = false;

            try
            {
                response = admin.SelectFullDetailAssessmentSubscription(request.AdminUserId, request.CompanyId, request.AssessmentId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public SubscriptionCreateResponse CreateAssessmentSubscription(SubscriptionRequest request)
        {
            SubscriptionCreateResponse response = new SubscriptionCreateResponse();
            response.Success = false;

            try
            {
                response = admin.CreateAssessmentSubscription(request.AdminUserId, request.CompanyId, request.AssessmentId, request.NumberOfLicences, request.DurationType, request.VisibleDaysAfterCompletion, request.NumberOfRetake, request.Status, request.StartDate, request.EndDate, request.TargetedDepartmentIds, request.TargetedUserIds);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentUpdateResponse UpdateAssessmentStatus(AssessmentRequest request)
        {
            AssessmentUpdateResponse response = new AssessmentUpdateResponse();
            response.Success = false;

            try
            {
                response = admin.UpdateAssessmentStatus(request.AdminUserId, request.AssessmentId, request.NewStatus);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Request
        public class CreateAssessmentRequest
        {
            public string AdminUserId { get; set; }
            public string Title { get; set; }
            public string Introduction { get; set; }
            public string ClosingWords { get; set; }
            public string IconUrl { get; set; }
            public string CoachId { get; set; }
            public string CoachName { get; set; }
            public bool IsDisplayCoachName { get; set; }
            public bool IsAnonymous { get; set; }
            public List<AssessmentImage> ImageBanners { get; set; }
            public List<AssessmentTabulation> Tabulations { get; set; }
            public List<AssessmentAnswer> Answers { get; set; }
            public string VideoUrl { get; set; }
            public string VideoTitle { get; set; }
            public string VideoDescription { get; set; }
            public string CssArea { get; set; }
        }

        public class AssessmentRequest
        {
            public string AdminUserId { get; set; }
            public string RequesterUserId { get; set; }
            public string AssessmentId { get; set; }
            public int NewStatus { get; set; }
            public string CardId { get; set; }
            public string CompanyId { get; set; }
        }

        public class CreateCardRequest
        {
            public string AdminUserId { get; set; }
            public string AssessmentId { get; set; }
            public string CardId { get; set; }
            public int Type { get; set; }
            public string Content { get; set; }
            public List<AssessmentImage> Images { get; set; }
            public bool ToBeSkipped { get; set; }
            public bool IsOptionRandomized { get; set; }
            public bool HasPageBreak { get; set; }
            public int BackgroundImageType { get; set; }
            public string Note { get; set; }

            public int MaxRange { get; set; }
            public string MaxRangeLabel { get; set; }
            public int MidRange { get; set; }
            public string MidRangeLabel { get; set; }
            public int MinRange { get; set; }
            public string MinRangeLabel { get; set; }
            public int StartRangePosition { get; set; }

            public List<AssessmentCardOption> Options { get; set; }
            public int MiniOptionToSelect { get; set; }
            public int MaxOptionToSelect { get; set; }

            public int TotalPointsAllocation { get; set; }
        }

        public class SubscriptionRequest
        {
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
            public string AssessmentId { get; set; }
            public int NumberOfLicences { get; set; }
            public int DurationType { get; set; }
            public int VisibleDaysAfterCompletion { get; set; }
            public int NumberOfRetake { get; set; }
            public int Status { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public List<string> TargetedDepartmentIds { get; set; }
            public List<string> TargetedUserIds { get; set; }
        }
    }
}