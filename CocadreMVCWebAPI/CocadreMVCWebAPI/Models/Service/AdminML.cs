﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class AdminML
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService admin = new AdminService();

        public MLCategorySelectAllResponse SelectAllMLCategories(SelectAllMLCategoriesRequest request)
        {
            MLCategorySelectAllResponse response = new MLCategorySelectAllResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAllMLCategories(request.AdminUserId, request.CompanyId, request.QueryType);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLTopicCreateResponse CreateMLTopic(CreateMLTopicRequest request)
        {
            MLTopicCreateResponse response = new MLTopicCreateResponse();
            response.Success = false;

            try
            {
                response = admin.CreateMLTopic(request.AdminUserId,
                                                request.CompanyId,
                                                request.Title,
                                                request.Introduction,
                                                request.Instructions,
                                                request.ClosingWords,
                                                request.TopicIconUrl,
                                                request.PassingGrade,
                                                request.CategoryId,
                                                request.CategoryTitle,
                                                request.DurationType,
                                                request.TargetedDepartmentIds,
                                                request.TargetedUserIds,
                                                request.StartDate,
                                                request.EndDate);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLTopicSelectAllBasicResponse SelectAllBasicByCategory(SelectAllMLTopicsRequest request)
        {
            MLTopicSelectAllBasicResponse response = new MLTopicSelectAllBasicResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAllMLTopicByCategory(request.AdminUserId,
                                                             request.CompanyId,
                                                             request.SelectedCategoryId,
                                                             request.ContainsName);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLCardCreateResponse CreateCard(CreateCardRequest request)
        {
            MLCardCreateResponse response = new MLCardCreateResponse();
            response.Success = false;

            try
            {
                response = admin.CreateMLCard(request.CardId,
                                               request.Type,
                                               request.IsOptionRandomized,
                                               request.HasPageBreak,
                                               request.BackgroundImageType,
                                               request.Note,
                                               request.CategoryId,
                                               request.TopicId,
                                               request.AdminUserId,
                                               request.CompanyId,
                                               request.Content,
                                               request.ScoringCalculationType,
                                               request.MinOptionToSelect,
                                               request.MaxOptionToSelect,
                                               request.Description,
                                               request.UploadedContent,
                                               request.Options,
                                               request.PagesContent);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLTopicSelectResponse SelectTopic(SelectMLTopicRequest request)
        {
            MLTopicSelectResponse response = new MLTopicSelectResponse();
            response.Success = false;

            try
            {
                response = admin.SelectFullDetailMLTopic(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLCardSelectResponse SelectCard(SelectMLCardRequest request)
        {
            MLCardSelectResponse response = new MLCardSelectResponse();
            response.Success = false;

            try
            {
                response = admin.SelectFullDetailMLCard(request.AdminUserId, request.CompanyId, request.CardId, request.TopicId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLTopicUpdateResponse UpdateStatus(UpdateCardRequest request)
        {
            MLTopicUpdateResponse response = new MLTopicUpdateResponse();
            response.Success = false;

            try
            {
                response = admin.UpdateMLTopicStatus(request.TopicId, request.CategoryId, request.AdminUserId, request.CompanyId, request.Status);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLSelectCandidateResultResponse SelectCandidateResult(PreviewRequest request)
        {
            MLSelectCandidateResultResponse response = new MLSelectCandidateResultResponse();
            response.Success = false;

            try
            {
                response = admin.SelectMLCandidateResult(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId, request.SearchId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLSelectQuestionResultResponse SelectQuestionResult(PreviewRequest request)
        {
            MLSelectQuestionResultResponse response = new MLSelectQuestionResultResponse();
            response.Success = false;

            try
            {
                response = admin.SelectMLQuestionResult(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId, request.SearchId, request.QuestionToggleType);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLSelectCardQuestionResultResponse SelectCardQuestionResult(PreviewRequest request)
        {
            MLSelectCardQuestionResultResponse response = new MLSelectCardQuestionResultResponse();
            response.Success = false;

            try
            {
                response = admin.SelectMLCardQuestionResult(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId, request.CardId, request.SearchId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLSelectUserAttemptHistoryResponse SelectUserAttemptResult(PreviewRequest request)
        {
            MLSelectUserAttemptHistoryResponse response = new MLSelectUserAttemptHistoryResponse();
            response.Success = false;

            try
            {
                response = admin.SelectMLUserAttemptHistoryResult(request.AdminUserId, request.CompanyId, request.AnsweredByUserId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public MLSelectSortedCardAttemptHistoryResponse SelectSortedCardHistoryResult(PreviewRequest request)
        {
            MLSelectSortedCardAttemptHistoryResponse response = new MLSelectSortedCardAttemptHistoryResponse();
            response.Success = false;

            try
            {
                response = admin.SelectSortedCardHistoryResult(request.AdminUserId, request.CompanyId, request.AnsweredByUserId, request.TopicId, request.CategoryId, request.QuestionToggleType, request.ContainsName);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        #region Education
        public MLEducationCardCreateResponse CreateEducationCard(CreateEducationCardRequest request)
        {
            MLEducationCardCreateResponse response = new MLEducationCardCreateResponse();
            response.Success = false;

            try
            {
                response = admin.CreateMLEducationCard(request.cardId,
                    request.type,
                    request.isOptionRandomized,
                    request.backgoundType,
                    request.categoryId,
                    request.educationId,
                    request.adminUserId,
                    request.companyId,
                    request.content,
                    request.minOptionToSelect,
                    request.maxOptionToSelect,
                    request.description,
                    request.uploadedContent,
                    request.options,
                    request.pagesContent
                    );
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }
        #endregion

        // Requests
        public class SelectAllMLCategoriesRequest
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public int QueryType { get; set; }
        }

        public class CreateMLTopicRequest
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public string Title { get; set; }
            public string Introduction { get; set; }
            public string Instructions { get; set; }
            public string ClosingWords { get; set; }
            public string CategoryId { get; set; }
            public string CategoryTitle { get; set; }
            public string TopicIconUrl { get; set; }
            public int DurationType { get; set; }
            public int Status { get; set; }
            public List<string> TargetedDepartmentIds { get; set; }
            public List<string> TargetedUserIds { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public bool IsRandomizedAllCards { get; set; }
            public string PassingGrade { get; set; }
            public bool IsDisplayAuthorOnClient { get; set; }
            public bool IsAllowRetestForFailures { get; set; }
            public bool IsAllowRetestForPasses { get; set; }
            public int ReviewAnswerType { get; set; }
            public int DisplayAnswerType { get; set; }
        }

        public class SelectAllMLTopicsRequest
        {
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
            public string SelectedCategoryId { get; set; }
            public string ContainsName { get; set; }
            public string RequesterUserId { get; set; }
        }

        public class CreateCardRequest
        {
            public string CardId { get; set; }
            public int Type { get; set; }
            public string Content { get; set; }
            public bool IsOptionRandomized { get; set; }
            public bool HasPageBreak { get; set; }
            public int BackgroundImageType { get; set; }
            public string Note { get; set; }
            public string CategoryId { get; set; }
            public string TopicId { get; set; }
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
            public int ScoringCalculationType { get; set; }
            public List<MLUploadedContent> UploadedContent { get; set; }
            public List<MLOption> Options { get; set; }
            public List<MLPageContent> PagesContent { get; set; }
            public string Description { get; set; }
            public int MinOptionToSelect { get; set; }
            public int MaxOptionToSelect { get; set; }
        }

        public class SelectMLTopicRequest
        {
            public string TopicId { get; set; }
            public string CategoryId { get; set; }
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
        }

        public class SelectMLCardRequest
        {
            public string TopicId { get; set; }
            public string CardId { get; set; }
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
        }

        public class PreviewRequest
        {
            public string TopicId { get; set; }
            public string CategoryId { get; set; }
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }

            public string CardId { get; set; }
            public string OptionId { get; set; }
            public string AnsweredByUserId { get; set; }

            public string SearchId { get; set; }
            public string QuestionToggleType { get; set; }
            public string ContainsName { get; set; }
        }

        public class UpdateCardRequest
        {
            public string CardId { get; set; }
            public string TopicId { get; set; }
            public string CategoryId { get; set; }
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
            public List<MLUploadedContent> UploadedContent { get; set; }
            public List<MLOption> Options { get; set; }
            public string Note { get; set; }
            public bool HasPageBreak { get; set; }
            public string Content { get; set; }
            public bool HasImage { get; set; }
            public int MiniOptionToSelect { get; set; }
            public int MaxOptionToSelect { get; set; }
            public int Status { get; set; }
        }

        public class CreateEducationCardRequest
        {
            public string cardId { get; set; }
            public int type { get; set; }
            public bool isOptionRandomized { get; set; }
            public int backgoundType { get; set; }
            public string categoryId { get; set; }
            public string educationId { get; set; }
            public string adminUserId { get; set; }
            public string companyId { get; set; }
            public string content { get; set; }
            public int minOptionToSelect { get; set; }
            public int maxOptionToSelect { get; set; }
            public string description { get; set; }
            public List<MLUploadedContent> uploadedContent { get; set; }
            public List<MLEducationOption> options { get; set; }
            public List<MLPageContent> pagesContent { get; set; }
            public int ordering { get; set; }
        }
    }
}