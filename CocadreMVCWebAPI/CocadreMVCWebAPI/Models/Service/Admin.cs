﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class Admin
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public UserCreateResponse CreateAdmin(CreateAdminRequest request)
        {
            UserCreateResponse response = new UserCreateResponse();
            response.Success = false;

            try
            {
                string companyTitle = request.CompanyTitle;
                string companyLogoUrl = request.CompanyLogoUrl;
                string plainPassword = request.PlainPassword;
                string firstName = request.FirstName;
                string lastName = request.LastName;
                string email = request.Email;
                string profileImageUrl = request.ProfileImageUrl;
                string position = request.Position;

                response = client.CreateAdmin(null, companyTitle, companyLogoUrl, null, plainPassword, firstName, lastName, email, profileImageUrl, position, null, null, null, null, null, null);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Request
        public class CreateAdminRequest
        {
            public string CompanyTitle { get; set; }
            public string CompanyLogoUrl { get; set; }
            public string PlainPassword { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string ProfileImageUrl { get; set; }
            public string Position { get; set; }
        }
    }
}