﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class ForgetPassword
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public AuthenticationUpdateResponse ResetPasswordWithToken(string token)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = false;

            try
            {
                response = client.ResetPasswordWithToken(token);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }
    }
}