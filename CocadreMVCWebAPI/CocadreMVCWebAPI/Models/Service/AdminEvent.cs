﻿
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using System;
using System.IO;
using System.Web.Configuration;
using System.Web.Mvc;

namespace CocadreMVCWebAPI.Models.Service
{
    public class AdminEvent
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService client = new AdminService();

        public AnalyticSelectEventResultResponse GetEventResult(string adminUserId, string companyId, string eventId)
        {
            AnalyticSelectEventResultResponse response = new AnalyticSelectEventResultResponse();
            response.Success = false;
            try
            {
                response = client.SelectEventResult(adminUserId, companyId, eventId);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public class EventRequest
        {
            public string EventId { get; set; }
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
        }
    }
}