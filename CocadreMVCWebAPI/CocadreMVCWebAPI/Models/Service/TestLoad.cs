﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class TestLoad
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public UserSelectWithCompanyResponse TestWithoutLoop(TestLoadRequest request)
        {
            UserSelectWithCompanyResponse response = new UserSelectWithCompanyResponse();
            response.Success = false;

            try
            {
                response = client.TestWithoutLoop(request.RequesterUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public CategorySelectAllWithTopicResponse TestWithLoop(TestLoadRequest request)
        {
            CategorySelectAllWithTopicResponse response = new CategorySelectAllWithTopicResponse();
            response.Success = false;

            try
            {
                response = client.TestWithLoop(request.RequesterUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public BrainSelectResponse TestWithMultipleKeyspaces(TestLoadRequest request)
        {
            BrainSelectResponse response = new BrainSelectResponse();
            response.Success = false;

            try
            {
                response = client.TestWithMultipleKeyspaces(request.RequesterUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }
    }

    public class TestLoadRequest
    {
        public string RequesterUserId { get; set; }
        public string UserId { get; set; }
        public string CompanyId { get; set; }
    }
}