﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class AdminAchievement
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService admin = new AdminService();

        public GamificationCreateAchievementResponse CreateAchievement(Request request)
        {
            GamificationCreateAchievementResponse response = new GamificationCreateAchievementResponse();
            response.Success = false;

            try
            {
                response = admin.CreateAchievement(request.AdminUserId, request.CompanyId, request.AchievementId, request.Title, request.TemplateType, request.IsIconCustomized, request.IconImageUrl, request.IconColor, request.Type, request.RuleType, request.TopicIds, request.PlayTimes, request.FailTimes);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public GamificationSelectAllAchievementResponse SelectAllAchievement(Request request)
        {
            GamificationSelectAllAchievementResponse response = new GamificationSelectAllAchievementResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAllAchievements(request.AdminUserId, request.CompanyId, request.ContainsName);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public GamificationSelectAchievementResponse SelectAchievement(Request request)
        {
            GamificationSelectAchievementResponse response = new GamificationSelectAchievementResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAchievement(request.AdminUserId, request.CompanyId, request.AchievementId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public GamificationSelectTopicResponse SelectTopicsForAchievement(Request request)
        {
            GamificationSelectTopicResponse response = new GamificationSelectTopicResponse();
            response.Success = false;

            try
            {
                response = admin.SelectTopicsForAchievement(request.AdminUserId, request.CompanyId, request.ContainsName);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public GamificationSelectAchievementResultResponse SelectAchievementResult(Request request)
        {
            GamificationSelectAchievementResultResponse response = new GamificationSelectAchievementResultResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAchievementResult(request.AdminUserId, request.CompanyId, request.AchievementId, request.SearchId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public GamificationSelectUserAchievementResultResponse SelectUserAchievementResult(Request request)
        {
            GamificationSelectUserAchievementResultResponse response = new GamificationSelectUserAchievementResultResponse();
            response.Success = false;

            try
            {
                response = admin.SelectUserAchievementResult(request.AdminUserId, request.CompanyId, request.AchieverId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        // Requests
        public class Request
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public string AchievementId { get; set; }
            public string SearchId { get; set; }
            public string AchieverId { get; set; }

            public string ContainsName { get; set; }

            public string Title { get; set; }
            public int TemplateType { get; set; }
            public bool IsIconCustomized { get; set; }
            public string IconImageUrl { get; set; }
            public string IconColor { get; set; }
            public int Type { get; set; }
            public int RuleType { get; set; }
            public List<string> TopicIds { get; set; }
            public int PlayTimes { get; set; }
            public int FailTimes { get; set; }
        }
    }
}