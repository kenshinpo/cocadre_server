﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using PubNubMessaging.Core;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class UpdateScript
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService admin = new AdminService();

        public UpdateScriptResponse InsertValidStatusColumnToFeed(UpdateScriptRequest request)
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update feed valid status");
                response = admin.InsertValidStatusColumnToFeed(request.AdminUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse InsertNewColumnsAndTablesToChallenge()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Insert new columns and tables to challenge");
                response = admin.InsertNewColumnsAndTablesToChallenge();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse InsertTableChallengeByTopic()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Insert new table to challenge for event");
                response = admin.InsertTableChallengeByTopic();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse CreateNewTablesForUserToken()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Insert new table for user token");
                response = admin.CreateNewTablesForUserToken();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse CreateNewTablesFoNotification()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Insert new table for notification");
                response = admin.CreateNewTablesForNotification();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse CreateNewColumnsForUser()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Insert new columns for user");
                response = admin.CreateNewColumnsForUser();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse CreateNewColumnsForCompany()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Insert new columns for company");
                response = admin.CreateNewColumnsForCompany();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse CreateMatchUpActivity()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Create new table for matchup");
                response = admin.CreateMatchUpActivity();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse CreateMatchUpTopicAttempt()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Create new table for topic attempt");
                response = admin.CreateMatchUpTopicAttempt();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse CreateMatchUpQuestionAttempt()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Create new table for question attempt");
                response = admin.CreateMatchUpQuestionAttempt();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse CreateMatchUpOptionAttempt()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Create new table for option attempt");
                response = admin.CreateMatchUpOptionAttempt();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateFeedUser()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update column for feed user");
                response = admin.UpdateFeedUser();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateSurveyCategory()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update survey for event category");
                response = admin.UpdateSurveyCategory();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateSurvey()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update survey");
                response = admin.UpdateSurvey();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateSurveyCard()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update survey card");
                response = admin.UpdateSurveyCard();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateMatchupDifficulty()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update matchup");
                response = admin.UpdateMatchupDifficulty();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateDynamicPulseCustomized()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update dynamic pulse customized");
                response = admin.UpdateDynamicPulseCustomized();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateUserEmailLower()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update user email lower");
                response = admin.UpdateUserEmailLower();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdatePhoneCountryForUser(UpdateScriptRequest request)
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update user phone country");
                response = admin.UpdatePhoneCountryForUser(request.AdminUserId, request.CompanyId, request.CountryCode, request.CountryName);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateFeedPoint()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update feed point");
                response = admin.UpdateFeedPointByTimestamp();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdatePrivacyForFeed()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update feed privacy");
                response = admin.UpdatePrivacyForFeed();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateLogin()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update login");
                response = admin.UpdateLogin();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse OptimizeNotification()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update notification");
                response = admin.OptimizeNotification();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateCompanyJobs()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update company jobs");
                response = admin.UpdateCompanyJobs();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateUserJob()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update user job");
                response = admin.UpdateUserLowestJob();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateAppraisalCommentRating()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update appraisal comment rating");
                response = admin.UpdateAppraisalCommentRating();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public UpdateScriptResponse UpdateDeletedUserJob()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                Log.Debug("Update deleted user job");
                response = admin.UpdateDeletedUserJob();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        // Requests
        public class UpdateScriptRequest
        {
            public string AdminUserId { get; set; }
            public string CompanyId { get; set; }
            public string CountryCode { get; set; }
            public string CountryName { get; set; }
        }
    }
}