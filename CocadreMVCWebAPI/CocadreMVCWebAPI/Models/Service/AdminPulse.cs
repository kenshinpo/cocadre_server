﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class AdminPulse
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService admin = new AdminService();
        private ClientService client = new ClientService();

        #region Feed
        public PulseCreateResponse CreateBanner(PulseRequest request)
        {
            try
            {
                PulseCreateResponse response = new PulseCreateResponse();
                response.Success = false;

                try
                {
                    response = admin.CreateBanner(null,
                                                    request.AdminUserId,
                                                    request.CompanyId,
                                                    request.Title,
                                                    request.IsFilterApplied,
                                                    request.TextColor,
                                                    request.BackgroundUrl,
                                                    request.Description,
                                                    request.Status,
                                                    request.StartDate,
                                                    request.EndDate,
                                                    request.IsPrioritized,
                                                    request.TargetedDepartmentIds,
                                                    request.TargetedUserIds);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseCreateResponse CreateAnnouncement(PulseRequest request)
        {
            try
            {
                PulseCreateResponse response = new PulseCreateResponse();
                response.Success = false;

                try
                {
                    response = admin.CreateAnnouncement(null,
                                                        request.AdminUserId,
                                                        request.CompanyId,
                                                        request.Title,
                                                        request.Description,
                                                        request.Status,
                                                        request.StartDate,
                                                        request.EndDate,
                                                        request.IsPrioritized,
                                                        request.TargetedDepartmentIds,
                                                        request.TargetedUserIds);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectAllResponse SelectAllAnnouncementBasic(PulseRequest request)
        {
            try
            {
                PulseSelectAllResponse response = new PulseSelectAllResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectAllAnnouncementBasic(request.AdminUserId,
                                                                request.CompanyId,
                                                                null,
                                                                null,
                                                                request.Title);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectAllResponse SelectAllBannerBasic(PulseRequest request)
        {
            try
            {
                PulseSelectAllResponse response = new PulseSelectAllResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectAllBannerBasic(request.AdminUserId,
                                                          request.CompanyId,
                                                          null,
                                                          null,
                                                          request.Title);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectResponse SelectFullDetailAnnouncement(PulseRequest request)
        {
            try
            {
                PulseSelectResponse response = new PulseSelectResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectFullDetailAnnouncement(request.AdminUserId,
                                                                  request.CompanyId,
                                                                  request.PulseId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectResponse SelectFullDetailBanner(PulseRequest request)
        {
            try
            {
                PulseSelectResponse response = new PulseSelectResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectFullDetailBanner(request.AdminUserId,
                                                            request.CompanyId,
                                                            request.PulseId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectAllResponse SelectAllByUser(PulseRequest request)
        {
            try
            {
                PulseSelectAllResponse response = new PulseSelectAllResponse();
                response.Success = false;

                try
                {
                    response = client.SelectAllByUser(request.RequesterUserId,
                                                      request.CompanyId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectSingleAnalyticResponse SelectFeedAnalytic(PulseRequest request)
        {
            try
            {
                PulseSelectSingleAnalyticResponse response = new PulseSelectSingleAnalyticResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectAnalyticForFeedPulse(request.RequesterUserId,
                                                                request.CompanyId,
                                                                request.PulseId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Dynamic
        public PulseDeckCreateResponse CreateDeck(DeckRequest request)
        {
            try
            {
                PulseDeckCreateResponse response = new PulseDeckCreateResponse();
                response.Success = false;

                try
                {
                    response = admin.CreateDeck(null,
                                                request.AdminUserId,
                                                request.CompanyId,
                                                request.Title,
                                                request.IsCompulsory,
                                                request.AnonymityCount,
                                                request.PublishMethodType,
                                                request.Status,
                                                request.StartDate,
                                                request.EndDate,
                                                request.IsPrioritized,
                                                request.TargetedDepartmentIds,
                                                request.TargetedUserIds,
                                                request.NumberOfCardsPerTimeFrame,
                                                request.PerTimeFrameType);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectAllDecksResponse SelectAllDeckBasic(DeckRequest request)
        {
            try
            {
                PulseSelectAllDecksResponse response = new PulseSelectAllDecksResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectAllDeckBasic(request.AdminUserId,
                                                        request.CompanyId,
                                                        null,
                                                        null,
                                                        request.Title);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseCreateResponse CreateDynamic(DeckRequest request)
        {
            try
            {
                PulseCreateResponse response = new PulseCreateResponse();
                response.Success = false;

                try
                {
                    response = admin.CreateDynamicCard(request.PulseId,
                                                         request.DeckId,
                                                         request.AdminUserId,
                                                         request.CompanyId,
                                                         request.Title,
                                                         request.Description,
                                                         request.CardType,
                                                         request.QuestionType,
                                                         request.StartDate,
                                                         request.NumberOfOptions,
                                                         request.RangeType,
                                                         null,
                                                         request.MinRangeLabel,
                                                         request.MaxRangeLabel,
                                                         request.Options
                                                         );
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectDeckResponse SelectFullDetailDeck(DeckRequest request)
        {
            try
            {
                PulseSelectDeckResponse response = new PulseSelectDeckResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectFullDetailDeck(request.AdminUserId,
                                                          request.CompanyId,
                                                          request.DeckId,
                                                          request.IsOrderedByAscending);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseUpdateResponse UpdateDeckStatus(DeckRequest request)
        {
            try
            {
                PulseUpdateResponse response = new PulseUpdateResponse();
                response.Success = false;

                try
                {
                    response = admin.UpdateDeckStatus(request.AdminUserId,
                                                      request.CompanyId,
                                                      request.DeckId,
                                                      request.UpdatedStatus);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectDeckAnalyticResponse SelectDeckAnalytic(DeckRequest request)
        {
            try
            {
                PulseSelectDeckAnalyticResponse response = new PulseSelectDeckAnalyticResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectDeckAnalytic(request.AdminUserId,
                                                        request.CompanyId,
                                                        request.DeckId,
                                                        request.SearchId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectDeckCardAnalyticResponse SelectDeckCardAnalytic(DeckRequest request)
        {
            try
            {
                PulseSelectDeckCardAnalyticResponse response = new PulseSelectDeckCardAnalyticResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectDeckCardAnalytic(request.AdminUserId,
                                                            request.CompanyId,
                                                            request.DeckId,
                                                            request.PulseId,
                                                            request.SearchId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectDeckCardOptionAnalyticResponse SelectDeckCardOptionAnalytic(DeckRequest request)
        {
            try
            {
                PulseSelectDeckCardOptionAnalyticResponse response = new PulseSelectDeckCardOptionAnalyticResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectDeckCardOptionAnalytic(request.AdminUserId,
                                                                  request.CompanyId,
                                                                  request.DeckId,
                                                                  request.PulseId,
                                                                  request.SelectedOptionId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectDeckCardCustomAnswerAnalyticResponse SelectDeckCardCustomAnswerAnalytic(DeckRequest request)
        {
            try
            {
                PulseSelectDeckCardCustomAnswerAnalyticResponse response = new PulseSelectDeckCardCustomAnswerAnalyticResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectDeckCardCustomAnswerAnalytic(request.AdminUserId,
                                                                        request.CompanyId,
                                                                        request.DeckId,
                                                                        request.PulseId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseSelectDeckAnalyticByUserResponse SelectDeckAnalyticByUser(DeckRequest request)
        {
            try
            {
                PulseSelectDeckAnalyticByUserResponse response = new PulseSelectDeckAnalyticByUserResponse();
                response.Success = false;

                try
                {
                    response = admin.SelectDeckAnalyticByUser(request.AdminUserId,
                                                              request.CompanyId,
                                                              request.DeckId,
                                                              request.AnsweredByUserId);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public PulseUpdateResponse ImportDeck(DeckRequest request)
        {
            try
            {
                PulseUpdateResponse response = new PulseUpdateResponse();
                response.Success = false;

                try
                {
                    response = admin.ImportDeck(request.AdminUserId,
                                                request.CompanyId,
                                                request.DeckId,
                                                request.DeckIds);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                }

                return response;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        // Requests
        public class PulseRequest
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public string RequesterUserId { get; set; }
            public string PulseId { get; set; }
            public string Title { get; set; }
            public int Status { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public bool IsPrioritized { get; set; }
            public List<string> TargetedDepartmentIds { get; set; }
            public List<string> TargetedUserIds { get; set; }

            public bool IsFilterApplied { get; set; }
            public string TextColor { get; set; }
            public string BackgroundUrl { get; set; }
            public string Description { get; set; }

            public int PulseType { get; set; }
        }

        public class DeckRequest
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public string RequesterUserId { get; set; }
            public string Title { get; set; }
            public int DeckType { get; set; }
            public bool IsCompulsory { get; set; }
            public int AnonymityCount { get; set; }
            public int PublishMethodType { get; set; }
            public int Status { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public bool IsPrioritized { get; set; }
            public List<string> TargetedDepartmentIds { get; set; }
            public List<string> TargetedUserIds { get; set; }
            public int NumberOfCardsPerTimeFrame { get; set; }
            public int PerTimeFrameType { get; set; }

            public string PulseId { get; set; }
            public string DeckId { get; set; }
            public string Description { get; set; }
            public int CardType { get; set; }
            public int QuestionType { get; set; }
            public int NumberOfOptions { get; set; }
            public int RangeType { get; set; }
            public List<DeckCardOption> Options { get; set; }

            public string MinRangeLabel { get; set; }
            public string MaxRangeLabel { get; set; }

            public int UpdatedStatus { get; set; }

            public string AnsweredByUserId { get; set; }
            public int SelectedRange { get; set; }
            public string SelectedOptionId { get; set; }
            public string CustomAnswer { get; set; }

            public string SearchId { get; set; }

            public List<string> DeckIds { get; set; }

            public bool IsOrderedByAscending { get; set; }
        }
    }
}