﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.CognitoIdentity.Model;
using Amazon.Runtime;
using CassandraService.ServiceInterface;
using CocadreMVCWebAPI.Utilities;
using log4net;
using Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using Amazon.ElasticTranscoder.Model;
using Amazon.S3;
using Amazon.S3.Model;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CocadreMVCWebAPI.Models.v5;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PubNubMessaging.Core;
using CassandraService.Entity;
using Feed = CocadreMVCWebAPI.Models.v5.Feed;

namespace CocadreMVCWebAPI.Models.Service
{
    public class Aws
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService clientService = new ClientService();
        private Feed feedModel = new Feed();
        private Account accountModel = new Account();

        public SignedUrlResponse GetSignedUrl(SignedUrlRequest request)
        {
            SignedUrlResponse signedUrlResponse = new SignedUrlResponse();
            signedUrlResponse.Success = false;

            try
            {
                signedUrlResponse.Success = true;
                
                signedUrlResponse.SignedUrl = AWSUrlSigner.GetSignedUrl(request.Url);

                //Debug.WriteLine(signedUrlResponse);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return signedUrlResponse;
        }

        public UserImageProgressResponse LambdaUpdateUserThumbnailingProgress(string userId, string imageName, double progress, string environment, string companyId)
        {
            UserImageProgressResponse userImageProgressResponse = new UserImageProgressResponse();

            userImageProgressResponse = clientService.SelectUserImageProgress(userId, imageName);

            UserImageProgress userImageProgress = userImageProgressResponse.userImageProgress;

            if (userImageProgress != null)
            {
                // update progress
                if (progress > userImageProgress.Progress)
                {
                    userImageProgress.Progress = progress;
                }

                userImageProgress.Environment = environment;

                userImageProgressResponse = clientService.UpdateUserImageProgress(userImageProgress);
            }
            else
            {
                // create it
                bool uploaded = true;
                bool completed = false;

                userImageProgressResponse = clientService.CreateUserImageProgress(userId, imageName, progress, completed,
                    uploaded, environment, companyId);
            }

            accountModel.CalculateUserProgressUpdatePubNub(userId, imageName);

            return userImageProgressResponse;
        }

        // LambdaUpdateThumbnailingProgress
        public FeedImageProgressResponse LambdaUpdateFeedThumbnailingProgress(string feedId, string imageName, double progress, int expectedCount, string environment, string companyId)
        {
            //// do all the checking here
            //// call client service which calls aws entity
            FeedImageProgressResponse feedImageProgressResponse = new FeedImageProgressResponse();

            feedImageProgressResponse = clientService.SelectFeedImageProgress(feedId, imageName);

            // this is where you check if db already has the FeedImageObject
            // if yes: update progress
            // if no: create it 
            FeedImageProgress feedImageProgress = feedImageProgressResponse.feedImageProgress;

            if (feedImageProgress != null)
            {
                // update progress    
                if (progress > feedImageProgress.Progress)
                {
                    feedImageProgress.Progress = progress;
                }

                feedImageProgress.Environment = environment;

                feedImageProgressResponse = clientService.UpdateFeedImageProgress(feedImageProgress);
            }
            else
            {
                // create it (lambda)
                bool completed = false;
                bool uploaded = true;

                feedImageProgressResponse = clientService.CreateFeedImageProgress(
                    feedId, 
                    imageName, 
                    progress, 
                    completed, 
                    uploaded, 
                    expectedCount, 
                    environment, 
                    companyId
                    );
            }

            feedModel.CalculateImageProgressUpdatePubNub(feedId);

            return feedImageProgressResponse;
        }

        // LambdaSetThumbnailingAsCompleted
        public UserImageProgressResponse LambdaSetUserThumbnailingAsCompleted(string userId, string imageName, string env, string companyId)
        {
            UserImageProgressResponse userImageProgressResponse = new UserImageProgressResponse();
            userImageProgressResponse.Success = true;

            userImageProgressResponse = clientService.SelectUserImageProgress(userId, imageName);

            UserImageProgress userImageProgress = userImageProgressResponse.userImageProgress;

            if (userImageProgress != null)
            {
                userImageProgress.Progress = 100.0;
                userImageProgress.Completed = true;
                userImageProgressResponse = clientService.UpdateUserImageProgress(userImageProgress);
            }

            accountModel.CalculateUserProgressUpdatePubNub(userId, imageName);

            return userImageProgressResponse;
        }

        public FeedImageProgressResponse LambdaSetFeedThumbnailingAsCompleted(string feedId, string imageName, string env, string companyId)
        {
            FeedImageProgressResponse feedImageResponse = new FeedImageProgressResponse();
            feedImageResponse.Success = true;
            
            feedImageResponse = clientService.SelectFeedImageProgress(feedId, imageName);

            FeedImageProgress feedImageProgress = feedImageResponse.feedImageProgress;

            if (feedImageProgress != null)
            {
                feedImageProgress.Progress = 100.0;
                feedImageProgress.Completed = true;
                feedImageResponse = clientService.UpdateFeedImageProgress(feedImageProgress);
            }

            feedModel.CalculateImageProgressUpdatePubNub(feedId);

            return feedImageResponse;
        }

        public void LambdaVideoTranscodingConfirmation(AWSSNSNotification AwsSNSNotification)
        {
            string subscribeUrl = AwsSNSNotification.SubscribeURL;
            string html = string.Empty;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(subscribeUrl);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }
        }

        public void LambdaVideoTranscodingNotification(AWSSNSNotification AwsSNSNotification)
        {
            JToken messageJsonBody = JsonConvert.DeserializeObject<JToken>(AwsSNSNotification.Message);

            // progress
            string progressString = messageJsonBody["state"].ToString();
            double progress = 0;
            bool completed = false;

            JToken playlistToken = messageJsonBody["playlists"].ToArray()[0];
            string playListName = playlistToken["name"].ToString();

            // input
            JToken inputToken = messageJsonBody["input"];
            string s3SrcKey = inputToken["key"].ToString();
            string s3OutputKeyPrefix = messageJsonBody["outputKeyPrefix"].ToString();

            string environment = s3SrcKey.Split('/')[0];

            if (s3SrcKey.Contains("assessment"))
            {
                // assessment
                string assessmentIdRegexPattern = @"^(?:.*\/assessment\/)(A\w+)(?:\/)";
                var regex = new Regex(assessmentIdRegexPattern);
                var match = regex.Match(s3SrcKey);
                string assessmentId = match.Groups[1].Value;
                string companyId = s3SrcKey.Split('/')[1];

                if (progressString.Equals("PROGRESSING"))
                {
                    // progressing
                    progress = 50.0;
                }
                else
                {
                    // completed
                    progress = 100.0;
                    completed = true;

                    // shift video to correct bucket here
                    AWSCredentials credentials = new BasicAWSCredentials(AwsConfig.AccessKey, AwsConfig.SecretKey);
                    IAmazonS3 s3Client = new AmazonS3Client(credentials, Amazon.RegionEndpoint.APSoutheast1);

                    // copy cocadre-elastic-transcoder-input to output company assessment bucket
                    string srcBucket = string.Format("cocadre-elastic-transcoder-input/{0}", s3OutputKeyPrefix);
                    string dstBucket = string.Format("cocadre-marketplace/assessment/{0}", assessmentId);

                    Service.Aws.CopyFolder(srcBucket, dstBucket, true);

                    // copy objects from transcoder output to company assessment bucket
                    srcBucket = string.Format("cocadre-elastic-transcoder-output/{0}",
                        s3OutputKeyPrefix);
                    dstBucket = string.Format("cocadre-marketplace/assessment/{0}", assessmentId);

                    Service.Aws.CopyFolder(srcBucket, dstBucket, true);
                }
            }
            else if (s3SrcKey.Contains("mlearnings"))
            {
                // mlearning
                string dstBucket = null;
                string companyId = s3SrcKey.Split('/')[1];

                // mLearningCardId
                string mLearningCardIdRegexPattern = @"^(?:.*\/mlearnings\/)(?:.*\/)(MLC\w+)(?:\/)";
                var regex = new Regex(mLearningCardIdRegexPattern);
                var match = regex.Match(s3SrcKey);
                string mLearningCardId = match.Groups[1].Value;

                if (s3SrcKey.Contains("educations"))
                {
                    // educations
                    // mLearningEducationId
                    string mLearningEducationIdRegexPattern = @"^(?:.*\/mlearnings\/)(?:.*\/)(MLED\w+)(?:\/)";
                    regex = new Regex(mLearningEducationIdRegexPattern);
                    match = regex.Match(s3SrcKey);
                    string mLearningEducationId = match.Groups[1].Value;

                    dstBucket = string.Format("cocadre-{0}/mlearnings/educations/{1}/{2}", companyId.ToLower(), mLearningEducationId, mLearningCardId);
                }
                else
                {
                    // evaluations
                    // mLearningTopicId
                    string mLearningTopicIdRegexPattern = @"^(?:.*\/mlearnings\/)(?:.*\/)(MLT\w+)(?:\/)";
                    regex = new Regex(mLearningTopicIdRegexPattern);
                    match = regex.Match(s3SrcKey);
                    string mLearningTopicId = match.Groups[1].Value;

                    dstBucket = string.Format("cocadre-{0}/mlearnings/evaluations/{1}/{2}", companyId.ToLower(), mLearningTopicId, mLearningCardId);
                }

                if (progressString.Equals("PROGRESSING"))
                {
                    // progressing
                    progress = 50.0;
                }
                else
                {
                    // completed
                    progress = 100.0;
                    completed = true;

                    // shift video to correct bucket here
                    AWSCredentials credentials = new BasicAWSCredentials(AwsConfig.AccessKey, AwsConfig.SecretKey);
                    IAmazonS3 s3Client = new AmazonS3Client(credentials, Amazon.RegionEndpoint.APSoutheast1);

                    // copy cocadre-elastic-transcoder-input to output company assessment bucket
                    string srcBucket = string.Format("cocadre-elastic-transcoder-input/{0}", s3OutputKeyPrefix);
                    
                    Service.Aws.CopyFolder(srcBucket, dstBucket, true);

                    // copy objects from transcoder output to company assessment bucket
                    srcBucket = string.Format("cocadre-elastic-transcoder-output/{0}",
                        s3OutputKeyPrefix);
                    
                    Service.Aws.CopyFolder(srcBucket, dstBucket, true);
                }
            }
            else if (s3SrcKey.Contains("feeds"))
            {
                // feedId
                string feedIdRegexPattern = @"^(?:.*\/feeds\/)(F\w+)(?:\/)";
                var regex = new Regex(feedIdRegexPattern);
                var match = regex.Match(s3SrcKey);
                string feedId = match.Groups[1].Value;

                // client feed
                if (progressString.Equals("PROGRESSING"))
                {
                    // progressing
                    progress = 40.0;
                }
                else
                {
                    // completed
                    progress = 95.0;
                    completed = true;
                }

                // check if there's already existing feedId (means client came in first)
                FeedVideoProgressResponse feedVideoProgressResponse = new FeedVideoProgressResponse();
                feedVideoProgressResponse = clientService.SelectFeedVideoProgress(feedId);

                FeedVideoProgress feedVideoProgress = feedVideoProgressResponse.feedVideoProgress;

                if (feedVideoProgress != null)
                {
                    // existing, update only
                    if (progress > feedVideoProgress.Progress)
                    {
                        feedVideoProgress.Progress = progress;
                    }

                    if (!feedVideoProgress.Completed)
                    {
                        // dont update it if it is already true
                        feedVideoProgress.Completed = completed;
                    }

                    feedVideoProgress.Environment = environment;
                    feedVideoProgress.PlaylistName = playListName;
                    feedVideoProgress.S3OutputKeyPrefix = s3OutputKeyPrefix;
                    feedVideoProgress.S3SrcKey = s3SrcKey;

                    feedVideoProgressResponse = clientService.UpdateFeedVideoProgress(feedVideoProgress);
                }
                else
                {
                    // create new feedVideoProgress
                    string userId = null;
                    bool uploaded = true;
                    int feedType = -1;

                    string companyIdRegexPattern = @"^(?:.*\/)(c\w+)(?:\/)";
                    regex = new Regex(companyIdRegexPattern);
                    match = regex.Match(s3SrcKey);
                    string companyId = match.Groups[1].Value;

                    feedVideoProgressResponse = clientService.CreateFeedVideoProgress(feedId, playListName, progress, completed, uploaded, environment,
                        companyId, feedType, s3SrcKey, s3OutputKeyPrefix);

                    feedVideoProgress = feedVideoProgressResponse.feedVideoProgress;
                }

                feedModel.CalculateVideoProgressUpdatePubNub(feedId);
            }
        }

        public GetCognitoTokenResponse GetCognitoToken(AwsRequest request)
        {
            GetCognitoTokenResponse response = new GetCognitoTokenResponse();
            response.Success = false;

            try
            {
                if (String.IsNullOrEmpty(request.CompanyId))
                {
                    Log.Warn("CompanyId parameter is null or empty.");
                    response.ErrorCode = 999;
                    response.ErrorMessage = "CompanyId parameter is null or empty.";
                    return response;
                }

                if (String.IsNullOrEmpty(request.UserId))
                {
                    Log.Warn("UserId parameter is null or empty.");
                    response.ErrorCode = 999;
                    response.ErrorMessage = "UserId parameter is null or empty.";
                    return response;
                }

                AWSCredentials credentials = new BasicAWSCredentials(AwsConfig.AccessKey, AwsConfig.SecretKey);
                AmazonCognitoIdentityClient client = new AmazonCognitoIdentityClient(credentials, RegionEndpoint.APNortheast1);

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add(AwsConfig.CognitoLinkedLogin, request.CompanyId + ":" + request.UserId);

                GetOpenIdTokenForDeveloperIdentityRequest identityRequest = new GetOpenIdTokenForDeveloperIdentityRequest();
                identityRequest.IdentityPoolId = AwsConfig.CognitoIdentityPoolId;
                identityRequest.Logins = dic;
                identityRequest.TokenDuration = Convert.ToInt64(AwsConfig.CognitoTokenDuration);
                GetOpenIdTokenForDeveloperIdentityResponse identityResponse = client.GetOpenIdTokenForDeveloperIdentity(identityRequest);

                response = new GetCognitoTokenResponse
                {
                    Success = true,
                    Token = identityResponse.Token,
                    RoleArn = WebConfigurationManager.AppSettings["AwsCognitoRoleArn"].ToString(),
                    IdentityId = identityResponse.IdentityId
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public static bool DeleteFolder(string source)
        {
            AWSCredentials credentials = new BasicAWSCredentials(AwsConfig.AccessKey, AwsConfig.SecretKey);
            IAmazonS3 s3Client = new AmazonS3Client(credentials, Amazon.RegionEndpoint.APSoutheast1);

            var strippedSource = source;

            // process source
            if (strippedSource.StartsWith("/"))
                strippedSource = strippedSource.Substring(1);
            if (strippedSource.EndsWith("/"))
                strippedSource = source.Substring(0, strippedSource.Length - 1);

            var sourceParts = strippedSource.Split('/');
            var sourceBucket = sourceParts[0];

            var sourcePrefix = new StringBuilder();
            for (var i = 1; i < sourceParts.Length; i++)
            {
                sourcePrefix.Append(sourceParts[i]);
                sourcePrefix.Append("/");
            }

            ListObjectsRequest listObjectsRequest = new ListObjectsRequest();
            listObjectsRequest.BucketName = sourceBucket;
            listObjectsRequest.Prefix = sourcePrefix.ToString();
            listObjectsRequest.Delimiter = "/";

            var listObjectsResult = s3Client.ListObjects(listObjectsRequest);

            // copy each file
            foreach (var file in listObjectsResult.S3Objects)
            {
                // initiate delete
                var deleteRequest = new DeleteObjectRequest();
                deleteRequest.BucketName = sourceBucket;
                deleteRequest.Key = file.Key;

                var deleteResponse = s3Client.DeleteObject(deleteRequest);
            }

            // delete subfolders
            foreach (var folder in listObjectsResult.CommonPrefixes)
            {
                var actualFolder = folder.Substring(sourcePrefix.Length);
                actualFolder = actualFolder.Substring(0, actualFolder.Length - 1);
                DeleteFolder(strippedSource + "/" + actualFolder);
            }

            return true;
        }

        public static bool CopyFolder(string source, string destination, bool move = false)
        {
            AWSCredentials credentials = new BasicAWSCredentials(AwsConfig.AccessKey, AwsConfig.SecretKey);
            IAmazonS3 s3Client = new AmazonS3Client(credentials, Amazon.RegionEndpoint.APSoutheast1);

            var strippedSource = source;
            var strippedDestination = destination;

            // process source
            if (strippedSource.StartsWith("/"))
                strippedSource = strippedSource.Substring(1);
            if (strippedSource.EndsWith("/"))
                strippedSource = source.Substring(0, strippedSource.Length - 1);

            var sourceParts = strippedSource.Split('/');
            var sourceBucket = sourceParts[0];

            var sourcePrefix = new StringBuilder();
            for (var i = 1; i < sourceParts.Length; i++)
            {
                sourcePrefix.Append(sourceParts[i]);
                sourcePrefix.Append("/");
            }

            // process destination
            if (strippedDestination.StartsWith("/"))
                strippedDestination = destination.Substring(1);
            if (strippedDestination.EndsWith("/"))
                strippedDestination = destination.Substring(0, strippedDestination.Length - 1);

            var destinationParts = strippedDestination.Split('/');
            var destinationBucket = destinationParts[0];

            var destinationPrefix = new StringBuilder();
            for (var i = 1; i < destinationParts.Length; i++)
            {
                destinationPrefix.Append(destinationParts[i]);
                destinationPrefix.Append("/");
            }

            ListObjectsRequest listObjectsRequest = new ListObjectsRequest();
            listObjectsRequest.BucketName = sourceBucket;
            listObjectsRequest.Prefix = sourcePrefix.ToString();
            listObjectsRequest.Delimiter = "/";

            var listObjectsResult = s3Client.ListObjects(listObjectsRequest);

            // copy each file
            foreach (var file in listObjectsResult.S3Objects)
            {
                var request = new CopyObjectRequest();
                request.SourceBucket = sourceBucket;
                request.SourceKey = file.Key;
                request.DestinationBucket = destinationBucket;
                request.DestinationKey = destinationPrefix + file.Key.Substring(sourcePrefix.Length);
                request.CannedACL = S3CannedACL.PublicRead;
                var response = (CopyObjectResponse)s3Client.CopyObject(request);

                if(response.HttpStatusCode.Equals(HttpStatusCode.OK) && move)
                {
                    // initiate delete
                    var deleteRequest = new DeleteObjectRequest();
                    deleteRequest.BucketName = sourceBucket;
                    deleteRequest.Key = file.Key;

                    var deleteResponse = s3Client.DeleteObject(deleteRequest);
                }
            }

            // copy subfolders
            foreach (var folder in listObjectsResult.CommonPrefixes)
            {
                var actualFolder = folder.Substring(sourcePrefix.Length);
                actualFolder = actualFolder.Substring(0, actualFolder.Length - 1);
                CopyFolder(strippedSource + "/" + actualFolder, strippedDestination + "/" + actualFolder);
            }

            return true;
        }

        //Request
        public class AwsRequest
        {
            public string RequesterUserId { get; set; }
            public string UserId { get; set; }
            public string CompanyId { get; set; }
        }

        public class SignedUrlRequest
        {
            public String Url { get; set; }
        }

        // Response
        [DataContract]
        public class SignedUrlResponse : ErrorResponse
        {
            [DataMember]
            public String SignedUrl { get; set; }
        }

        [DataContract]
        public class GetCognitoTokenResponse : ErrorResponse
        {
            [DataMember]
            public String Token { get; set; }
            [DataMember]
            public String RoleArn { get; set; }
            [DataMember]
            public String IdentityId { get; set; }
            [DataMember]
            public String IdentityPoolId { get; set; }
        }        
    }
}