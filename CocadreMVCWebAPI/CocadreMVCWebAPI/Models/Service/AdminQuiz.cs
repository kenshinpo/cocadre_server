﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using log4net;
using System;
using System.Collections.Generic;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class AdminQuiz
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService admin = new AdminService();

        public List<ChallengeQuestion> GetRandomQuestions(QuizRequest request)
        {
            List<ChallengeQuestion> questions = new List<ChallengeQuestion>();
            try
            {
                questions = admin.SelectRandomQuestions(request.TopicId, request.CategoryId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return questions;
        }

        // Request
        public class QuizRequest
        {
            public string CompanyId { get; set; }
            public string TopicId { get; set; }
            public string CategoryId { get; set; }
        }
    }
}