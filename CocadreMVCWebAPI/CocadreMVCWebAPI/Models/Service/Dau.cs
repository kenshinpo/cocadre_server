﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class Dau
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();
        private AdminService admin = new AdminService();

        public AnalyticsUpdateDailyActiveUserResponse CreateDAU()
        {
            AnalyticsUpdateDailyActiveUserResponse response = new AnalyticsUpdateDailyActiveUserResponse();
            response.Success = false;

            try
            {
                Log.Debug("Creating DAU for yesterday");

                Thread thread = new Thread(() => client.UpdateDailyActiveUser(DateTime.UtcNow.Date.AddDays(1)));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticsUpdateDailyActiveUserResponse CreateThirtyDaysDAU()
        {
            AnalyticsUpdateDailyActiveUserResponse response = new AnalyticsUpdateDailyActiveUserResponse();
            response.Success = false;

            try
            {
                Log.Debug("Creating DAU for 30 days");

                for (int index = 30; index >= 0; index--)
                {
                    response = client.UpdateDailyActiveUser(DateTime.UtcNow.Date.AddDays(-index));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticsSelectBasicResponse SelectSummaryReport()
        {
            AnalyticsSelectBasicResponse response = new AnalyticsSelectBasicResponse();
            response.Success = false;

            try
            {
                Log.Debug("Select basic report");

                response = client.SelectSummaryReport("Udb0e704ee7244d21aea7930c27709fee", "C8b28091502514318bdcf48bec7c69129");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticsSelectDetailDauResponse SelectDauReport(DauRequest request)
        {
            AnalyticsSelectDetailDauResponse response = new AnalyticsSelectDetailDauResponse();
            response.Success = false;

            try
            {
                Log.Debug("Select dau report");

                int timeActivityFrameBase = request.TimeActivityFrameBase;
                response = client.SelectDau("Udb0e704ee7244d21aea7930c27709fee", "C8b28091502514318bdcf48bec7c69129", request.StartDate, request.EndDate);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        // To APO: only copy from this method onwards
        public AnalyticSelectMatchUpOverallResponse SelectMatchUpOverall(MatchupRequest request)
        {
            AnalyticSelectMatchUpOverallResponse response = new AnalyticSelectMatchUpOverallResponse();
            response.Success = false;

            try
            {
                response = admin.SelectMatchUpOverall(request.AdminUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectTopicAttemptResponse SelectTopicAttempts(MatchupRequest request)
        {
            AnalyticSelectTopicAttemptResponse response = new AnalyticSelectTopicAttemptResponse();
            response.Success = false;

            try
            {
                response = admin.SelectTopicAttempts(request.AdminUserId, request.CompanyId, request.Metric, request.Limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectTopicAttemptResponse SelectDailyTopicActivities(MatchupRequest request)
        {
            AnalyticSelectTopicAttemptResponse response = new AnalyticSelectTopicAttemptResponse();
            response.Success = false;

            try
            {
                response = admin.SelectDailyTopicActivities(request.AdminUserId, request.CompanyId, request.Datestamp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectTopicOverallResponse SelectTopicOverall(MatchupRequest request)
        {
            AnalyticSelectTopicOverallResponse response = new AnalyticSelectTopicOverallResponse();
            response.Success = false;

            try
            {
                response = admin.SelectTopicOverall(request.AdminUserId, request.CompanyId, request.TopicId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectQuestionAttemptResponse SelectQuestionAttempts(MatchupRequest request)
        {
            AnalyticSelectQuestionAttemptResponse response = new AnalyticSelectQuestionAttemptResponse();
            response.Success = false;

            try
            {
                response = admin.SelectQuestionAttempts(request.AdminUserId, request.CompanyId, request.TopicId, request.Metric, request.Limit);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticSelectOptionAttemptResponse SelectOptionAttempts(MatchupRequest request)
        {
            AnalyticSelectOptionAttemptResponse response = new AnalyticSelectOptionAttemptResponse();
            response.Success = false;

            try
            {
                response = admin.SelectOptionAttempts(request.AdminUserId, request.CompanyId, request.TopicId, request.QuestionId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public AnalyticActivityResponse SelectUserActivityPerDay(DauRequest request)
        {
            AnalyticActivityResponse response = new AnalyticActivityResponse();
            response.Success = false;

            try
            {
                response = admin.GetActivity(request.AdminUserId, request.CompanyId, request.StartDate.Date, request.StartDate.Date.AddDays(1), false);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        // Requests
        public class DauRequest
        {
            public int TimeActivityFrameBase { get; set; }

            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }

            public int Metric { get; set; }
            public int Limit { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
        }

        public class MatchupRequest
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public string TopicId { get; set; }
            public string QuestionId { get; set; }

            public int Metric { get; set; }
            public int Limit { get; set; }

            public string Datestamp { get; set; }
        }
    }
}