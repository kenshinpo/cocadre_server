﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class Scheduler
    {
        private delegate TriggerScheduledNotificationResponse TriggerDelegate(string scheduledId, string companyId);

        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService admin = new AdminService();

        public HttpRequestGetScheduleResponse GetSchedules()
        {
            HttpRequestGetScheduleResponse response = new HttpRequestGetScheduleResponse();
            response.Success = false;

            try
            {
                response = admin.PullNotificationSchedules();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public TriggerScheduledNotificationResponse TriggerScheduledNotification(ApiRequest request)
        {
            TriggerScheduledNotificationResponse response = new TriggerScheduledNotificationResponse();
            response.Success = false;

            try
            {
                Log.Debug("Triggered scheduled notification");
                TriggerDelegate dTrigger = new TriggerDelegate(admin.TriggerScheduledNotification);
                Log.Debug("Going to call the method asynchronously");
                dTrigger.BeginInvoke(request.ScheduledId, request.CompanyId, new AsyncCallback(TriggerOnCallBack), dTrigger);
                Log.Debug("Return response");
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        private void TriggerOnCallBack(IAsyncResult iar)
        {
            Log.Debug("Get result");

            // Cast the state object back to the delegate type
            TriggerDelegate triggerDelegate = (TriggerDelegate)iar.AsyncState;

            // Call EndInvoke on the delegate to get the results
            TriggerScheduledNotificationResponse response = triggerDelegate.EndInvoke(iar);

            if (response.Success)
            {
                Log.Debug($"Trigged scheduled notification is success!");

                foreach (CassandraService.Entity.Notification notification in response.PushNotifications)
                {
                    string targetedUserId = notification.TaggedUserId;
                    string notificationText = notification.NotificationText;
                    int numberOfNotification = notification.NumberOfNotificationForTargetedUser;
                    int notificationType = notification.Type;
                    int notificationSubType = notification.SubType;
                    string pulseId = notification.TaggedPulseId;
                    string appraisalId = notification.TaggedAppraisalId;

                    if(notificationType == (int)CassandraService.Entity.Notification.NotificationType.Pulse)
                    {
                        if (notificationSubType == (int)CassandraService.Entity.Notification.NotificationPulseSubType.Announcement)
                        {
                            PushNotification.PushAnnouncementNotification(targetedUserId, notificationText, numberOfNotification, notificationType, notificationSubType, pulseId);
                        }
                        else if(notificationSubType == (int)CassandraService.Entity.Notification.NotificationPulseSubType.Live360)
                        {
                            PushNotification.Push360Notification(targetedUserId, notificationText, numberOfNotification, notificationType, notificationSubType, pulseId, appraisalId);
                        }
                    }

                    //Log.Debug($"Push scheduled notification {notificationText} to: {targetedUserId}");
                }
            }
            else
            {
                Log.Debug($"Trigged scheduled notification has failed");
            }
        }

        // Requests
        public class ApiRequest
        {
            public string ScheduledId { get; set; }
            public string CompanyId { get; set; }
        }
    }
}