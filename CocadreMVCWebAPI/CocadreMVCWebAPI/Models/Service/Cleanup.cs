﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using PubNubMessaging.Core;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class Cleanup
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService client = new AdminService();

        public UserCleanUpResponse CleanUpUser()
        {
            UserCleanUpResponse response = new UserCleanUpResponse();
            response.Success = false;

            try
            {
                Log.Debug("Remove recently deleted users");
                response = client.RemoveRecentlyDeletedUsers();
           } 
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }
    }
}