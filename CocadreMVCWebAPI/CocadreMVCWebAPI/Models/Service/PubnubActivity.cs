﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using PubNubMessaging.Core;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class PubnubActivity
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public void UpdatePubnubActivity(PubnubActivityRequest request)
        {
            string channel = HttpUtility.UrlDecode(request.channel);
            string userId = request.uuid;

            string action = request.action;

            if (channel.Contains("COMPANY@"))
            {
                bool isActive = false;

                if (action.Equals("join"))
                {
                    Log.Debug("Joining channel");
                    isActive = true;
                }
                else
                {
                    Log.Debug("Leaving channel");
                }

                string companyId = channel.Replace("COMPANY@", string.Empty);
                client.UpdateUserActivity(userId, companyId, isActive);
                Log.Debug("Update activty completed");
            }

            else if (channel.Contains("CHALLENGE@"))
            {
                if (action.Equals("timeout"))
                {
                    Pubnub pubnub = PubnubConfig.ConfigPubnub();

                    Log.Debug("Leaving challenge channel");
                    string challengeId = channel.Replace("CHALLENGE@", string.Empty);

                    ChallengeInvalidateResponse serviceResponse = client.InvalidateChallenge(userId, null, challengeId, (int)Topic.InvalidateChallengeReason.NetworkError);

                    if (serviceResponse.Success)
                    {
                        User notifiedUser = userId == serviceResponse.InitiatedUser.UserId ? serviceResponse.ChallengedUser : serviceResponse.InitiatedUser;
                        string killMessage = WebConfigurationManager.AppSettings["kill_message"].ToString();
                        string firstName = userId == serviceResponse.InitiatedUser.UserId ? serviceResponse.InitiatedUser.FirstName : serviceResponse.ChallengedUser.FirstName;

                        string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, notifiedUser.UserId);

                        CocadreMVCWebAPI.Models.v1.ChallengeEvent.CancelChallengeResponse response = new CocadreMVCWebAPI.Models.v1.ChallengeEvent.CancelChallengeResponse
                        {
                            EventName = PubnubEvent.ChallengeKill,
                            ChallengeId = challengeId,
                            Message = string.Format(killMessage, firstName),
                            Success = true
                        };

                        pubnub.Publish<string>(clientChannel, response, PubnubCallback.PublishToKillChallengeMessage, PubnubCallback.PublishToCancelKillErrorMessage);

                        string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, challengeId);
                        string groupInitiatedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, serviceResponse.InitiatedUser.UserId);
                        string groupChallengedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, serviceResponse.ChallengedUser.UserId);

                        //pubnub.Unsubscribe<string>(challengeChannel, groupInitiatedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);
                        //pubnub.Unsubscribe<string>(challengeChannel, groupChallengedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);

                        pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupInitiatedChannel, PubnubCallback.RemoveGameChannelFromGroupMessage, PubnubCallback.RemoveGameChannelFromGroupErrorMessage);
                        pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupChallengedChannel, PubnubCallback.RemoveGameChannelFromGroupMessage, PubnubCallback.RemoveGameChannelFromGroupErrorMessage);
                    }
                }
            }
        }

        // Requests
        public class PubnubActivityRequest
        {
            public string action { get; set; }
            public string uuid { get; set; }
            public string channel { get; set; }
        }
    }
}