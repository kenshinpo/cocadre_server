﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class AdminAppraisal
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService admin = new AdminService();

        public AppraisalCreateTemplateResponse CreateTemplate(AppraisalRequest request)
        {
            AppraisalCreateTemplateResponse response = new AppraisalCreateTemplateResponse();
            response.Success = false;

            try
            {
                response = admin.CreateAppraisalTemplate(request.AdminUserId, request.CompanyId, request.TemplateType, request.TemplateTitle, request.TemplateDescription, false, request.Anonymity, request.IsUserAllowToCreateQuestions, request.GroupTeamType, request.QuestionLimit, null, null, request.PublishMethod, request.StartDate, request.EndDate);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }


        public AppraisalSelectAllTemplatesResponse SelectTemplates(AppraisalRequest request)
        {
            AppraisalSelectAllTemplatesResponse response = new AppraisalSelectAllTemplatesResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAppraisalTemplates(request.AdminUserId, request.CompanyId, request.TemplateType);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalUpdateResponse UpdateStatus(AppraisalRequest request)
        {
            AppraisalUpdateResponse response = new AppraisalUpdateResponse();
            response.Success = false;

            try
            {
                response = admin.UpdateAppraisalStatus(request.AdminUserId, request.CompanyId, request.AppraisalId, request.UpdatedStatus);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalCategoryCreateResponse CreateCategory(AppraisalRequest request)
        {
            AppraisalCategoryCreateResponse response = new AppraisalCategoryCreateResponse();
            response.Success = false;

            try
            {
                response = admin.CreateAppraisalTemplateCategory(request.AdminUserId, request.CompanyId, request.AppraisalId, request.CategoryTitle);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalSelectTemplateResponse SelectTemplate(AppraisalRequest request)
        {
            AppraisalSelectTemplateResponse response = new AppraisalSelectTemplateResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAppraisalFullTemplateByAdmin(request.AdminUserId, request.CompanyId, request.AppraisalId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalCardCreateTemplateResponse CreateCard(AppraisalRequest request)
        {
            AppraisalCardCreateTemplateResponse response = new AppraisalCardCreateTemplateResponse();
            response.Success = false;

            try
            {
                response = admin.CreateAppraisalTemplateCard(request.AdminUserId, request.CompanyId, request.AppraisalId, request.CategoryId, request.CardContent, request.CardType, request.Options);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalSelectResponse SelectResult(AppraisalRequest request)
        {
            AppraisalSelectResponse response = new AppraisalSelectResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAppraisalResult(request.CreatorUserId, request.CompanyId, request.AppraisalId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalSelectOverallReportsResponse SelectReports(AppraisalRequest request)
        {
            AppraisalSelectOverallReportsResponse response = new AppraisalSelectOverallReportsResponse();
            response.Success = false;

            try
            {
                response = admin.SelectAppraisalReportsByAdmin(request.AdminUserId, request.CompanyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Request
        public class AppraisalRequest
        {
            public string AdminUserId { get; set; }
            public string CreatorUserId { get; set; }
            public string CompanyId { get; set; }
            public string AppraisalId { get; set; }
            public string CategoryId { get; set; }
            public string CategoryTitle { get; set; }
            public int TemplateType { get; set; }
            public string TemplateTitle { get; set; }
            public string TemplateDescription { get; set; }
            public int Anonymity { get; set; }
            public bool IsUserAllowToCreateQuestions { get; set; }
            public int PublishMethod { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public int UpdatedStatus { get; set; }
            public string CardContent { get; set; }
            public int CardType { get; set; }
            public List<AppraisalCardOption> Options { get; set; }
            public int GroupTeamType { get; set; }
            public int QuestionLimit { get; set; }
        }
    }
}