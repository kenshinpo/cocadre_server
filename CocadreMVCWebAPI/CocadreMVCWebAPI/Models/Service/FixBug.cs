﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.Service
{
    public class FixBug
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private AdminService admin = new AdminService();

        public FixBugResponse FixSurveyCustomAnswerGrouping(FixBugRequest request)
        {
            FixBugResponse response = new FixBugResponse();
            response.Success = false;

            try
            {
                response = admin.FixSurveyCustomAnswerGrouping(request.CardId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public FixBugResponse FixSurveyCompletion()
        {
            FixBugResponse response = new FixBugResponse();
            response.Success = false;

            try
            {
                response = admin.FixSurveyCompletion();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public FixBugResponse FixImpressionTally()
        {
            FixBugResponse response = new FixBugResponse();
            response.Success = false;

            try
            {
                response = admin.FixImpressionTally();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public FixBugResponse RemoveAppraisalPulses()
        {
            FixBugResponse response = new FixBugResponse();
            response.Success = false;

            try
            {
                response = admin.RemoveAppraisalPulses();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public FixBugResponse UpdateMissingDau(FixBugRequest request)
        {
            FixBugResponse response = new FixBugResponse();
            response.Success = false;

            try
            {
                response = admin.UpdateMissingDau(request.Date.AddDays(1));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }
    }

    public class FixBugRequest
    {
        public string CardId { get; set; }
        public DateTime Date { get; set; }
    }
}