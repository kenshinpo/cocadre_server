﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using PubNubMessaging.Core;
using Resources;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v1
{
    public class ChallengeEvent
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();
        private Pubnub pubnub = PubnubConfig.ConfigPubnub();

        public CreateChallengeResponse CreateChallenge(ChallengeEventRequest request)
        {
            CreateChallengeResponse response = new CreateChallengeResponse();
            response.Success = false;

            try
            {
                string initiatorUserId = request.UserId;
                string challengedUserId = request.OpponentUserId;
                string companyId = request.CompanyId;
                string topicId = request.TopicId;
                string topicCategoryId = request.TopicCategoryId;

                ChallengeCreateResponse serviceResponse = client.CreateChallengeWithTopicId(companyId, initiatorUserId, challengedUserId, topicId, topicCategoryId);

                if (serviceResponse.Success)
                {
                    Log.Debug("Challenge created: " + serviceResponse.ChallengeId);

                    response = new CreateChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengePrepareToJoin,
                        Success = true,
                        ChallengeId = serviceResponse.ChallengeId,
                        Topic = serviceResponse.Topic,
                        Users = serviceResponse.Players
                    };

                    string targetedUserId = serviceResponse.Notification.TaggedUserId;
                    string notificationText = serviceResponse.Notification.NotificationText;
                    int numberOfNotification = serviceResponse.Notification.NumberOfNotificationForTargetedUser;
                    int subType = serviceResponse.Notification.SubType;
                    PushNotification.PushChallengeJoinNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Challenge, subType, serviceResponse.ChallengeId);

                    string opponentClientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, challengedUserId);
                    pubnub.Publish<string>(opponentClientChannel, response, PubnubCallback.PublishToJoinChallengeMessage, PubnubCallback.PublishToJoinChallengeErrorMessage);
                }
                else
                {
                    Log.Error("Challenge cannot be created");
                    Log.Error(serviceResponse.ErrorMessage);

                    response = new CreateChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengePrepareToJoin,
                        Success = false,
                        ErrorCode = serviceResponse.ErrorCode,
                        ErrorMessage = serviceResponse.ErrorMessage
                    };

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());

                response = new CreateChallengeResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    Success = false,
                    ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError),
                    ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError
                };
            }

            return response;
        }

        public CreateChallengeResponse GetChallengeForOffline(ChallengeEventRequest request)
        {
            CreateChallengeResponse response = new CreateChallengeResponse();
            response.Success = false;

            try
            {
                string requesterId = request.UserId;
                string companyId = request.CompanyId;
                string challengeId = request.ChallengeId;

                ChallengeSelectResponse serviceResponse = client.SelectChallengeWithChallengeId(companyId, challengeId, requesterId);

                if (serviceResponse.Success)
                {
                    Log.Debug("Offline challenge found: " + serviceResponse.ChallengeId);

                    response = new CreateChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengePrepareToJoin,
                        Success = true,
                        ChallengeId = serviceResponse.ChallengeId,
                        Topic = serviceResponse.Topic,
                        Users = serviceResponse.Players
                    };

                    string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, response.ChallengeId);
                    string requesterGroupChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, requesterId);

                    pubnub.AddChannelsToChannelGroup<string>(new string[] { challengeChannel }, requesterGroupChannel, PubnubCallback.AddChannelToGroupMessage, PubnubCallback.AddChannelToGroupErrorMessage);

                }
                else
                {
                    response = new CreateChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengePrepareToJoin,
                        Success = false,
                        ErrorCode = serviceResponse.ErrorCode,
                        ErrorMessage = serviceResponse.ErrorMessage
                    };
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());

                response = new CreateChallengeResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    Success = false,
                    ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError),
                    ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError
                };
            }

            return response;
        }

        public QuestionSelectAllResponse FetchQuestions(ChallengeEventRequest request)
        {
            QuestionSelectAllResponse response = new QuestionSelectAllResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string challengeId = request.ChallengeId;

                response = client.SelectAllQuestionsForChallenge(userId, companyId, challengeId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public ChallengeIsReadyResponse PlayerIsReady(ChallengeEventRequest request)
        {
            ChallengeIsReadyResponse serviceResponse = new ChallengeIsReadyResponse();
            serviceResponse.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string challengeId = request.ChallengeId;

                serviceResponse = client.SetPlayerReadyForChallenge(userId, companyId, challengeId);

                if (serviceResponse.Success)
                {
                    Log.Debug(string.Format("Player {0} is ready for challenge id: {1}", userId, challengeId));
                    Log.Debug("is_live: " + serviceResponse.IsLive);

                    string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, challengeId);
                    string requesterGroupChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, userId);

                    // Adding challenge channel to both requester channels
                    pubnub.AddChannelsToChannelGroup<string>(new string[] { challengeChannel }, requesterGroupChannel, PubnubCallback.AddChannelToGroupMessage, PubnubCallback.AddChannelToGroupErrorMessage);

                    // Non-live game
                    if (!serviceResponse.IsLive)
                    {
                        Log.Debug(string.Format("Challenge id: {0} is not live", challengeId));

                        string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                        SinglePlayerToStartResponse response = new SinglePlayerToStartResponse
                        {
                            EventName = PubnubEvent.ChallengeStartOffline,
                            ChallengeId = challengeId,
                            Success = true
                        };

                        pubnub.Publish<string>(clientChannel, response, PubnubCallback.PublishToJoinChallengeMessage, PubnubCallback.PublishToJoinChallengeErrorMessage);
                    }
                    // Live game
                    else
                    {
                        if (serviceResponse.HaveBothPlayersAccepted)
                        {
                            Log.Debug(string.Format("Challenge id: {0} is live", challengeId));

                            StartChallengeResponse response = new StartChallengeResponse
                            {
                                EventName = PubnubEvent.ChallengeStart,
                                ChallengeId = challengeId,
                                Success = true,
                                HaveBothPlayersAccepted = true
                            };

                            pubnub.Publish<string>(challengeChannel, response, PubnubCallback.PublishToJoinChallengeMessage, PubnubCallback.PublishToJoinChallengeErrorMessage);
                        }

                    }
                }
                else
                {
                    Log.Debug("Player is not ready for game");
                    Log.Error(serviceResponse.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                serviceResponse.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                serviceResponse.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return serviceResponse;
        }

        public CorrectAnswerResponse AnswerQuestion(ChallengeEventRequest request)
        {
            CorrectAnswerResponse response = new CorrectAnswerResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string challengeId = request.ChallengeId;
                string questionId = request.QuestionId;
                string topicId = request.TopicId;
                string answer = request.Answer;
                float timeTaken = request.TimeTaken;
                int round = request.Round;

                QuestionAnswerResponse serviceResponse = client.AnswerChallengeQuestion(challengeId, companyId, userId, answer, timeTaken, questionId, topicId, round);

                if (serviceResponse.Success)
                {
                    Log.Debug("Question answered successfully");

                    // Current user 
                    response = new CorrectAnswerResponse
                    {
                        ChallengeId = challengeId,
                        CorrectAnswer = serviceResponse.CorrectAnswer,
                        OpponentAnswer = serviceResponse.AnswerOfOpponent,
                        Success = true
                    };

                    if (serviceResponse.IsLive)
                    {
                        Log.Debug("Question answered LIVE");

                        // Let opponent know current user answer
                        // Using current user play data
                        OpponentQuestionAnsweredResponse opponentResponse = new OpponentQuestionAnsweredResponse
                        {
                            ChallengeId = challengeId,
                            EventName = PubnubEvent.ChallengeOpponentQuestionAnswered,
                            Success = true,
                            Round = round,
                            OpponentId = userId,
                            OpponentAnswer = answer,
                            TimeTakenByOpponent = timeTaken,
                            IsCorrect = serviceResponse.CorrectAnswer.Equals(request.Answer) ? true : false
                        };

                        string opponentChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, serviceResponse.OpponentId);
                        pubnub.Publish<string>(opponentChannel, opponentResponse, PubnubCallback.PublishOpponentAnswerMessage, PubnubCallback.PublishOpponentAnswerErrorMessage);
                    }
                    else
                    {
                        Log.Debug("Question answered OFFLINE");

                        // Current user is challengedUser
                        if (!serviceResponse.IsInitiator)
                        {
                            // Back to challenged user
                            // Using previously stored opponent play data
                            Log.Debug("Challenged user answer");

                            if (serviceResponse.TimeTakenByOpponent > 0 && !request.HasOpponentAnswered)
                            {
                                Log.Debug("Offline round start does not have answer for challenged user");
                                Log.Debug("Initiated user answer: " + serviceResponse.AnswerOfOpponent);
                                Log.Debug("Correct answer: " + serviceResponse.CorrectAnswer);
                                Log.Debug("Is corrrect: " + serviceResponse.CorrectAnswer.Equals(serviceResponse.AnswerOfOpponent));

                                OpponentQuestionAnsweredResponse opponentResponse = new OpponentQuestionAnsweredResponse
                                {
                                    ChallengeId = challengeId,
                                    EventName = PubnubEvent.ChallengeOpponentQuestionAnswered,
                                    Success = true,
                                    Round = round,
                                    OpponentId = serviceResponse.OpponentId,
                                    OpponentAnswer = serviceResponse.AnswerOfOpponent,
                                    TimeTakenByOpponent = serviceResponse.TimeTakenByOpponent,
                                    IsCorrect = serviceResponse.CorrectAnswer.Equals(serviceResponse.AnswerOfOpponent) ? true : false
                                };

                                string opponentChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);
                                pubnub.Publish<string>(opponentChannel, opponentResponse, PubnubCallback.PublishOpponentAnswerMessage, PubnubCallback.PublishOpponentAnswerErrorMessage);

                                string jsonString = JsonHelper.JsonSerializer<OpponentQuestionAnsweredResponse>(opponentResponse);
                                Log.Debug(string.Format("Returned to {0}, {1}: ", userId, jsonString));
                            }

                            if (serviceResponse.IsLastRound)
                            {
                                string targetedUserId = serviceResponse.Notification.TaggedUserId;
                                string notificationText = serviceResponse.Notification.NotificationText;
                                int numberOfNotification = serviceResponse.Notification.NumberOfNotificationForTargetedUser;
                                int subType = serviceResponse.Notification.SubType;
                                PushNotification.PushChallengeCompletedNotification(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Challenge, subType, challengeId);
                            }
                        }
                        else
                        {
                            Log.Debug("Initiated user answer");
                            Log.Debug("Challenger time: " + serviceResponse.TimeTakenByOpponent);

                            // If initiator took longer time than opponent for a particular question in offline mode
                            //&& timeTaken >= serviceResponse.TimeTakenByOpponent
                            //if (serviceResponse.TimeTakenByOpponent != 0)
                            //{
                            Log.Debug("Challenged user caught up to initiated user");
                            Log.Debug("Initiated user took longer time to answer than challenged user");

                            // Back to challenged user
                            OpponentQuestionAnsweredResponse opponentResponse = new OpponentQuestionAnsweredResponse
                            {
                                ChallengeId = challengeId,
                                EventName = PubnubEvent.ChallengeOpponentQuestionAnswered,
                                Success = true,
                                Round = round,
                                OpponentId = userId,
                                OpponentAnswer = answer,
                                TimeTakenByOpponent = timeTaken,
                                IsCorrect = serviceResponse.CorrectAnswer.Equals(request.Answer) ? true : false
                            };

                            string opponentChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, serviceResponse.OpponentId);
                            pubnub.Publish<string>(opponentChannel, opponentResponse, PubnubCallback.PublishOpponentAnswerMessage, PubnubCallback.PublishOpponentAnswerErrorMessage);

                            string jsonString = JsonHelper.JsonSerializer<OpponentQuestionAnsweredResponse>(opponentResponse);
                            Log.Debug(string.Format("Returned to {0}, {1}: ", serviceResponse.OpponentId, jsonString));
                            //}
                        }
                    }

                    if (serviceResponse.IsLastRound)
                    {
                        string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, challengeId);
                        string groupChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, userId);

                        //Log.Debug("Unsubscribe challenge from channel: " + challengeChannel);
                        //pubnub.Unsubscribe<string>(challengeChannel, groupChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);

                        //Log.Debug("Remove challenge from channel: " + challengeChannel);
                        //pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                    }

                }
                else
                {
                    Log.Error("Error answering question");
                    Log.Error(serviceResponse.ErrorMessage);

                    string opponentChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    // Current user 
                    response = new CorrectAnswerResponse
                    {
                        Success = false,
                        ChallengeId = challengeId,
                        ErrorCode = serviceResponse.ErrorCode,
                        ErrorMessage = serviceResponse.ErrorMessage
                    };

                    OpponentQuestionAnsweredResponse opponentResponse = new OpponentQuestionAnsweredResponse
                    {
                        ChallengeId = challengeId,
                        EventName = PubnubEvent.ChallengeError,
                        Success = false,
                        ErrorCode = serviceResponse.ErrorCode,
                        ErrorMessage = serviceResponse.ErrorMessage
                    };

                    pubnub.Publish<string>(opponentChannel, opponentResponse, PubnubCallback.PublishOpponentAnswerMessage, PubnubCallback.PublishOpponentAnswerErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public SinglePlayerToStartResponse ChallengeStartWithoutOpponent(ChallengeEventRequest request)
        {
            SinglePlayerToStartResponse response = new SinglePlayerToStartResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string challengeId = request.ChallengeId;

                ChallengeStartWithoutOpponentResponse serviceResponse = client.StartChallengeWithoutOpponent(challengeId, userId, companyId);

                string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                if (serviceResponse.Success)
                {
                    Log.Debug("Start game without opponent is successful");

                    response = new SinglePlayerToStartResponse
                    {
                        EventName = PubnubEvent.ChallengeStartOffline,
                        ChallengeId = challengeId,
                        Success = true
                    };

                    string returnedJsonString = JsonHelper.JsonSerializer<SinglePlayerToStartResponse>(response);
                    Log.Debug("returnedJsonString: " + returnedJsonString);

                    pubnub.Publish<string>(clientChannel, response, PubnubCallback.PublishToJoinChallengeMessage, PubnubCallback.PublishToJoinChallengeErrorMessage);
                }
                else
                {
                    Log.Error("Start game without opponent has error");
                    Log.Error(serviceResponse.ErrorMessage);

                    response = new SinglePlayerToStartResponse
                    {
                        EventName = PubnubEvent.ChallengeError,
                        ChallengeId = challengeId,
                        Success = false,
                        ErrorCode = serviceResponse.ErrorCode,
                        ErrorMessage = serviceResponse.ErrorMessage
                    };

                    pubnub.Publish<string>(clientChannel, response, PubnubCallback.PublishToJoinChallengeMessage, PubnubCallback.PublishToJoinChallengeErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public ChallengeOfflineSelectOpponentAnswerResponse GetOpponentAnswer(ChallengeEventRequest request)
        {
            ChallengeOfflineSelectOpponentAnswerResponse response = new ChallengeOfflineSelectOpponentAnswerResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string challengeId = request.ChallengeId;
                int round = request.Round;

                response = client.SelectOpponentAnswerForOfflineGame(challengeId, userId, companyId, round);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // For invalid/ leave challenge
        public ChallengeInvalidateResponse KillChallenge(ChallengeEventRequest request)
        {
            ChallengeInvalidateResponse response = new ChallengeInvalidateResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string challengeId = request.ChallengeId;

                response = client.InvalidateChallenge(userId, companyId, challengeId, (int)Topic.InvalidateChallengeReason.InvalidChallenge);

                if (response.Success)
                {
                    User notifiedUser = userId == response.InitiatedUser.UserId ? response.ChallengedUser : response.InitiatedUser;
                    string killMessage = WebConfigurationManager.AppSettings["kill_message"].ToString();
                    string firstName = userId == response.InitiatedUser.UserId ? response.InitiatedUser.FirstName : response.ChallengedUser.FirstName;

                    string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, notifiedUser.UserId);

                    CancelChallengeResponse cancelResponse = new CancelChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengeKill,
                        ChallengeId = challengeId,
                        Message = string.Format(killMessage, firstName),
                        Success = true
                    };

                    pubnub.Publish<string>(clientChannel, cancelResponse, PubnubCallback.PublishToKillChallengeMessage, PubnubCallback.PublishToCancelKillErrorMessage);

                    //string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, challengeId);
                    //string groupInitiatedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, response.InitiatedUser.UserId);
                    //string groupChallengedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, response.ChallengedUser.UserId);

                    //pubnub.Unsubscribe<string>(challengeChannel, groupInitiatedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);
                    //pbnub.Unsubscribe<string>(challengeChannel, groupChallengedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);

                    //pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupInitiatedChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                    //pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupChallengedChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public ChallengeInvalidateResponse CancelChallenge(ChallengeEventRequest request)
        {
            ChallengeInvalidateResponse response = new ChallengeInvalidateResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string challengeId = request.ChallengeId;
                bool fromInitiator = request.FromInitiator;

                response = client.InvalidateChallenge(userId, companyId, challengeId, fromInitiator ? (int)Topic.InvalidateChallengeReason.CancelChallengeByInitiator : (int)Topic.InvalidateChallengeReason.CancelChallengeByChallenge);

                if (response.Success)
                {
                    User notifiedUser = userId == response.InitiatedUser.UserId ? response.ChallengedUser : response.InitiatedUser;
                    string cancelMessage = WebConfigurationManager.AppSettings["cancel_message"].ToString();
                    string firstName = userId == response.InitiatedUser.UserId ? response.InitiatedUser.FirstName : response.ChallengedUser.FirstName;

                    string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, notifiedUser.UserId);

                    CancelChallengeResponse cancelResponse = new CancelChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengeCancel,
                        ChallengeId = challengeId,
                        Message = string.Format(cancelMessage, firstName),
                        Success = true
                    };

                    pubnub.Publish<string>(clientChannel, cancelResponse, PubnubCallback.PublishToCancelChallengeMessage, PubnubCallback.PublishToCancelChallengeErrorMessage);

                    //string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, challengeId);
                    //string groupInitiatedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, response.InitiatedUser.UserId);
                    //string groupChallengedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, response.ChallengedUser.UserId);

                    //pubnub.Unsubscribe<string>(challengeChannel, groupInitiatedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);
                    //pubnub.Unsubscribe<string>(challengeChannel, groupChallengedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);

                    //pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupInitiatedChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                    //pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupChallengedChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class ChallengeEventRequest
        {
            public string EventName { get; set; }
            public string Channel { get; set; }
            public string UserId { get; set; }
            public bool HasOpponentAnswered { get; set; }

            public string CompanyId { get; set; }
            public string TopicId { get; set; }
            public string TopicCategoryId { get; set; }
            public string OpponentUserId { get; set; }
            public string ChallengeId { get; set; }
            public string Answer { get; set; }
            public float TimeTaken { get; set; }
            public string QuestionId { get; set; }
            public int Round { get; set; }
            public bool FromInitiator { get; set; }
        }

        //public class CreateChallengeRequest
        //{
        //    public string ChannelName { get; set; }
        //    public string CompanyId { get; set; }
        //    public string TopicId { get; set; }
        //    public string TopicCategoryId { get; set; }
        //    public string UserId { get; set; }
        //    public string OpponentUserId { get; set; }
        //}

        //public class GetQuestionsRequest
        //{
        //    public string CompanyId { get; set; }
        //    public string ChallengeId { get; set; }
        //    public string UserId { get; set; }
        //}

        //public class PlayerIsReadyRequest
        //{
        //    public string ChannelName { get; set; }
        //    public string CompanyId { get; set; }
        //    public string ChallengeId { get; set; }
        //    public string UserId { get; set; }
        //}

        //public class AnswerQuestionRequest
        //{
        //    public string ChannelName { get; set; }
        //    public string ChallengeId { get; set; }
        //    public string CompanyId { get; set; }
        //    public string QuestionId { get; set; }
        //    public string TopicId { get; set; }
        //    public string UserId { get; set; }
        //    public string Answer { get; set; }
        //    public int Round { get; set; }
        //    public float TimeTaken { get; set; }
        //    public bool HasOpponentAnswered { get; set; }
        //}

        //public class StartGameWithoutOpponentRequest
        //{
        //    public string ChallengeId { get; set; }
        //    public string CompanyId { get; set; }
        //    public string UserId { get; set; }
        //}

        //public class PrepareToJoinRequest
        //{
        //    public string EventName { get; set; }
        //    public string ChallengeId { get; set; }
        //    public string CompanyId { get; set; }
        //    public string UserId { get; set; }
        //}

        //public class GetOpponentAnswerRequest
        //{
        //    public string CompanyId { get; set; }
        //    public string ChallengeId { get; set; }
        //    public string UserId { get; set; }
        //    public int Round { get; set; }
        //}

        //public class InvalidateChallengeRequest
        //{
        //    public string CompanyId { get; set; }
        //    public string ChallengeId { get; set; }
        //    public string UserId { get; set; }
        //    public bool FromInitiator { get; set; }
        //}

        // Responses
        [DataContract]
        public class CreateChallengeResponse : ErrorResponse
        {
            [DataMember(EmitDefaultValue = false)]
            public string EventName { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ChallengeChannel { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ChallengeId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public List<User> Users { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public Topic Topic { get; set; }
        }

        [DataContract]
        public class SinglePlayerToStartResponse : ErrorResponse
        {
            [DataMember]
            public string EventName { get; set; }

            [DataMember]
            public string ChallengeId { get; set; }
        }

        [DataContract]
        public class StartChallengeResponse : ErrorResponse
        {
            [DataMember(EmitDefaultValue = false)]
            public string EventName { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ChallengeId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public bool HaveBothPlayersAccepted { get; set; }
        }

        [DataContract]
        public class CorrectAnswerResponse : ErrorResponse
        {
            [DataMember]
            public string ChallengeId { get; set; }

            [DataMember]
            public string CorrectAnswer { get; set; }

            [DataMember]
            public string OpponentAnswer { get; set; }
        }

        [DataContract]
        public class OpponentQuestionAnsweredResponse : ErrorResponse
        {
            [DataMember(EmitDefaultValue = false)]
            public string ChallengeId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string EventName { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int Round { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string OpponentId { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string OpponentAnswer { get; set; }

            [DataMember]
            public float TimeTakenByOpponent { get; set; }

            [DataMember]
            public bool IsCorrect { get; set; }
        }

        [DataContract]
        public class CancelChallengeResponse : ErrorResponse
        {
            [DataMember]
            public string EventName { get; set; }

            [DataMember]
            public string Message { get; set; }

            [DataMember]
            public string ChallengeId { get; set; }
        }
    }
}