﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.CognitoIdentity.Model;
using Amazon.Runtime;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using PubNubMessaging.Core;
using Resources;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v1
{
    public class Account
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public UserCreateResponse CreateAdmin(CreateAdminRequest request)
        {
            UserCreateResponse response = new UserCreateResponse();
            response.Success = false;

            try
            {
                string companyTitle = request.CompanyTitle;
                string companyLogoUrl = request.CompanyLogoUrl;
                string plainPassword = request.PlainPassword;
                string firstName = request.FirstName;
                string lastName = request.LastName;
                string email = request.Email;
                string profileImageUrl = request.ProfileImageUrl;
                string position = request.Position;

                response = client.CreateAdmin(null, companyTitle, companyLogoUrl, null, plainPassword, firstName, lastName, email, profileImageUrl, position, null, null, null, null, null, null);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectTokenWithCompanyResponse GetAccount(AccountRequest request)
        {
            UserSelectTokenWithCompanyResponse response = new UserSelectTokenWithCompanyResponse();
            response.Success = false;

            try
            {
                Pubnub pubnub = PubnubConfig.ConfigPubnub();
                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = client.SelectUserTokenWithCompany(userId, companyId);

                if (response.Success)
                {
                    string groupChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, userId);
                    string companyChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixCompany, companyId);
                    string clientChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    pubnub.ChannelGroupGrantPresenceAccess<string>(groupChannelName, true, true, PubnubCallback.GrantClientAccessToChannelMessage, PubnubCallback.GrantClientAccessToChannelErrorMessage);
                    pubnub.ChannelGroupGrantAccess<string>(groupChannelName, true, true, PubnubCallback.GrantClientAccessToChannelMessage, PubnubCallback.GrantClientAccessToChannelErrorMessage);

                    foreach (Department department in response.User.Departments)
                    {
                        string departmentChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixDepartment, department.Id);
                        pubnub.AddChannelsToChannelGroup<string>(new string[] { departmentChannelName }, groupChannelName, PubnubCallback.AddChannelToGroupMessage, PubnubCallback.AddChannelToGroupErrorMessage);
                    }

                    pubnub.AddChannelsToChannelGroup<string>(new string[] { companyChannelName, clientChannelName }, groupChannelName, PubnubCallback.AddChannelToGroupMessage, PubnubCallback.AddChannelToGroupErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public GetCognitoTokenResponse GetCognitoToken(AccountRequest request)
        {
            GetCognitoTokenResponse response = new GetCognitoTokenResponse();
            response.Success = false;

            try
            {
                if (String.IsNullOrEmpty(request.CompanyId))
                {
                    Log.Warn("CompanyId parameter is null or empty.");
                    response.ErrorCode = 999;
                    response.ErrorMessage = "CompanyId parameter is null or empty.";
                    return response;
                }

                if (String.IsNullOrEmpty(request.UserId))
                {
                    Log.Warn("UserId parameter is null or empty.");
                    response.ErrorCode = 999;
                    response.ErrorMessage = "UserId parameter is null or empty.";
                    return response;
                }

                AWSCredentials credentials = new BasicAWSCredentials(AwsConfig.AccessKey, AwsConfig.SecretKey);
                AmazonCognitoIdentityClient client = new AmazonCognitoIdentityClient(credentials, RegionEndpoint.APNortheast1);

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add(AwsConfig.CognitoLinkedLogin, request.CompanyId + ":" + request.UserId);

                GetOpenIdTokenForDeveloperIdentityRequest identityRequest = new GetOpenIdTokenForDeveloperIdentityRequest();
                identityRequest.IdentityPoolId = AwsConfig.CognitoIdentityPoolId;
                identityRequest.Logins = dic;
                identityRequest.TokenDuration = Convert.ToInt64(AwsConfig.CognitoTokenDuration);
                GetOpenIdTokenForDeveloperIdentityResponse identityResponse = client.GetOpenIdTokenForDeveloperIdentity(identityRequest);

                response = new GetCognitoTokenResponse
                {
                    Success = true,
                    Token = identityResponse.Token,
                    RoleArn = WebConfigurationManager.AppSettings["AwsCognitoRoleArn"].ToString(),
                    IdentityId = identityResponse.IdentityId
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectAllByDepartmentResponse GetColleagues(AccountRequest request)
        {
            UserSelectAllByDepartmentResponse response = new UserSelectAllByDepartmentResponse();
            response.Success = false;

            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;

                response = client.SelectAllUsersByDepartment(requesterUserId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationSelectUserResponse GoLoginAccount(LoginRequest request)
        {
            AuthenticationSelectUserResponse response = new AuthenticationSelectUserResponse();
            response.Success = false;

            try
            {
                string email = request.Email;
                string encryptedPassword = request.EncryptedPassword;

                response = client.AuthenticateUserForLogin(email, encryptedPassword);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserUpdateLoginResponse GoLogoutAccount(AccountRequest request)
        {
            UserUpdateLoginResponse response = new UserUpdateLoginResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = client.UpdateUserLogin(userId, companyId, false);

                if (response.Success)
                {
                    Pubnub pubnub = PubnubConfig.ConfigPubnub();

                    string groupChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, userId);
                    string companyChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixCompany, companyId);
                    string clientChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { companyChannelName, clientChannelName }, groupChannelName, PubnubCallback.RemoveChannelMessage, PubnubCallback.RemoveChannelErrorMessage);
                    pubnub.RemoveChannelGroup<string>(string.Empty, groupChannelName, PubnubCallback.RemoveGroupMessage, PubnubCallback.RemoveGroupErrorMessage);

                    if (!string.IsNullOrEmpty(response.DeviceToken) && response.DeviceType != 0)
                    {
                        PushTypeService pushType = PushTypeService.APNS;

                        int deviceType = response.DeviceType;
                        string deviceToken = response.DeviceToken;

                        if (deviceType == 2)
                        {
                            pushType = PushTypeService.GCM;
                        }

                        pubnub.UnregisterDeviceForPush<string>(pushType, deviceToken, PubnubCallback.UnregisterDeviceForPushMessage, PubnubCallback.UnregisterDeviceForPushErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserUpdateDeviceTokenResponse UpdateDeviceToken(UpdateDeviceTokenRequest request)
        {
            UserUpdateDeviceTokenResponse response = new UserUpdateDeviceTokenResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string newDeviceToken = request.DeviceToken;
                int newDeviceType = request.DeviceType;

                response = client.UpdateUserDeviceToken(userId, companyId, newDeviceToken, newDeviceType);

                if (response.Success)
                {
                    Pubnub pubnub = PubnubConfig.ConfigPubnub();

                    string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    PushTypeService pushType = PushTypeService.APNS;

                    User user = new User();

                    if (newDeviceType == 2)
                    {
                        pushType = PushTypeService.GCM;
                    }

                    pubnub.RegisterDeviceForPush<string>(clientChannel, pushType, newDeviceToken, PubnubCallback.RegisterDeviceForPushMessage, PubnubCallback.RegisterDeviceForPushErrorMessage);

                    if (!string.IsNullOrEmpty(response.OutdatedDeviceToken) && !response.OutdatedDeviceToken.Equals(request.DeviceToken))
                    {
                        PushTypeService outdatedPushType = PushTypeService.APNS;

                        int outdatedDeviceType = response.OutdatedDeviceType;
                        string outdatedDeviceToken = response.OutdatedDeviceToken;

                        if (outdatedDeviceType == 2)
                        {
                            outdatedPushType = PushTypeService.GCM;
                        }

                        pubnub.UnregisterDeviceForPush<string>(outdatedPushType, outdatedDeviceToken, PubnubCallback.UnregisterDeviceForPushMessage, PubnubCallback.UnregisterDeviceForPushErrorMessage);
                    }

                    response.OutdatedDeviceToken = null;
                    response.OutdatedDeviceType = 0;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationUpdateResponse UpdatePassword(UpdatePasswordRequest request)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string newPassword = request.NewPassword;
                string email = request.Email;

                if (!string.IsNullOrEmpty(userId))
                {
                    response = client.UpdatePasswordForUser(userId, email, companyId, newPassword);
                }
                else
                {
                    response = client.ResetPasswordForEmail(email);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserUploadProfileImageResponse UploadProfileImage(UploadProfileImageRequest request)
        {
            UserUploadProfileImageResponse response = new UserUploadProfileImageResponse();
            response.Success = false;

            try
            {
                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string newProfileImageUrl = request.NewProfileImageUrl;

                response = client.UploadProfileImage(userId, companyId, newProfileImageUrl, (int)Dashboard.ProfileApprovalState.Pending);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class AccountRequest
        {
            public string RequesterUserId { get; set; }
            public string UserId { get; set; }
            public string CompanyId { get; set; }
        }

        public class CreateAdminRequest
        {
            public string CompanyTitle { get; set; }
            public string CompanyLogoUrl { get; set; }
            public string PlainPassword { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string ProfileImageUrl { get; set; }
            public string Position { get; set; }
        }

        public class LoginRequest
        {
            public string Email { get; set; }
            public string EncryptedPassword { get; set; }
        }

        public class UpdateDeviceTokenRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string DeviceToken { get; set; }
            public int DeviceType { get; set; }
        }

        public class UpdatePasswordRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string NewPassword { get; set; }
            public string Email { get; set; }
        }

        public class UploadProfileImageRequest
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string NewProfileImageUrl { get; set; }
        }

        // Responses
        [DataContract]
        public class GetCognitoTokenResponse : ErrorResponse
        {
            [DataMember]
            public String Token { get; set; }
            [DataMember]
            public String RoleArn { get; set; }
            [DataMember]
            public String IdentityId { get; set; }
            [DataMember]
            public String IdentityPoolId { get; set; }
        }
    }
}