﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v2
{
    public class Challenge
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public ChallengeSelectStatsResponse GetChallengeStats(GetChallengeStatsRequest request)
        {
            ChallengeSelectStatsResponse response = new ChallengeSelectStatsResponse();
            response.Success = false;

            try
            {
                string topicId = request.TopicId;
                string requesterUserId = request.RequesterUserId;
                string opponentUserId = request.OpponentUserId;
                string companyId = request.CompanyId;

                response = client.SelectStatsForChallenge(topicId, requesterUserId, opponentUserId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectAllBasicResponse GetOpponentsForTopic(GetOpponentsForTopicRequest request)
        {
            UserSelectAllBasicResponse response = new UserSelectAllBasicResponse();
            response.Success = false;

            try
            {
                string topicId = request.TopicId;
                string topicCategoryId = request.TopicCategoryId;
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string startsWithName = request.StartsWithName;

                response = client.SelectAllUsersByTopicId(topicId, topicCategoryId, requesterUserId, companyId, startsWithName);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public TopicSelectAllBasicResponse GetTopicsForOpponent(GetTopicsForOpponentRequest request)
        {
            TopicSelectAllBasicResponse response = new TopicSelectAllBasicResponse();
            response.Success = false;

            try
            {
                string requesterUserId = request.RequesterUserId;
                string opponentUserId = request.OpponentId;
                string companyId = request.CompanyId;
                string startsWithName = request.StartsWithName;

                response = client.SelectAllTopicBasicByOpponent(requesterUserId, opponentUserId, companyId, startsWithName);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public CategorySelectAllWithTopicResponse GetTopics(ChallengeRequest request)
        {
            CategorySelectAllWithTopicResponse response = new CategorySelectAllWithTopicResponse();
            response.Success = false;

            try
            {
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;

                response = client.SelectAllTopicBasicByUserAndCategory(requesterUserId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class ChallengeRequest
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
        }

        public class GetChallengeStatsRequest
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
            public string OpponentUserId { get; set; }
            public string TopicId { get; set; }
        }

        public class GetOpponentsForTopicRequest
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
            public string TopicId { get; set; }
            public string TopicCategoryId { get; set; }
            public string StartsWithName { get; set; }
        }

        public class GetTopicsForOpponentRequest
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
            public string OpponentId { get; set; }
            public string StartsWithName { get; set; }
        }
    }
}