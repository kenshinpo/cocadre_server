﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Models.v2
{
    public class Notification
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        private ClientService client = new ClientService();

        public NotificationSelectAllResponse GetNotification(GetNotificationRequest request)
        {
            NotificationSelectAllResponse response = new NotificationSelectAllResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                string searchContent = request.SearchContent;
                DateTime? newestTimestamp = request.NewestTimestamp;
                DateTime? oldestTimestamp = request.OldestTimestamp;

                response = client.SelectNotificationByUser(userId, companyId, 0, newestTimestamp, oldestTimestamp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public NotificationSelectNumberResponse GetNotificationNumber(NotificationRequest request)
        {
            NotificationSelectNumberResponse response = new NotificationSelectNumberResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = client.SelectNotificationNumberByUser(userId, companyId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        public NotificationUpdateSeenResponse UpdateSeenNotification(UpdateNotificationSeenRequest request)
        {
            NotificationUpdateSeenResponse response = new NotificationUpdateSeenResponse();
            response.Success = false;

            try
            {
                string userId = request.UserId;
                string companyId = request.CompanyId;
                int notificationType = request.NotificationType;
                int notificationSubType = request.NotificationSubType;
                string challengeId = request.ChallengeId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string replyId = request.ReplyId;
                string notificationId = request.NotificationId;

                response = client.UpdateSeenNotification(userId, companyId, notificationId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                response.ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.SystemError);
                response.ErrorMessage = CassandraService.GlobalResources.ErrorMessage.SystemError;
            }

            return response;
        }

        // Requests
        public class NotificationRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
        }

        public class GetNotificationRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string SearchContent { get; set; }
            public int NumberOfPostsLoaded { get; set; }
            public DateTime? NewestTimestamp { get; set; }
            public DateTime? OldestTimestamp { get; set; }
        }

        public class UpdateNotificationSeenRequest
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public int NotificationType { get; set; }
            public int NotificationSubType { get; set; }
            public string ChallengeId { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string ReplyId { get; set; }
            public string NotificationId { get; set; }
        }
    }
}