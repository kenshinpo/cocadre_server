﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Handlers
{
    public class ApiKeyHandler : DelegatingHandler
    {
        private static readonly ILog Log = LogManager.GetLogger(WebConfigurationManager.AppSettings["log4net_webapi"]);

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string[] whiteListAddress = new string[]
            {
                "account/gologinaccount",
                "account/forgetpassword",
                "account/gologincompany",

                "service/"
            };

            bool isTokenExists = false;
            bool isTokenValid = false;

            //Validate that the api key exists, and if so, validate
            // Header need contains
            // 1. X-ApiKey
            // 2. X-UserKey
            if (!whiteListAddress.Any(address => request.RequestUri.AbsolutePath.ToLower().Contains(address)))
            {
                if (request.Headers.Contains(WebConfigurationManager.AppSettings["apikey_header"]))
                {
                    ClientService client = new ClientService();
                    string apiKey = request.Headers.GetValues(WebConfigurationManager.AppSettings["apikey_header"]).First();
                    string userKey = string.Empty;

                    // Get version
                    ///v4/feed/SelectVoters
                    int version = Convert.ToInt32(request.RequestUri.AbsolutePath.ToString().Split('/')[1].Replace("v", ""));

                    if (version > 3)
                    {
                        if(request.Headers.Contains(WebConfigurationManager.AppSettings["apiuser_header"]))
                        {
                            userKey = request.Headers.GetValues(WebConfigurationManager.AppSettings["apiuser_header"]).First();
                        }
                    }

                    UserTokenValidityResponse serviceResponse = client.CheckUserTokenValidity(apiKey, userKey);

                    if (serviceResponse.Success)
                    {
                        isTokenExists = serviceResponse.isTokenExists;
                        isTokenValid = serviceResponse.isTokenValid;
                    }
                }
            }
            else
            {
                isTokenExists = true;
                isTokenValid = true;
            }

            //If the key is not valid, return an http status code. This message could, of course, be localized using resources.
            //Token exists previously

            if(!isTokenValid)
            {
                ErrorResponse errorResponse = null;
                if (isTokenExists)
                {
                    errorResponse = new ErrorResponse
                    {
                        Success = false,
                        ErrorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.UserInvalid),
                        ErrorMessage = CassandraService.GlobalResources.ErrorMessage.LogoutDueToTokenInvalid
                    };
                }
                else
                {
                    int errorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.UserInvalid);
                    string errorMessage = CassandraService.GlobalResources.ErrorMessage.LogoutDueToMissingToken;
                    if (request.RequestUri.AbsolutePath.ToLower().Contains("account/getaccount"))
                    {
                        errorCode = Convert.ToInt16(CassandraService.GlobalResources.ErrorCode.UserAuthenticationTokenMismatchForCompany);
                        errorMessage = CassandraService.GlobalResources.ErrorMessage.UserAuthenticationTokenMismatchForCompany;
                    }

                    errorResponse = new ErrorResponse
                    {
                        Success = false,
                        ErrorCode = errorCode,
                        ErrorMessage = errorMessage
                    };
                }

                HttpResponseMessage responseError = request.CreateResponse(HttpStatusCode.OK, errorResponse);
                responseError.Content.Headers.Add(WebConfigurationManager.AppSettings["webapi_header"], WebConfigurationManager.AppSettings["webapi_version"]);
                return responseError;
            }

            //Allow the request to process further down the pipeline
            var response = await base.SendAsync(request, cancellationToken);

            //Return the response back up the chain
            return response;
        }
    }
}