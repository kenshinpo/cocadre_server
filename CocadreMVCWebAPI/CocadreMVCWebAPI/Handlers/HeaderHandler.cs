﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace CocadreMVCWebAPI.Handlers
{
    public class HeaderHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {

            //Log request headers and URL
            //var requestHeaders = request.Headers.ToDictionary(h => h.Key, h => h.Value);
            //string headersToLog = String.Join("\r\n", requestHeaders.Select(h => h.Key + ": " + String.Join(",", h.Value)));
            //Logger logger = new Logger();
            //logger.Log("Url: " + request.RequestUri + "\r\n---------\r\n\r\nHeaders: \r\n" + headersToLog + "\r\n------------\r\n\r\nBody: \r\n" + await request.Content.ReadAsStringAsync());

            //Response comes back
            var response = await base.SendAsync(request, cancellationToken);

            //Log response
            if (response.Content != null)
            {
                response.Content.Headers.Add(WebConfigurationManager.AppSettings["webapi_header"], WebConfigurationManager.AppSettings["webapi_version"]);
                //string responseMessage = await response.Content.ReadAsStringAsync();
                //logger.Log("Response: \r\n" + responseMessage);
            }


            //Return response
            return response;
        }
    }
}