﻿using CocadreMVCWebAPI.Handlers;
using CocadreMVCWebAPI.Utilities;
using Newtonsoft.Json;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Dispatcher;

namespace CocadreMVCWebAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.MapHttpAttributeRoutes();

            // Configure cors
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Ignore null
            config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Set up date format
            config.Formatters.JsonFormatter.SerializerSettings.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.MicrosoftDateFormat;

            // Ignore loop reference
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            // Registering handlers
            config.MessageHandlers.Add(new ApiKeyHandler());
            config.MessageHandlers.Add(new HeaderHandler());

            // Register default route

            // ~/v2/Brain/GetBrain.ashx
            config.Routes.MapHttpRoute(
                name: "NamespaceWithControllerAndActionWithFormat",
                routeTemplate: "{namespace}/{controller}/{action}.{format}",
                defaults: new { format = RouteParameter.Optional }
            );

            // ~/reset/RPT12345
            config.Routes.MapHttpRoute(
                name: "PrefixResetWithToken",
                routeTemplate: "{namespace}/{controller}/{action}/{token}",
                defaults: new { token = RouteParameter.Optional }
            );

            // ~/adminEvent/AdminUserId/CompanyId/EventId
            config.Routes.MapHttpRoute(
                name: "PrefixEventResult",
                routeTemplate: "{namespace}/{controller}/{action}/{AdminUserId}/{CompanyId}/{EventId}",
                defaults: new { AdminUserId = RouteParameter.Optional, CompanyId = RouteParameter.Optional, EventId = RouteParameter.Optional }
            );

            // ~/Brain/GetBrain.ashx
            config.Routes.MapHttpRoute(
                name: "ControllerAndActionWithFormat",
                routeTemplate: "{controller}/{action}.{format}",
                defaults: new { format = RouteParameter.Optional }
            );

            // ~/v2/Brain/GetBrain
            config.Routes.MapHttpRoute(
                name: "NamespaceWithControllerAndAction",
                routeTemplate: "{namespace}/{controller}/{action}",
                defaults: new { format = RouteParameter.Optional }
            );

            // ~/service/
            config.Routes.MapHttpRoute(
               name: "Service with Controller",
               routeTemplate: "{namespace}/{controller}",
               defaults: new { action = RouteParameter.Optional }
           );

            config.Services.Replace(typeof(IHttpControllerSelector), new NamespaceHttpControllerSelector(config));
            config.Filters.Add(new HandleApiException());

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "{folder}/{controller}.ashx/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            //// Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            //// To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            //// For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            ////config.EnableQuerySupport();

            //// To disable tracing in your application, please comment out or remove the following line of code
            //// For more information, refer to: http://www.asp.net/web-api
            //config.EnableSystemDiagnosticsTracing();
        }
    }
}
