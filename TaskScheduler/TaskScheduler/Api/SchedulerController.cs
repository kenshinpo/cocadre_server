﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Http;
using TaskScheduler.Utility;

namespace WebSocketServer.Api
{
    public class SchedulerController : ApiController 
    {
        private static ILog Log = LogManager.GetLogger("SocketServerServiceLog");

        public ApiResponse AddToSchedule(ApiRequest request)
        {
            ApiResponse response = new ApiResponse();
            response.Success = false;
            try
            {
                Log.Debug($"Received {JsonConvert.SerializeObject(request)}");
                JobScheduler.CreateJob(request.ScheduledId, request.CompanyId, request.GroupName, request.ToBeNotifiedAt);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        // Requests
        public class ApiRequest
        {
            public string ScheduledId { get; set; }
            public string CompanyId { get; set; }
            public string GroupName { get; set; }
            public DateTime ToBeNotifiedAt { get; set; }
        }

        public class ApiResponse
        {
            public bool Success { get; set; }
        }
    }
}
