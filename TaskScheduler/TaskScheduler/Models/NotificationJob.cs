﻿using log4net;
using Quartz;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Configuration;
using TaskScheduler.Resources;
using TaskScheduler.Utility;

namespace TaskScheduler.Models
{
    public class NotificationJob : IJob
    {       
        private static ILog Log = LogManager.GetLogger("TaskSchedulerLog");

        public void Execute(IJobExecutionContext context)
        {
            JobDataMap dataMap = context.JobDetail.JobDataMap;

            string companyId = dataMap.GetString(Key.CompanyId);
            string scheduledTaskId = dataMap.GetString(Key.ScheduledId);
            string toBeNotifiedAt = dataMap.GetString(Key.ToBeNotifiedAt);

            Log.Debug($"Going to start job: {scheduledTaskId} at {DateTime.UtcNow.ToString(Key.DateTimeFormat)}");

            HttpRequestHandler.TriggerNotification(scheduledTaskId, companyId);
        }
    }
}