﻿using System;

namespace TaskScheduler.Models
{
    public class Schedule
    {       
        public string ScheduledId { get; set; }
        public string CompanyId { get; set; }
        public string GroupName { get; set; }
        public DateTime ScheduledOnTimestamp { get; set; }
    }
}