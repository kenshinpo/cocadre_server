﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TaskScheduler.Models;

namespace TaskScheduler.ServiceResponses
{
    public class HttpRequestGetScheduleResponse : BaseResponse
    {
        [DataMember]
        public List<Schedule> Schedules { get; set; }
    }
}