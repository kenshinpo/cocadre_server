﻿using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using TaskScheduler.Models;
using TaskScheduler.ServiceResponses;

namespace TaskScheduler.Utility
{
    public class HttpRequestHandler
    {
        private static readonly HttpClient client = new HttpClient();
        private static ILog Log = LogManager.GetLogger("TaskSchedulerLog");

        public static async void TriggerNotification(string scheduledId, string companyId)
        {
            string endpoint = WebConfigurationManager.AppSettings["trigger_notification_endpoint"];

            Schedule task = new Schedule
            {
                CompanyId = companyId,
                ScheduledId = scheduledId
            };

            string request = JsonConvert.SerializeObject(task);
            HttpContent content = new StringContent(request, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(endpoint, content);

            var responseString = await response.Content.ReadAsStringAsync();

            Log.Debug(responseString);
        }

        public static async Task<HttpRequestGetScheduleResponse> GetSchedules()
        {
            HttpRequestGetScheduleResponse response = new HttpRequestGetScheduleResponse();
            response.Schedules = new List<Schedule>();
            response.Success = false;
            try
            {
                string endpoint = WebConfigurationManager.AppSettings["pull_schedule_endpoint"];

                HttpContent content = new StringContent("", Encoding.UTF8, "application/json");

                var responseMessage = await client.PostAsync(endpoint, content);

                var responseString = await responseMessage.Content.ReadAsStringAsync();

                response = JsonConvert.DeserializeObject<HttpRequestGetScheduleResponse>(responseString);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return response;
        }
    }
}