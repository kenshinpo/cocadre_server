﻿using log4net;
using Quartz;
using Quartz.Impl;
using System;
using System.Threading.Tasks;
using TaskScheduler.Models;
using TaskScheduler.Resources;
using TaskScheduler.ServiceResponses;

namespace TaskScheduler.Utility
{
    public class JobScheduler
    {
        private static ILog Log = LogManager.GetLogger("TaskSchedulerLog");
        private static IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();

        private delegate Task<HttpRequestGetScheduleResponse> PullScheduleDelegate();

        public static void Start()
        {
            if (!scheduler.IsStarted)
            {
                scheduler.Start();

                PullScheduleDelegate dTrigger = new PullScheduleDelegate(HttpRequestHandler.GetSchedules);
                dTrigger.BeginInvoke(new AsyncCallback(PullScheduleOnCallBack), dTrigger);
            }

            Log.Debug("Task Scheduler starts");
        }

        private static void PullScheduleOnCallBack(IAsyncResult iar)
        {
            try
            {
                Log.Debug("Load schedules");

                // Cast the state object back to the delegate type
                PullScheduleDelegate triggerDelegate = (PullScheduleDelegate)iar.AsyncState;

                // Call EndInvoke on the delegate to get the results
                HttpRequestGetScheduleResponse response = triggerDelegate.EndInvoke(iar).Result;
                if (response.Success)
                {
                    foreach(Schedule schedule in response.Schedules)
                    {
                        CreateJob(schedule.ScheduledId, schedule.CompanyId, schedule.GroupName, schedule.ScheduledOnTimestamp);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public static void CreateJob(string scheduledTaskId, string companyId, string groupName, DateTime notifiedAtTimestamp)
        {
            string jobName = $"{Key.Job}{scheduledTaskId}{companyId}";
            JobKey jobKey = new JobKey(jobName, groupName);

            if (scheduler.CheckExists(jobKey))
            {
                scheduler.DeleteJob(jobKey);
                Log.Debug($"Remove job:{jobName} in group:{groupName}");
            }

            IJobDetail job = JobBuilder.Create<NotificationJob>()
                            .WithIdentity(jobName, groupName)
                            .UsingJobData(Key.ScheduledId, scheduledTaskId)
                            .UsingJobData(Key.CompanyId, companyId)
                            .UsingJobData(Key.ToBeNotifiedAt, notifiedAtTimestamp.ToString(Key.DateTimeFormat))
                            .Build();

            ITrigger trigger = null;
            if (notifiedAtTimestamp < DateTime.UtcNow)
            {
                trigger = TriggerBuilder.Create()
                       .StartNow()
                       .ForJob(job)
                       .Build();
                Log.Debug($"Scheduler starts at now");
            }
            else
            {
                trigger = TriggerBuilder.Create()
                        .StartAt(notifiedAtTimestamp)
                        .ForJob(job)
                        .Build();
                Log.Debug($"Scheduler starts at {notifiedAtTimestamp.ToString(Key.DateTimeFormat)}");
            }

            scheduler.ScheduleJob(job, trigger);
        }

    }
}